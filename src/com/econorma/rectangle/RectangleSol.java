package com.econorma.rectangle;

import java.awt.geom.Rectangle2D;
import java.util.LinkedHashMap;

import com.econorma.data.Chiave;
import com.econorma.testo.Testo;

public class RectangleSol {

	
	private LinkedHashMap<Chiave, Rectangle2D> rectangles = new LinkedHashMap<Chiave, Rectangle2D>();
	
	public RectangleSol(){
		init();
	}
	
	public void init(){
			GUARNIER();
			UNICOMM();
			CADORO();
			CONAD();
			OTTAVIAN();
			GARDERE();
			APULIA();
			ROMA();
			VISOTTO();
			IPERCONAD();
			RDAVENETA();
			SGR_IPER();
			LEGNARO();
			LANDO();
			CATTEL();
			ELIOR();
			VISOTTO2();
			ALI();
			EUROSPIN();
			EUROLAZIO();
			RESINELLI();
			VEGA();
			CUCINA_NOSTRANA();
			GARDENA();
			SIDAL();
			GARBELOTTO();
			SAN_FELICI();
			CENTRALE_LATTE();
			EURORISTORAZIONE();
			GARDINI();
			SALAROMEO();
			PRIX();
			COOP();
			VALSANA();
			ROSSETTO();
			CAFORM();
			DECARNE();
			BERETTA();
			CONSORZIO_NORD_OVEST();
			BOCON();
			SAIT();
			BRUNELLO();
	}
	
	public LinkedHashMap<Chiave, Rectangle2D> getRectangles(){
		return rectangles;
	}
	
	
	public void GUARNIER(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GUARNIER);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(20, 100, 115, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GUARNIER);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double( 460, 100, 100, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GUARNIER);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(100, 160, 500, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GUARNIER);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 250, 2000, 300);
//		rect = new Rectangle2D.Double(0, 290, 2000, 300);
		add(chiave, rect);
		
	}
	
	public void UNICOMM(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.UNICOMM);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(20, 100, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.UNICOMM);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double( 460, 100, 100, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.UNICOMM);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(100, 160, 500, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.UNICOMM);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 250, 2000, 300);
//		rect = new Rectangle2D.Double(0, 290, 2000, 300);
		add(chiave, rect);
		
	}
	
	public void CADORO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(90, 220, 40, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
 		rect = new Rectangle2D.Double(0, 10, 1000, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 10, 1000, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 270, 2000, 500);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
 		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 2000, 500);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
 		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 2000, 500);
		add(chiave, rect);
		 
	}
	
	public void CONAD(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CONAD);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		Rectangle2D rect = new Rectangle2D.Double(260, 10, 220, 30);
		Rectangle2D rect = new Rectangle2D.Double(260, 10, 280, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CONAD);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
 		rect = new Rectangle2D.Double(0, 210, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CONAD);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 200, 1000, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CONAD);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 360, 2000, 100);
		add(chiave, rect);
		 
	}
	
	public void OTTAVIAN(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.OTTAVIAN);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 120, 500, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.OTTAVIAN);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 230, 500, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.OTTAVIAN);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 150, 1000, 300);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.OTTAVIAN);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 260, 2000, 300);
		add(chiave, rect);
	 
		 
	}
	
	public void GARDERE(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDERE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 100, 1000, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDERE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 200, 2000, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDERE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 15, 1000, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDERE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 250, 2000, 200);
		add(chiave, rect);
	 
		 
	}
 
	
	
	public void APULIA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.APULIA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 200, 500, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.APULIA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 300, 500, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.APULIA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 300, 500, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.APULIA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 350, 2000, 300);
		add(chiave, rect);
	 
		 
	}
	
	public void ROMA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROMA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
//		Rectangle2D rect = new Rectangle2D.Double(0, 130, 70, 30);
		Rectangle2D rect = new Rectangle2D.Double(0, 130, 100, 30);
		
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROMA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(0, 100, 800, 10);
		
		//OLD
//		rect = new Rectangle2D.Double(210, 90, 800, 10);
		
		//NEW
		rect = new Rectangle2D.Double(210, 90, 800, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROMA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(0, 100, 800, 10);
		
		//OLD
//		rect = new Rectangle2D.Double(210, 90, 800, 10);
		
		//NEW
		rect = new Rectangle2D.Double(210, 90, 800, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROMA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 380, 2000, 300);
		add(chiave, rect);
	 		 
	}
	
	public void VISOTTO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		Rectangle2D rect = new Rectangle2D.Double(0, 80, 200, 10);
//		Rectangle2D rect = new Rectangle2D.Double(0, 90, 200, 20);
		Rectangle2D rect = new Rectangle2D.Double(400, 100, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(600, 60, 200, 10);
		rect = new Rectangle2D.Double(600, 80, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(430, 30, 200, 20);
		rect = new Rectangle2D.Double(350, 20, 200, 40);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 170, 3000, 2000);
		rect = new Rectangle2D.Double(0, 190, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		 rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 150, 3000, 2000);
		rect = new Rectangle2D.Double(0, 20, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 150, 3000, 2000);
		rect = new Rectangle2D.Double(0, 20, 3000, 2000);
		add(chiave, rect);
	}
	
	public void IPERCONAD(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.IPERCONAD);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 240, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.IPERCONAD);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(250, 310, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.IPERCONAD);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.IPERCONAD);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 360, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.IPERCONAD);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.IPERCONAD);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.IPERCONAD);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.IPERCONAD);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 20, 3000, 2000);
		add(chiave, rect);
	}
	
	public void RDAVENETA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.RDAVENETA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 70, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.RDAVENETA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(250, 310, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.RDAVENETA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.RDAVENETA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 360, 3000, 2000);
		add(chiave, rect);
	}
	
	public void LANDO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 0, 800, 90);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 140, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 110, 200, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 240, 5000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 180, 5000, 1000);
		add(chiave, rect);
	}
	
	public void SGR_IPER(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SGR_IPER);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 210, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SGR_IPER);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(250, 310, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SGR_IPER);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SGR_IPER);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 360, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SGR_IPER);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SGR_IPER);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SGR_IPER);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SGR_IPER);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 3000, 2000);
		add(chiave, rect);
	}
	
	public void LEGNARO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.LEGNARO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 70, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.LEGNARO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(250, 310, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.LEGNARO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.LEGNARO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 360, 3000, 2000);
		add(chiave, rect);
	}
	
	public void CATTEL(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CATTEL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(250, 100, 400, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CATTEL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 190, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CATTEL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 40, 800, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CATTEL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 240, 5000, 1000);
		add(chiave, rect);
	}
	
	public void ELIOR(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ELIOR);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(200, 110, 1000, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ELIOR);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 100, 400, 20);
		rect = new Rectangle2D.Double(0, 300, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ELIOR);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ELIOR);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 290, 3000, 2000);
		add(chiave, rect);
	}
	
	public void VISOTTO2(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VISOTTO2);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(700, 80, 200, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VISOTTO2);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(700, 40, 200, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VISOTTO2);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(476, 20, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VISOTTO2);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 190, 3000, 2000);
		add(chiave, rect);
	}
	
	
	public void ALI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ALI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(200, 20, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ALI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(50, 110, 200, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ALI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(50, 110, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ALI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ALI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ALI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ALI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ALI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 60, 3000, 2000);
		add(chiave, rect);
	}
	
	public void EUROSPIN(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.EUROSPIN_TIRRENICA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		Rectangle2D rect = new Rectangle2D.Double(0, 120, 400, 20);
//		Rectangle2D rect = new Rectangle2D.Double(0, 120, 800, 20);
		Rectangle2D rect = new Rectangle2D.Double(0, 110, 800, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.EUROSPIN_TIRRENICA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 110, 400, 10);
		rect = new Rectangle2D.Double(0, 110, 800, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.EUROSPIN_TIRRENICA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 60, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.EUROSPIN_TIRRENICA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 260, 3000, 2000);
		rect = new Rectangle2D.Double(0, 230, 3000, 2000);
		add(chiave, rect);
	}
	
	public void EUROLAZIO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.EUROSPIN_LAZIO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		Rectangle2D rect = new Rectangle2D.Double(0, 120, 400, 20);
//		Rectangle2D rect = new Rectangle2D.Double(0, 120, 800, 20);
		Rectangle2D rect = new Rectangle2D.Double(0, 110, 800, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.EUROSPIN_LAZIO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 110, 400, 10);
		rect = new Rectangle2D.Double(0, 110, 800, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.EUROSPIN_LAZIO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 60, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.EUROSPIN_LAZIO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 260, 3000, 2000);
		rect = new Rectangle2D.Double(0, 230, 3000, 2000);
		add(chiave, rect);
	}
	
	public void RESINELLI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.RESINELLI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 110, 300, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.RESINELLI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 350, 100, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.RESINELLI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 160, 400, 70);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.RESINELLI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 350, 3000, 2000);
		rect = new Rectangle2D.Double(0, 340, 3000, 2000);
		add(chiave, rect);
	}
	
	public void VEGA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 50, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(700, 10, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(380, 10, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 180, 1000, 100);
		rect = new Rectangle2D.Double(0, 180, 1000, 120);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 180, 1000, 100);
		rect = new Rectangle2D.Double(0, 180, 1000, 120);
		add(chiave, rect);
	}
	
	public void CUCINA_NOSTRANA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CUCINA_NOSTRANA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 10, 300, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CUCINA_NOSTRANA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 120, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CUCINA_NOSTRANA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 130, 300, 20);
		rect = new Rectangle2D.Double(0, 120, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CUCINA_NOSTRANA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 250, 3000, 2000);
		add(chiave, rect);
	}
	
	public void GARDENA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 60, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 270, 310, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 180, 250, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 2000, 1000);
		add(chiave, rect);
	}
	
	public void SIDAL(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SIDAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 190, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SIDAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SIDAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 70, 160, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SIDAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 280, 2000, 1000);
		add(chiave, rect);
		
	}
	
	public void GARBELOTTO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARBELOTTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 160, 2000, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARBELOTTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(400, 330, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARBELOTTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 260, 200, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARBELOTTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 330, 2000, 1000);
		add(chiave, rect);
		
	}
	
	public void SAN_FELICI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SAN_FELICI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(250, 250, 250, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SAN_FELICI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 330, 100, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SAN_FELICI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(462, 330, 200, 30);
		rect = new Rectangle2D.Double(460, 330, 200, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SAN_FELICI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 400, 2000, 1000);
		add(chiave, rect);
		
	}
	
	public void CENTRALE_LATTE(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CENTRALE_LATTE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 120, 250, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CENTRALE_LATTE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 280, 400, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CENTRALE_LATTE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 250, 1000, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CENTRALE_LATTE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 2000, 1000);
		add(chiave, rect);
		
	}
	
	public void EURORISTORAZIONE(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.EURORISTORAZIONE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(300, 160, 350, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.EURORISTORAZIONE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(380, 250, 300, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.EURORISTORAZIONE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(155, 220, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.EURORISTORAZIONE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 370, 2000, 1000);
		rect = new Rectangle2D.Double(0, 360, 2000, 1000);
		add(chiave, rect);
		
	}
	
	public void GARDINI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDINI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 80, 350, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDINI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(110, 110, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDINI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(400, 80, 500, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDINI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 180, 2000, 1000);
		rect = new Rectangle2D.Double(0, 150, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDINI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDINI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDINI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.GARDINI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 20, 2000, 1000);
		add(chiave, rect);
		
	}
	
	public void SALAROMEO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SALAROMEO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 60, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SALAROMEO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 250, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SALAROMEO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 180, 250, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SALAROMEO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 2000, 1000);
		add(chiave, rect);
	}
	
	public void PRIX(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.PRIX);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 70, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.PRIX);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 120, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.PRIX);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 20, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.PRIX);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 220, 2000, 1000);
		add(chiave, rect);
	}
	
	public void COOP(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.COOP_VA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 220, 1000, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.COOP_VA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 210, 1000, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.COOP_VA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 120, 1000, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.COOP_VA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 310, 2000, 500);
		add(chiave, rect);
	}
	
	public void VALSANA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VALSANA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 50, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VALSANA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 170, 310, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VALSANA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(80, 150, 250, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VALSANA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 380, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VALSANA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VALSANA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VALSANA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.VALSANA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 380, 2000, 1000);
		add(chiave, rect);
	}
	
	public void ROSSETTO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(500, 60, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(500, 40, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(420, 10, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 3000, 2000);
		add(chiave, rect);
		
	}
	
	public void CAFORM(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CAFORM);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(300, 280, 200, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CAFORM);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 390, 1000, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CAFORM);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 270, 1000, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CAFORM);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 390, 1000, 2000);
		add(chiave, rect);
	}
	
	public void DECARNE(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.DECARNE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 200, 1000, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.DECARNE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(310, 280, 250, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.DECARNE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 280, 250, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.DECARNE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 360, 1000, 2000);
		add(chiave, rect);
	}
	
	public void BERETTA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.BERETTA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(300, 160, 500, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.BERETTA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 250, 500, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.BERETTA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(20, 250, 200, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.BERETTA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 1000, 2000);
		add(chiave, rect);
	}
	
	public void BOCON(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.BOCON);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 130, 500, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.BOCON);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(230, 330, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.BOCON);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 230, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.BOCON);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 1000, 2000);
		add(chiave, rect);
	}
	
	public void CONSORZIO_NORD_OVEST(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CONSORZIO_NORD_OVEST);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 240, 1000, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CONSORZIO_NORD_OVEST);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(110, 280, 1000, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CONSORZIO_NORD_OVEST);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(200, 200, 1200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.CONSORZIO_NORD_OVEST);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 380, 2000, 500);
		add(chiave, rect);
	}
	
	public void SAIT(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SAIT);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 30, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SAIT);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(400, 150, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SAIT);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 160, 400, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.SAIT);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 250, 1000, 1000);
		add(chiave, rect);
	}
	
	public void BRUNELLO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.BRUNELLO);
		chiave.setTipo(Testo.PASSIVO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 130, 500, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.BRUNELLO);
		chiave.setTipo(Testo.PASSIVO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(230, 180, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.BRUNELLO);
		chiave.setTipo(Testo.PASSIVO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(50, 190, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_SOLIGO);
		chiave.setCliente(Testo.BRUNELLO);
		chiave.setTipo(Testo.PASSIVO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 260, 1000, 2000);
		add(chiave, rect);
	}
	
	public void add(Chiave chiave, Rectangle2D rect){
		rectangles.put(chiave, rect);
	}
	
}
