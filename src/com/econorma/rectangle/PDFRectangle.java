package com.econorma.rectangle;

import java.awt.geom.Rectangle2D;
import java.util.LinkedHashMap;

import com.econorma.data.Chiave;
import com.econorma.testo.Testo;

public class PDFRectangle {


	private LinkedHashMap<Chiave, Rectangle2D> rectangles = new LinkedHashMap<Chiave, Rectangle2D>();
	private String azienda;

	public PDFRectangle(String azienda){
		this.azienda = azienda;
		init();
	}

	public void init(){

		switch (azienda) {
		case Testo.AZIENDA_SOLIGO:
			RectangleSol rectSol = new RectangleSol();
			rectangles = rectSol.getRectangles();
		case Testo.AZIENDA_COPEGO:
			RectangleCop rectCop = new RectangleCop();
			break;
		case Testo.AZIENDA_AMBROSI:
			RectangleAmb rectAmb = new RectangleAmb();
			rectangles = rectAmb.getRectangles();
			break;
		case Testo.AZIENDA_VIPITENO:
			RectangleVip rectVip = new RectangleVip();
			rectangles = rectVip.getRectangles();
			break;
		case Testo.AZIENDA_ANTICA:
			RectangleAntica rectAcs = new RectangleAntica();
			rectangles = rectAcs.getRectangles();
			break;
		case Testo.AZIENDA_AMBROSI_USA:
			RectangleAfu rectAfu = new RectangleAfu();
			rectangles = rectAfu.getRectangles();
			break;
		case Testo.AZIENDA_CESENA:
			RectangleCesena rectCesena = new RectangleCesena();
			rectangles = rectCesena.getRectangles();
			break;
		case Testo.AZIENDA_FRESCOLAT:
			RectangleFresco rectFresco = new RectangleFresco();
			rectangles = rectFresco.getRectangles();
			break;
		case Testo.AZIENDA_TONIOLO:
			RectangleTon rectTon = new RectangleTon();
			rectangles = rectTon.getRectangles();
			break;
		default:
			break;
		}
		 
	}
 

 
	public Rectangle2D rectByType(String type, int page){
		String name = type + "_" + page;
		Rectangle2D rectangle = rectangles.get(name);

		if (rectangle == null) {
			name = type;
			rectangle = rectangles.get(name);
		}
		return rectangle;
	}

	public LinkedHashMap<Chiave, Rectangle2D> getRectangles() {
		return rectangles;
	}
 
 
	
}

