package com.econorma.rectangle;

import java.awt.geom.Rectangle2D;
import java.util.LinkedHashMap;

import com.econorma.data.Chiave;
import com.econorma.testo.Testo;

public class RectangleVip {

	
	private LinkedHashMap<Chiave, Rectangle2D> rectangles = new LinkedHashMap<Chiave, Rectangle2D>();
	
	public RectangleVip(){
		init();
	}
	
	public void init(){
			COOP();
			CONSORZIO_NORD_OVEST();
			SEVEN();
			UNICOOP();
			UNICOOP_TIRRENO();
			ALLEANZA();
			DAO();
			PAC();
			LANDO();
			TRENTO();
			TATO();
			CONAD();
			BENNET();
			ITALBRIX();
			MORGESE();
			ARENA();
			ERGON();
			CEDI();
			VISOTTO();
			TIGROS();
			GARDENA();
			DADO();
			LAGOGEL();
			MEGAMARK();
			CADORO();
			VEGA();
			CEDI_GROSS();
			ALI();
			ROSSETTO();
			OPRAMOLLA();
			ALFI();
			BRENDOLAN();
			COAL();
			DCFOOD();
			CRAI();
			GANASSA();
			GEGEL();
			IPERAL();
			ISA();
			BRIMI();
			MIGROSS();
			VALFOOD();
			TOSANO();
			MAIORA();
			MODERNA();
			MOSCA();
			UNES();
			MARTINELLI();
			SUPEREMME();
			SUPERCENTRO();
			SISA();
			REALCO();
			CARAMICO();
			MULTICEDI();
			PAC2000();
			GRAMM();
			CAMST();
			CDC();
			CEDI_SIGMA();
			ALLEANZA_30();
			CSS();
			GASTRO();
			TOCAL();
			WOERNDLE();
			MARR();
			BAUER();
			HEIDERBECK();
			SABELLI();
			WEIHE();
			RETAILPRO();
	}
	
	public LinkedHashMap<Chiave, Rectangle2D> getRectangles(){
		return rectangles;
	}
	
	
	public void COOP(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_VA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 220, 1000, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_VA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 210, 1000, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_VA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 120, 1000, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_VA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 310, 2000, 500);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_VA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_VA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_VA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_VA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 150, 2000, 500);
		add(chiave, rect);
		 
	}
	
	public void CONSORZIO_NORD_OVEST(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CONSORZIO_NORD_OVEST);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 240, 1000, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CONSORZIO_NORD_OVEST);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(110, 280, 1000, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CONSORZIO_NORD_OVEST);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(200, 200, 1200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CONSORZIO_NORD_OVEST);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 400, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CONSORZIO_NORD_OVEST);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CONSORZIO_NORD_OVEST);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CONSORZIO_NORD_OVEST);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CONSORZIO_NORD_OVEST);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 180, 2000, 500);
		add(chiave, rect);
		 
	}
	
	public void ALLEANZA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 180, 800, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 250, 150, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 180, 150, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 310, 2000, 500);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 310, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 310, 2000, 500);
		add(chiave, rect);
		 
	}
	
//	public void SEVEN(){
//		Chiave chiave = new Chiave();
//		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
//		chiave.setCliente(Testo.SEVEN);
//		chiave.setKey(Testo.SPEDIZIONE);
//		chiave.setPage(0);
//		chiave.setVersion(Testo.V1);
//		Rectangle2D rect = new Rectangle2D.Double(0, 100, 1000, 10);
//		add(chiave, rect);
//		
//		chiave = new Chiave();
//		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
//		chiave.setCliente(Testo.SEVEN);
//		chiave.setKey(Testo.DATA_CONSEGNA);
//		chiave.setPage(0);
//		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(0, 220, 1000, 10);
//		add(chiave, rect);
//		
//		chiave = new Chiave();
//		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
//		chiave.setCliente(Testo.SEVEN);
//		chiave.setKey(Testo.ORDINE);
//		chiave.setPage(0);
//		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(0, 10, 1000, 50);
//		add(chiave, rect);
//		
//		chiave = new Chiave();
//		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
//		chiave.setCliente(Testo.SEVEN);
//		chiave.setKey(Testo.RIGHE);
//		chiave.setPage(0);
//		chiave.setVersion(Testo.V1);
//
//		rect = new Rectangle2D.Double(0, 340, 2000, 500);
//		add(chiave, rect);
//		
//		chiave = new Chiave(); 
//		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
//		chiave.setCliente(Testo.SEVEN);
//		chiave.setKey(Testo.SPEDIZIONE);
//		chiave.setPage(1);
//		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(0, 0, 0, 0);
//		add(chiave, rect);
//		
//		chiave = new Chiave();
//		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
//		chiave.setCliente(Testo.SEVEN);
//		chiave.setKey(Testo.DATA_CONSEGNA);
//		chiave.setPage(1);
//		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(0, 0, 0, 0);
//		add(chiave, rect);
//		
//		chiave = new Chiave();
//		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
//		chiave.setCliente(Testo.SEVEN);
//		chiave.setKey(Testo.ORDINE);
//		chiave.setPage(1);
//		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(0, 0, 0, 0);
//		add(chiave, rect);
//		
//		chiave = new Chiave();
//		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
//		chiave.setCliente(Testo.SEVEN);
//		chiave.setKey(Testo.RIGHE);
//		chiave.setPage(1);
//		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(0, 100, 2000, 500);
//		add(chiave, rect);
//		
//		chiave = new Chiave(); 
//		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
//		chiave.setCliente(Testo.SEVEN);
//		chiave.setKey(Testo.SPEDIZIONE);
//		chiave.setPage(2);
//		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(0, 0, 0, 0);
//		add(chiave, rect);
//		
//		chiave = new Chiave();
//		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
//		chiave.setCliente(Testo.SEVEN);
//		chiave.setKey(Testo.DATA_CONSEGNA);
//		chiave.setPage(2);
//		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(0, 0, 0, 0);
//		add(chiave, rect);
//		
//		chiave = new Chiave();
//		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
//		chiave.setCliente(Testo.SEVEN);
//		chiave.setKey(Testo.ORDINE);
//		chiave.setPage(2);
//		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(0, 0, 0, 0);
//		add(chiave, rect);
//		
//		chiave = new Chiave();
//		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
//		chiave.setCliente(Testo.SEVEN);
//		chiave.setKey(Testo.RIGHE);
//		chiave.setPage(2);
//		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(0, 100, 2000, 500);
//		add(chiave, rect);
//		 
//	}
	
	
	public void SEVEN(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SEVEN);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(300, 30, 400, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SEVEN);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 170, 200, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SEVEN);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 0, 400, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SEVEN);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 260, 2000, 500);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SEVEN);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SEVEN);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SEVEN);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SEVEN);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 240, 2000, 500);
		add(chiave, rect);
		
	}
	
	public void UNICOOP_TIRRENO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 50, 1000, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 150, 1000, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 150, 1000, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 250, 2000, 500);
		rect = new Rectangle2D.Double(0, 210, 2000, 500);
		add(chiave, rect);
		
		chiave = new Chiave(); 
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 160, 2000, 500);
//		rect = new Rectangle2D.Double(0, 200, 2000, 500);
		add(chiave, rect);
		
		chiave = new Chiave(); 
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 160, 2000, 500);
		add(chiave, rect);
		
		chiave = new Chiave(); 
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 160, 2000, 500);
		add(chiave, rect);
		 
	}
	
	public void DAO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 0, 800, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 60, 500, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 800, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 2000, 500);
		add(chiave, rect);
		
		chiave = new Chiave(); 
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 2000, 500);
		add(chiave, rect);
		
		chiave = new Chiave(); 
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave(); 
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 2000, 500);
		add(chiave, rect);
		
		chiave = new Chiave(); 
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DAO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 2000, 500);
		add(chiave, rect);
		 
	}
	
	public void PAC(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(580, 160, 1000, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(580, 150, 500, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 150, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 2000, 500);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 70, 2000, 800);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 70, 2000, 800);
		add(chiave, rect);
		
		 
	}
	
	public void LANDO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 0, 800, 90);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 140, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 110, 200, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 240, 5000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 180, 5000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LANDO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 180, 5000, 1000);
		add(chiave, rect);
		
	}
	
	public void TRENTO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TRENTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 0, 800, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TRENTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 270, 5000, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TRENTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 180, 150, 80);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TRENTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 5000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TRENTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TRENTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TRENTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TRENTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 5000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TRENTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TRENTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TRENTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TRENTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 5000, 1000);
		add(chiave, rect);
		
	}
	
	public void TATO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 50, 100, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 190, 2000, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 190, 2000, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 340, 5000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 5000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 5000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 5000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 5000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TATO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 5000, 1000);
		add(chiave, rect);
				
	}
	
	public void CONAD(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CONAD);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(260, 10, 220, 70);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CONAD);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
 		rect = new Rectangle2D.Double(0, 210, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CONAD);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 200, 1000, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CONAD);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 360, 3000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CONAD);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CONAD);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
 		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CONAD);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CONAD);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 120, 2000, 1000);
		add(chiave, rect);
		 
	}
	
	public void BENNET(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BENNET);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(300, 100, 1000, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BENNET);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(260, 10, 1000, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BENNET);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(260, 10, 1000, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BENNET);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 170, 1000, 320);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BENNET);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BENNET);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BENNET);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BENNET);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 170, 1000, 320);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BENNET);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BENNET);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BENNET);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BENNET);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 170, 1000, 320);
		add(chiave, rect);
		 
	}
	
	public void ITALBRIX(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ITALBRIX);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 500, 20, 200);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ITALBRIX);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 550, 20, 200);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ITALBRIX);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 550, 20, 200);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ITALBRIX);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(310, 500, 300, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ITALBRIX);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ITALBRIX);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ITALBRIX);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ITALBRIX);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 500, 500, 3000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ITALBRIX);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ITALBRIX);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ITALBRIX);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ITALBRIX);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 500, 500, 3000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ITALBRIX);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ITALBRIX);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ITALBRIX);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ITALBRIX);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 500, 500, 3000);
		add(chiave, rect);
		 
	}
	
	public void MORGESE(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 100, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 190, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 150, 200, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 320, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 320, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 320, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 320, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 320, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(5);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 320, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(6);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(6);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(6);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MORGESE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(6);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 320, 2000, 1000);
		add(chiave, rect);
	}
	
	public void ARENA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 60, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 160, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 130, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 290, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 370, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 370, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 370, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 370, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(5);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 370, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(6);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(6);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(6);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ARENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(6);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 370, 2000, 1000);
		add(chiave, rect);
	}
	
	public void ERGON(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ERGON);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(200, 120, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ERGON);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 60, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ERGON);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 190, 210, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ERGON);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 230, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ERGON);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ERGON);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ERGON);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ERGON);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 60, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ERGON);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ERGON);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ERGON);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ERGON);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 60, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ERGON);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ERGON);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ERGON);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ERGON);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 60, 3000, 2000);
		add(chiave, rect);
	}
	
	public void CEDI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 50, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(460, 160, 170, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 160, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 260, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 3000, 2000);
		add(chiave, rect);
	}
	
	public void VISOTTO_OLD(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 80, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(600, 60, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(430, 30, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 170, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		 rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 150, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 150, 3000, 2000);
		add(chiave, rect);
	}
	
	public void VISOTTO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 70, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(600, 60, 600, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(390, 30, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 190, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VISOTTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 3000, 2000);
		add(chiave, rect);
		
	}
	
	public void TIGROS(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 50, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(460, 150, 170, 10);
		rect = new Rectangle2D.Double(460, 150, 170, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(200, 160, 200, 20);
		rect = new Rectangle2D.Double(200, 150, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 270, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 250, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 250, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 250, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TIGROS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 250, 3000, 2000);
		add(chiave, rect);
		
	}
	
	public void GARDENA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 60, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 270, 310, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 180, 250, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(6);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(6);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(6);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GARDENA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(6);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 2000, 1000);
		add(chiave, rect);
	}
	
	public void MEGAMARK(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MEGAMARK);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		Rectangle2D rect = new Rectangle2D.Double(300, 150, 250, 20);
		Rectangle2D rect = new Rectangle2D.Double(300, 150, 350, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MEGAMARK);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 340, 310, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MEGAMARK);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 150, 250, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MEGAMARK);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 210, 2000, 1000);
		add(chiave, rect);
	}
	
	public void CADORO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(90, 220, 40, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
 		rect = new Rectangle2D.Double(0, 10, 1000, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 10, 1000, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 270, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
 		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
 		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V1);
 		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V1);
 		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 2000, 1000);
		add(chiave, rect);
		 
	}
	
	public void VEGA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 50, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(700, 10, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(380, 10, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 180, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 180, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VEGA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 180, 3000, 2000);
		add(chiave, rect);
	}
	
	public void ROSSETTO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(500, 60, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(500, 40, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(420, 10, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 3000, 2000);
		add(chiave, rect);
		
	}
	
	public void OPRAMOLLA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.OPRAMOLLA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(300, 60, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.OPRAMOLLA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 240, 300, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.OPRAMOLLA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 240, 300, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.OPRAMOLLA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.OPRAMOLLA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.OPRAMOLLA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.OPRAMOLLA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.OPRAMOLLA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 2000, 1000);
		add(chiave, rect);
	}
	
	public void LAGOGEL(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 100, 310, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(400, 150, 200, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 150, 250, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.LAGOGEL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 2000, 1000);
		add(chiave, rect);
	}
	
	public void ALFI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ALFI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 70, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ALFI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(500, 80, 600, 10);
		rect = new Rectangle2D.Double(500, 65, 600, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ALFI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 50, 600, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ALFI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 160, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ALFI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ALFI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ALFI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ALFI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 160, 3000, 2000);
		add(chiave, rect);
	}
	
	public void ALI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ALI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(200, 20, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ALI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(50, 110, 200, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ALI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(50, 110, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ALI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ALI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ALI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ALI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ALI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 10, 3000, 2000);
		add(chiave, rect);
	}
	
	public void DADO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DADO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 60, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DADO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(350, 260, 300, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DADO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 240, 310, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DADO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 390, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DADO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DADO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DADO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DADO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DADO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DADO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DADO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DADO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 2000, 1000);
		add(chiave, rect);
	}
	
	public void add(Chiave chiave, Rectangle2D rect){
		rectangles.put(chiave, rect);
	}
	
	public void BRENDOLAN(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(200, 50, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 270, 250, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 180, 250, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 2000, 380);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 2000, 600);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 2000, 600);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 2000, 600);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRENDOLAN);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 2000, 600);
		add(chiave, rect);
	}
	
	public void CEDI_GROSS(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_GROSS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 40, 400, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_GROSS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(80, 100, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_GROSS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(80, 80, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_GROSS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 170, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_GROSS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_GROSS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_GROSS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_GROSS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 170, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_GROSS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_GROSS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_GROSS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_GROSS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 170, 3000, 2000);
		add(chiave, rect);
	}
	
	public void COAL(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(300, 70, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 70, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 70, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 210, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 210, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 210, 2000, 1000);
		add(chiave, rect);
	}
	
	public void DCFOOD(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DCFOOD);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 100, 310, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DCFOOD);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(380, 200, 330, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DCFOOD);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(380, 200, 330, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DCFOOD);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 270, 2000, 400);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DCFOOD);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DCFOOD);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DCFOOD);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DCFOOD);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 270, 2000, 400);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DCFOOD);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DCFOOD);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DCFOOD);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.DCFOOD);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 270, 2000, 400);
		add(chiave, rect);
		
	}
	
	public void CRAI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CRAI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(200, 30, 310, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CRAI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 110, 100, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CRAI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 110, 250, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CRAI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 160, 2000, 280);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CRAI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CRAI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CRAI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CRAI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 2000, 280);
		add(chiave, rect);
		
	}
	public void GANASSA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GANASSA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(200, 10, 310, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GANASSA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 700, 1000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GANASSA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 10, 310, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GANASSA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 2000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GANASSA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GANASSA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GANASSA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GANASSA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 30, 2000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GANASSA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GANASSA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GANASSA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GANASSA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 30, 2000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GANASSA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GANASSA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GANASSA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GANASSA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 30, 2000, 2000);
		add(chiave, rect);
	}
	
	public void GEGEL(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GEGEL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(50, 50, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GEGEL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(350, 200, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GEGEL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(100, 200, 250, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GEGEL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 250, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GEGEL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GEGEL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GEGEL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GEGEL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 250, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GEGEL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GEGEL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GEGEL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GEGEL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 250, 2000, 1000);
		add(chiave, rect);
	}
	
	public void IPERAL(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(350, 60, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 130, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 110, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 170, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 170, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 170, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 170, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.IPERAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 170, 2000, 1000);
		add(chiave, rect);
		
	}
	
	public void ISA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 50, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(480, 150, 100, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 150, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 270, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.ISA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 3000, 2000);
		add(chiave, rect);
	}
	
	public void BRIMI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRIMI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 20, 200, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRIMI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(400, 160, 250, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRIMI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(400, 160, 250, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRIMI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 310, 2000, 600);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRIMI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRIMI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRIMI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRIMI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 2000, 600);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRIMI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRIMI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRIMI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BRIMI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 2000, 600);
		add(chiave, rect);
	}
	
	public void MIGROSS(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 50, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(480, 150, 100, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 150, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 270, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MIGROSS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 3000, 2000);
		add(chiave, rect);
		
	}	
	
	public void VALFOOD(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VALFOOD);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 30, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VALFOOD);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 140, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VALFOOD);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(120, 130, 420, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.VALFOOD);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 280, 2000, 1000);
		add(chiave, rect);
	}
	
	public void TOSANO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TOSANO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(480, 150, 100, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TOSANO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 120, 200, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TOSANO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(610, 10, 500, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TOSANO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 240, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TOSANO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TOSANO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TOSANO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TOSANO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 240, 3000, 2000);
		add(chiave, rect);
	}
	
	public void MAIORA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MAIORA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(300, 80, 300, 40);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MAIORA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 250, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MAIORA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 20, 100, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MAIORA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 150, 600, 400);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MAIORA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MAIORA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MAIORA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MAIORA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 150, 600, 400);
		add(chiave, rect);
	}
	
	public void MODERNA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MODERNA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(200, 110, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MODERNA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(280, 160, 200, 40);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MODERNA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 160, 600, 40);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MODERNA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 220, 2000, 1000);
		rect = new Rectangle2D.Double(0, 270, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MODERNA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MODERNA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MODERNA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MODERNA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 90, 2000, 1000);
		add(chiave, rect);
	}
	
	public void MOSCA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MOSCA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(200, 190, 400, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MOSCA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 150, 250, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MOSCA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 250, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MOSCA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 370, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MOSCA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MOSCA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MOSCA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MOSCA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MOSCA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MOSCA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MOSCA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MOSCA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 2000, 1000);
		add(chiave, rect);
	}
	
	public void UNICOOP(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(300, 160, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 210, 500, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 70, 500, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNICOOP);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 280, 600, 1000);
		add(chiave, rect);
	}
	
	public void UNES(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNES);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 70, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNES);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(400, 50, 400, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNES);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(400, 60, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.UNES);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 150, 3000, 2000);
		add(chiave, rect);
	}
	
	public void MARTINELLI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 100, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(400, 60, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 10, 500, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 200, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(6);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(6);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(6);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(6);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(7);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(7);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(7);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(7);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(8);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(8);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(8);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(8);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(9);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(9);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(9);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARTINELLI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(9);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 50, 3000, 2000);
		add(chiave, rect);
	}
	
	public void SUPEREMME(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SUPEREMME);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 180, 250, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SUPEREMME);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(95, 50, 310, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SUPEREMME);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 30, 250, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SUPEREMME);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 240, 2000, 1000);
		add(chiave, rect);
		
	}
	
	public void SUPERCENTRO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SUPERCENTRO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(95, 70, 310, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SUPERCENTRO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 100, 400, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SUPERCENTRO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 400, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SUPERCENTRO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 320, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SUPERCENTRO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SUPERCENTRO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SUPERCENTRO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SUPERCENTRO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 60, 2000, 1000);
		add(chiave, rect);
	}
	
	public void SISA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SISA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 100, 1000, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SISA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 70, 310, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SISA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 150, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SISA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 190, 2000, 600);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SISA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SISA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SISA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SISA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 190, 2000, 600);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SISA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SISA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SISA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SISA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 190, 2000, 600);
		add(chiave, rect);
	}
	
	public void REALCO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 40, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(250, 200, 310, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(50, 200, 310, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 340, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 340, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 340, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 340, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.REALCO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 340, 2000, 1000);
		add(chiave, rect);
		
	}
	
	public void CARAMICO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CARAMICO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 40, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CARAMICO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 190, 310, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CARAMICO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 140, 500, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CARAMICO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 285, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CARAMICO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CARAMICO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CARAMICO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CARAMICO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 285, 2000, 1000);
		add(chiave, rect);
	}
	
	public void MULTICEDI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MULTICEDI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 60, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MULTICEDI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 180, 310, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MULTICEDI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 250, 500, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MULTICEDI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 305, 2000, 380);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MULTICEDI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MULTICEDI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MULTICEDI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MULTICEDI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 305, 2000, 380);
		rect = new Rectangle2D.Double(0, 285, 2000, 380);
		add(chiave, rect);
	}
	
	public void PAC2000(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC2000);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		Rectangle2D rect = new Rectangle2D.Double(0, 50, 200, 20);
		Rectangle2D rect = new Rectangle2D.Double(0, 80, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC2000);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(420, 170, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC2000);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(180, 170, 280, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC2000);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 270, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC2000);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC2000);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC2000);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC2000);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC2000);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC2000);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC2000);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC2000);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC2000);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC2000);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC2000);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.PAC2000);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 3000, 2000);
		add(chiave, rect);
	}
	
	public void GRAMM(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GRAMM);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 30, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GRAMM);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(220, 180, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GRAMM);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 180, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GRAMM);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 260, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GRAMM);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GRAMM);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GRAMM);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GRAMM);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GRAMM);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GRAMM);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GRAMM);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GRAMM);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 2000, 1000);
		add(chiave, rect);
	}
	
	public void CAMST(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CAMST);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 350, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CAMST);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(250, 160, 310, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CAMST);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(250, 150, 310, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CAMST);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 520, 2000, 1000);
		add(chiave, rect);
	}
	
	public void CDC(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CDC);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 10, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CDC);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(400, 30, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CDC);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(600, 20, 400, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CDC);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 120, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CDC);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CDC);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CDC);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CDC);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 120, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CDC);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CDC);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CDC);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CDC);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 120, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CDC);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CDC);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CDC);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CDC);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 120, 3000, 2000);
		add(chiave, rect);
	}
	
	public void CEDI_SIGMA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_SIGMA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(400, 70, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_SIGMA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 140, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_SIGMA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 300, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_SIGMA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 200, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_SIGMA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_SIGMA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_SIGMA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CEDI_SIGMA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 200, 2000, 1000);
		add(chiave, rect);
	}
	
	public void ALLEANZA_30(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA_30);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(400, 100, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA_30);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 180, 800, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA_30);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 130, 800, 40);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA_30);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 230, 2000, 700);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA_30);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA_30);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA_30);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.COOP_ALLEANZA_30);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 100, 2000, 700);
		add(chiave, rect);
	}
	
	public void CSS(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CSS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 120, 2000, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CSS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 190, 2000, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CSS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 170, 2000, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CSS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CSS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CSS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CSS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CSS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CSS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CSS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CSS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.CSS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 2000, 1000);
		add(chiave, rect);
	}
	
	public void GASTRO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GASTRO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(150, 270, 310, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GASTRO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 300, 310, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GASTRO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(420, 340, 500, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GASTRO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 400, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GASTRO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GASTRO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GASTRO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.GASTRO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 400, 2000, 1000);
		add(chiave, rect);
	}
	
	public void TOCAL(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TOCAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 20, 200, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TOCAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 60, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TOCAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 180, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TOCAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 230, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TOCAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TOCAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TOCAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.TOCAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 60, 2000, 1000);
		add(chiave, rect);
	}
	
	public void WOERNDLE(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.WOERNDLE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		Rectangle2D rect = new Rectangle2D.Double(0, 30, 200, 20);
		Rectangle2D rect = new Rectangle2D.Double(0, 30, 250, 60);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.WOERNDLE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 160, 400, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.WOERNDLE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 400, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.WOERNDLE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 280, 3000, 2000);
		add(chiave, rect);
	}
	
	public void MARR(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARR);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
//		Rectangle2D rect = new Rectangle2D.Double(200, 240, 100, 20);
		Rectangle2D rect = new Rectangle2D.Double(0, 240, 1000, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARR);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(150, 270, 310, 20);
		rect = new Rectangle2D.Double(50, 270, 310, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARR);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(150, 120, 500, 30);
		rect = new Rectangle2D.Double(130, 110, 500, 50);
//		rect = new Rectangle2D.Double(0, 50, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.MARR);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 420, 2000, 1000);
		add(chiave, rect);
	}
	
	public void BAUER(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BAUER);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 160, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BAUER);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 250, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BAUER);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 600, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BAUER);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 450, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BAUER);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BAUER);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BAUER);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BAUER);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 160, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BAUER);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BAUER);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BAUER);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.BAUER);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 190, 3000, 2000);
		add(chiave, rect);
	}
	
	public void HEIDERBECK(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.HEIDERBECK);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 0, 500, 100);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.HEIDERBECK);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 240, 1500, 10);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.HEIDERBECK);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 180, 1500, 60);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.HEIDERBECK);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.HEIDERBECK);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.HEIDERBECK);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.HEIDERBECK);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.HEIDERBECK);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 2000, 600);
		add(chiave, rect);

	}
	
	public void SABELLI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SABELLI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 70, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SABELLI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 190, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SABELLI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 120, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SABELLI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 220, 3000, 600);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SABELLI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SABELLI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SABELLI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.SABELLI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 220, 3000, 1000);
		add(chiave, rect);
	}
	
	public void WEIHE(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.WEIHE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 100, 500, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.WEIHE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 180, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.WEIHE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 220, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.WEIHE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 250, 3000, 600);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.WEIHE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.WEIHE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.WEIHE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.WEIHE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 250, 3000, 600);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.WEIHE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.WEIHE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.WEIHE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.WEIHE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 250, 3000, 600);
		add(chiave, rect);
	}
	
	public void RETAILPRO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.RETAILPRO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 80, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.RETAILPRO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.RETAILPRO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(130, 10, 500, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.RETAILPRO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 230, 3000, 600);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.RETAILPRO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.RETAILPRO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.RETAILPRO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_VIPITENO);
		chiave.setCliente(Testo.RETAILPRO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 20, 3000, 600);
		add(chiave, rect);
	}
}
