package com.econorma.rectangle;

import java.awt.geom.Rectangle2D;
import java.util.LinkedHashMap;

import com.econorma.data.Chiave;
import com.econorma.testo.Testo;

public class RectangleAmb {

	private LinkedHashMap<Chiave, Rectangle2D> rectangles = new LinkedHashMap<Chiave, Rectangle2D>();

	public RectangleAmb(){
		init();
	}

	public void init(){
		CARREFOUR();
		COOP();
		HEIDERBECK();
		QUESOS();
		EMMI();
		GEIA();
		CHASAL();
		CIRCLEVIEW();
		RIMI();
		EURO();
		NADIA();
		DAVINCI();
		CARNIATO();
		BORG();
		METRO();
		AMBROSI();
		ROWCLIFFE();
		TAVELLA();
		NOSAWA();
	}

	public LinkedHashMap<Chiave, Rectangle2D> getRectangles(){
		return rectangles;
	}

	public void CARREFOUR(){

		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARREFOUR);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(350, 50, 300, 50);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARREFOUR);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(500, 150, 300, 20);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARREFOUR);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 10, 1000, 30);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARREFOUR);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 230, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARREFOUR);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARREFOUR);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARREFOUR);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARREFOUR);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 240, 2000, 500);
		add(chiave, rect);

	}

	public void COOP(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.COOP);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 220, 1000, 50);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.COOP);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(20, 230, 1000, 100);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.COOP);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 200, 500, 20);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.COOP);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 375, 2000, 500);
		add(chiave, rect);


	}

	public void HEIDERBECK(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.HEIDERBECK);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 0, 500, 100);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.HEIDERBECK);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 240, 1500, 10);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.HEIDERBECK);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 180, 1500, 60);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.HEIDERBECK);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 300, 2000, 500);
		rect = new Rectangle2D.Double(0, 300, 2000, 380);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.HEIDERBECK);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.HEIDERBECK);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.HEIDERBECK);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.HEIDERBECK);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 100, 2000, 600);
		rect = new Rectangle2D.Double(0, 300, 2000, 380);
		add(chiave, rect);

	}

	public void QUESOS(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.QUESOS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 500, 500, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.QUESOS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 500, 500, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.QUESOS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 270, 250, 50);
//		rect = new Rectangle2D.Double(0, 520, 300, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.QUESOS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 310, 2000, 500);
		rect = new Rectangle2D.Double(0, 290, 2000, 500);
		add(chiave, rect);

	}

	public void EMMI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.EMMI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 0, 500, 70);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.EMMI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 40, 1600, 20);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.EMMI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 120, 300, 30);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.EMMI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 200, 2000, 500);
		add(chiave, rect);

	}

	public void GEIA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.GEIA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 270, 250, 50);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.GEIA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 210, 1000, 20);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.GEIA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 170, 510, 200);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.GEIA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		//		rect = new Rectangle2D.Double(0, 430, 2000, 500);
		rect = new Rectangle2D.Double(0, 350, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.GEIA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.GEIA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.GEIA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.GEIA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 2000, 500);
		//		rect = new Rectangle2D.Double(0, 430, 500, 500);
		add(chiave, rect);

	}

	public void CIRCLEVIEW(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CIRCLEVIEW);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 130, 150, 10);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CIRCLEVIEW);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(630, 170, 300, 10);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CIRCLEVIEW);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 170, 320, 10);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CIRCLEVIEW);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CIRCLEVIEW);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CIRCLEVIEW);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CIRCLEVIEW);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CIRCLEVIEW);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CIRCLEVIEW);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CIRCLEVIEW);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CIRCLEVIEW);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CIRCLEVIEW);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 2000, 500);
		add(chiave, rect);


	}

	public void CHASAL(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 130, 150, 10);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(630, 170, 300, 10);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 170, 320, 10);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(6);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(6);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(6);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(6);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(7);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(7);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(7);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(7);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(8);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(8);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(8);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(8);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(9);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(9);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(9);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CHASAL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(9);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 350, 2000, 500);
		add(chiave, rect);

	}

	public void RIMI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.RIMI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 160, 150, 50);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.RIMI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 50, 320, 100);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.RIMI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(140, 170, 320, 50);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.RIMI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 2000, 500);
		add(chiave, rect);

	}

	public void EURO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.EURO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 300, 2000, 50);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.EURO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 2000, 50);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.EURO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 2000, 100);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.EURO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 180, 2000, 200);
		add(chiave, rect);

	}

	public void NADIA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.NADIA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 100, 1000, 50);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.NADIA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(20, 290, 300, 20);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.NADIA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(390, 290, 1000, 20);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.NADIA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 355, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.NADIA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.NADIA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.NADIA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.NADIA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 355, 2000, 500);
		add(chiave, rect);

	}

	public void DAVINCI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.DA_VINCI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(250, 190, 2000, 100);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.DA_VINCI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 170, 2000, 10);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.DA_VINCI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 100, 2000, 100);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.DA_VINCI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 280, 2000, 1000);
		add(chiave, rect);

	}

	public void CARNIATO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARNIATO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 0, 200, 50);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARNIATO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(10, 50, 2000, 20);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARNIATO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(10, 0, 2000, 50);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.CARNIATO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 240, 2000, 1000);
		add(chiave, rect);

	}

	public void BORG(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.BORG);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 0, 200, 80);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.BORG);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 150, 2000, 10);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.BORG);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 120, 2000, 40);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.BORG);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 250, 2000, 1000);
		add(chiave, rect);

	}

	public void METRO(){

		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.METRO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(500, 150, 300, 50);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.METRO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 290, 160, 30);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.METRO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 270, 160, 20);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.METRO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 360, 2000, 1000);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.METRO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.METRO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.METRO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.METRO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 140, 1000, 300);
		add(chiave, rect);

	}

	public void AMBROSI(){

		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(200, 240, 80, 10);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(500, 240, 80, 10);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(400, 240, 200, 10);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 320, 1000, 400);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 280, 2000, 400);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 280, 2000, 400);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 280, 2000, 400);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.AZIENDA_AMBROSI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 280, 2000, 400);
		add(chiave, rect);

	}

	public void ROWCLIFFE(){

		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.ROWCLIFFE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 40, 150, 50);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.ROWCLIFFE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(280, 290, 50, 20);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.ROWCLIFFE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(460, 40, 160, 50);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.ROWCLIFFE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 290, 2000, 1000);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.ROWCLIFFE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.ROWCLIFFE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.ROWCLIFFE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.ROWCLIFFE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 290, 2000, 1000);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.ROWCLIFFE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.ROWCLIFFE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.ROWCLIFFE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.ROWCLIFFE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 290, 2000, 1000);
		add(chiave, rect);
	}

	public void TAVELLA(){

		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 80, 200, 30);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(700, 150, 500, 30);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(400, 150, 450, 50);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(2);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(3);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(4);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.TAVELLA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(5);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 2000, 1000);
		add(chiave, rect);
	}
	
	public void NOSAWA(){

		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.NOSAWA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 110, 200, 20);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.NOSAWA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 600, 2000, 200);
		rect = new Rectangle2D.Double(0, 500, 2000, 500);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.NOSAWA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(400, 150, 450, 50);
		rect = new Rectangle2D.Double(300, 110, 450, 100);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI);
		chiave.setCliente(Testo.NOSAWA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 230, 2000, 1000);
		add(chiave, rect);
		
	}
		
	public void add(Chiave chiave, Rectangle2D rect){
		rectangles.put(chiave, rect);
	}

}
