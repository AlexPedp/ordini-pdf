package com.econorma.rectangle;

import java.awt.geom.Rectangle2D;
import java.util.LinkedHashMap;

import com.econorma.data.Chiave;
import com.econorma.testo.Testo;

public class RectangleFresco {

	
	private LinkedHashMap<Chiave, Rectangle2D> rectangles = new LinkedHashMap<Chiave, Rectangle2D>();
	
	public RectangleFresco(){
		init();
	}
	
	public void init(){
		LANDO_FRE();
		RAGIS();
		SERENISSIMA();
		MAXIDI();
		SOGEGROSS();
		CATTEL();
		CADORO();
		CONAD_FRE();
		LADISA();
		ROSSI();
		GS();
		PREGIS();
		VIVO();
		DAC();
		PRIX();
		COOP();
		UNICOOP_TIRRENO();
		RIAFRE();
		ECOR();
		IGES();
		MADIA();
		BIOLOGICA();
		CRAI_FRE();
		SIAF();
		ROSSETTO();
		NUME();
		GMF();
		VIS_FOOD();
		CIRFOOD();
		CAMST();
	}
	
	public LinkedHashMap<Chiave, Rectangle2D> getRectangles(){
		return rectangles;
	}
	
	
	public void LANDO_FRE(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.LANDO_FRE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 50, 250, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.LANDO_FRE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(460, 190, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.LANDO_FRE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 200, 300, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.LANDO_FRE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 290, 3000, 2000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.LANDO_FRE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2); 
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.LANDO_FRE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.LANDO_FRE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.LANDO_FRE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 80, 3000, 2000);
		add(chiave, rect);
		 
		
	}
	
	public void RAGIS(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.RAGIS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(150, 180, 400, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.RAGIS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 250, 310, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.RAGIS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(100, 60, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.RAGIS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 2000, 1000);
		add(chiave, rect);
	}
	
	public void SERENISSIMA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.SERENISSIMA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 180, 400, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.SERENISSIMA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 200, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.SERENISSIMA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 150, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.SERENISSIMA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 290, 2000, 1000);
		add(chiave, rect);
	}
	
	public void MAXIDI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.MAXIDI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(500, 80, 300, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.MAXIDI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(400, 140, 300, 20);
		rect = new Rectangle2D.Double(400, 120, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.MAXIDI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(0, 140, 300, 20);
		rect = new Rectangle2D.Double(0, 120, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.MAXIDI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
//		rect = new Rectangle2D.Double(0, 190, 3000, 2000);
		rect = new Rectangle2D.Double(0, 170, 3000, 2000);
		add(chiave, rect);
		
	}
	
	public void SOGEGROSS(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.SOGEGROSS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		Rectangle2D rect = new Rectangle2D.Double(280, 30, 500, 10);
		Rectangle2D rect = new Rectangle2D.Double(280, 30, 150, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.SOGEGROSS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(360, 190, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.SOGEGROSS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 200, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.SOGEGROSS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 320, 3000, 2000);
		add(chiave, rect);
		
	}
	
	public void CATTEL(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CATTEL);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(250, 100, 400, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CATTEL);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 190, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CATTEL);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 40, 800, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CATTEL);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 240, 5000, 1000);
		add(chiave, rect);
	}
	
	public void CADORO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(90, 220, 40, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
 		rect = new Rectangle2D.Double(0, 10, 1000, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 10, 1000, 100);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CADORO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 270, 2000, 500);
		add(chiave, rect);
	}
	
	public void CONAD_FRE(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CONAD_FRE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 280, 220, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CONAD_FRE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
 		rect = new Rectangle2D.Double(400, 210, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CONAD_FRE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 210, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CONAD_FRE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 360, 2000, 100);
		add(chiave, rect);
		 
	}
	
	public void LADISA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.LADISA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 250, 400, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.LADISA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 180, 500, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.LADISA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 140, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.LADISA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 320, 2000, 1000);
		add(chiave, rect);
	}
	
	public void ROSSI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.ROSSI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 100, 400, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.ROSSI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 220, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.ROSSI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 220, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.ROSSI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 250, 2000, 1000);
		add(chiave, rect);
	}
	
	public void GS(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.GS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 180, 400, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.GS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(310, 280, 500, 20);
		rect = new Rectangle2D.Double(0, 600, 500, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.GS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(310, 280, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.GS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 370, 2000, 1000);
		add(chiave, rect);
	}
	
	public void PREGIS(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.PREGIS);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 270, 400, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.PREGIS);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 230, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.PREGIS);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 130, 400, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.PREGIS);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 370, 2000, 1000);
		add(chiave, rect);
	}
	
	public void VIVO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.VIVO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 80, 400, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.VIVO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 400, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.VIVO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 90, 250, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.VIVO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 180, 2000, 1000);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.VIVO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.VIVO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.VIVO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 0, 0, 0);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.VIVO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 180, 2000, 1000);
		add(chiave, rect);
	}
	
	public void DAC(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.DAC);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		Rectangle2D rect = new Rectangle2D.Double(0, 50, 400, 20);
//		Rectangle2D rect = new Rectangle2D.Double(0, 80, 400, 20);
		Rectangle2D rect = new Rectangle2D.Double(180, 50, 400, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.DAC);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(200, 170, 200, 10);
//		rect = new Rectangle2D.Double(200, 190, 200, 10);
		rect = new Rectangle2D.Double(200, 170, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.DAC);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 170, 250, 10);
//		rect = new Rectangle2D.Double(0, 190, 250, 10);
		rect = new Rectangle2D.Double(0, 170, 250, 40);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.DAC);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 460, 2000, 1000);
//		rect = new Rectangle2D.Double(0, 500, 2000, 1000);
		rect = new Rectangle2D.Double(0, 460, 2000, 1000);
		add(chiave, rect);
	}
	
	public void PRIX(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.PRIX);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 70, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.PRIX);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 120, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.PRIX);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 20, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.PRIX);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 220, 2000, 1000);
		add(chiave, rect);
	}
	
	public void COOP(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.COOP_VA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 220, 1000, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.COOP_VA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 210, 1000, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.COOP_VA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 120, 1000, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.COOP_VA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 310, 2000, 500);
		add(chiave, rect);
	}
	
	public void UNICOOP_TIRRENO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 50, 1000, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 150, 1000, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 150, 1000, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.UNICOOP_TIRRENO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
//		rect = new Rectangle2D.Double(0, 250, 2000, 500);
		rect = new Rectangle2D.Double(0, 210, 2000, 500);
		add(chiave, rect);
	}
	
	public void RIAFRE(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.RIAFRE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(300, 40, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.RIAFRE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 120, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.RIAFRE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 90, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.RIAFRE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 170, 2000, 1000);
		add(chiave, rect);
	}
	
	public void ECOR(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.ECOR);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 200, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.ECOR);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 190, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.ECOR);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 40, 400, 40);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.ECOR);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 2000, 1000);
		add(chiave, rect);
	}
	
	public void IGES(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.IGES);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(400, 180, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.IGES);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(360, 150, 350, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.IGES);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 150, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.IGES);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 320, 3000, 2000);
		add(chiave, rect);
		
	}
	
	public void MADIA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.MADIA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(200, 100, 400, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.MADIA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 50, 400, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.MADIA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(200, 50, 400, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.MADIA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 290, 2000, 1000);
		add(chiave, rect);
	}
	
	public void BIOLOGICA(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.BIOLOGICA);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(350, 170, 400, 40);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.BIOLOGICA);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(400, 220, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.BIOLOGICA);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 220, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.BIOLOGICA);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 300, 2000, 1000);
		add(chiave, rect);
	}
	
	public void CRAI_FRE(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CRAI_FRE);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(200, 30, 310, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CRAI_FRE);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(500, 130, 350, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CRAI_FRE);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(500, 110, 350, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CRAI_FRE);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 250, 2000, 280);
		add(chiave, rect);
	}
	
	public void SIAF(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.SIAF);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 50, 1000, 150);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.SIAF);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 220, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.SIAF);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 220, 500, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.SIAF);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 250, 2000, 1000);
		add(chiave, rect);
	}
	
	public void ROSSETTO(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(500, 60, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(500, 40, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(420, 10, 200, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.ROSSETTO);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 130, 3000, 2000);
		add(chiave, rect);
	}
	
	public void NUME(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.NUME);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 80, 300, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.NUME);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(300, 180, 300, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.NUME);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(150, 180, 200, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.NUME);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 320, 3000, 2000);
		add(chiave, rect);
		
	}
	
	public void GMF(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.GMF);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 80, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.GMF);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 140, 400, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.GMF);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(180, 120, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.GMF);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 220, 3000, 2000);
		add(chiave, rect);
	}
	
	public void VIS_FOOD(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.VIS_FOOD);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 280, 180, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.VIS_FOOD);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 260, 400, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.VIS_FOOD);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 160, 200, 10);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.VIS_FOOD);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 410, 3000, 1000);
		add(chiave, rect);
	}
	
	public void CIRFOOD(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CIRFOOD);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 60, 180, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CIRFOOD);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(400, 70, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CIRFOOD);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(510, 50, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CIRFOOD);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 220, 3000, 1000);
		add(chiave, rect);
	}
	
	public void CAMST(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CAMST);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		Rectangle2D rect = new Rectangle2D.Double(0, 350, 200, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CAMST);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(250, 160, 310, 50);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CAMST);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(250, 150, 310, 20);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_FRESCOLAT);
		chiave.setCliente(Testo.CAMST);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V2);
		rect = new Rectangle2D.Double(0, 520, 2000, 1000);
		add(chiave, rect);
	}
	
	public void add(Chiave chiave, Rectangle2D rect){
		rectangles.put(chiave, rect);
	}
	
}
