package com.econorma.rectangle;

import java.awt.geom.Rectangle2D;
import java.util.LinkedHashMap;

import com.econorma.data.Chiave;
import com.econorma.testo.Testo;

public class RectangleAfu {

	private LinkedHashMap<Chiave, Rectangle2D> rectangles = new LinkedHashMap<Chiave, Rectangle2D>();

	public RectangleAfu(){
		init();
	}

	public void init(){
		TONYS_FINE_FOODS();
	}

	public LinkedHashMap<Chiave, Rectangle2D> getRectangles(){
		return rectangles;
	}

	public void TONYS_FINE_FOODS(){

		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI_USA);
		chiave.setCliente(Testo.TONYS_FINE_FOOD);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(350, 50, 150, 10);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI_USA);
		chiave.setCliente(Testo.TONYS_FINE_FOOD);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(400, 80, 500, 10);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI_USA);
		chiave.setCliente(Testo.TONYS_FINE_FOOD);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(400, 50, 1000, 20);
		add(chiave, rect);

		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_AMBROSI_USA);
		chiave.setCliente(Testo.TONYS_FINE_FOOD);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(1);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 140, 2000, 500);
//		rect = new Rectangle2D.Double(0, 130, 2000, 500);
		add(chiave, rect);

	}
	
	public void add(Chiave chiave, Rectangle2D rect){
		rectangles.put(chiave, rect);
	}
	
}
