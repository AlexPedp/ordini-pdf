package com.econorma.rectangle;

import java.awt.geom.Rectangle2D;
import java.util.LinkedHashMap;

import com.econorma.data.Chiave;
import com.econorma.testo.Testo;

public class RectangleCesena {

	
	private LinkedHashMap<Chiave, Rectangle2D> rectangles = new LinkedHashMap<Chiave, Rectangle2D>();
	
	public RectangleCesena(){
		init();
	}
	
	public void init(){
		EMMECI();
	}
	
	public LinkedHashMap<Chiave, Rectangle2D> getRectangles(){
		return rectangles;
	}
	
	
	public void EMMECI(){
		Chiave chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_CESENA);
		chiave.setCliente(Testo.EMMECI);
		chiave.setKey(Testo.SPEDIZIONE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		Rectangle2D rect = new Rectangle2D.Double(0, 200, 220, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_CESENA);
		chiave.setCliente(Testo.EMMECI);
		chiave.setKey(Testo.DATA_CONSEGNA);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 300, 500, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_CESENA);
		chiave.setCliente(Testo.EMMECI);
		chiave.setKey(Testo.ORDINE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 300, 500, 30);
		add(chiave, rect);
		
		chiave = new Chiave();
		chiave.setAzienda(Testo.AZIENDA_CESENA);
		chiave.setCliente(Testo.EMMECI);
		chiave.setKey(Testo.RIGHE);
		chiave.setPage(0);
		chiave.setVersion(Testo.V1);
		rect = new Rectangle2D.Double(0, 350, 3000, 1000);
		add(chiave, rect);
		 
	}
		
	public void add(Chiave chiave, Rectangle2D rect){
		rectangles.put(chiave, rect);
	}
	
}
