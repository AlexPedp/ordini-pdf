package com.econorma.extract.doc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.UUID;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;

import com.econorma.testo.Testo;

public class ExtractDoc {

	private String type;
	private String deposito;

	public void execute(File file, String azienda) throws IOException, ParseException{


		FileInputStream fs= new FileInputStream(file.getAbsoluteFile());
		BufferedReader br = new BufferedReader(new InputStreamReader(fs));
		
		HWPFDocument doc = new HWPFDocument(fs);
		WordExtractor extractor = new WordExtractor(doc);
		String delimiter = "\\s+";
		String text = extractor.getText();

		type = getType(text);
		deposito = getDeposito();
		
		File csvFile = new File(type + "_" +UUID.randomUUID() + ".csv");
		PrintWriter pw = new PrintWriter(csvFile);
		StringBuilder sb = new StringBuilder();

		if (text != null) {
			boolean startReading = false;
			String art = null;
			String desc = null;
			String strArray[] = new String[7];

			for (String line : text.split("\n|\r")) {
				
				if (line.isEmpty() || line.trim().length() == 0){
					continue;
				}

				switch (type) {
				case Testo.NARITA:

					if (line.toUpperCase().contains(Testo.ITEM_NAME)){
						try {
							line = line.replaceAll("[^\\x00-\\x7F]", "").toUpperCase();
							String[] split = line.split(Testo.QUANTITY);
							art = split[0].replaceAll(Testo.ITEM_NAME, "").trim();
							desc = split[0].replaceAll(Testo.ITEM_NAME, "").trim();
							String[] split2 = split[1].split("=");
							strArray[0] = split2[0].replaceAll(Testo.CTN, "").trim();
							strArray[1] = "";
							strArray[2] = "";
							strArray[3] = "";
							strArray[4] = "";
							strArray[5] = "";
							strArray[6] = "";
							startReading = true;
						}
						
						 catch (Exception e) {
							 
						}
						
					} else {
						startReading = false;
					}


					break;
				case Testo.VRM:

					if (line.toUpperCase().contains(Testo.SALUTI)){
						break;
					}
 

					if (startReading) {

						int firstNumber=0;
						if (line.contains(Testo.KG)){
							int indexOf = line.indexOf(Testo.KG) + 2;
							String newLine = line.substring(indexOf, line.length());
							firstNumber = getFirstNumber(newLine) + indexOf;
						} else {
							firstNumber = getFirstNumber(line);
						}

						String artDesc[] = line.substring(0, firstNumber-1).split(delimiter);
						art = artDesc[0].toUpperCase();
						desc = artDesc[1].toUpperCase();

						strArray = line.substring(firstNumber, line.length()).split(delimiter);
 
					}

					if (line.toUpperCase().contains(Testo.PRODOTTI)){
						startReading = true;
					}

					break;
				}

				
				if (startReading){
					System.out.println(line);
					sb.append(type);
					sb.append(';');
					sb.append(deposito);
					sb.append(';');
					sb.append("");
					sb.append(';');
					sb.append("");
					sb.append(';');
					sb.append("");
					sb.append(';');
					
					switch (type) {
					case Testo.NARITA:
						sb.append(art);
						sb.append(';');
						sb.append(desc);
						break;
					case Testo.VRM:	
						sb.append(art + " " + desc);
						sb.append(';');
						sb.append(art + " " + desc);
						break;
					}
					
					
					sb.append(';');
					sb.append(strArray[0]);
					sb.append(';');
					sb.append(strArray[2]);
					sb.append(';');
					sb.append(strArray[3]);
					sb.append(';');
					sb.append(strArray[4]);
					sb.append(';');
					sb.append(strArray[5]);
					sb.append(';');
					sb.append(strArray[6]);
					sb.append('\n');
				}
		
			}


			if (br != null){
				br.close();
			}

			pw.write(sb.toString());
			pw.close();
			System.out.println("Done");
		}

	}

	public int getFirstNumber(String data){

		for (int i = 0; i < data.length(); i++) {

			if (Character.isDigit(data.charAt(i))){
				return i;
			}

		}
		return 0;
	}

	private String getType(String text)  throws IOException{

		String type = Testo.VRM;

		for (String line : text.split("\n|\r")) {
			if (line.toUpperCase().trim().contains(Testo.NARITA)){
				return Testo.NARITA;
			} else if (line.toUpperCase().trim().contains(Testo.VRM_CODE)) {
				return Testo.VRM;   
			} 
		}

		return type;

	}

	private String getDeposito()  throws IOException{

		switch (type) {
		case Testo.NARITA:
			return Testo.NARITA;

		case Testo.VRM:
			return Testo.VRM_DEPOSITO;

		}

		return null;

	}

}


