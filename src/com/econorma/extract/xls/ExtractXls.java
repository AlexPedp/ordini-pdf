package com.econorma.extract.xls;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.econorma.extract.pdf.ExtractPdf;
import com.econorma.testo.Testo;
import com.econorma.util.Logger;


public class ExtractXls {

	private static final Logger logger = Logger.getLogger(ExtractPdf.class);
	private static final String TAG = "ExtractXls";

	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private static SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yy");
	
	private String type;
	private String deposito = "";
	private String[] dati = new String[2];
	private String consegna ="";
	private HSSFWorkbook workbook;
	private HSSFSheet sheet;
	private Iterator<Row> iterator;
	private StringBuilder sb;

	public void execute(File file, String azienda) throws IOException{

		FileInputStream inputStream = new FileInputStream(file);
		
		workbook = new HSSFWorkbook(inputStream);
		sheet = workbook.getSheetAt(0);
		iterator = sheet.iterator();
		sb = new StringBuilder();

		type = getType(iterator);

		if (type == null){
			System.out.println("Ordine di cliente non valido");
			sheet = workbook.getSheetAt(1);
			iterator = sheet.iterator();
			type = getType(iterator);
			if (type == null){
				System.out.println("Ordine di cliente non valido");
				return;
			}
			
		}
		
		String baseName = FilenameUtils.getFullPath(file.getAbsolutePath());
		File csvFile = new File(baseName + "\\" + type + "_" +UUID.randomUUID() + ".csv");
		PrintWriter pw = new PrintWriter(csvFile);
		int sheets = workbook.getNumberOfSheets();
		
		switch (type) {
		case Testo.MAKRO:
		case Testo.MAKRO_GRE:
		
			for (int i = 0; i < sheets; i++) {
				if (i==0){
					continue;
				}
				sheet = workbook.getSheetAt(i);
				iterator = sheet.iterator(); 
				extract();
			}
			break;
		case Testo.METRO_SLOVACCHIA:
		case Testo.METRO_SERBIA:
		
			for (int i = 0; i < sheets; i++) {
				sheet = workbook.getSheetAt(i);
				iterator = sheet.iterator(); 
				extract();
			}
			
			break;

		default:
			extract();
			break;
		}
 
		pw.write(sb.toString());
		pw.close();
		System.out.println("Done");

		workbook.close();
		inputStream.close();
	}


	public void extract () throws IOException{
		iterator = sheet.iterator();
		deposito = getDeposito(iterator);

		iterator = sheet.iterator();
		dati = getOrdine(iterator);

		boolean exit = false;

		iterator = sheet.iterator();
		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			switch (type) {
			case Testo.MAKRO:
			case Testo.MAKRO_GRE:
			case Testo.METRO_SLOVACCHIA:
			case Testo.METRO_SERBIA:
				if(nextRow.getRowNum() < 3){
					continue;  
				}
				break;
			case Testo.EUROSPIN:
				if(nextRow.getRowNum() < 19){
					continue;  
				}
				break;
			case Testo.ROWCLIFFE:
				if(nextRow.getRowNum() < 9){
					continue;  
				}
				break;
			case Testo.IPERCOOP:
				if(nextRow.getRowNum() < 9){
					continue;  
				}
				break;
			case Testo.BRAGA:
				if(nextRow.getRowNum() < 23){
					continue;  
				}
				break;
			case Testo.AMBROSI_PRODUZIONE:
				if(nextRow.getRowNum() < 3){
					continue;  
				}
				break;
			case Testo.AMBROSI_COMMERCIALIZZATI:
				if(nextRow.getRowNum() < 1){
					continue;  
				}
				break;
			case Testo.AMBROSI_USA:
				if(nextRow.getRowNum() < 1){
					continue;  
				}
				break;
			}


			Cell cell1 = null;

			switch (type) {
			case Testo.AMBROSI_COMMERCIALIZZATI:
				cell1 = nextRow.getCell(2);
				break;
			default:
				cell1 = nextRow.getCell(0);
				break;
			}


			if (cell1 == null){
				continue;
			}

			String string = getCellValue(cell1);
			String colli = "";

			switch (type) {
			case Testo.EUROSPIN:

				if (string.contains(Testo.ATTENZIONE)){
					break;
				}

				cell1 = nextRow.getCell(1);
				colli = getCellValue(cell1);
				if (colli == null || colli == "0") {
					continue;
				}

				break;
				
			case Testo.BRAGA:
				if (string.isEmpty()){
					exit = true;
				}
			 	cell1 = nextRow.getCell(3);
				colli = getCellValue(cell1);
				if (colli == null || colli == "0" || colli.trim().length()==0) {
					continue;
				}

				break;
				
			case Testo.MAKRO:
			case Testo.MAKRO_GRE:
			case Testo.METRO_SLOVACCHIA:
			case Testo.METRO_SERBIA:

				if (string.contains(Testo.TOTALE) || string.isEmpty()){
					exit = true;
					break;
				}

				cell1 = nextRow.getCell(6);
				colli = getCellValue(cell1);
				if (colli == null || colli.equals("0.0")) {
					continue;
				}
				break;

			case Testo.ROWCLIFFE:	
				if (string.contains(Testo.ATTENZIONE)){
					break;
				}

				cell1 = nextRow.getCell(1);
				colli = getCellValue(cell1);
				if (colli == null || colli == "0" || colli.isEmpty()) {
					continue;
				}

				break;
			case Testo.IPERCOOP:
				if (string.isEmpty()){
					exit = true;
				}
				break;

			case Testo.AMBROSI_COMMERCIALIZZATI:
				if (string.isEmpty()){
					continue;
				}
				break;

			default:
				break;
			}
			
			if (exit==true){
				break;
			}

			String art = null;
			String dart = null;
			switch (type) {
			case Testo.EUROSPIN:
				cell1 = nextRow.getCell(3);
				art = getCellValue(cell1);
				break;
			case Testo.ROWCLIFFE:
				cell1 = nextRow.getCell(8);
				art = getCellValue(cell1);
				cell1 = nextRow.getCell(3);
				dart = getCellValue(cell1);
				break;
			case Testo.IPERCOOP:
				cell1 = nextRow.getCell(1);
				art = getCellValue(cell1).split(" ")[0];
				cell1 = nextRow.getCell(1);
				dart = getCellValue(cell1);
				break;
			case Testo.AMBROSI_PRODUZIONE:
				cell1 = nextRow.getCell(0);
				art = getCellValue(cell1);
				cell1 = nextRow.getCell(1);
				dart = getCellValue(cell1);
				break;
			case Testo.AMBROSI_USA:
				cell1 = nextRow.getCell(0);
				art = getCellValue(cell1);
				cell1 = nextRow.getCell(1);
				dart = getCellValue(cell1);
				break;
			case Testo.BRAGA:
				cell1 = nextRow.getCell(0);
				art = getCellValue(cell1);
				cell1 = nextRow.getCell(1);
				dart = getCellValue(cell1);
				break;
			case Testo.MAKRO:
			case Testo.MAKRO_GRE:
			case Testo.METRO_SLOVACCHIA:
			case Testo.METRO_SERBIA:
				cell1 = nextRow.getCell(0);
				art = getCellValue(cell1);
				cell1 = nextRow.getCell(2);
				dart = getCellValue(cell1);
				break;
			case Testo.AMBROSI_COMMERCIALIZZATI:
				cell1 = nextRow.getCell(2);
				art = getCellValue(cell1);
				String[] split = art.split("-");

				if (split.length==2) {
					art = split[0];
					dart = split[1];
				} else {
					art = split[0];
					dart = split[0];
				}
				break;
			}


			sb.append(type);
			sb.append(';');
			sb.append(deposito.trim());
			sb.append(';');
			sb.append(dati[0]);
			sb.append(';');
			sb.append(dati[1]);
			sb.append(';');
			sb.append(consegna);
			sb.append(';');
			sb.append(art.trim().toUpperCase());
			sb.append(';');

			switch (type) {
			case Testo.EUROSPIN:
			case Testo.IPERCOOP:
				break;
			case Testo.ROWCLIFFE:
			case Testo.AMBROSI_PRODUZIONE:
			case Testo.AMBROSI_COMMERCIALIZZATI:
			case Testo.AMBROSI_USA:
			case Testo.MAKRO:
			case Testo.MAKRO_GRE:
			case Testo.METRO_SLOVACCHIA:
			case Testo.METRO_SERBIA:
				sb.append(dart.trim().toUpperCase());
				sb.append(';');
				break;
			}

			int i = 0;
			while (cellIterator.hasNext()) {

				i++;
				Cell cell = cellIterator.next();
				String stringCellValue = getCellValue(cell);

				switch (type) {
				case Testo.EUROSPIN:
					break;
				case Testo.IPERCOOP:
					if (stringCellValue.length()== 0){
						continue;
					}
					if (i != 2 && i != 7){
						continue;
					}
					break;
				case Testo.ROWCLIFFE:
					if (stringCellValue.length()== 0){
						continue;
					}
					if (i != 2){
						continue;
					}
					break;
				case Testo.BRAGA:
					if (stringCellValue.length()== 0){
						continue;
					}
					if (i != 4){
						continue;
					}
					break;
				case Testo.AMBROSI_PRODUZIONE:
				case Testo.AMBROSI_USA:
					if (stringCellValue.length()== 0){
						continue;
					}
					if (i != 3 && i != 4){
						continue;
					}
					if (i == 3) {
						stringCellValue = stringCellValue + ";;PZ";
					}
					if (i == 4) {
						stringCellValue = stringCellValue + ";;CT";
					}
					break;
				case Testo.AMBROSI_COMMERCIALIZZATI:
					if (stringCellValue.length()== 0){
						continue;
					}
					if (i != 3 && i != 4){
						continue;
					}

					String[] split = stringCellValue.split(" ");

					if (split.length==2) {
						stringCellValue = split[0] + ";;" +  split[1].toUpperCase();
					} else {
						stringCellValue= split[0].toUpperCase();
					}

					break;
					
				case Testo.MAKRO:
				case Testo.MAKRO_GRE:
					if (stringCellValue.length()== 0){
						continue;
					}
					if (i != 7){
						continue;
					}
					
					stringCellValue = String.format("%.0f", Double.parseDouble(stringCellValue));
					
					break;
					
				case Testo.METRO_SLOVACCHIA:
				case Testo.METRO_SERBIA:
					if (stringCellValue.length()== 0){
						continue;
					}
					if (i != 7){
						continue;
					}
					
					stringCellValue = String.format("%.0f", Double.parseDouble(stringCellValue));
					
					break;
				}

				System.out.print(" - ");
				sb.append(stringCellValue);
				sb.append(";"); 	

			}

			sb.append("\n");
			System.out.println();
		}
	}

	public String getCellValue(Cell cell){


		String stringCellValue = "";
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			stringCellValue = cell.getStringCellValue();
			System.out.println(cell.getStringCellValue());
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			System.out.println(cell.getBooleanCellValue());
			break;
		case Cell.CELL_TYPE_NUMERIC:
			stringCellValue = null;
			if (HSSFDateUtil.isCellDateFormatted(cell)) {
				stringCellValue = sdf.format(cell.getDateCellValue());
				System.out.println(cell.getNumericCellValue());
			} else {

				cell.setCellType(Cell.CELL_TYPE_STRING);
				stringCellValue = cell.getStringCellValue();

				//	stringCellValue = String.valueOf(cell.getNumericCellValue());  
				System.out.println(cell.getStringCellValue());
			}

			break;
		case Cell.CELL_TYPE_FORMULA:
			stringCellValue = null;
			
			 switch (cell.getCachedFormulaResultType()) {
             case Cell.CELL_TYPE_STRING:
            	 stringCellValue = cell.getRichStringCellValue().getString();
                 System.out.println(cell.getRichStringCellValue().getString());
                 break;
             case Cell.CELL_TYPE_NUMERIC:
            	 if (HSSFDateUtil.isCellDateFormatted(cell)) {
     				stringCellValue = sdf.format(cell.getDateCellValue());
     			} else {
     				stringCellValue = String.valueOf(cell.getNumericCellValue());    	
     			}
            	 System.out.println(cell.getNumericCellValue());
                 break;
         }
				
			break;
		}
		return stringCellValue;

	}

	private String getType(Iterator<Row> iterator)  throws IOException{

		String type = null;

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			while(cellIterator.hasNext()){
				Cell cell = cellIterator.next();
				String stringCellValue = getCellValue(cell);
				if (stringCellValue.toUpperCase().trim().contains(Testo.EUROSPIN)){
					return Testo.EUROSPIN;
				} else if (stringCellValue.toUpperCase().trim().contains(Testo.BRAGA)) {
					return Testo.BRAGA; 
				} else if (stringCellValue.toUpperCase().trim().contains(Testo.ROWCLIFFE)) {
					return Testo.ROWCLIFFE; 
				} else if (stringCellValue.toUpperCase().trim().contains(Testo.IPERCOOP)) {
					return Testo.IPERCOOP; 
				} else if (stringCellValue.toUpperCase().trim().contains(Testo.AMBROSI_PRODUZIONE_CODE)) {
					return Testo.AMBROSI_PRODUZIONE; 
				} else if (stringCellValue.toUpperCase().trim().contains(Testo.AMBROSI_COMMERCIALIZZATI_CODE)) {
					return Testo.AMBROSI_COMMERCIALIZZATI; 
				}else if (stringCellValue.toUpperCase().trim().contains(Testo.AMBROSI_USA_CODE)) {
					return Testo.AMBROSI_USA; 
				}else if (stringCellValue.toUpperCase().trim().contains(Testo.MAKRO_GRE_CODE)) {
					return Testo.MAKRO_GRE; 
				}else if (stringCellValue.toUpperCase().trim().contains(Testo.MAKRO_CODE)) {
					return Testo.MAKRO; 
				}else if (stringCellValue.toUpperCase().trim().contains(Testo.METRO_SLOVACCHIA_CODE)) {
					return Testo.METRO_SLOVACCHIA; 
				}else if (stringCellValue.toUpperCase().trim().contains(Testo.METRO_SERBIA_CODE)) {
					return Testo.METRO_SERBIA; 
				}
			}
		}

		return type;

	}

	private String getDeposito(Iterator<Row> iterator)  throws IOException{

		String deposito = null;

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();

			switch (type) {
			case Testo.EUROSPIN:
				return Testo.EUROSPIN;
			case Testo.ROWCLIFFE:
				return Testo.ROWCLIFFE;
			case Testo.BRAGA:
				return Testo.BRAGA;
			case Testo.IPERCOOP:
				return Testo.FERRARA;
			case Testo.AMBROSI_PRODUZIONE:
				return Testo.AMBROSI_PRODUZIONE;
			case Testo.AMBROSI_COMMERCIALIZZATI:
				return Testo.AMBROSI_COMMERCIALIZZATI;
			case Testo.AMBROSI_USA:
				return Testo.AMBROSI_USA;
			case Testo.MAKRO:
			case Testo.MAKRO_GRE:
				return Testo.MAKRO;
			case Testo.METRO_SLOVACCHIA:
				return Testo.METRO_SLOVACCHIA;
			case Testo.METRO_SERBIA:
				return Testo.METRO_SERBIA;
			}

		}

		return deposito;


	}

	private String[] getOrdine(Iterator<Row> iterator)  throws IOException{

		String dati[] = new String[2];

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();
			String stringCellValue = null;
			
			Cell cell = cellIterator.next();
			switch (type) {
			case Testo.EUROSPIN:
				dati[0] = "";
				dati[1] = ""; 
				consegna = sdf.format(new Date());
				return dati;
				
			case Testo.BRAGA:
				dati[0] = "";
				dati[1] = sdf.format(new Date()); 
				consegna = sdf.format(new Date());
				return dati;

			case Testo.AMBROSI_PRODUZIONE:
			case Testo.AMBROSI_COMMERCIALIZZATI:
			case Testo.AMBROSI_USA:
				dati[0] = "";
				dati[1] = ""; 
				consegna = sdf.format(new Date());
				return dati;

			case Testo.IPERCOOP:
				if(nextRow.getRowNum() == 3){
					Cell cell2 = nextRow.getCell(2);
					String day = getCellValue(cell2);
					dati[0] = "";
					//					dati[1] = sdf.format(new Date());
					dati[1] =  sdf.format(getDate(day).getTime());
				}
				if(nextRow.getRowNum() == 4){
					Cell cell2 = nextRow.getCell(2);
					String day = getCellValue(cell2);
					//					consegna = sdf.format(new Date());
					consegna =  sdf.format(getDate(day).getTime());
					return dati;
				}
				break;
			case Testo.ROWCLIFFE:
				if(nextRow.getRowNum() == 3){
					Cell cell1 = nextRow.getCell(3);
					String data = getCellValue(cell1);

					Matcher m = Pattern.compile("([0-9]+)").matcher(data);
					if (m.find()) {
						dati[0] = m.group(0);
					}

					dati[1] = sdf.format(new Date()); 
					consegna = sdf.format(new Date());
					return dati;
				}
				break;

			case Testo.MAKRO:
			case Testo.MAKRO_GRE:
				Cell cell2 = nextRow.getCell(2);
				if (cell2==null)
					break;
				stringCellValue = getCellValue(cell2);
				if (stringCellValue.toUpperCase().trim().contains(Testo.PREPARAZIONE)){
					Cell cell3 = nextRow.getCell(5);
					String data = getCellValue(cell3);
					dati[1] = reformatData(format1, data);
				}
				if (stringCellValue.toUpperCase().trim().contains(Testo.RITIRO) || stringCellValue.toUpperCase().trim().contains(Testo.CONSEGNA_XLS)){
					Cell cell3 = nextRow.getCell(5);
					String data = getCellValue(cell3);
					consegna = reformatData(format1, data);
				}	
				if (stringCellValue.toUpperCase().trim().contains(Testo.ORDINE)){
					Cell cell3 = nextRow.getCell(3);
					String numero = getCellValue(cell3);
					dati[0] = numero;
					return dati;
				}	
				
					break;
					 
			case Testo.METRO_SLOVACCHIA:
			case Testo.METRO_SERBIA:
				Cell cell4 = nextRow.getCell(2);
				if (cell4==null){
					continue;
				}
				stringCellValue = getCellValue(cell4);
				if (stringCellValue.toUpperCase().trim().contains(Testo.PREPARAZIONE)){
					Cell cell3 = nextRow.getCell(5);
					String data = getCellValue(cell3);
					dati[1] = reformatData(format1, data);
				}
				if (stringCellValue.toUpperCase().trim().contains(Testo.CARICO) || stringCellValue.toUpperCase().trim().contains(Testo.CONSEGNA_XLS)){
					Cell cell3 = nextRow.getCell(5);
					String data = getCellValue(cell3);
					consegna = reformatData(format1, data);
				}	
				if (stringCellValue.toUpperCase().trim().contains(Testo.ORDINE)){
					Cell cell3 = nextRow.getCell(5);
					String numero = getCellValue(cell3);
					dati[0] = numero;
					return dati;
				}
				
				if(type == Testo.METRO_SERBIA){
					dati[0] = "";
					dati[1] = sdf.format(new Date());
					consegna = sdf.format(new Date());
				}
				
					break;
						
				}
			}


		return dati;
	}


	public Calendar getDate(String day){

		int dayOfWeek = 0;
		int hour = 0;  
		int minute = 0;

		switch (day) {
		case "LUN":
			dayOfWeek = Calendar.MONDAY;
			break;
		case "MAR":
			dayOfWeek = Calendar.TUESDAY;
			break;
		case "MER":
			dayOfWeek = Calendar.WEDNESDAY;		
			break;
		case "GIO":
			dayOfWeek = Calendar.THURSDAY;
			break;
		case "VEN":
			dayOfWeek = Calendar.FRIDAY;
			break;
		case "SAB":
			dayOfWeek = Calendar.SATURDAY;
			break;
		case "DOM":
			dayOfWeek = Calendar.SUNDAY;
			break;
		default:
			break;
		}

		Calendar cal = Calendar.getInstance();  
		if (cal.get(Calendar.DAY_OF_WEEK) != dayOfWeek) {
			cal.add(Calendar.DAY_OF_MONTH, 7);
		} else {

		}

		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minute);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		return cal;

	}

	public static String reformatData(SimpleDateFormat fromFormat, String originDate){
		String reformatteDate = null;
		try {
			reformatteDate = sdf.format(fromFormat.parse(originDate.trim()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reformatteDate;
	}



}


