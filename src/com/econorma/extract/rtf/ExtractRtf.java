package com.econorma.extract.rtf;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.rtf.RTFEditorKit;

import org.apache.commons.io.FilenameUtils;

import com.econorma.logic.Parsers;
import com.econorma.testo.Testo;

public class ExtractRtf {
 
	private String ordine ="";
	private String spedizione="";
	private String cliente = Testo.METRO;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	public void execute(File file,  String azienda) throws IOException, ParseException, BadLocationException{

		String baseName = FilenameUtils.getFullPath(file.getAbsolutePath());
	 	File csvFile = new File(baseName + "\\" + Testo.METRO + "_" +UUID.randomUUID() + ".csv");
		PrintWriter pw = new PrintWriter(csvFile);
		StringBuilder sb = new StringBuilder();

		RTFEditorKit rtf = new RTFEditorKit();
		Document doc = rtf.createDefaultDocument();
		FileInputStream fi = new FileInputStream(file.getAbsolutePath());
		rtf.read(fi, doc, 0);
		String text = doc.getText(0, doc.getLength());
		System.out.println(text);
		
		
		spedizione = Parsers.extractString(azienda, cliente, Testo.SPEDIZIONE, text);	
	 
		
		if (text != null) {
			boolean startReading = false;
			
			for (String line : text.split("\n|\r")) {
				
				if (line.trim().isEmpty()){
					continue;
				}
				
				if (line.toUpperCase().contains(Testo.ATTENZIONE) || line.toUpperCase().contains(Testo.DATA)){
					break;
				}
				
				if (line.toUpperCase().contains(Testo.ORDINE) && !line.toUpperCase().contains(Testo.ORDINE_MERCE)){
					String array[] = line.split("/");
					String str = array[array.length-2].replaceAll("\\D+","");
					ordine =str;
					continue;
				}
				
				if (startReading) {
					
					String strArray[] = line.split("\\s+");
					
					
					sb.append(Testo.METRO);
					sb.append(';');
					sb.append(spedizione);
					sb.append(';');
					sb.append(ordine);
					sb.append(';');
					sb.append(sdf.format(new Date()));
					sb.append(';');
					sb.append(sdf.format(new Date()));
					sb.append(';');
					sb.append(strArray[2].replaceFirst ("^0*", ""));
					sb.append(';');
					sb.append(strArray[3] + " " + strArray[4]);
					sb.append(';');
					sb.append(strArray[strArray.length-2]);
					sb.append(';');
					sb.append(strArray[strArray.length-1]);
					sb.append(';');
					sb.append('\n');
				}
				
				
				if (line.toUpperCase().contains(Testo.QUANTITA)){
					startReading = true;
				}
				
			}
		}
		 
	  
			pw.write(sb.toString());
			pw.close();
			fi.close();
			System.out.println("Done");
		}
  

}



