package com.econorma.extract.txt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import com.econorma.extract.pdf.ExtractPdf;
import com.econorma.logic.Parsers;
import com.econorma.testo.Testo;
import com.econorma.util.Logger;


public class ExtractTxt {

	private String ordine;
	private String data;
	private String consegna;
	private String spedizione;
	private String cliente;
	private String azienda;

	private static final Logger logger = Logger.getLogger(ExtractPdf.class);
	private static final String TAG = "ExtractTxt";
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	public void execute(File file, String azienda) throws IOException, ParseException{
		this.azienda=azienda;
		StringBuilder sb = new StringBuilder();
		List<String> elements = new ArrayList<>();
		String delimiter = " ";
		List<String> contents= new ArrayList<String>();

		FileInputStream fs= new FileInputStream(file.getAbsoluteFile());
		BufferedReader br = new BufferedReader(new InputStreamReader(fs));
		String line;

		StringBuilder  lines = new StringBuilder();
		while ((line = br.readLine()) != null) {
			lines.append(line);
			contents.add(line);
		}

		cliente = getType(contents);

		if (cliente == null){
			logger.error(TAG,"Ordine di cliente non valido");
			return;
		}

		logger.info(TAG,"Ordine del cliente: " + cliente);

		String baseName = FilenameUtils.getFullPath(file.getAbsolutePath());
		File csvFile = new File(baseName + "\\" + cliente + "_" +UUID.randomUUID() + ".csv");
		PrintWriter pw = new PrintWriter(csvFile);

		ordine = "";
		data = "";
		consegna = "";
		spedizione = "";

		if (cliente.equals(Testo.IPER) || (cliente.equals(Testo.RIALTO)) || cliente.equals(Testo.COOP_VA)){
			spedizione = Parsers.extractString(azienda, cliente, Testo.SPEDIZIONE, contents.toString());	
		}
		if (cliente.equals(Testo.HAMBERLIN)){
			spedizione = Testo.HAMBERGER;
		}
		if (cliente.equals(Testo.HAMMUNCHEN)) {
			spedizione = Parsers.extractString(azienda, cliente, Testo.SPEDIZIONE, contents.toString());
			if (spedizione.contains("ERNA-SAMUEL-STR")){
				cliente = Testo.HAMBERLIN;
			}
		}

		if (cliente.equals(Testo.GMF)){
			spedizione = Testo.GMF;
		}

		boolean rows = false;
		int i = 0;
		int count = 0;

		for (String row: contents) {
			i++;

			if (row.trim().isEmpty()){
				continue;
			}

			String[] dati = new String[2];

			if (cliente == Testo.VLEESWAREN){
				if (row.toUpperCase().trim().contains(Testo.ORDER)){
					ordine = row;
					dati = Parsers.extract(azienda, Testo.VLEESWAREN, Testo.ORDINE, ordine);
					ordine = dati[0];
					continue;
				}

				if (row.toUpperCase().trim().contains(Testo.DATUM)){
					data = row;
					dati = Parsers.extract(azienda, Testo.VLEESWAREN, Testo.DATA_ORDINE, data);
					data = dati[0];
					continue;
				}

				if (row.toUpperCase().trim().contains(Testo.BESTELLING)){
					consegna = row;
					dati = Parsers.extract(azienda, Testo.VLEESWAREN, Testo.DATA_CONSEGNA, consegna);
					consegna = dati[0];
					continue;
				}

				if (row.toUpperCase().trim().contains(Testo.LEVERADRES)){
					spedizione = row;
					spedizione = Parsers.extractString(azienda, Testo.VLEESWAREN, Testo.SPEDIZIONE, spedizione);
					continue;
				}

			}

			if (cliente == Testo.KALLAS){

				spedizione = Testo.KALLAS;

				if (row.toUpperCase().trim().contains(Testo.PURCHASE_ORDER)){
					ordine = row;
					dati = Parsers.extract(azienda, Testo.KALLAS, Testo.ORDINE, ordine);
					ordine = dati[0];
					continue;
				}

				if (row.toUpperCase().trim().contains(Testo.ORDER_DATE)){
					data = row;
					dati = Parsers.extract(azienda, Testo.KALLAS, Testo.DATA_ORDINE, data);
					data = dati[0];
					consegna = dati[0];
					continue;
				}

			}

			if (cliente == Testo.IPER || cliente == Testo.COOP_VA){

				if (row.toUpperCase().trim().contains(Testo.ORDIN_N)){
					ordine = row;
					dati = Parsers.extract(azienda, Testo.IPER, Testo.ORDINE, ordine);
					ordine = dati[0];
					continue;
				}

				if (row.toUpperCase().trim().contains(Testo.DATA_ORDINE)){
					data = row;
					dati = Parsers.extract(azienda, Testo.IPER, Testo.DATA_ORDINE, data);
					data = dati[0];
					continue;
				}

				if (row.toUpperCase().trim().contains(Testo.DATA_CONSEGNA)){
					consegna = row;
					dati = Parsers.extract(azienda, Testo.IPER, Testo.DATA_CONSEGNA, consegna);
					consegna = dati[0];
					continue;
				}

				if (row.contains(Testo.SEPARATORE1)){
					continue;
				}

				if (row.toUpperCase().contains(Testo.FINE_ORDINE)){
					break;
				}

			}

			if (cliente == Testo.RIALTO){

				if (row.toUpperCase().trim().contains(Testo.ORDIN_N)){
					ordine = row;
					dati = Parsers.extract(azienda, Testo.RIALTO, Testo.ORDINE, ordine);
					ordine = dati[0];
					data = sdf.format(new Date());
					continue;
				}

				if (row.toUpperCase().trim().contains(Testo.CONSEGNARE)){
					consegna = row;
					dati = Parsers.extract(azienda, Testo.RIALTO, Testo.DATA_CONSEGNA, consegna);
					consegna = dati[0];
					continue;
				}

				if (row.toUpperCase().trim().contains(Testo.IDONEI)){
					break;
				}

			}

			if (cliente == Testo.HAMBERLIN || cliente == Testo.HAMMUNCHEN){

				if (row.toUpperCase().trim().contains(Testo.ORDER_NUMMBER)){
					ordine = row;
					dati = Parsers.extract(azienda, Testo.HAMBERGER, Testo.ORDINE, ordine);
					ordine = dati[0];
					data = dati[1];
					continue;
				}

				if (row.toUpperCase().trim().contains(Testo.DELIVERY_DATE)){
					consegna = row;
					dati = Parsers.extract(azienda, Testo.HAMBERGER, Testo.DATA_CONSEGNA, consegna);
					consegna = dati[0];
					continue;
				}

				if (row.toUpperCase().trim().contains(Testo.PLEASE_NOTE)){
					break;
				}

				if (row.toUpperCase().contains(Testo.SOTTOLINEATURA_2) || row.toUpperCase().contains(Testo.DESIGNATION) || row.toUpperCase().contains(Testo.SIGNATURE)){
					continue;
				}

				if (row.trim().length() < 50){					
					continue;
				}

			}

			if (cliente == Testo.AMACRAI){

				spedizione = Testo.AMACRAI;

				if (row.toUpperCase().trim().contains(Testo.RIFERIMENTO_AMA)||row.toUpperCase().trim().contains(Testo.RIFERIMENTO_AMA_1)){
					ordine = row;
					dati = Parsers.extract(azienda, Testo.AMACRAI, Testo.ORDINE, ordine);
					ordine = dati[0];
					data = dati[1];
					continue;
				}

				if (row.toUpperCase().trim().contains(Testo.CONSEGNA_DEL)){
					consegna = row;
					dati = Parsers.extract(azienda, Testo.AMACRAI, Testo.DATA_CONSEGNA, consegna);
					consegna = dati[0];
					continue;
				}

				if (row.toUpperCase().contains(Testo.TOTALE_COLLI)){
					break;
				}

				if (row.trim().length() < 40){					
					continue;
				}

				if (row.toUpperCase().contains(Testo.COLLI) || row.toUpperCase().contains(Testo.EAN) || row.toUpperCase().contains(Testo.FORNITORE) ||
						row.toUpperCase().contains(Testo.LISTINO) || row.toUpperCase().contains(Testo.SOTTOLINEATURA_2) || row.toUpperCase().contains(Testo.SOTTOLINEATURA_4) ||
						row.toUpperCase().contains(Testo.SOTTOLINEATURA_5) 	|| row.toUpperCase().contains(Testo.PAG_2) ||
						row.toUpperCase().contains(Testo.MONTEBELLUNA)|| row.toUpperCase().replace(" ", "").contains(Testo.ORDINI_FORNITORI)||
						row.toUpperCase().contains(Testo.SC_PAG) || row.toUpperCase().contains(Testo.CENTRALINO)){
					continue;
				}

			}

			if (cliente == Testo.GMF){

				if (row.toUpperCase().trim().contains(Testo.ORDINE)){
					ordine = row;
					dati = Parsers.extract(azienda, Testo.GMF, Testo.ORDINE, ordine);
					ordine = dati[0];
					data = dati[1];
					continue;
				}

				if (row.toUpperCase().trim().contains(Testo.DATA_CONSEGNA)){
					consegna = row;
					dati = Parsers.extract(azienda, Testo.GMF, Testo.DATA_CONSEGNA, consegna);
					consegna = dati[0];
					continue;
				}

				if (row.toUpperCase().trim().contains(Testo.RIFERIMENTI_CONTRATTO)){
					break;
				}

				if (row.toUpperCase().contains(Testo.SOTTOLINEATURA_2)){
					continue;
				}

				if (row.trim().length() < 50){					
					continue;
				}

			}


			if (row.toUpperCase().trim().contains(Testo.CODE) || row.toUpperCase().trim().contains(Testo.CART) ||  row.toUpperCase().trim().contains(Testo.ARTICOLO) ||  row.toUpperCase().trim().contains(Testo.ART_NO) ||  row.toUpperCase().trim().contains(Testo.ART_LU)){
				rows = true;
				count=0;
				continue;
			}

			if (cliente == Testo.AMACRAI && count>8){
				rows = false;
			}


			if (rows == false){
				continue;	
			}


			row = row.replaceAll("[^\\x00-\\x7F]","");


			String rowArray[]  = null;
			String art = null;
			String desc = null;
			String qta = null;
			boolean isValid = true;

			switch (cliente) {
			case Testo.VLEESWAREN:
				rowArray = row.trim().split("\\s+");
				art =  rowArray[rowArray.length -4];
				desc = rowArray[2] +  rowArray[3];
				qta =  rowArray[rowArray.length -2];
				break;

			case Testo.KALLAS:
				rowArray = row.trim().split("\\s+");
				art =  rowArray[0].trim();
				desc = rowArray[2] +  rowArray[3];
				qta =  rowArray[rowArray.length -2];
				break;

			case Testo.HAMMUNCHEN:
			case Testo.HAMBERLIN:
				rowArray = row.trim().split("\\s+");
				art =  rowArray[0];
				desc = rowArray[1] +  rowArray[2];
				int indexOf = row.lastIndexOf("x");
				if (indexOf!=-1){
					row = row.substring(0, indexOf); 
				}
				rowArray = row.trim().split("\\s+");
				qta =  rowArray[rowArray.length -1];
				break;

			case Testo.IPER:
				rowArray = row.trim().split("\\s+");
				art =  rowArray[rowArray.length -7].replace(".", "");
				if(art.trim().length()>5){
					art = art.replaceFirst ("^0*", "");
				}
				if (azienda.equals(Testo.AZIENDA_SOLIGO)){
					art =rowArray[rowArray.length -6];
				}
				desc = rowArray[rowArray.length -8];
				qta =  rowArray[rowArray.length -2];
				break;

			case Testo.COOP_VA:
				rowArray = row.trim().split("\\s+");
				art =rowArray[rowArray.length -6];
				desc = rowArray[rowArray.length -8];
				qta =  rowArray[rowArray.length -2];
				break;

			case Testo.GMF:
				rowArray = row.trim().split("\\s+");
				art =  rowArray[0];
				desc = rowArray[1] + rowArray[2];
				indexOf = row.lastIndexOf("PZ");
				if (indexOf!=-1){
					row = row.substring(0, indexOf+10); 
				}
				rowArray = row.trim().split("\\s+");
				qta =  rowArray[rowArray.length-1];
				break;

			case Testo.AMACRAI:
				logger.info(TAG, row);
				rowArray = row.trim().split("\\s+");
				art =  rowArray[0];
				desc = rowArray[3] + rowArray[4];
				qta =  rowArray[rowArray.length -1];
				count++;
				break;

			case Testo.RIALTO:
				isValid = false;
				if (row.toUpperCase().contains(Testo.PARA)){
					int first = row.lastIndexOf(Testo.COLLI);
					int end = row.lastIndexOf(Testo.PARA);
					if (end > 0) {
						qta = row.substring(first+6, end).trim();
						elements.add(qta);
					}
					isValid = false;
				} else if (row.toUpperCase().contains(Testo.VS_CODE)){
					rowArray = row.trim().split("\\s+");
					art = rowArray[rowArray.length-1];
					elements.add(art);
					elements.add(Testo.DESCRIZIONE);
					isValid = true;
				} 

				break;

			default:
				break;
			}

			if(cliente.equals(Testo.RIALTO)){
				if (!isValid){
					continue;
				} else {
					art = elements.get(1);
					desc = elements.get(2);
					qta = elements.get(0);
					elements.clear();
				}

			}
			
			sb.append(Testo.ATTIVO);
			sb.append("|");
			sb.append(cliente.trim());
			sb.append(';');
			sb.append(spedizione);
			sb.append(';');
			sb.append(ordine.trim());
			sb.append(';');
			sb.append(data);
			sb.append(';');
			sb.append(consegna);
			sb.append(';');
			sb.append(art.trim());
			sb.append(';'); 
			sb.append(desc.trim());
			sb.append(';');
			sb.append(qta);
			sb.append(';');
			sb.append('\n');

		}

		if (br != null){
			br.close();
		}

		pw.write(sb.toString());
		pw.close();
		System.out.println("Done");

	}

	private String getType(List<String> contents)  throws IOException{
		String type = null;
		boolean isHamberger = false;

		if (azienda.equals(Testo.AZIENDA_SOLIGO)){
			for (String row: contents) {
				if (row.toUpperCase().trim().contains(Testo.ALLEANZA)){
					return Testo.COOP_VA;
				}
			}
		}

		if (azienda.equals(Testo.AZIENDA_VIPITENO)){
			for (String row: contents) {
				if (row.toUpperCase().trim().contains(Testo.HAMBERGER)) {
					isHamberger = true;
				}
				if (isHamberger && row.toUpperCase().trim().contains(Testo.BERLIN_ERNA)) {
					return Testo.HAMBERLIN;
				}
			}
		}

		isHamberger=false;
		for (String row: contents) {
			String normalize = StringUtils.stripAccents(row);
			if (row.toUpperCase().trim().contains(Testo.DEPUYDT)){
				return Testo.VLEESWAREN;
			} else if (row.toUpperCase().trim().contains(Testo.KALLAS)) {
				return Testo.KALLAS; 
			}  else if (row.toUpperCase().trim().contains(Testo.IPER)) {
				return Testo.IPER; 
			} else if (row.toUpperCase().trim().contains(Testo.RIALTO)) {
				return Testo.RIALTO; 
			}else if (row.toUpperCase().trim().contains(Testo.AMACRAI_CODE)) {
				return Testo.AMACRAI; 
			}else if (row.toUpperCase().trim().contains(Testo.HAMBERGER)) {
				isHamberger = true;
				if (normalize.toUpperCase().trim().contains(Testo.BERLIN)){
					return Testo.HAMBERLIN; 
				}
			}else if (isHamberger && normalize.toUpperCase().trim().contains(Testo.BERLIN)) {
				return Testo.HAMBERLIN; 
			}else if (isHamberger && normalize.toUpperCase().trim().contains(Testo.MUNCHEN)) {
				return Testo.HAMMUNCHEN; 
			}else if (row.toUpperCase().trim().contains(Testo.GMF_CODE)) {
				return Testo.GMF; 
			}
		}
		return type;
	}


}


