package com.econorma.extract.xml;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FilenameUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.econorma.data.Ordine;
import com.econorma.properties.OrdersProperties;
import com.econorma.testo.Testo;
import com.econorma.util.Logger;

public class ExtractXml {

	private static final Logger logger = Logger.getLogger(ExtractXml.class);
	private static final String TAG = "ExtractXml";

	private DocumentBuilderFactory dbFactory;
	private DocumentBuilder dBuilder;
	private Document doc;

	private List<Ordine> ordini = new ArrayList<Ordine>();
	private static final SimpleDateFormat toFormat = new SimpleDateFormat("dd/MM/yyyy");
	private static final SimpleDateFormat fromFormat = new SimpleDateFormat("dd.MM.yyyy");
	private static final SimpleDateFormat fileFormat = new SimpleDateFormat("ddMMyyyy");

	private StringBuilder sb;
	private PrintWriter pw;

	private String extractorDirectory;
	private String extractorBackup;
	private String nomeFile;
	private String cliente;

	public ExtractXml(){
		Map<String, String> map = OrdersProperties.getInstance().load();
		extractorDirectory = (String) map.get("extractor_directory");
		extractorBackup = (String) map.get("extractor_backup");
	}


	public void execute(File file, String azienda) throws Exception {

		try {

			sb = new StringBuilder();

			InputStream inputStream= new FileInputStream(file);
			Reader reader = new InputStreamReader(inputStream,"UTF-8");
			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");

			dbFactory = DocumentBuilderFactory.newInstance();
			dbFactory.setValidating(false);
			dbFactory.setNamespaceAware(true);
			dbFactory.setFeature("http://xml.org/sax/features/namespaces", false);
			dbFactory.setFeature("http://xml.org/sax/features/validation", false);
			dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(is);

			doc.getDocumentElement().normalize();

			nomeFile = readAttributes();
			cliente = readNodes();

			String baseName = FilenameUtils.getFullPath(file.getAbsolutePath());

			File csvFile = new File(baseName + "\\" + cliente  + "_" +UUID.randomUUID() + ".csv");
			pw = new PrintWriter(csvFile);

		} catch (Exception e) {
			logger.error(TAG, "Exception: " + e); 
		}

		if (ordini.size()>0){

			for (Ordine o: ordini){
				sb.append(o.getCliente());
				sb.append(";");
				sb.append(o.getSpedizione());
				sb.append(";");
				sb.append(o.getOrdine());
				sb.append(";");
				sb.append(reformatData(o.getData()));
				sb.append(";");
				sb.append(reformatData(o.getConsegna()));
				sb.append(";");
				sb.append(o.getContent());
				sb.append(";");
				sb.append(";");
				sb.append(";");
				sb.append(";");
				sb.append(";");
				sb.append(";");
				sb.append(";");
				sb.append(";");
				sb.append(";");
				sb.append(";");
				sb.append(";");
				sb.append(nomeFile);
				sb.append("\n");
			}

			pw.write(sb.toString());
			pw.close();

//			renameFile();

		}

	}

	public String  readAttributes(){

		String nomeFile = null;

		NodeList fields = doc.getElementsByTagName(Testo.ATTRIBUTES).item(0).getChildNodes();
		for(int i=0;i<fields.getLength();i++) {
			if(fields.item(i).getNodeType()== Node.ELEMENT_NODE) {
				Element child = (Element)fields.item(i);
				String tag = child.getAttributes().getNamedItem(Testo.KEY).getNodeValue();
				nomeFile = child.getAttributes().getNamedItem(Testo.VALUE).getNodeValue();
				logger.info(TAG, "File PDF name: " +  nomeFile);
			}
		}
		return FilenameUtils.getName(nomeFile);
	}

	public String  readNodes(){

		String cliente = null;
		String spedizione = null;
		String spedizioneSave = null;
		String numeroOrdine = null;
		String dataOrdine = null;
		String dataConsegna = null;
		String articolo = null;
		String descrizione = null;
		String quantita = null;

		NodeList fields = doc.getElementsByTagName(Testo.FIELD_GROUP).item(0).getChildNodes();
		for(int i=0;i<fields.getLength();i++) {
			if(fields.item(i).getNodeType()== Node.ELEMENT_NODE) {
				Element child = (Element)fields.item(i);
				String tag = child.getAttributes().getNamedItem(Testo.NAME).getNodeValue();
				String value = child.getAttributes().getNamedItem(Testo.VALUE).getNodeValue();

//				if (tag.equals(Testo.OR_NAME1)){
//					cliente = getRealCliente(value);
//				}
				if (tag.equals(Testo.OR_INVOICE_NUMBER)){
					cliente = value;
				}
				if (tag.equals(Testo.OR_SUPPLIER_NUMBER)){
					spedizione = value;
				}
				if (tag.equals(Testo.ORD_NUMBER)){
					numeroOrdine = value;
				}
				if (tag.equals(Testo.ORD_DATE)){
					dataOrdine = value;
				}
				if (tag.equals(Testo.ORD_DELIVERY_DATE)){
					dataConsegna = value;
				}
			}

		}
 
//		try {
//			spedizione = Parsers.extractString(cliente, Testo.SPEDIZIONE, spedizioneSave);
//		} catch (Exception e) {
//			logger.error(TAG, e);
//		}
		
//		if (spedizione==null){
//			spedizione = spedizioneSave;
//		}

		logger.info(TAG, "Ordine cliente: " + cliente + "|"  + spedizione + "|"+ numeroOrdine + "|" + dataOrdine + "|" + dataConsegna);

		int rows = doc.getElementsByTagName("ROW").getLength();

		for(int i=0;i<rows;i++) {

			NodeList data = doc.getElementsByTagName(Testo.ROW).item(i).getChildNodes();
			for(int x=0;x<data.getLength();x++) {

				if(data.item(x).getNodeType()== Node.ELEMENT_NODE) {
					Element child = (Element)data.item(x);
					String tag = child.getAttributes().getNamedItem(Testo.NAME).getNodeValue();
					String value = child.getAttributes().getNamedItem(Testo.VALUE).getNodeValue();
					if (tag.equals(Testo.ORI_ARTICLE_NO)){
						articolo = value;
					}
					if (tag.equals(Testo.ORI_QUANTITY)){
						quantita = value;
						descrizione = Testo.DESCRIPTION;
						logger.debug(TAG, "Articolo: " + articolo + "|"  + value);
						Ordine ordine = new Ordine();
						ordine.setCliente(cliente);
						ordine.setSpedizione(spedizione);
						ordine.setConsegna(dataConsegna);
						ordine.setOrdine(numeroOrdine);
						ordine.setData(dataOrdine);
						ordine.setContent(articolo + ";" + descrizione + ";" + quantita);
						ordini.add(ordine);
					}
				}
			}
		}

		return cliente;
	}

	public static String reformatData(String originDate){
		String reformatteDate = null;
		try {
			reformatteDate = toFormat.format(fromFormat.parse(originDate.trim()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reformatteDate;
	}

	public static String getRealCliente(String cliente){

		if (cliente.contains(Testo.COMMERCIANTI_INDIPENDENTI) || cliente.contains(Testo.COMMERCIANTI_INDIPENDENTI_1)){
			cliente = Testo.CONAD;
		}
		if (cliente.contains(Testo.COOP_CONSORZIO_NORD_OVEST) || cliente.contains(Testo.COOP_CONSORZIO_NORD_OVEST_1)){
			cliente = Testo.CONSORZIO_NORD_OVEST;
		}
		if (cliente.contains(Testo.TATO_PARIDE_NAME) || cliente.contains(Testo.ETA_SRL) || cliente.contains(Testo.MAGA_FRESCHI)){
			cliente = Testo.TATO;
		}
		if (cliente.contains(Testo.LE_DUE_SICILIE)){
			cliente = Testo.SICILIE;
		}
		if (cliente.contains(Testo.APULIA_DISTRIBUZIONE)){
			cliente = Testo.APULIA;
		}
		if (cliente.contains(Testo.MAIORANA_MAGGIORINO)){
			cliente = Testo.MAIORANA;
		}
		if (cliente.contains(Testo.MAXI_DI_SRL)){
			cliente = Testo.MAXI_DI;
		}
		
		if (cliente.contains(Testo.PREGIS_SPA)){
			cliente = Testo.PREGIS;
		}

		return cliente;

	}

	public void renameFile(){

		try {

			File fromFile = new File(extractorDirectory +  "\\" + nomeFile + ".pdf");
			String ordineFile = "DOCORDCL" + ordini.get(0).getOrdine() + fileFormat.format(new Date()) + ".pdf";
			File toFile = new File(extractorDirectory +  "\\" + ordineFile);
			File toAS400File = new File(extractorBackup +  "\\" + ordineFile);

			if (!fromFile.getParentFile().exists()){
				fromFile.getParentFile().mkdirs();
			}
			if (toFile.exists()){
				toFile.delete();
			}

			fromFile.renameTo(toFile);
			toFile.renameTo(toAS400File);
			logger.info(TAG, "File PDF renamed to: " + toFile.getAbsolutePath());
			
			
		} catch (Exception e) {
			logger.error(TAG, e);
		}


	}

}

