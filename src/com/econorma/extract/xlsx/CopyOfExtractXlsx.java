package com.econorma.extract.xlsx;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.econorma.extract.pdf.ExtractPdf;
import com.econorma.testo.Testo;
import com.econorma.util.Logger;


public class CopyOfExtractXlsx {

	private static final Logger logger = Logger.getLogger(ExtractPdf.class);
	private static final String TAG = "ExtractXlsx";

	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private static SimpleDateFormat format1= new SimpleDateFormat("dd.MM.yy");
	private static SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");
	private String type;
	private String deposito = "";
	private String[] dati = new String[2];
	private String consegna ="";
	private Workbook workbook;
	private Sheet sheet;
	private String sheetName;

	public void execute(File file) throws Exception{
		
		FileInputStream inputStream = new FileInputStream(file);
		workbook = new XSSFWorkbook(inputStream);
		sheet = workbook.getSheetAt(0);
		StringBuilder sb = new StringBuilder();
		
		Iterator<Row> iterator = sheet.iterator();

		type = getType(iterator);
		
		sheetName = workbook.getSheetName(0);
		if (sheetName.toUpperCase().contains(Testo.ARKUSZ1)){
			type = Testo.JANSEN;	
		}

		if (type == null){
			System.out.println("Ordine di cliente non valido");
			return ;
		}
		
		String baseName = FilenameUtils.getFullPath(file.getAbsolutePath());
	 	File csvFile = new File(baseName + "\\" + type + "_" +UUID.randomUUID() + ".csv");
		PrintWriter pw = new PrintWriter(csvFile);
		
		iterator = sheet.iterator();
		deposito = getDeposito(iterator);
		
		int page = workbook.getNumberOfSheets();
		
		switch (type) {
		case Testo.ROWCLIFFE:
			
			for(int i=0; i<page; i++) {
				if(i==0){
					continue;
				}
				sheet = workbook.getSheetAt(i);
				extract(sheet, sb);
			}
			
			break;

		default:
			sheet = workbook.getSheetAt(0);
			extract(sheet, sb);
			break;
		}

		pw.write(sb.toString());
		pw.close();
		System.out.println("Done");

		workbook.close();
		inputStream.close();
		
	}

	
	public void extract(Sheet sheet, StringBuilder sb) throws Exception {
		
		boolean exit = false;
		
		Iterator<Row> iterator = sheet.iterator();
		dati = getOrdine(iterator);
		int columns = 0;

		iterator = sheet.iterator();
		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			Cell cell1 = null;
			String check = null;

			switch (type) {
			case Testo.MIGUEL:
				if(nextRow.getRowNum() < 15){
					continue;
				}
				if(nextRow.getRowNum() < 5){
					continue;
				}

				cell1 = nextRow.getCell(0);
				check = getCellValue(cell1);
				switch (type) {
				case Testo.MIGUEL:
					if (check.contains(Testo.PEDIDO)){
						exit = true;
					}
					break;
				}
				break;

			case Testo.JANSEN:
				cell1 = nextRow.getCell(0);
				check = getCellValue(cell1);
				if (check.contains(Testo.CODE)  || (check.isEmpty())){
					continue;
				}
				break;

			case Testo.MINI:
				if(nextRow.getRowNum() < 6){
					continue;
				}
				cell1 = nextRow.getCell(3);
				check = getCellValue(cell1);
				if (check.isEmpty()){
					continue;
				}

				break;

			case Testo.BONTALIA:
				cell1 = nextRow.getCell(5);
				if (cell1 == null) {
					continue;
				}

				check = getCellValue(cell1);
				if(!check.matches(".*\\d.*")){
					continue;   
				}

				break;
				
			case Testo.ROWCLIFFE:
				if(nextRow.getRowNum() < 4){
					continue;  
				}
				
				columns = sheet.getRow(4).getLastCellNum();
				
				if (columns == 5){
					cell1 = nextRow.getCell(4);
				} else {
					cell1 = nextRow.getCell(3);
				}
				
				String colli = getCellValue(cell1);
				if (colli == null || colli == "0" || colli.isEmpty()) {
					continue;
				}
				
				break;
			}

			if (exit){
				break;
			}

			sb.append(type);
			sb.append(';');
			sb.append(deposito.trim());
			sb.append(';');
			sb.append(dati[0]);
			sb.append(';');
			sb.append(dati[1]);
			sb.append(';');
			sb.append(consegna);
			sb.append(';');

			int i = 0;
			while (cellIterator.hasNext()) {

				String stringCellValue = null;

				i++;
				Cell cell = cellIterator.next();
				stringCellValue = getCellValue(cell);
				
				String string = getCellValue(cell1);
				String colli = "";

				switch (type) {
				case Testo.MIGUEL:

					if (stringCellValue.length()== 0){
						continue;
					}

					if (i==2 || i == 6 || i == 7){
						continue;
					}

					if (i==8){
						stringCellValue = stringCellValue.toUpperCase().replace("CAJAS", "");	
					}
					break;

				case Testo.BONTALIA:
					if (i==3 || i == 4 || i == 5){
						continue;
					}
					int indexOf = stringCellValue.indexOf("CT");
					if (indexOf != -1){
						stringCellValue = stringCellValue.replace("CT", "");	
					}

					break;

				case Testo.JANSEN:

					if (i==3 || i==4 || i == 6 || i == 7 || i == 8 || i == 9 || i == 11){
						continue;
					}
					break;

				case Testo.MINI:

					if (stringCellValue.length()== 0){
						continue;
					}

					if(i == 3 && stringCellValue.isEmpty()){
						continue;   
					}

					if (i != 1 && i != 2 && i != 3 && i != 10){
						continue;
					}
					break;
					
				case Testo.ROWCLIFFE:	
					
					switch (columns) {
					case 5:
						if (i != 2 && i != 3 && i != 5){
							continue;
						}
						break;
					default:
						if (i != 2 && i != 3 && i != 5){
							continue;
						}
						break;
					}
					

					break;
				}
				
//				String art = null;
//				String dart = null;
//				switch (type) {
//				case Testo.ROWCLIFFE:
//					cell1 = nextRow.getCell(8);
//					art = getCellValue(cell1);
//					cell1 = nextRow.getCell(3);
//					dart = getCellValue(cell1);
//					sb.append(art.trim().toUpperCase());
//					sb.append(';');
//					sb.append(dart.trim().toUpperCase());
//					sb.append(';');
//					sb.append(stringCellValue.trim());
//					sb.append(';'); 
//					break;
//				default:
					sb.append(stringCellValue.trim());
					sb.append(';'); 		
//				}

				
			}


			if (exit){
				break;
			}

			sb.append('\n');
			System.out.println();
		}

	}
	
	private String getType(Iterator<Row> iterator)  throws IOException{

		String type = null;

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			while(cellIterator.hasNext()){
				Cell cell = cellIterator.next();
				String stringCellValue = getCellValue(cell);
				if (stringCellValue.toUpperCase().trim().contains(Testo.MIGUEL)){
					return Testo.MIGUEL;
				} else if (stringCellValue.toUpperCase().trim().contains(Testo.BONTALIA)) {
					return Testo.BONTALIA; 
				}  else if (stringCellValue.toUpperCase().trim().contains(Testo.MINI_ELLADA)) {
					return Testo.MINI; 
				} else if (stringCellValue.toUpperCase().trim().contains(Testo.ROWCLIFFE)) {
					return Testo.ROWCLIFFE; 
				}
			}
		}

		return type;

	}

	private String getDeposito(Iterator<Row> iterator)  throws IOException{

		String deposito = null;

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			String art = null;
			boolean exit = false;

			switch (type) {
			case Testo.MIGUEL:
				if(nextRow.getRowNum() == 8){
					Cell cell1 = nextRow.getCell(2);
					String data = getCellValue(cell1);
					deposito = data;
					return deposito;
				}
				break;
			case Testo.BONTALIA:
				if(nextRow.getRowNum() == 0){
					Cell cell1 = nextRow.getCell(0);
					String data = getCellValue(cell1);
					String split[] = data.trim().split(" ");
					deposito = split[0];
					return deposito;
				}
				break;
			case Testo.MINI:
				return Testo.MINI;
			case Testo.JANSEN:
				return Testo.JANSEN;
			case Testo.ROWCLIFFE:
				return Testo.ROWCLIFFE;
			}

		}

		return deposito;


	}

	private String[] getOrdine(Iterator<Row> iterator)  throws IOException{

		String dati[] = new String[2];

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			switch (type) {
			case Testo.MIGUEL:
				if(nextRow.getRowNum() == 3){
					Cell cell1 = nextRow.getCell(2);
					String data = getCellValue(cell1);
					dati[0] = "";
					dati[1] = data; 
					consegna = data;
					return dati;
				}
				break;
			case Testo.BONTALIA:
				if(nextRow.getRowNum() == 2){
					Cell cell1 = nextRow.getCell(1);
					String data = getCellValue(cell1);
					dati[0] = "";
					dati[1] = data; 
					consegna = data;
					return dati;
				}
				break;
			case Testo.JANSEN:
				dati[0] = "";
				dati[1] = ""; 
				consegna = sdf.format(new Date());
				return dati;

			case Testo.MINI:
				dati[0] = "";
				dati[1] = ""; 
				consegna = sdf.format(new Date());
				return dati;
				
			case Testo.ROWCLIFFE:
				if(nextRow.getRowNum() == 1){
					Cell cell1 = nextRow.getCell(1);
					String data = getCellValue(cell1);
					
					Matcher m = Pattern.compile("(\\d+.\\d{2}.\\d{2})").matcher(data);
					if (m.find()) {
						dati[1] = reformatData(format1, m.group(0));
					}
					
					cell1 = nextRow.getCell(2);
					String numero = getCellValue(cell1);

					m = Pattern.compile("([0-9]+)").matcher(numero);
					if (m.find()) {
						dati[0] = m.group(0);
					}

//					dati[1] = sdf.format(new Date()); 
					consegna = sdf.format(new Date());
					return dati;
				}
				break;
			}


		}
		return dati;
	}


	public String getCellValue(Cell cell){


		String stringCellValue = "";
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			stringCellValue = cell.getStringCellValue();
			System.out.println(cell.getStringCellValue());
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			System.out.println(cell.getBooleanCellValue());
			break;
		case Cell.CELL_TYPE_NUMERIC:
			stringCellValue = null;
			if (HSSFDateUtil.isCellDateFormatted(cell)) {
				stringCellValue = sdf.format(cell.getDateCellValue());
				System.out.println(cell.getNumericCellValue());
			} else {

				cell.setCellType(Cell.CELL_TYPE_STRING);
				stringCellValue = cell.getStringCellValue();

				//	stringCellValue = String.valueOf(cell.getNumericCellValue());  
				System.out.println(cell.getStringCellValue());
			}
			break;
		case Cell.CELL_TYPE_FORMULA:
			stringCellValue = null;
			String cellFormula = cell.getCellFormula();
			try {
				if (HSSFDateUtil.isCellDateFormatted(cell)) {
					stringCellValue = sdf.format(cell.getDateCellValue());
				} else {
					stringCellValue = String.valueOf(cell.getNumericCellValue());    
				}
			} catch (Exception e) {
				stringCellValue = ""; 
			}
			System.out.println(stringCellValue);
			break;
		}
		return stringCellValue;

	}
	
	public static String reformatData(SimpleDateFormat fromFormat, String originDate){
		String reformatteDate = null;
		try {
			reformatteDate = format2.format(fromFormat.parse(originDate.trim()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reformatteDate;
	}

}


