package com.econorma.extract.pdf;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.econorma.data.Ordine;
import com.econorma.data.Riga;
import com.econorma.testo.Testo;
import com.econorma.util.Logger;

public class ExtractRows {

	private static final String TAG = ExtractRows.class.getSimpleName();
	private static final Logger logger = Logger.getLogger(ExtractRows.class);
	private static final DecimalFormat df = new DecimalFormat("#0.000");
	private String azienda;
	private String cliente;
	private Riga row;
	private Ordine order;
	private List<Riga> rows = new ArrayList<Riga>();

	public ExtractRows(Ordine order) {
		this.order=order;
	}

	public List<Riga> readRows(String content){

		azienda = order.getAzienda();
		cliente = order.getCliente();

		String delimiter = " ";
		String colli = "";
		int count = 0;

		String artSave = null;
		String descSave = null;
		String qtaSave = null;
		String prepSave = null;
		String umSave = null;
		String noteSave = null;


		List<Riga> rows = new ArrayList<Riga>();
		int indexSave=0;

		if (cliente == Testo.APULIA){
			content = content.replace("B2", "\r\n");
		}

		for (String line : content.split("\n|\r")) {
			if (!line.trim().isEmpty()) {

				String prezzo = "";
				String unita = "";
				String consegna = null;

				if (cliente == Testo.CONAD) {
					if (azienda==Testo.AZIENDA_SOLIGO) {

						colli = line.substring(7, 9).trim();

						if (!colli.isEmpty()){
							prezzo = line.substring(0, 10).trim();
							row.setPrezzo(prezzo);
							rows.add(row);
						}
					}
				}


				if (cliente == Testo.CADORO) {

					if (line.contains(Testo.SCALA)){
						unita = StringUtils.substringBetween(line, "(", ")");
						row.setUm(unita);
						continue;
					}

					if (line.contains(Testo.COSTO)){
						unita = StringUtils.substringBetween(line, "(", ")");
						prezzo = line.substring(80, 92).trim();
						row.setPrezzo(prezzo);
						if (row.getUm().trim().length()==0) {
							row.setUm(unita);
						}
						rows.add(row);
					}

					colli = line.substring(40, 45).trim();

					String extractNumber = extractNumber(colli);

					if (extractNumber.isEmpty()){
						continue;
					}

					colli = "";
					//					String[] rowArray = line.trim().split("\\s+");
					//					colli = extractNumber(rowArray[rowArray.length-9]);	

					int indexOf = line.indexOf("COSTO");
					if (indexOf!=-1){
						line = line.substring(0, indexOf); 
					}
					String[] rowArray = line.trim().split("\\s+");
					colli = rowArray[rowArray.length-3].replace("|", "");	

				}

				if (cliente == Testo.GUARNIER || cliente == Testo.UNICOMM) {

					if (line.contains(Testo.COSTO_KG) || line.contains(Testo.COSTO_PZ)){
						prezzo = line.substring(15, 26).trim();
						row.setPrezzo(prezzo);
						rows.add(row);
					}

					colli = line.substring(0, 4).trim();

					String extractNumber = extractNumber(colli);

					if (extractNumber.isEmpty()){
						continue;
					}
				}

				ExtractFilter filter = new ExtractFilter(azienda, cliente);
				String result = filter.scanLine(line);

				if (result.equals("break")) {
					break;
				}
				if (result.equals("continue")) {
					continue;
				}

				try {

					String art = null;
					String desc = null;
					String qta = null;
					String um = null;
					String um2 = null;
					String note = null;
					String art2 = null;
					String desc2 = null;
					String qta2 = null;
					String lotto = null;
					String rowArray[]  = null;
					int len = 0;

					int indexOf=0;
					switch (cliente) {

					case Testo.OPRAMOLLA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0].split("-")[1];
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-1];
						break;
						
					case Testo.GEMOS:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-1];
						break;
						
					case Testo.SICLA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						indexOf = line.toUpperCase().indexOf("PZ");
						if (indexOf!=-1){
							line = line.substring(indexOf+2, line.length()); 
						} 
						if (line.trim().length()==0) {
							continue;
						}
					
						rowArray = line.trim().split("\\s+");
						if (rowArray.length==0) {
							continue;
						}
						qta = rowArray[0];
						
						StringBuilder sb = new StringBuilder();
						for (int i = 1; i < rowArray.length; i = i+1) {
							sb.append(rowArray[i]);
							sb.append(" ");
						}
						if (sb.toString().length()>0) {
							note = sb.toString();
						}
						break;

					case Testo.CAFORM:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-3];
						break;

					case Testo.BOCON:
						count++;
						rowArray = line.trim().split("\\s+");
						switch (count) {
						case 1:
							artSave = rowArray[0];
							descSave = rowArray[1];
							continue;
						case 2:
							continue;
						case 3:
							art=artSave;
							desc=descSave;
							qta = rowArray[rowArray.length-3];
							prezzo = rowArray[rowArray.length-2];
							String regex = "(\\d{2}/\\d{2}/\\d{4})";
							Matcher m = Pattern.compile(regex).matcher(line);
							if (m.find()) {
								consegna = m.group(1);
							}  
							count=0;
							break;
						}

						break;

					case Testo.BERETTA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[2];
						indexOf = line.indexOf("KG");
						String lineO = null;
						if (indexOf!=-1){
							lineO = line.substring(0, indexOf+2); 
						}
						rowArray = lineO.trim().split("\\s+");
						qta = rowArray[rowArray.length-2];
						if (indexOf!=-1){
							lineO = line.substring(indexOf+2, line.length()); 
						}
						rowArray = lineO.trim().split("\\s+");
						prezzo = rowArray[0];
						break;

					case Testo.DECARNE:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						prezzo =rowArray[rowArray.length-2];
						indexOf = line.lastIndexOf("COTR");
						if (indexOf!=-1){
							line = line.substring(indexOf+4, line.length()); 
						}
						rowArray = line.trim().split("\\s+");
						qta = StringUtils.getDigits(rowArray[0]);
						break;

					case Testo.NOSAWA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[3]+ rowArray[4];
						qta = rowArray[0];
						break;

					case Testo.SAIT:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[3]+ rowArray[4];
						indexOf = line.lastIndexOf("CT");
						if (indexOf!=-1){
							line = line.substring(indexOf+2, line.length()); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[0];
						prezzo = rowArray[rowArray.length-1];
						break;

					case Testo.SALAROMEO:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-3];
						break;

					case Testo.VALSANA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ " " + rowArray[2];
						prezzo = rowArray[rowArray.length-1];
						indexOf = line.lastIndexOf("KG");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-3];
						break;

					case Testo.GMF:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[2]+ rowArray[2];
						qta = rowArray[rowArray.length-7];
						break;

					case Testo.GARDINI:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[2]+ rowArray[2];
						if (rowArray[rowArray.length-1].contains("50")) {
							qta = rowArray[rowArray.length-2];
						} else {
							qta = rowArray[rowArray.length-1];
						}
						if (Integer.parseInt(StringUtils.getDigits(qta))>=100) {
							continue;
						}

						break;

					case Testo.PAOLINI:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-2];
						um = rowArray[rowArray.length-1];
						break;

					case Testo.SELECTA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-5];
						break;

					case Testo.SICONAD:
						count++;
						rowArray = line.trim().split("\\s+");

						switch (count) {
						case 1:
							descSave = rowArray[0]+ rowArray[1]+ rowArray[2];
							continue;
						case 2:
							artSave = StringUtils.substringBetween(rowArray[0], "(", ")");
							continue;
						case 3:
							art=artSave;
							desc=descSave;
							qta = rowArray[rowArray.length-2];
							count=0;
							break;
						}

						break;

					case Testo.VIS_FOOD:
						count++;
						rowArray = line.trim().split("\\s+");

						if (count==1){
							indexOf = line.lastIndexOf("PZ");
							if (indexOf==-1){
								indexOf = line.lastIndexOf("KG");
								if (indexOf==-1){
									indexOf = line.lastIndexOf("CT"); 
								}
							}
						} else{
							indexOf=-1;
						}

						if (indexOf!=-1){
							art = rowArray[0];
							desc = rowArray[1]+ rowArray[2];
							qta = rowArray[rowArray.length-3];
							um = rowArray[rowArray.length-4];
							count=0;
						} else{
							if (count == 1){
								artSave = rowArray[0];
								descSave = rowArray[1]+ rowArray[2];
								continue;
							}
							if (count == 2){
								art = artSave;
								desc = descSave;
								qta = rowArray[rowArray.length-3];
								um = rowArray[rowArray.length-4];
								count=0;
							}
						}

						break;

					case Testo.PRIX:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						prezzo = rowArray[rowArray.length-3];
						indexOf = line.lastIndexOf("PZ");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-1];
						break;

					case Testo.DAC:
						count++;

						line = line.replaceAll("\\|", "!");
						rowArray = line.trim().split("!");

						if (count == 1){
							indexOf = line.indexOf("Vs.cod.:");
							if (indexOf!=-1){
								line = line.substring(0, indexOf); 
							}
							artSave = extractNumber(line);
							continue;	
						}

						art = artSave;
						desc = rowArray[1];
						qta = rowArray[4];
						count=0;
						break;

					case Testo.SERENISSIMA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[3]+ rowArray[4];
						qta = rowArray[rowArray.length-1];
						um = rowArray[rowArray.length-2];
						break;

					case Testo.CIRFOOD:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0].replace("(","").replace(")", "");
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-3];
						//						um = rowArray[rowArray.length-2];
						break;

					case Testo.ELIOR:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-2].replace(".", "");
						um = rowArray[rowArray.length-3];
						break;

					case Testo.BIOLOGICA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-5];
						um = rowArray[rowArray.length-6].replaceAll("CL", "CT");
						break;

					case Testo.VIVO:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[2]+ rowArray[3];
						indexOf = line.lastIndexOf("X");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-1];
						break;

					case Testo.RIAFRE:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[2]+ rowArray[3];
						indexOf = line.lastIndexOf("G");
						if (indexOf!=-1){
							line = line.substring(indexOf, line.length()); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[3];
						break;

					case Testo.MAXIDI:

						count++;
						rowArray = line.trim().split("\\s+");

						if (count == 1){
							artSave = rowArray[0];
							descSave = Testo.ARTICOLO;
							continue;	
						}
						if (count == 2 && azienda.equals(Testo.AZIENDA_TONIOLO)){
							artSave = rowArray[0];
							continue;
						}

						if (line.toUpperCase().contains(Testo.PALLET)){

							if (line.toUpperCase().contains(Testo.PZ) || line.toUpperCase().contains(Testo.KILI)){
								art = artSave;
								desc = descSave;
								indexOf = line.lastIndexOf("PZ");
								if (indexOf!=-1){
									line = line.substring(0, indexOf); 
								} else {
									indexOf = line.lastIndexOf("KG");
									if (indexOf!=-1){
										line = line.substring(0, indexOf);
									}
								}
								rowArray = line.trim().split("\\s+");
								qta = rowArray[rowArray.length-1];
								count= 0; 
							} else {
								continue;
							}

						} else {
							if (line.toUpperCase().contains(Testo.PZ) || (line.toUpperCase().contains(Testo.KILI) && !line.toUpperCase().contains(Testo.ASTERISCO))
									&& !line.toUpperCase().contains(Testo.DOP) && !line.toUpperCase().contains(Testo.BIORDO)){
								art = artSave;
								desc = descSave;
								rowArray = line.trim().split("\\s+");
								qta = rowArray[0];
								count= 0;
							}


							else {
								continue;	
							}
						}

						if (azienda.equals(Testo.AZIENDA_TONIOLO)) {
							//							if (art.substring(0, 1).equals("0")){
							//								art = art.substring(1, art.length());	
							//							}
							if (art.trim().length() < 5) {
								art = "0"  + art;
							}

						}

						break;

					case Testo.WOERNDLE:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[3]+ rowArray[4];
						indexOf = line.lastIndexOf("N");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-3];
						break;

					case Testo.SABELLI:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[4]+ rowArray[5];
						//						indexOf = line.lastIndexOf("N");
						//						if (indexOf!=-1){
						//							line = line.substring(0, indexOf); 
						//						}
						//						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-1];
						break;

					case Testo.IPERCONAD:
					case Testo.RDAVENETA:
					case Testo.SGR_IPER:
					case Testo.LEGNARO:
						rowArray = line.trim().split("\\s+");
						//						art = rowArray[1].replace("O", "0");
						art = rowArray[0];
						desc = rowArray[3]+ rowArray[4];
						indexOf = line.lastIndexOf("Imb");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-1];
						prezzo = rowArray[rowArray.length-3];
						break;

					case Testo.CATTEL:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						if (azienda.equals(Testo.AZIENDA_TONIOLO)) {
							art =StringUtils.stripStart(art,"0");
						}
						desc = rowArray[1]+ rowArray[2];
						um = rowArray[rowArray.length-5];
						prezzo = rowArray[rowArray.length-3];
						indexOf = line.lastIndexOf("NR");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						} else {
							indexOf = line.lastIndexOf("KG");
							line = line.substring(0, indexOf);
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-1];
						break;

					case Testo.LADISA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[2]+ rowArray[3];
						indexOf = line.indexOf(",");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-1];
						um = rowArray[rowArray.length-2].toUpperCase();
						break;

					case Testo.ROSSI:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						indexOf = line.indexOf(",");
						if (indexOf!=-1){
							line = line.substring(0, indexOf+4); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-1].replace(".", "");
						um = rowArray[rowArray.length-2].toUpperCase();
						break;

					case Testo.TAVELLA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						indexOf = line.lastIndexOf("€");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-2];
						break;

					case Testo.BAUER:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[2]+ rowArray[3];
						indexOf = line.lastIndexOf("x");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-1];
						break;

					case Testo.MARR:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						if (azienda.equals(Testo.AZIENDA_ANTICA)){
							art = rowArray[1];
						}
						desc = rowArray[2]+ rowArray[3];
						indexOf = line.lastIndexOf("CT");
						if (indexOf==-1){
							indexOf = line.lastIndexOf("PZ");
						}
						if (indexOf!=-1){
							line = line.substring(0, indexOf);
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-1];
						break;

					case Testo.CSS:
						rowArray = line.trim().split("\\s+");
						art = rowArray[2];
						desc = rowArray[4]+ rowArray[5];
						indexOf = line.indexOf("IF");
						if (indexOf!=-1){
							line = line.substring(indexOf, line.length()); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[1];
						break;

					case Testo.CAMST:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[2]+ rowArray[3];
						qta = rowArray[rowArray.length-4];
						um = "";
						if (azienda.equals(Testo.AZIENDA_FRESCOLAT)){
							um = rowArray[rowArray.length-3];
						}
						break;

					case Testo.REALCO:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[2]+ rowArray[3];
						qta = rowArray[rowArray.length-5];
						break;

					case Testo.TOCAL:
						rowArray = line.trim().split("\\s+");
						art = rowArray[2];
						desc = rowArray[3]+ rowArray[4];
						qta = rowArray[rowArray.length-4];
						break;

					case Testo.GRAMM:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-4];
						break;

					case Testo.CARAMICO:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-7];
						break;

					case Testo.MULTICEDI:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[2]+ rowArray[3];
						qta = rowArray[rowArray.length-3];
						break;

					case Testo.PAC2000:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1] +rowArray[2];
						desc = rowArray[3]+ rowArray[4];
						line = line.replace("FR/", "");
						//						indexOf = line.indexOf("/");
						indexOf = line.lastIndexOf("/");
						if (indexOf!=-1){
							line = line.substring(0, indexOf+1); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-2];
						break;

					case Testo._2000PAC:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1] +rowArray[2];
						if (azienda.equals(Testo.AZIENDA_ANTICA)){
							art = rowArray[rowArray.length-2];
						}
						desc = rowArray[3]+ rowArray[4];
						indexOf = line.indexOf("IF");
						if (indexOf!=-1){
							line = line.substring(indexOf+2, line.length()); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[0];
						break;

					case Testo.SUPEREMME:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[3]+ rowArray[4];
						indexOf = line.indexOf("Collo");
						if (indexOf!=-1){
							line = line.substring(indexOf+5, line.length()); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[0];
						break;

					case Testo.SUPERCENTRO:
						//						line = line.replace("0,1", "");
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						indexOf = line.indexOf("€");
						if (indexOf!=-1){
							line = line.substring(0, indexOf+1); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-7];
						break;

					case Testo.UNES:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						indexOf = line.indexOf("€");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-2];
						break;

					case Testo.DIMAR:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						if (!art.substring(0, 1).equals("0")) {
							art = "0" + rowArray[0];	
						}
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-7];
						break;

					case Testo.MAIORA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[2]+ rowArray[3];
						indexOf = line.indexOf("CAR");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-1];
						break;

					case Testo.WEIHE:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[2]+ rowArray[3];
						indexOf = line.indexOf("KOLLI");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						} else{
							indexOf = line.indexOf("KARTON");
							if (indexOf!=-1){
								line = line.substring(0, indexOf); 
							}
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-1];
						break;

					case Testo.RETAILPRO:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[2]+ rowArray[3];
						indexOf = line.indexOf("PZ");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						} 
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-1];
						break;

					case Testo.CONAD_FRE:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[2]+ rowArray[3];
						indexOf = line.indexOf("CT");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						} 
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-1];
						break;

					case Testo.MODERNA:
						line = line.replaceAll("__", " ");
						line = line.replaceAll("_", "");
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						if (isNumeric(rowArray[2])){
							art = art + rowArray[2];
						}
						desc = rowArray[3]+ rowArray[4];
						indexOf = line.indexOf("/");
						if (indexOf!=-1){
							line = line.substring(0, indexOf-1); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-2];
						if (!isNumeric(qta)){
							qta = rowArray[rowArray.length-1];	
						}
						break;

					case Testo.MOSCA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[2]+ rowArray[3];
						qta = rowArray[rowArray.length-2];
						break;

					case Testo.CDC:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[2]+ rowArray[3];
						qta = rowArray[rowArray.length-3];
						if (!isNumeric(qta)){
							qta = rowArray[rowArray.length-2];
						}
						break;

					case Testo.CEDI_SIGMA:
						count++;
						rowArray = line.trim().split("\\s+");

						if (rowArray[0].length()==7){
							count = 1;
						}

						if (count == 1){
							artSave = rowArray[0];
							descSave = rowArray[1]+ rowArray[2];
							continue;	
						}
						if (count == 2){
							qtaSave = rowArray[rowArray.length-3];
							continue;	
						}
						art = rowArray[0];
						desc = rowArray[1] + rowArray[2];
						qta = qtaSave;
						count= 0;
						break;

					case Testo.SISA:
						rowArray = line.trim().split("\\s+");
						//						art = rowArray[0];
						art = rowArray[1];
						desc = rowArray[2]+ rowArray[3];
						indexOf = line.indexOf("CT");
						if (indexOf!=-1){
							line = line.substring(indexOf+2, line.length());
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[0];
						break;

					case Testo.VALFOOD:
						indexOf = line.indexOf("PZ");
						String lineGG=null;
						String lineHH=null;
						if (indexOf!=-1){
							lineGG = line.substring(0, indexOf); 
							lineHH = line.substring(indexOf+2, line.length());
						}
						String[] hArray = lineGG.trim().split("\\s+");
						String[] iArray = lineHH.trim().split("\\s+");
						art = hArray[0];
						desc = hArray[2]+ hArray[3];
						qta = iArray[2];
						break;

					case Testo.TOSANO:
						indexOf = line.indexOf("COSTO");
						String lineII=null;
						String lineLL=null;
						if (indexOf!=-1){
							lineII = line.substring(0, indexOf); 
							lineLL = line.substring(indexOf+5, line.length());
						}
						String[] lArray = lineII.trim().split("\\s+");
						String[] mArray = lineLL.trim().split("\\s+");
						art = lArray[0];
						desc = lArray[1]+ lArray[2];
						qta = lArray[lArray.length-3];
						break;

					case Testo.MIGROSS:
						indexOf = line.indexOf("CT");
						String lineEE=null;
						String lineFF=null;
						if (indexOf!=-1){
							lineEE = line.substring(0, indexOf); 
							lineFF = line.substring(indexOf+2, line.length());
						}
						String[] eArray = lineEE.trim().split("\\s+");
						String[] fArray = lineFF.trim().split("\\s+");

						art = fArray[fArray.length-1];
						if (art.contains(",")){
							art = "";
						}
						desc = eArray[0]+ eArray[1];
						qta = eArray[eArray.length-1];
						break;

					case Testo.ISA:

						int conta = StringUtils.countMatches(line, "CT");
						if (conta==1){

							indexOf = line.indexOf("CT");
							String lineAA=null;
							String lineBB=null;
							if (indexOf!=-1){
								lineAA = line.substring(0, indexOf+2); 
								lineBB = line.substring(indexOf+2, line.length());
							}

							String[] aArray = lineAA.trim().split("\\s+");
							String[] bArray = lineBB.trim().split("\\s+");

							art = bArray[bArray.length-1];
							if (art.contains(",")){
								art = "";
							}
							desc = aArray[0]+ aArray[1];
							qta = bArray[0];
							art2=null;
							desc2=null;
							qta2=null;

						} else {
							indexOf = StringUtils.ordinalIndexOf(line, "CT", 1);

							String lineAA=null;
							String lineBB=null;
							if (indexOf!=-1){
								lineAA = line.substring(0, indexOf+2); 
								lineBB = line.substring(indexOf+2, indexOf+5);
							}

							String[] aArray = lineAA.trim().split("\\s+");
							String[] bArray = lineBB.trim().split("\\s+");

							art = bArray[bArray.length-1];
							if (art.contains(",") || art.trim().length()<5){
								art = "";
							}
							desc = aArray[0]+ aArray[1];
							if (!desc.contains("YOG")){
								desc= Testo.YOGURT;
							}
							qta = bArray[0];


							indexOf = StringUtils.ordinalIndexOf(line, "CT", 2);

							String lineCC=null;
							String lineDD=null;
							if (indexOf!=-1){
								lineCC = line.substring(indexOf-30, indexOf+2); 
								lineDD = line.substring(indexOf+2, line.length());
							}

							String[] cArray = lineCC.trim().split("\\s+");
							String[] dArray = lineDD.trim().split("\\s+");

							art2 = cArray[cArray.length-1];
							if (art.contains(",")|| art.trim().length()<5){
								art2 = "";
							}
							desc2 = cArray[0]+ cArray[1];
							if (!desc2.contains("YOG")){
								desc2= Testo.YOGURT;
							}
							qta2 = dArray[0];

						}


						break;

					case Testo.DCFOOD:
						indexOf = line.lastIndexOf("PZ");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[2]+ rowArray[3];
						qta = rowArray[rowArray.length-1];
						break;
					case Testo.GS:
						rowArray = line.trim().split("\\s+");
						//						art = rowArray[rowArray.length-4];
						art = rowArray[rowArray.length-5];
						desc = rowArray[1] + rowArray[2];
						qta = rowArray[rowArray.length-1];
						break;
					case Testo.ECOR:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1] + rowArray[2];
						indexOf = line.lastIndexOf("PZ");
						if (indexOf!=-1){
							line = line.substring(indexOf, line.length()); 
						} 
						rowArray = line.trim().split("\\s+");
						qta = rowArray[2];
						break;
					case Testo.PREGIS:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[3] + rowArray[4];
						//						indexOf = line.lastIndexOf("PZ");
						//						if (indexOf!=-1){
						//							line = line.substring(0, indexOf+7); 
						//						} else {
						//							indexOf = line.lastIndexOf("KG");
						//							if (indexOf!=-1){
						//								line = line.substring(0, indexOf+7);
						//							}
						//						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-3];
						//						um = rowArray[rowArray.length-2];
						break;
					case Testo.BRIMI:
						if (line.contains("10451") || line.contains("10650")){
							String ok = null;		
						}
						count++;
						rowArray = line.trim().split("\\s+");
						if (count == 1){
							artSave = rowArray[1];
							continue;	
						}
						if (count == 2){
							descSave = rowArray[1] + rowArray[2];
							continue;	
						} 

						indexOf = line.lastIndexOf("Stk");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");
						art = artSave; 
						desc = descSave;
						qta = rowArray[rowArray.length-2];
						count= 0;
						break;

					case Testo.CEDI_GROSS:
						count++;
						if (count == 1){
							indexOf = line.lastIndexOf("x");
							if (indexOf!=-1){
								line = line.substring(0, indexOf); 
							}
							String[] firstArray = line.trim().split("\\s+");
							descSave = firstArray[1] + firstArray[2];
							qtaSave =  firstArray[firstArray.length-1];
							continue;
						} else {
							String[] secondArray = line.toUpperCase().trim().split("\\s+");
							art = secondArray[secondArray.length-1];
							if (art.trim().length()<=1){
								art = secondArray[secondArray.length-2];	
							}
							if (azienda.equals(Testo.AZIENDA_TONIOLO)){
								if (art.trim().length()<5){
									art = '0' + art;	
								}
							}
							desc = descSave;
							qta = qtaSave;
							count= 0;
						}
						break;

					case Testo.ROSSETTO:
						count++;

						switch (count) {
						case 1:
							String[] firstArray = line.trim().split("\\s+");
							descSave = firstArray[1] + firstArray[2];
							qtaSave =  firstArray[firstArray.length-10];
							continue;
						case 2:
							String[] secondArray = line.toUpperCase().trim().split("\\s+");
							artSave = secondArray[1];
							continue;
						case 3:
							String[] thirdArray = line.toUpperCase().trim().split("\\s+");
							art = artSave;
							desc = descSave;
							qta = qtaSave;
							prezzo = thirdArray[thirdArray.length-4];
							count= 0;
							break;

						default:
							break;
						}

						break;

					case Testo.COAL:
						indexOf = line.lastIndexOf("x");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1] + rowArray[2];
						qta =  rowArray[rowArray.length-1];
						break;

					case Testo.SIAF:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1] + rowArray[2];
						qta =  rowArray[rowArray.length-1];
						break;

					case Testo.CRAI_FRE:
						count++;
						rowArray = line.trim().split("\\s+");

						if (count == 1){
							indexOf = line.indexOf(",");
							if (indexOf!=-1){
								line = line.substring(0, indexOf); 
							}
							rowArray = line.trim().split("\\s+");
							descSave = rowArray[7];
							qtaSave =  rowArray[rowArray.length-3];
							continue;
						}
						if (count == 2){
							art = rowArray[1];
							desc = descSave;
							qta = qtaSave;
							count=0;
						}

						break;

					case Testo.CRAI:
						indexOf = line.lastIndexOf("/");
						if (indexOf!=-1){
							line = line.substring(0, indexOf-3); 
						}
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[2] + rowArray[3];
						qta =  rowArray[rowArray.length-3];
						break;

					case Testo.GANASSA:
						if (line.contains("43009") || line.contains("10650")){
							String a = "";
						}
						indexOf = line.toUpperCase().lastIndexOf("Y");
						if (indexOf<7){
							indexOf=-1;
						}
						String line8=null;
						String line9=null;
						if (indexOf==-1) {
							indexOf = line.toUpperCase().lastIndexOf("FIT");	
						}
						if (indexOf==-1) {
							indexOf = line.toUpperCase().lastIndexOf("BUR");	
						}
						if (indexOf==-1) {
							indexOf = line.toUpperCase().lastIndexOf("LAT");	
						}
						if (indexOf==-1) {
							indexOf = line.toUpperCase().lastIndexOf("YOG");	
						}

						if (indexOf!=-1){
							if (line.toUpperCase().contains("FIT") && line.toUpperCase().contains("LAT")){
								int indexOf1 = line.toUpperCase().lastIndexOf("FIT");
								int indexOf2 = line.toUpperCase().lastIndexOf("LAT");
								indexOf = ((indexOf1>indexOf2) ? indexOf1 : indexOf2);
							}

						}

						if (indexOf!=-1){
							if (line.trim().length()>60) {
								line8 = line.substring(0, indexOf-7); 
								line9 = line.substring(indexOf-7, line.length());	
							} else {
								line8 = line.substring(0, line.length()); 
								line9 = "";	
							}
						}

						String[] tenthArray = line8.trim().split("\\s+");
						String[] eleventhArray = line9.trim().split("\\s+");
						art = tenthArray[0].replace("Y", "");
						desc = tenthArray[1] + tenthArray[2];
						qta =  tenthArray[tenthArray.length-2];

						if (line9.trim().length()>0) {
							art2 = eleventhArray[0].replace("Y", "");
							desc2 = eleventhArray[1] + eleventhArray[2];
							qta2 =  eleventhArray[eleventhArray.length-2];	
						} else {
							qta2 = "0";
						}

						if (qta.equals("0") && qta2.equals("0")){
							continue;
						}

						if (qta.equals("0") || art.toUpperCase().contains(Testo.ORDINE) || art.toUpperCase().contains(Testo.ATTENZIONE)|| art.toUpperCase().contains(Testo.ALTRIMENTI)){
							art=null;
						}


						if (!isNumeric(qta)){
							art = null;
						}

						if (qta2.equals("0")){
							art2=null;
						}

						if (!isNumeric(qta2)){
							art2 = null;
						}

						break;

					case Testo.LANDO:

						if (line.toUpperCase().contains("**")){
							noteSave = line.trim();
							continue;
						}

						count++;
						if (count == 1){
							String[] firstArray = line.toUpperCase().trim().split("-");

							int contax = StringUtils.countMatches(line, "-");
							if (contax==2){
								int i = line.lastIndexOf("-");
								String seconLine = line.substring(i-4, line.length()); 
								firstArray = seconLine.toUpperCase().trim().split("-");
								art = firstArray[0];
								desc = firstArray[1];
								note = noteSave;
								String newLine = line.substring(0, i-4);
								String[] thirdArray = newLine.toUpperCase().trim().split("\\s+");
								qta = thirdArray[thirdArray.length-1].split(",")[0];
								noteSave = "";
								count= 0;
								break;
							}

							String code = firstArray[0];
							if (!isNumeric(code)){
								code = code.replace("0,1%", "");
								code= code.replaceAll("[^0-9]", "");
								firstArray[0] = code;
							}

							String[] secondArray = firstArray[0].toUpperCase().trim().split("\\s+");
							artSave = secondArray[secondArray.length-1];
							descSave = firstArray[1];
							continue;
						} else {
							String[] thirdArray = line.toUpperCase().trim().split("\\s+");
							art = artSave;
							desc = descSave;
							note = noteSave;
							qta = thirdArray[thirdArray.length-1].split(",")[0];
							count= 0;
							noteSave = "";
						}
						break;
					case Testo.VEGA:
						switch (azienda) {
						case Testo.AZIENDA_SOLIGO:
							indexOf = line.lastIndexOf("/");
							if (indexOf!=-1){
								line = line.substring(0, indexOf); 
							}
							rowArray = line.trim().split("\\s+");
							art = rowArray[0];
							desc = rowArray[1]+ rowArray[2];
							qta = rowArray[rowArray.length-5];
							break;
						case Testo.AZIENDA_VIPITENO:
						case Testo.AZIENDA_ANTICA:
						case Testo.AZIENDA_TONIOLO:
							indexOf = line.lastIndexOf("/");
							if (indexOf!=-1){
								line = line.substring(0, indexOf); 
							}
							rowArray = line.trim().split("\\s+");
							art = rowArray[0];
							desc = rowArray[1]+ rowArray[2];
							qta = rowArray[rowArray.length-2];
							break;
						}
						break;
					case Testo.GEGEL:
						String lineA = null;
						String lineB = null;
						indexOf = line.lastIndexOf("CT");
						if (indexOf!=-1){
							lineA = line.substring(0, indexOf-2); 
							lineB = line.substring(indexOf+2, line.length());
						}
						String[] newFirstArray = lineA.trim().split("\\s+");
						String[] newSecondArry = lineB.trim().split("\\s+");
						art = newFirstArray[1];
						desc = newFirstArray[2]+ newFirstArray[3];
						qta = newSecondArry[0];
						break;
					case Testo.IPERAL:
						indexOf = line.lastIndexOf("NU");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[2]+ rowArray[3];
						qta = rowArray[rowArray.length-1];
						break;
					case Testo.RAGIS:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-3].replace(".", "");
						um = rowArray[rowArray.length-4];
						break;
					case Testo.GARBELOTTO:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						prezzo = rowArray[rowArray.length-2];
						if (line.contains("(1)")){
							prezzo = rowArray[rowArray.length-3];
							String sconto = rowArray[rowArray.length-2].replace("(1)", "") + "%";
							try {
								prezzo = getNetPrice(sconto, prezzo);									
							} catch (Exception e) {

							}		
						}
						indexOf = line.lastIndexOf("KG");
						if (indexOf!=-1){
							line = line.substring(indexOf+3, line.length());
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[1];
						break;
					case Testo.EURORISTORAZIONE:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-3];
						um = rowArray[rowArray.length-2];
						break;
					case Testo.SAN_FELICI:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[2]+ rowArray[3];
						qta = rowArray[rowArray.length-3];
						break;
					case Testo.CENTRALE_LATTE:
						count++;
						rowArray = line.trim().split("\\s+");
						if (count == 1){
							artSave = rowArray[1];
							qtaSave = rowArray[rowArray.length-6].replace(".", "");
							int unit=0;
							double total=0;
							try {
								total = Double.parseDouble(rowArray[rowArray.length-4].replace(",", "."));
								unit = Integer.parseInt(rowArray[rowArray.length-2].replace(".", ""));	
								prepSave=String.valueOf(total/unit);
							} catch (Exception e) {

							}
							continue;
						}
						art = artSave;
						qta = qtaSave;
						prezzo=prepSave;
						desc = rowArray[0] + rowArray[1];
						count=0;
						break;
					case Testo.SIDAL:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						//						art = rowArray[1];
						desc = rowArray[2]+ rowArray[3];
						indexOf = line.lastIndexOf("Collo");
						if (indexOf!=-1){
							line = line.substring(indexOf+5, line.length()); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[0];
						prezzo = rowArray[rowArray.length-2];
						break;
					case Testo.ASPIAG:
						rowArray = line.trim().split("\\s+");
						//						art = rowArray[1];
						art = rowArray[rowArray.length-1];
						desc = rowArray[2]+ rowArray[3];
						indexOf = line.lastIndexOf("IF");
						if (indexOf!=-1){
							line = line.substring(indexOf+2, line.length()); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[0].replace("/", "");
						break;
					case Testo.GARDENA:
						boolean isColli = false;
						indexOf = line.lastIndexOf("CAR");
						if (indexOf!=-1){
							isColli = true;
						}
						String line3=null;
						String line4=null;
						String lineX=null;

						if (indexOf==-1){
							indexOf = line.lastIndexOf("CT");
						}
						if (indexOf==-1){
							indexOf = line.lastIndexOf("KG");
						} 
						if (indexOf!=-1){
							line3 = line.substring(0, indexOf+10); 
							line4 = line.substring(indexOf+3, line.length());
						}

						String[] thirdArray = line3.trim().split("\\s+");
						String[] fourthArray = line4.trim().split("\\s+");

						art = thirdArray[0];
						desc = thirdArray[2]+ thirdArray[3];
						qta = fourthArray[0];
						um = thirdArray[thirdArray.length-1];

						if (isNumeric(um.replace(",", "."))) {
							um = thirdArray[thirdArray.length-2];
						}

						indexOf = line.lastIndexOf("KG");
						if (indexOf!=-1){
							lineX = line.substring(indexOf+3, line.length());
						}
						String[] eightArray = lineX.trim().split("\\s+");

						if (eightArray.length>=3) {
							prezzo = eightArray[1];
							String sconto = eightArray[2] + "%";
							try {
								prezzo = getNetPrice(sconto, prezzo);									
							} catch (Exception e) {
								prezzo = prepSave;
							}	
						} else {
							prezzo = eightArray[eightArray.length-1];
						}

						if (!isColli) {
							um2 = um;
						}

						break;
					case Testo.BRENDOLAN:
						indexOf = line.lastIndexOf("NR");
						String line5=null;
						String line6=null;
						if (indexOf!=-1){
							line5 = line.substring(0, indexOf+10); 
							line6 = line.substring(indexOf+2, line.length());
						}

						String[] fifthArray = line5.trim().split("\\s+");
						String[] sixthArray = line6.trim().split("\\s+");

						art = fifthArray[1];
						desc = fifthArray[2]+ fifthArray[3];
						qta = sixthArray[0];

						break;
					case Testo.DADO:
						indexOf = line.lastIndexOf("IF");
						if (indexOf!=-1){
							line = line.substring(0, indexOf+10); 
						}
						rowArray = line.trim().split("\\s+");
						art = rowArray[1]+rowArray[2];
						desc = rowArray[3]+ rowArray[4];
						qta = rowArray[rowArray.length-1].replace("*", "");
						break;
					case Testo.ALI:
						count++;
						if (count == 1){
							indexOf = line.lastIndexOf("GR");
							if (indexOf==-1){
								indexOf = line.lastIndexOf("ML");
							}
							String line1=null;
							String line2=null;
							if (indexOf!=-1){
								line1 = line.substring(0, indexOf+20);
								line2 = line.substring(indexOf+2, line.length());
							}

							String[] firstArray = line1.trim().split("\\s+");
							String[] secondArray = line2.trim().split("\\s+");

							artSave = firstArray[0];
							descSave = firstArray[1]+ firstArray[2];
							qtaSave = secondArray[3];
							continue;
						} else{
							art = artSave;
							desc = descSave;
							qta = qtaSave;

							//							indexOf = line.indexOf("€");
							//							if (indexOf!=-1){
							//								line = line.substring(indexOf+1, line.length());
							//							}
							indexOf = StringUtils.ordinalIndexOf(line, "€", 2);
							if (indexOf!=-1){
								line = line.substring(indexOf+1, line.length()); 
							}
							String[] lastArray = line.trim().split("\\s+");
							if (lastArray[1].contains("-")){
								prezzo = lastArray[1].split("-")[0];	
							} else {
								prezzo = lastArray[1];
							}
							count= 0;
						}

						break;
					case Testo.ALFI:
						indexOf = StringUtils.ordinalIndexOf(line, "|", 3);
						if (indexOf!=-1){
							line = line.substring(0, indexOf+10); 
						}
						rowArray = line.trim().split("\\s+");

						art = rowArray[0].replaceAll("\\|", "");
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-2];
						break;
					case Testo.LAGOGEL:
						rowArray = line.trim().split("\\s+");
						art = rowArray[2];
						desc = rowArray[4]+ rowArray[5];

						indexOf = line.lastIndexOf("IF");
						if (indexOf!=-1){
							line = line.substring(indexOf+2, line.length()); 
						}
						rowArray = line.trim().split("\\s+");

						qta = rowArray[0];
						break;
					case Testo.MADIA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[2];

						indexOf = line.indexOf("CT");
						if (indexOf!=-1){
							line = line.substring(indexOf+2, line.length()); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[0];
						break;
					case Testo.MEGAMARK:
						indexOf = line.lastIndexOf("/");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[3]+ rowArray[4];
						qta = rowArray[rowArray.length-3].replace(".", "");
						break;
					case Testo.TRENTO:
						rowArray = line.trim().split(" ");
						art = rowArray[rowArray.length-6];
						desc = rowArray[0]+ rowArray[1];
						qta = rowArray[rowArray.length-2];
						break;
					case Testo.ITALBRIX:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[3];
						qta = rowArray[rowArray.length-1];
						if (!qta.contains(",")){
							qta = rowArray[rowArray.length-2];	
						}
						break;
					case Testo.ERGON:
						rowArray = line.trim().split("\\s+");;
						art = rowArray[1];
						desc = rowArray[2];
						qta = rowArray[rowArray.length-4];
						break;
					case Testo.UNICANT:
						rowArray = line.trim().split("\\s+");;
						art = rowArray[0];
						desc = rowArray[2];
						indexOf = line.lastIndexOf("IF");
						if (indexOf!=-1){
							line = line.substring(indexOf+2, line.length()); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[1];
						break;
					case Testo.VISOTTO:
						count++;

						indexOf = line.lastIndexOf("Fornitore:");
						if (indexOf!=-1){
							line = line.substring(indexOf, line.length()); 
						}

						rowArray = line.trim().split("\\s+");

						if (count == 1){
							artSave = rowArray[0];
							descSave = rowArray[1];
							qtaSave = rowArray[rowArray.length-8];
							if (artSave.equals(Testo.SEPARATORE_4)){
								artSave = rowArray[1];
								descSave = rowArray[2];
								qtaSave = rowArray[rowArray.length-8];
							}
							prepSave = rowArray[rowArray.length-4];
							continue;
						}
						if (count == 2){
							art = artSave;
							desc = descSave;
							qta = qtaSave;
							prezzo = prepSave;
							um = rowArray[1];
							count=0;
						}
						break;
					case Testo.VISOTTO2:
						rowArray = line.trim().split("\\s+");
						art = rowArray[rowArray.length-2];
						desc = rowArray[0];
						qta = rowArray[rowArray.length-10];
						break;
					case Testo.TIGROS:
						indexOf = line.lastIndexOf("/");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");;
						art = rowArray[1];
						desc = rowArray[2]+rowArray[3];
						qta = rowArray[rowArray.length-2];
						break;
					case Testo.LANDO_FRE:
						rowArray = line.trim().split("\\s+");
						art = rowArray[rowArray.length-1];
						indexOf = line.lastIndexOf("IF");
						if (indexOf!=-1){
							line = line.substring(0, indexOf+12); 
						}
						rowArray = line.trim().split("\\s+");
						//						art = rowArray[1];
						desc = rowArray[2]+rowArray[3];
						qta = rowArray[rowArray.length-1];
						break;
					case Testo.SOGEGROSS:
						rowArray = line.trim().split("\\s+");
						//						art = rowArray[1];
						desc = rowArray[4]+rowArray[5];
						indexOf = line.toUpperCase().lastIndexOf("X");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						}
						rowArray = line.trim().split("\\s+");
						art = rowArray[rowArray.length-2];
						indexOf = line.lastIndexOf("IF");
						if (indexOf!=-1){
							line = line.substring(0, indexOf+12); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-1];
						break;
					case Testo.IGES:
						rowArray = line.trim().split("\\s+");
						art = rowArray[rowArray.length-2];
						desc = rowArray[0]+rowArray[1];
						indexOf = line.lastIndexOf("IF");
						if (indexOf!=-1){
							line = line.substring(0, indexOf+12); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-1];
						break;
					case Testo.NUME:
						rowArray = line.trim().split("\\s+");
						//						art = rowArray[rowArray.length-2];
						art = rowArray[1];
						desc = rowArray[0]+rowArray[1];
						indexOf = line.lastIndexOf("IF");
						if (indexOf!=-1){
							line = line.substring(0, indexOf+12); 
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-1];
						break;
					case Testo.CEDI:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];	
						indexOf = line.indexOf("CT");
						if (indexOf!=-1){
							line = line.substring(indexOf+2, line.length()); 
						}
						rowArray = line.trim().split("\\s+");
						if (!azienda.equals(Testo.AZIENDA_TONIOLO)) {
							art = rowArray[rowArray.length-1];
						}
						desc = Testo.YOGURT;
						qta = rowArray[0];
						break;
					case Testo.ARENA:
						count++;
						rowArray = line.trim().split("\\s+");
						if (count == 1){
							artSave = rowArray[0];
							descSave = rowArray[3];
							continue;
						} else {
							art = artSave;
							desc = descSave;
							qta = rowArray[0];
							count= 0;
						}

						break;
					case Testo.MORGESE:
						count++;
						rowArray = line.trim().split("\\s+");
						if (count == 1){
							artSave = rowArray[0];
							descSave = rowArray[3];
							continue;
						} else {
							art = artSave;
							desc = descSave;
							qta = rowArray[0];
							count= 0;
						}

						break;
					case Testo.TATO:
						rowArray = line.trim().split(" ");
						art = rowArray[rowArray.length-1];
						if (art.equals("")){
							art = rowArray[rowArray.length-6];
						}

						desc = rowArray[0]+ rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-55];
						if (qta.equals("")){
							qta = rowArray[rowArray.length-54];
						}
						if (qta.equals("")){
							qta = rowArray[rowArray.length-53];
						}
						if (qta.equals("")){
							qta = rowArray[rowArray.length-52];
						}
						if (qta.equals("")){
							qta = rowArray[rowArray.length-51];
						}
						if (qta.equals("")){
							qta = rowArray[rowArray.length-50];
						}
						if (qta.contains("/")){
							qta = rowArray[rowArray.length-56];
						}
						break;
					case Testo.FIORITAL:
						art = line.substring(0, 7);
						indexOf = line.indexOf("AL");
						desc = line .substring(8, indexOf+2);
						qta = line.substring(indexOf+2, line.length());
						break;
					case Testo.CARREFOUR:
						line = line.replaceAll(" 01", " ");
						rowArray = line.trim().split(" ");
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						qta = rowArray[rowArray.length-6];
						prezzo = rowArray[rowArray.length-3];
						break;
					case Testo.EUROSPIN:
					case Testo.EUROSPIN_TIRRENICA:
					case Testo.EUROSPIN_LAZIO:
						rowArray = line.trim().split("\\s+");
						switch (azienda) {
						case Testo.AZIENDA_SOLIGO:
							art = rowArray[2];
							desc = rowArray[3];
							qta = rowArray[rowArray.length-5].replace(".", "");
							//							um = rowArray[rowArray.length-4];
							break;
						case Testo.AZIENDA_ANTICA:
							indexOf = line.indexOf("IF");
							if (indexOf != -1){
								line = line.substring(0, indexOf); 
								rowArray = line.trim().split("\\s+");
							}
							art = rowArray[2];
							desc = rowArray[4];
							qta = rowArray[rowArray.length-1].replace(".", "");
							break;
						default:
							art = line.substring(0, 15).trim();
							desc = line .substring(30, 60).trim();
							qta = line.substring(60, line.length());
							break;
						}

						break;
					case Testo.RESINELLI:
						count++;
						rowArray = line.trim().split("\\s+");
						if (count == 1){
							artSave = rowArray[1];
							descSave = rowArray[3] + rowArray[4];

							indexOf = line.indexOf("€");
							if (indexOf != -1){
								line = line.substring(0, indexOf); 
								rowArray = line.trim().split("\\s+");
								prepSave = rowArray[rowArray.length-1];
							}
							continue;
						} else {
							art = artSave;
							desc = descSave;
							qta = rowArray[rowArray.length-2];
							prezzo=prepSave;
							count= 0;
						}
						break;
					case Testo.CUCINA_NOSTRANA:
						rowArray = line.trim().split("\\s+");
						count++;
						if (count == 1){
							artSave = rowArray[0];
							descSave = rowArray[1] + rowArray[2];
							qtaSave = rowArray[rowArray.length-2].replace(".", "");
							umSave = rowArray[rowArray.length-1];
							continue;
						} else {
							art = artSave;
							desc = descSave;
							qta = qtaSave;
							um = umSave;
							if (line.contains(Testo.ASTERISCO)){
								prezzo = rowArray[rowArray.length-1];
								um = umSave.replace("LT", "L2");
							}
							count= 0;
						}
						break;
					case Testo.SMA:
						art = line.substring(0, 15).trim();
						desc = line .substring(30, 60).trim();
						qta = line.substring(60, line.length());
						break;
					case Testo.CENTRALE:
						art = line.substring(0, 9).trim();
						indexOf = line.indexOf("KG");
						int next = line.indexOf("KG", indexOf+1);
						if (next != -1){
							indexOf = next;
						}
						desc = line .substring(10, indexOf+2).trim();
						qta = line.substring(indexOf+2, line.length());
						break;
					case Testo.CONAD_ACS:
						rowArray = line.trim().split("\\s+");
						desc = rowArray[0]+ rowArray[1];
						indexOf = line.indexOf("CT");
						if (indexOf!=-1){
							line = line.substring(indexOf+2, line.length());	
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[0];
						indexOf = line.indexOf("IF");
						if (indexOf!=-1){
							line = line.substring(indexOf+2, line.length());	
						}
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						break;
					case Testo.CONAD:
						switch (azienda) {
						case Testo.AZIENDA_SOLIGO:
							count++;
							rowArray = line.trim().split("\\s+");
							if (count == 1){
								artSave = rowArray[0];
								descSave = rowArray[3]+ rowArray[4];
								qtaSave = rowArray[rowArray.length-4];
								prepSave=rowArray[rowArray.length-1];
								continue;
							} else {
								art = artSave;
								desc = descSave;
								qta = qtaSave;
								um = rowArray[0];
								try {
									prezzo = getNetPrice(line, prepSave);									
								} catch (Exception e) {
									prezzo = prepSave;
								}
								count= 0;
							}
							break;
						case Testo.AZIENDA_VIPITENO:
						case Testo.AZIENDA_ANTICA:
							rowArray = line.trim().split("\\s+");;
							art = rowArray[0]+"-"+rowArray[1];
							desc = rowArray[3]+ rowArray[4];
							qta = rowArray[rowArray.length-3];
							break;
						case Testo.AZIENDA_TONIOLO:
							rowArray = line.trim().split("\\s+");;
							art = rowArray[0];
							desc = rowArray[3]+ rowArray[4];
							qta = rowArray[rowArray.length-3];
							break;
						default:
							break;
						}
						break;
					case Testo.CADORO:
						art = line.substring(0, 7).trim();
						desc = line .substring(8, 36).trim();
						qta = colli.trim();
						um =unita.trim();
						break;
					case Testo.GUARNIER:
					case Testo.UNICOMM:
						art = line.substring(6, 15).trim();
						desc = line .substring(17, 57);
						qta = line.substring(58, 67).trim();
						break;
					case Testo.OTTAVIAN:
						indexOf = line.indexOf("7GA");
						if (indexOf==-1){
							indexOf = line.indexOf("7BL");
						}
						if (indexOf==-1){
							indexOf = line.indexOf("524");
						}
						art = line .substring(indexOf, indexOf+11);
						desc = line .substring(6, 20).trim();

						String toEnd = line .substring(indexOf+11, line.length());
						rowArray = toEnd.trim().split("\\s+");
						qta = rowArray[3];
						prezzo = rowArray[4];
						um = rowArray[2];
						break;
					case Testo.GARDERE:
						art = line.substring(5, 15).trim();
						desc = line .substring(15, 50).trim();
						qta = line.substring(61, line.length());
						break;
					case Testo.APULIA:
						if (azienda.equals(Testo.AZIENDA_SOLIGO)) {
							art = line.substring(0, 4).trim();
							indexOf = line.indexOf("DOP");
							desc = line .substring(4, indexOf+3);
							qta = line.substring(indexOf+4, indexOf+9) + " " + line.substring(indexOf+9, line.length());
						}
						if (azienda.equals(Testo.AZIENDA_TONIOLO)) {
							count++;
							rowArray = line.trim().split("\\s+");
							if (count == 1){
								artSave = rowArray[0];
								descSave = rowArray[1] +  rowArray[2];
								prepSave = rowArray[rowArray.length-3];
								continue;
							} else {
								art = artSave;
								desc = descSave;
								prezzo = prepSave;
								qta = rowArray[rowArray.length-1].replaceAll("_", "");
								count= 0;
							}
						}
						break;
					case Testo.METRO:

						count++;
						rowArray = line.trim().split("\\s+");
						if (count == 1){
							if (rowArray[rowArray.length-1].contains(Testo.COMMA)){
								artSave =  rowArray[rowArray.length-4];
								descSave = rowArray[1] + rowArray[2];
								qtaSave = rowArray[rowArray.length-2];								
							} else {
								artSave =  rowArray[rowArray.length-5];
								descSave = rowArray[1] + rowArray[2];
								qtaSave = rowArray[rowArray.length-3];
							}
							continue;
						} else {
							art = artSave;
							desc = descSave;
							qta = qtaSave;
							switch (rowArray[rowArray.length-1]) {
							case Testo.KGM:
								um = Testo.KGM;
								break;
							case Testo.PCE:
								um = Testo.PCE;
								break;
							default:
								break;
							}
							count= 0;
						}

						break;
					case Testo.EMMECI:
						indexOf = line.indexOf("Imb.");
						line = line.substring(0, indexOf);
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[3] + rowArray[4];
						qta = rowArray[rowArray.length-1];
						break;
					case Testo.BENNET:
						if (azienda.equals(Testo.AZIENDA_TONIOLO)) {
							indexOf = line.indexOf("%");
							line = line.substring(0, indexOf);
						}else {
							line = line.replace("0,1", " ");
							line = line.replace("-", " ");
							indexOf = line.indexOf(",");
							line = line.substring(0, indexOf);
						}

						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						try {
							desc = rowArray[1] + rowArray[2];
						} catch (Exception e) {
							desc = rowArray[1];
						}
						qta = rowArray[rowArray.length-5];

						break;
					case Testo.COOP:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[2] + rowArray[3];

						if (rowArray[rowArray.length-1].equals(Testo.KILI)){
							qta = rowArray[rowArray.length-4];
							prezzo = rowArray[rowArray.length-3];
						} else {

							if (rowArray[rowArray.length-2].equals(Testo.P)){
								qta = rowArray[rowArray.length-4];
								prezzo = rowArray[rowArray.length-3];
							} else {
								qta = rowArray[rowArray.length-3];
								prezzo = rowArray[rowArray.length-2];
							}
						}
						break;
					case Testo.AZIENDA_AMBROSI:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[2] + rowArray[3];
						qta = rowArray[rowArray.length-7];
						prezzo = rowArray[rowArray.length-4];
						break;
					case Testo.ROMA:
						indexOf = line.lastIndexOf("KG");
						if (indexOf==-1){
							indexOf = line.lastIndexOf("PZ");
						}
						if (indexOf==-1){
							indexOf = line.lastIndexOf("NR");
						}
						if (indexOf==-1){
							indexOf = line.lastIndexOf("LT");
						}
						//						if (indexOf==-1){
						//							indexOf = line.lastIndexOf("CT");
						//						}
						String lineEnd = line.substring(indexOf+2, line.length());
						line = line.substring(0, indexOf+2);
						String [] rowArrayB = line.trim().split(Pattern.quote("│"));
						String [] rowArrayC = lineEnd.trim().split(Pattern.quote("│"));
						rowArray = line.replaceAll("\\W", " ").split("\\s+");
						art = rowArray[3];
						desc = rowArray[4] + rowArray[5];
						qta = rowArray[1];
						prezzo = rowArrayB[rowArrayB.length-2];
						um = rowArrayB[rowArrayB.length-1];

						try {
							String discount = rowArrayC[rowArrayC.length-2];
							if (discount.trim().length()>0 && !discount.trim().equals("0")) {
								if (!discount.trim().contains(",")) {
									discount = discount+ ",0" + "%";
								}
								prezzo = getNetPrice(discount, prezzo);	
							}

						} catch (Exception e) {
							logger.error(TAG, e);
						}

						break;
					case Testo.ROWCLIFFE:
						rowArray = line.trim().split("\\s+");
						art = rowArray[2];
						desc = rowArray[3] + rowArray[4];
						qta = rowArray[rowArray.length-3];
						prezzo = rowArray[rowArray.length-2].replace("€","");
						break;
					case Testo.COOP_VA:
						rowArray = line.trim().split("\\s+");
						if (rowArray[rowArray.length-1].contains("S") || rowArray[rowArray.length-1].contains("P")  || rowArray[rowArray.length-1].contains("I")) {
							rowArray = ArrayUtils.removeElement(rowArray, rowArray[rowArray.length-1]);
						}
						art = rowArray[0];
						if(azienda.equals(Testo.AZIENDA_VIPITENO) && art.trim().length()>5){
							art = art.replaceFirst ("^0*", "");
						}
						if(azienda.equals(Testo.AZIENDA_SOLIGO)){
							art= rowArray[1];
						}
						if(azienda.equals(Testo.AZIENDA_TONIOLO) && art.trim().length()<5){
							art = "0" + rowArray[0];;
						}
						desc = rowArray[2] + rowArray[3];
						qta = rowArray[rowArray.length-2].replace(".", "");
						break;
					case Testo.COOP_ALLEANZA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[rowArray.length-6];
						if(azienda.equals(Testo.AZIENDA_VIPITENO) && art.trim().length()>5){
							art = art.replaceFirst ("^0*", "");
						}
						desc = rowArray[0] + " " + rowArray[1];
						qta = rowArray[rowArray.length-2];
						break;
					case Testo.COOP_ALLEANZA_SIC:
						rowArray = line.trim().split("\\s+");
						art = rowArray[rowArray.length-6];
						desc = rowArray[0] + " " + rowArray[1];
						qta = rowArray[rowArray.length-2];
						break;
					case Testo.COOP_ALLEANZA_30:
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-2];
						desc = rowArray[0] + " " + rowArray[1];
						indexOf = line.indexOf("PZ");
						if (indexOf!=-1){
							line = line.substring(0, indexOf);
						}
						rowArray = line.trim().split("\\s+");
						art = rowArray[rowArray.length-2];
						break;
					case Testo.TONYS_FINE_FOOD:
						rowArray = line.trim().split("\\s+");
						art = rowArray[4];
						desc = rowArray[6] + rowArray[7] + rowArray[8]  + rowArray[9];;
						qta = rowArray[5];
						int l = rowArray.length;
						prezzo = rowArray[l-2];
						break;
					case Testo.HEIDERBECK:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[2] + " " + rowArray[3];
						//						indexOf = line.indexOf("cartone");
						//						if (indexOf!=-1){
						//							line = line.substring(0, indexOf);
						//						} else {
						//							indexOf = line.indexOf("acquisto");
						//							if (indexOf!=-1){
						//								line = line.substring(0, indexOf);
						//							}
						//						}
						//						rowArray = line.trim().split("\\s+");
						//						qta = rowArray[rowArray.length-1];

						qta = rowArray[rowArray.length-3];	
						//						if (azienda.equals(Testo.AZIENDA_VIPITENO)){
						//							qta = rowArray[rowArray.length-3];	
						//						}

						break;
					case Testo.GASTRO:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[2] + " " + rowArray[3];
						indexOf = line.lastIndexOf("NR");
						if (indexOf==-1){
							indexOf = line.lastIndexOf("KG");	
						}
						if (indexOf!=-1){
							line = line.substring(0, indexOf);
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-2];
						break;
					case Testo.QUESOS:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[3] + " " + rowArray[4];
						len = rowArray.length;
						int index = rowArray[len-1].indexOf(",");
						if(index != -1) {
							qta = rowArray[len-3];
						} else {
							qta = rowArray[len-2];
						}
						break;
					case Testo.EMMI:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1] + " " + rowArray[2];
						len = rowArray.length;
						qta = rowArray[len-2];
						break;
					case Testo.GEIA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1].replaceAll("\\D+","");
						desc = rowArray[2] + " " + rowArray[3];
						len = rowArray.length;
						//						qta = rowArray[len-3].replace(".", "");
						qta = rowArray[len-5].replace(".", "");
						qta = qta.replaceAll("\\u00A0", "");
						break;
					case Testo.CHASAL:
						String thirdLine = null;
						String rowArray3[]  = null;
						indexOf = line.toUpperCase().indexOf("€");
						if (indexOf!=-1){
							thirdLine = line.substring(indexOf, line.length());
							line = line .substring(0, indexOf);	
						}
						if (thirdLine !=null){
							rowArray3 = thirdLine.trim().split("\\s+");	
						}

						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1] + rowArray[2];
						qta = rowArray[rowArray.length-2];
						prezzo = rowArray3[1];	
						break;

					case Testo.CIRCLEVIEW:
						String secondLine = null;
						String rowArray1[]  = null;
						indexOf = line.toUpperCase().indexOf("€");
						if (indexOf!=-1){
							secondLine = line.substring(indexOf, line.length());
							line = line .substring(0, indexOf);	
						}
						if (secondLine !=null){
							rowArray1 = secondLine.trim().split("\\s+");	
						}

						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1] + rowArray[2];
						qta = rowArray[rowArray.length-2];
						prezzo = rowArray1[1];	
						break;
					case Testo.RIMI:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[2] + rowArray[3];
						qta = rowArray[rowArray.length-4];
						prezzo = rowArray[rowArray.length-2];
						break;
					case Testo.EURO:
						rowArray = line.trim().split("\\s+");
						art = rowArray[rowArray.length-1];
						desc = rowArray[rowArray.length-2];
						qta = rowArray[1];
						break;
					case Testo.NADIA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[2] + rowArray[3];
						qta = rowArray[rowArray.length-4];
						String qta1 = rowArray[rowArray.length-4];
						String qtaNew = rowArray[rowArray.length-3];
						if (qtaNew.trim().equals("1")){
							um = "KG";	
						} else {
							um = "CT";
						}
						break;
					case Testo.DA_VINCI:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						if (art.trim().contains("(")){
							art = StringUtils.replaceEach(art, new String[]{"(", ")", "AMB"}, new String[]{"", "", ""});
							desc = rowArray[0];
							qta = rowArray[2];
							um = rowArray[3];
						} else {
							desc = rowArray[1];
							qta = rowArray[3];
							um = rowArray[4];	
						}
						break;
					case Testo.CARNIATO:
						rowArray = line.trim().split("!");
						art = rowArray[1].trim();
						desc = rowArray[2].trim();
						qta = rowArray[4].trim();
						break;
					case Testo.SEVEN:
						indexOf = line.toUpperCase().indexOf("IF");
						if (indexOf!=-1){
							secondLine = line.substring(indexOf, line.length());
							line = line .substring(0, indexOf+10);	
						}

						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						//						if(azienda.equals(Testo.AZIENDA_VIPITENO) && art.trim().length()>5){
						//							art = art.replaceFirst ("^0*", "");
						//						}
						desc = rowArray[3] + rowArray[4];
						qta = rowArray[rowArray.length-1];
						break;	
					case Testo.BORG:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[2] + rowArray[3];
						qta = rowArray[rowArray.length-3];
						break;

					case Testo.MARTINELLI:

						line = line.replace("0,1%", "");
						count++;
						if (count == 1){
							indexOf = line.indexOf(",");
							if (indexOf!=-1){
								line = line.substring(0, indexOf);
							}
							rowArray = line.trim().split("\\s+");
							qtaSave = rowArray[rowArray.length-3];
							descSave = rowArray[0] +  rowArray[1];
							continue;
						} else {
							rowArray = line.trim().split("\\s+");
							qta = qtaSave;
							desc = descSave;
							art =  rowArray[0];
							count= 0;
						}
						break;

					case Testo.UNICOOP_TIRRENO:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						if(azienda.equals(Testo.AZIENDA_VIPITENO)){
							if (art.trim().length()>5){
								art = art.replaceFirst ("^0*", "");
							}
						}
						desc = rowArray[4] + " " + rowArray[5];
						qta = rowArray[rowArray.length-3];
						prezzo = rowArray[rowArray.length-1];
						break;

					case Testo.UNICOOP:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1] + " " + rowArray[2];
						qta = rowArray[rowArray.length-3].replace(".", "");
						break;

					case Testo.DAO:
						String [] rowArray2= line.trim().split("->");
						rowArray = rowArray2[0].trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1] + " " + rowArray[2];
						qta = rowArray[rowArray.length-5];
						break;
					case Testo.PAC:
						rowArray = line.trim().split("\\s+");
						art = rowArray[rowArray.length-1];
						if(azienda.equals(Testo.AZIENDA_VIPITENO) && art.trim().length()>5){
							art = art.replaceFirst ("^0*", "");
						}
						if (azienda.equals(Testo.AZIENDA_TONIOLO) && !art.substring(0, 1).equals("0") && art.trim().length()<5) {
							art = "0" + art;	
						}
						desc = rowArray[0] + " " + rowArray[1];
						indexOf = line.indexOf("IF");
						if (indexOf!=-1){
							line = line.substring(indexOf+2, line.length());
						}
						rowArray = line.trim().split("\\s+");
						qta = rowArray[0] ;
						if (!isNumeric(qta)){
							qta = "";
						}
						break;
					case Testo.CONSORZIO_NORD_OVEST:
						rowArray = line.trim().split("\\s+");
						if (rowArray[rowArray.length-2].contains("S") || rowArray[rowArray.length-2].contains("P")  || rowArray[rowArray.length-2].contains("I")) {
							rowArray = ArrayUtils.removeElement(rowArray, rowArray[rowArray.length-2]);
						}
						art = rowArray[1];
						desc = rowArray[2];
						qta = rowArray[rowArray.length-4];
						if(azienda.equals(Testo.AZIENDA_SOLIGO)){
							qta = rowArray[rowArray.length-5].replace(".", "");	
							if (!isNumeric(qta)) {
								qta = rowArray[rowArray.length-4].replace(".", "");
							}
						}
						prezzo = rowArray[rowArray.length-1].replace("_", "");
						if (!isNumeric(prezzo.replace(".", "").replace(",",""))) {
							prezzo = rowArray[rowArray.length-2].replace("_", "");
						}
						break;

					case Testo.BRUNELLO:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1] + rowArray[2];
						prezzo = rowArray[rowArray.length-4];
						qta =  rowArray[rowArray.length-5];
						um = rowArray[rowArray.length-6];
						lotto = rowArray[rowArray.length-7];
						break;

					default:
						art = line.substring(0, 10);
						desc = line .substring(10, 30);
						qta = line.substring(40, line.length());
						break;
					}

					row = new Riga();
					row.setCliente(cliente);
					row.setDeposito(order.getSpedizione().toUpperCase());
					row.setNumero(order.getOrdine());
					row.setData(order.getData());
					row.setConsegna(order.getConsegna());
					row.setArticolo(art);
					row.setDescrizione(desc);

					switch (cliente) {
					case Testo.CADORO:
						row.setQuantita(qta);
						row.setPrezzo(prezzo);
						row.setUm(um);
						break;
					case Testo.EMMECI:
					case Testo.ITALBRIX:
						//					case Testo.VISOTTO:
					case Testo.VISOTTO2:
					case Testo.CEDI:
					case Testo.ROSSETTO:
					case Testo.COAL:
					case Testo.SIAF:
					case Testo.VEGA:
					case Testo.TIGROS:
					case Testo.LANDO_FRE:
					case Testo.SOGEGROSS:
					case Testo.IGES:
					case Testo.NUME:
					case Testo.ASPIAG:
					case Testo.LAGOGEL:
					case Testo.MADIA:
					case Testo.MEGAMARK:
					case Testo.ERGON:
					case Testo.UNICANT:
					case Testo.ARENA:
					case Testo.MORGESE:
					case Testo.TRENTO:
					case Testo.TATO:
					case Testo.GUARNIER:
					case Testo.UNICOMM:
					case Testo.CONAD_ACS:
					case Testo.HEIDERBECK:
					case Testo.QUESOS:
					case Testo.EMMI:
					case Testo.COOP_VA:
					case Testo.COOP_ALLEANZA:
					case Testo.COOP_ALLEANZA_SIC:
					case Testo.COOP_ALLEANZA_30:
					case Testo.GEIA:
					case Testo.CHASAL:
					case Testo.CIRCLEVIEW:
					case Testo.RIMI:
					case Testo.EURO:
					case Testo.CARNIATO:
					case Testo.SEVEN:
					case Testo.UNICOOP:
					case Testo.UNICOOP_TIRRENO:
					case Testo.DAO:
					case Testo.CONSORZIO_NORD_OVEST:
					case Testo.PAC:
					case Testo.BORG:
					case Testo.AZIENDA_AMBROSI:
					case Testo.ROWCLIFFE:
					case Testo.BENNET:
					case Testo.OPRAMOLLA:
					case Testo.GEMOS:
					case Testo.CAFORM:
					case Testo.BERETTA:
					case Testo.DECARNE:
					case Testo.GMF:
					case Testo.GARDINI:
					case Testo.PAOLINI:
					case Testo.SELECTA:
					case Testo.SICONAD:
					case Testo.NOSAWA:
					case Testo.SALAROMEO:
					case Testo.PRIX:
					case Testo.ALFI:
					case Testo.DADO:
					case Testo.BRENDOLAN:
					case Testo.CEDI_GROSS:
					case Testo.DCFOOD:
					case Testo.CRAI:
					case Testo.CRAI_FRE:
					case Testo.GEGEL:
					case Testo.IPERAL:
					case Testo.BRIMI:
					case Testo.GS:
					case Testo.ECOR:
					case Testo.VALFOOD:
					case Testo.TOSANO:
					case Testo.MAIORA:
					case Testo.MODERNA:
					case Testo.MOSCA:
					case Testo.UNES:
					case Testo.DIMAR:
					case Testo.MARTINELLI:
					case Testo.SUPEREMME:
					case Testo.SUPERCENTRO:
					case Testo.SISA:
					case Testo.REALCO:
					case Testo.CARAMICO:
					case Testo.MULTICEDI:
					case Testo.PAC2000:
					case Testo._2000PAC:
					case Testo.GRAMM:
						//					case Testo.CAMST:
					case Testo.CDC:
					case Testo.CEDI_SIGMA:
					case Testo.CSS:
					case Testo.GASTRO:
					case Testo.TOCAL:
					case Testo.WOERNDLE:
					case Testo.MARR:
					case Testo.TAVELLA:
					case Testo.BAUER:
					case Testo.IPERCONAD:
					case Testo.RDAVENETA:
					case Testo.SGR_IPER:
					case Testo.LEGNARO:
					case Testo.SABELLI:
					case Testo.WEIHE:
					case Testo.MAXIDI:
					case Testo.RETAILPRO:
					case Testo.CONAD_FRE:
					case Testo.VIVO:
					case Testo.DAC:
					case Testo.RIAFRE:
					case Testo.PREGIS:
					case Testo.CIRFOOD:
					case Testo.EUROSPIN:
					case Testo.EUROSPIN_TIRRENICA:
					case Testo.EUROSPIN_LAZIO:
					case Testo.RESINELLI:
					case Testo.ALI:
					case Testo.SIDAL:
					case Testo.GARBELOTTO:
					case Testo.SAN_FELICI:
					case Testo.CENTRALE_LATTE:
					case Testo.VALSANA:
					case Testo.SAIT:
						row.setQuantita(qta);
						row.setPrezzo(prezzo);
						rows.add(row);
						break;
					case Testo.BOCON:
						row.setQuantita(qta);
						row.setPrezzo(prezzo);
						if (consegna!=null) {
							row.setConsegna(consegna);
						}
						rows.add(row);
						break;
					case Testo.CARREFOUR:
					case Testo.COOP:
					case Testo.OTTAVIAN:
					case Testo.METRO:
					case Testo.RAGIS:
					case Testo.SERENISSIMA:
					case Testo.LADISA:
					case Testo.ROSSI:
					case Testo.BIOLOGICA:
					case Testo.ELIOR:
					case Testo.VIS_FOOD:
						//					case Testo.PREGIS:
					case Testo.CAMST:
					case Testo.ROMA:
					case Testo.VISOTTO:
					case Testo.CONAD:
					case Testo.CATTEL:
					case Testo.CUCINA_NOSTRANA:
					case Testo.EURORISTORAZIONE:
						row.setQuantita(qta);
						row.setPrezzo(prezzo);
						row.setUm(um);
						rows.add(row);
						break;
					case Testo.GARDENA:
						row.setQuantita(qta);
						row.setPrezzo(prezzo);
						row.setUm(um);
						row.setUm2(um2);
						rows.add(row);
						break;
					case Testo.TONYS_FINE_FOOD:
					case Testo.NADIA:
					case Testo.DA_VINCI:
						row.setQuantita(qta);
						row.setPrezzo(prezzo);
						row.setUm(um);
						rows.add(row);
						break;
					case Testo.LANDO:
					case Testo.SICLA:
						row.setQuantita(qta);
						row.setPrezzo(prezzo);
						row.setNote(note);
						rows.add(row);
						break;
					case Testo.GANASSA:
					case Testo.ISA:
					case Testo.MIGROSS:
						if (art!=null){
							row.setQuantita(qta);
							row.setPrezzo(prezzo);
							rows.add(row);	
						}

						if (art2!=null){
							row = new Riga();
							row.setCliente(cliente);
							row.setDeposito(order.getSpedizione().toUpperCase());
							row.setNumero(order.getOrdine());
							row.setData(order.getData());
							row.setConsegna(order.getConsegna());
							row.setArticolo(art2);
							row.setDescrizione(desc2);
							row.setQuantita(qta2);
							row.setPrezzo(prezzo);	
							rows.add(row);	
						}

						break;
					case Testo.BRUNELLO:
						row.setTipo(Testo.PASSIVO);
						row.setQuantita(qta);
						row.setPrezzo(prezzo);
						row.setUm(um);
						row.setLotto(lotto);
						rows.add(row);
						break;
					default:

						if (azienda.equals(Testo.AZIENDA_TONIOLO) && cliente.equals(Testo.APULIA)) {
							row.setQuantita(qta);
							row.setPrezzo(prezzo);
							rows.add(row);
							break;
						}

						String strArray[] = qta.trim().split(delimiter);
						int i = 0;
						for (String s: strArray) {
							if (!s.isEmpty()){
								i++;
								switch (i) {
								case 1:
									switch (cliente) {
									case Testo.GARDERE:
										row.setQuantita(s);;
										break;
									case Testo.APULIA:
										row.setPrezzo(s);
										break;
									default:
										break;
									}
									break;
								case 2:
									switch (cliente) {
									case Testo.APULIA:
										row.setQuantita(s);
										break;
									default:
										break;
									}

									break;
								case 3:
									switch (cliente) {
									case Testo.GARDERE:
										row.setPrezzo(s);;
										break;
									default:
										break;
									}
									break;
								case 4:
									break;
								case 5:
									break;
								case 6:
									break;
								case 7:
									break;
								case 8:

									break;
								case 9:

									break;
								default:
									break;
								}
							}
						}
						rows.add(row);
						break;
					}


				} catch (Exception e){
					System.out.println(e +  "|" + line);
				}


			}

		}

		return rows;

	}


	public String extractNumber(final String str) {                

		if(str == null || str.isEmpty()) return "";

		StringBuilder sb = new StringBuilder();
		boolean found = false;
		for(char c : str.toCharArray()){
			if(Character.isDigit(c)){
				sb.append(c);
				found = true;
			} else if(found){
				break;                
			}
		}

		return sb.toString();
	}


	public static boolean isNumeric(String str) {  
		try {   
			double d = Double.parseDouble(str);  
		}  
		catch(NumberFormatException nfe) {  
			return false;  
		}  
		return true;  
	}


	public String getNetPrice(String line, String price) {

		List<Double> discounts = new ArrayList<Double>();

		String[] splitted = line.split("%");
		if (splitted.length>0) {

			for (int i=0; i<splitted.length; i++ ) {
				String value = splitted[i].replaceAll(",", ".");
				String discount = value.replaceAll("[^0-9.]", "");
				if (toNumber(discount)>0) {
					discounts.add(toNumber(discount));
					logger.info(TAG, "Sconti: " + discount + "%");
				}
			}

		}

		String netPrice = df.format(calculate(discounts, price));
		logger.info(TAG, "Net price: " + netPrice);

		return netPrice;

	}

	public static Double calculate(List<Double> discounts,  String price) {

		Double netPrice=0.0;
		Double grossPrice = toNumber(price);
		for(Double d: discounts) {
			netPrice =  grossPrice*((100-d)/100);
			grossPrice = netPrice;
		}

		return netPrice;

	}

	public static Double toNumber(String str) { 

		Double result = 0.0;
		try {  
			result= Double.parseDouble(str.replace(",", "."));  

		} catch(NumberFormatException e){  
			result =0.0;  
		}  
		return result;
	}

}
