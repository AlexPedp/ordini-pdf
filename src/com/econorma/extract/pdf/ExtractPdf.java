package com.econorma.extract.pdf;

import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import com.econorma.data.Chiave;
import com.econorma.data.Coordinate;
import com.econorma.data.Ordine;
import com.econorma.data.Riga;
import com.econorma.logic.Parsers;
import com.econorma.main.Application;
import com.econorma.rectangle.PDFRectangle;
import com.econorma.testo.Testo;
import com.econorma.util.Logger;

public class ExtractPdf {

	private static final Logger logger = Logger.getLogger(ExtractPdf.class);
	private static final String TAG = "ExtractPDF";

	private String cliente;
	private String spedizione ="";
	private String consegna ="";
	private String ordine ="";
	private String ordineStr ="";
	private String data ="";
	private String righe ="";
	private String azienda;

	private Riga row;
	private List<Riga> rows = new ArrayList<Riga>();
	private ArrayList<String> ordini = new ArrayList<String>();
	private ArrayList<String> depositi = new ArrayList<String>();


	public void init(String azienda) {
		this.azienda = azienda;
		Application.getInstance().getDao().deleteAllCoordinates(azienda);
		PDFRectangle pdfr = new PDFRectangle(azienda);
		LinkedHashMap<Chiave, Rectangle2D> rectangles = pdfr.getRectangles();
		logger.debug(TAG, "Inizio scrittura: "+ new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
		Application.getInstance().getDao().insertAllCoordinates(azienda, rectangles);
		
//		for (Map.Entry<Chiave, Rectangle2D> entry : rectangles.entrySet()) {
//			Chiave key = entry.getKey();
//			Rectangle2D value = entry.getValue();
//
//			Coordinate c = new Coordinate();
//			c.setAzienda(azienda);
//			c.setKey(key.getKey());
//			c.setCliente(key.getCliente());
//			c.setPage(key.getPage());
//			c.setVersion(key.getVersion());
//
//			Double x = value.getX();
//			Double y = value.getY();
//			Double width = value.getWidth();
//			Double height = value.getHeight();
//
//			c.setX(x.intValue());
//			c.setY(y.intValue());
//			c.setWidth(width.intValue());
//			c.setHeight(height.intValue());
//			logger.debug(TAG, "Coordinates:" +  c.getAzienda() +"|" + c.getCliente());
//			Application.getInstance().getDao().insertCoordinates(c);
//
//		} 
		logger.debug(TAG, "Fine scrittura: "+ new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));

	}
 

	public void execute(File file, String azienda) throws Exception{

		ExtractType type = new ExtractType(file, azienda);
		cliente = type.getType();

		if (cliente == null){
			logger.error(TAG,"Ordine di cliente non valido");
			return;
		}

//		if (cliente.equals(Testo.VIVO)) {
//			ordini = getOrdini(file);
//			depositi = getDepositi(file);	
//		}


		logger.info(TAG,"Ordine del cliente: " + cliente);

		try(PDDocument doc = PDDocument.load(file)) {
			PDPageTree pages = doc.getPages();
			int count = pages.getCount();
			String allText =  new PDFTextStripper().getText(doc);

			for (int i = 0; i < count;  i++) {
				List<Coordinate> coordinates = Application.getInstance().getDao().findAllByAzienda(azienda, cliente, i);

				PDPage page = pages.get(i); 


				for(int x = 0 ; x < coordinates.size() ; x++){
					Coordinate c =  coordinates.get(x);

					if (c.getX()==0 && c.getY()==0 && c.getWidth()==0 && c.getHeight()==0){
						continue;
					}

					String valueOf1 = String.valueOf(c.getX());
					String valueOf2 = String.valueOf(c.getY());
					String valueOf3 = String.valueOf(c.getWidth());
					String valueOf4 = String.valueOf(c.getHeight());
					String[] dati = new String[2];

					switch (x) {
					case 0:
						spedizione = getText(page, c.getX(), c.getY(), c.getWidth(), c.getHeight(), c.getVersion(), azienda, cliente, Testo.SPEDIZIONE, allText);
						logger.info(TAG, valueOf1 + "," +valueOf2 + "," + valueOf3 + "," + valueOf4);
						logger.info(TAG, "SPEDIZIONE: "+ spedizione);
						spedizione = Parsers.extractString(azienda, cliente, Testo.SPEDIZIONE, spedizione);
						break;
					case 1:
						consegna = getText(page, c.getX(), c.getY(), c.getWidth(), c.getHeight(), c.getVersion(), azienda, cliente, Testo.CONSEGNA, allText);
						logger.info(TAG, valueOf1 + "," +valueOf2 + "," + valueOf3 + "," + valueOf4);
						logger.info(TAG, "DATA CONSEGNA: " + consegna);
						dati = Parsers.extract(azienda, cliente, Testo.DATA_CONSEGNA, consegna);
						consegna = dati[0];
						break;
					case 2:
						ordineStr = getText(page, c.getX(), c.getY(), c.getWidth(), c.getHeight(), c.getVersion(), azienda, cliente, Testo.ORDINE, allText);
						logger.info(TAG, valueOf1 + "," +valueOf2 + "," + valueOf3 + "," + valueOf4);
						logger.info(TAG, "ORDINE: " + ordineStr);
						dati = Parsers.extract(azienda, cliente, Testo.ORDINE, ordineStr);
						ordine = dati[0];
						data = dati[1];
						break;
					case 3:
						righe += getText(page, c.getX(), c.getY(), c.getWidth(), c.getHeight(), c.getVersion(), azienda, cliente, Testo.RIGHE, allText);
						logger.info(TAG, valueOf1 + "," +valueOf2 + "," + valueOf3 + "," + valueOf4);
						logger.info(TAG, "RIGHE: " + righe);
						break;
					default:
						break;
					}

				}

			}

			Ordine order = new Ordine();
			order.setAzienda(azienda);
			order.setCliente(cliente);
			order.setData(data);
			order.setOrdine(ordine);
			order.setSpedizione(spedizione);
			order.setConsegna(consegna);
			order.setContent(righe);

			ExtractRows extract = new ExtractRows(order);
			rows = extract.readRows(righe);

			createCSV(file, rows);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static String getText(PDPage page, int x, int y, int width, int height, String version, 
			String azienda, String cliente, String tipo, String allText) throws IOException{

		Rectangle2D region = new Rectangle2D.Double(x, y, width, height);
		String regionName = "region";
		PDFTextStripperByArea stripper;
		stripper = new PDFTextStripperByArea();

		if (version.equals(Testo.V2)){
			stripper.setSortByPosition( true );
		}

		stripper.addRegion(regionName, region);
		stripper.extractRegions(page);
		String s =  stripper.getTextForRegion("region");

		//		if (azienda.equals(Testo.AZIENDA_VIPITENO) && cliente.equals(Testo.MARR) && tipo.equals(Testo.ORDINE)){
		//			s = allText;
		//		}

		return s;

	}


	public String extractNumber(final String str) {                

		if(str == null || str.isEmpty()) return "";

		StringBuilder sb = new StringBuilder();
		boolean found = false;
		for(char c : str.toCharArray()){
			if(Character.isDigit(c)){
				sb.append(c);
				found = true;
			} else if(found){
				break;                
			}
		}

		return sb.toString();
	}


	public void createCSV(File file, List<Riga> rows) {
		
		PrintWriter pw = null;
		StringBuilder sb = null;
		
		try {
			String fname = file.getName();
			int pos = fname.lastIndexOf(".");
			if (pos > 0) {
				fname = fname.substring(0, pos);
			}

			if (cliente.trim().contains(Testo.PAC) || cliente.trim().contains(Testo._2000PAC)){
				cliente = Testo.PAC2000;	
			}
			if (cliente.trim().contains(Testo.CSS)){
				cliente = Testo.LAGOGEL;	
			}
			if (azienda.equals(Testo.AZIENDA_SOLIGO) && cliente.equals(Testo.GARDENA)){
				cliente = Testo.GARDENA_SOL;	
			}
			if (azienda.equals(Testo.AZIENDA_SOLIGO) && cliente.equals(Testo.COOP_VA)){
				cliente = Testo.COOP30_VA;	
			}
			String[] clienteNew = cliente.trim().split(" ");
			String baseName = FilenameUtils.getFullPath(file.getAbsolutePath());
			File csvFile = new File(baseName + "\\" + clienteNew[0] + "_" +UUID.randomUUID() + ".csv");

			pw = new PrintWriter(csvFile);
			sb = new StringBuilder();


			for (Riga r: rows) {
				sb.append(r.getTipo());
				sb.append("|");
				
				if (r.cliente.trim().contains(Testo.PAC) || r.cliente.trim().contains(Testo._2000PAC)){
					sb.append(Testo.PAC2000);	
				} else if (r.cliente.trim().contains(Testo.CSS)){
					sb.append(Testo.LAGOGEL);	
				} else {
					sb.append(r.getCliente().trim());	
				}
				sb.append(";");
				sb.append(r.getDeposito().trim());
				sb.append(";");
				sb.append(r.getNumero().trim());
				sb.append(";");
				sb.append(r.getData().trim());
				sb.append(";");
				sb.append(r.getConsegna().trim());
				sb.append(";");
				sb.append(r.getArticolo().trim());
				sb.append(";"); 
				sb.append(r.getDescrizione());
				sb.append(";"); 
				sb.append(r.getQuantita().trim());
				sb.append(";");
				sb.append(r.getPrezzo().trim());
				sb.append(";");
				sb.append(r.getUm());
				sb.append(";");
				
				sb.append(";");
				sb.append(";");
				sb.append(";");
				sb.append(";");
				sb.append(";");
				sb.append(";");
				sb.append(";");
				sb.append(";");
				sb.append(";");
							
				sb.append(r.getNote());
				sb.append(";");
				sb.append(file.getName());
				sb.append(";");
				sb.append(r.getUm2());
				sb.append(";");
				sb.append(r.getLotto());
				sb.append(";");
				sb.append("\n");		
			}


			pw.write(sb.toString());
			pw.close();
		} catch (Exception e) {
			pw.close();
		}

	}




	public String [] parser(String line, String regex){
		Matcher m = Pattern.compile(regex).matcher(line);
		String [] dati =  new String[3];
		if (m.find()) {
			logger.info(TAG, m.group(0));
			logger.info(TAG, m.group(1));
			logger.info(TAG, m.group(2));
			logger.info(TAG, m.group(3));

			dati[0] = m.group(1);
			dati[1] = m.group(3);
			dati[2] = m.group(2);
		}


		return dati;

	}

	public static boolean isNumeric(String str) {  
		try {   
			double d = Double.parseDouble(str);  
		}  
		catch(NumberFormatException nfe) {  
			return false;  
		}  
		return true;  
	}

//	private ArrayList<String> getOrdini(File pdfFile)  throws Exception{
//
//		PDDocument doc = PDDocument.load(pdfFile);
//		String s =  new PDFTextStripper().getText(doc);
//
//		for (String line : s.split("\n|\r")) {
//			if (line.toUpperCase().trim().contains("ORDINE NR.")){
//				int indexOf = line.toUpperCase().indexOf("NR.");
//				if (indexOf!=-1){
//					line = line.substring(indexOf, line.length()); 
//				}
//
//				String [] dati = Parsers.extract(cliente, Testo.ORDINE, line);	
//				ordini.add(dati[0]);
//			}
//		}
//
//		return ordini;
//	}
//
//	private ArrayList<String> getDepositi(File pdfFile)  throws Exception{
//
//		PDDocument doc = PDDocument.load(pdfFile);
//		String s =  new PDFTextStripper().getText(doc);
//
//		for (String line : s.split("\n|\r")) {
//			if (line.toUpperCase().trim().contains("DESTINAZIONE:")){
//				String consegna = Parsers.extractString(cliente, Testo.SPEDIZIONE, line);	
//				depositi.add(consegna);
//			}
//		}
//
//		return depositi;
//	}


}
