package com.econorma.extract.pdf;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import com.econorma.testo.Testo;

public class ExtractType {

	private File pdfFile;
	private String azienda;

	public ExtractType(File pdfFile, String azienda) {
		this.pdfFile=pdfFile;
		this.azienda=azienda;
	}

	public String getType()  throws IOException{


		String type = null;
		try(PDDocument doc = PDDocument.load(pdfFile)) {
			String s =  new PDFTextStripper().getText(doc);

			if (azienda.equalsIgnoreCase(Testo.AZIENDA_VIPITENO) || azienda.equalsIgnoreCase(Testo.AZIENDA_SOLIGO)){
				for (String line : s.split("\n|\r")) {
					if (line.toUpperCase().trim().contains(Testo.COOP_CONSORZIO_NORD_OVEST)) {
						return Testo.CONSORZIO_NORD_OVEST; 
					}
				}
			}

			if (azienda.equalsIgnoreCase(Testo.AZIENDA_AMBROSI)){
				for (String line : s.split("\n|\r")) {
					if (line.toUpperCase().trim().contains(Testo.AMBROSI_REG_IMP)) {
						return Testo.AZIENDA_AMBROSI; 
					}
				}
			}

			if (azienda.equalsIgnoreCase(Testo.AZIENDA_VIPITENO) || azienda.equalsIgnoreCase(Testo.AZIENDA_TONIOLO)){
				String line = s.replaceAll("\\n", "").replaceAll("\\r", "");
				if (line.toUpperCase().trim().contains(Testo.ITALBRIX)) {
					return Testo.ITALBRIX; 
				}
			}

			boolean isPAC2000 = false; 
			if (azienda.equalsIgnoreCase(Testo.AZIENDA_VIPITENO) || azienda.equalsIgnoreCase(Testo.AZIENDA_ANTICA)|| azienda.equalsIgnoreCase(Testo.AZIENDA_TONIOLO)){
				for (String line : s.split("\n|\r")) {
					if (line.toUpperCase().trim().contains(Testo.PAC_2000)) {
						isPAC2000 = true;
						break;
					}
				}
			}

			boolean isCoopAlleanza = false; 
			if (azienda.equalsIgnoreCase(Testo.AZIENDA_VIPITENO)){
				for (String line : s.split("\n|\r")) {
					if (line.toUpperCase().trim().contains(Testo.COOP_ALLEANZA)) {
						isCoopAlleanza = true;
					}
					if (isCoopAlleanza && line.toUpperCase().trim().contains(Testo.FORMIGINE)) {
						return Testo.COOP_ALLEANZA_30;
					}

				}
			}
		
			boolean isCoop = false; 
			if (azienda.equalsIgnoreCase(Testo.AZIENDA_VIPITENO) || azienda.equals(Testo.AZIENDA_FRESCOLAT)|| azienda.equals(Testo.AZIENDA_ANTICA) 
					|| azienda.contentEquals(Testo.AZIENDA_SOLIGO) || azienda.contentEquals(Testo.AZIENDA_TONIOLO)){
				for (String line : s.split("\n|\r")) {
					if (line.toUpperCase().trim().contains(Testo.CENTRALE_ADRIATICA) || line.toUpperCase().trim().contains(Testo.PIVA_COOP)) {
						isCoop = true;
					}
					if (line.toUpperCase().trim().contains(Testo.ADRIATICA)) {
						isCoop = true;
					}
				}
			}
 
			if (azienda.equalsIgnoreCase(Testo.AZIENDA_VIPITENO) || azienda.equals(Testo.AZIENDA_FRESCOLAT) || azienda.equals(Testo.AZIENDA_ANTICA) 
					|| azienda.equals(Testo.AZIENDA_SOLIGO) || azienda.equals(Testo.AZIENDA_TONIOLO)){
				if (isCoop){
					for (String line : s.split("\n|\r")) {
						if (line.toUpperCase().trim().contains(Testo.CONSORZIO_FOR_2) || line.toUpperCase().trim().contains(Testo.CONSORZIO_FOR_3) 
								|| line.toUpperCase().contains(Testo.CONSORZIO_FOR_4) || line.toUpperCase().contains(Testo.CONSORZIO_FOR_5)
								|| line.toUpperCase().contains(Testo.CONSORZIO_FOR_6) || line.toUpperCase().contains(Testo.CONSORZIO_FOR_7)
								|| line.toUpperCase().contains(Testo.CONSORZIO_FOR_8) || line.toUpperCase().contains(Testo.CONSORZIO_FOR_9)
								|| line.toUpperCase().contains(Testo.CONSORZIO_FOR_10)|| line.toUpperCase().contains(Testo.CONSORZIO_FOR_11)) {
							return Testo.COOP_VA;
						}
					}
				}
			}
			
			
			if (azienda.equalsIgnoreCase(Testo.AZIENDA_SOLIGO)){
				for (String line : s.split("\n|\r")) {
					if (line.toUpperCase().trim().contains(Testo.IPERCONAD) || (line.toUpperCase().trim().contains(Testo.IPER_PORTOGRUARO) || (line.toUpperCase().trim().contains(Testo.IPER_SAN_BIAGIO) || (line.toUpperCase().contains(Testo.IPERMERCATO))))) {
						return Testo.IPERCONAD;
					}
				}
			}
			
			if (azienda.equalsIgnoreCase(Testo.AZIENDA_ANTICA)){
				for (String line : s.split("\n|\r")) {
					if (line.toUpperCase().trim().contains(Testo.EUROSPIN)) {
						return Testo.EUROSPIN;
					}
				}
			}
			
			boolean isVisotto = false;
			if (azienda.equalsIgnoreCase(Testo.AZIENDA_SOLIGO)){
				for (String line : s.split("\n|\r")) {
					if (line.toUpperCase().trim().contains(Testo.VISOTTO)) {
						isVisotto = true;
					}
				}
			}
			
//			if (azienda.equalsIgnoreCase(Testo.AZIENDA_SOLIGO)){
//				if (isVisotto){
//					for (String line : s.split("\n|\r")) {
//						if (line.toUpperCase().trim().contains(Testo.UDINE)) {
//							return Testo.VISOTTO2;
//						}
//					}
//				}
//				
//			}
			
			if (azienda.equalsIgnoreCase(Testo.AZIENDA_ANTICA)){
				for (String line : s.split("\n|\r")) {
					if (line.toUpperCase().trim().contains(Testo.UNICOMM)) {
						return Testo.UNICANT;
					}
				}
			}
			
			if (azienda.equalsIgnoreCase(Testo.AZIENDA_SOLIGO)){
				for (String line : s.split("\n|\r")) {
					if (line.toUpperCase().trim().contains(Testo.EUROSPIN_LAZIO_CODE)) {
						return Testo.EUROSPIN_LAZIO;
					}
				}
			}
			
			boolean isAlleanza = false; 
			if (azienda.equalsIgnoreCase(Testo.AZIENDA_ANTICA)){
				for (String line : s.split("\n|\r")) {
					if (line.toUpperCase().trim().contains(Testo.ALLEANZA)) {
						isAlleanza = true;
					}
				}
			}
			
			if (azienda.equalsIgnoreCase(Testo.AZIENDA_ANTICA)){
				if (isAlleanza){
					for (String line : s.split("\n|\r")) {
						if (line.toUpperCase().trim().contains(Testo.ALLEANZA_CODE)) {
							return Testo.COOP_ALLEANZA_SIC;
						}
					}	
				}
			}
		 
			if (azienda.equalsIgnoreCase(Testo.AZIENDA_ANTICA)){
				for (String line : s.split("\n|\r")) {
					if (line.toUpperCase().trim().contains(Testo.PAOLINI)) {
						return Testo.PAOLINI;
					}
				}
			}
			
			if (azienda.equalsIgnoreCase(Testo.AZIENDA_ANTICA)){
				for (String line : s.split("\n|\r")) {
					if (line.toUpperCase().trim().contains(Testo.SELECTA_CODE)) {
						return Testo.SELECTA;
					}
				}
			}
			
			if (azienda.equalsIgnoreCase(Testo.AZIENDA_ANTICA)){
				for (String line : s.split("\n|\r")) {
					if (line.toUpperCase().trim().contains(Testo.GEMOS)) {
						return Testo.GEMOS;
					}
				}
			}
 
			if (azienda.equalsIgnoreCase(Testo.AZIENDA_ANTICA)){
				for (String line : s.split("\n|\r")) {
					if (line.toUpperCase().trim().contains(Testo.SICLA)) {
						return Testo.SICLA;
					}
				}
			}
			
			for (String line : s.split("\n|\r")) {
				
			 	if (line.toUpperCase().trim().contains(Testo.TATO_PARIDE)){
					return Testo.TATO;
			 	} else	if (line.toUpperCase().trim().contains(Testo.GEMOS)){
						return Testo.GEMOS;
			 	} else	if (line.toUpperCase().trim().contains(Testo.CAFORM_CODE)){
					return Testo.CAFORM;
			 	} else	if (line.toUpperCase().trim().contains(Testo.BRUNELLO)){
					return Testo.BRUNELLO;
			 	} else	if (line.toUpperCase().trim().contains(Testo.SAIT)){
					return Testo.SAIT;
			 	} else	if (line.toUpperCase().trim().contains(Testo.BERETTA_CODE)){
					return Testo.BERETTA;
			 	} else	if (line.toUpperCase().trim().contains(Testo.BOCON)){
					return Testo.BOCON;
				} else	if (line.toUpperCase().trim().contains(Testo.DECARNE_CODE)){
					return Testo.DECARNE;
			 	} else	if (line.toUpperCase().trim().contains(Testo.SIDAL_SRL)){
					return Testo.SIDAL;
			 	} else	if (line.toUpperCase().trim().contains(Testo.VALSANA)){
					return Testo.VALSANA;
			 	}  else	if (line.toUpperCase().trim().contains(Testo.GARDINI)){
					return Testo.GARDINI;
				}  else	if (line.toUpperCase().trim().contains(Testo.SALA_ROMEO_CODE)){
					return Testo.SALAROMEO;
			 	} else	if (line.toUpperCase().trim().contains(Testo.SELECTA)){
					return Testo.SELECTA;
			 	} else	if (line.toUpperCase().trim().contains(Testo.ANTICA_CASCINA_SRL)&& !isPAC2000){
					return Testo.SICONAD;
			 	} else	if (line.toUpperCase().trim().contains(Testo.SAN_FELICI)){
					return Testo.SAN_FELICI;
				} else	if (line.toUpperCase().trim().contains(Testo.EURORISTORAZIONE)){
					return Testo.EURORISTORAZIONE;
			 	} else	if (line.toUpperCase().trim().contains(Testo.CENTRALE_LATTE_ITALIA)){
					return Testo.CENTRALE_LATTE;
			 	} else	if (line.toUpperCase().trim().contains(Testo.GARBELOTTO_CODE)){
					return Testo.GARBELOTTO;
				} else	if (line.toUpperCase().trim().contains(Testo.CAMST)){
					return Testo.CAMST;
				} else	if (line.toUpperCase().trim().contains(Testo.ASPIAG)){
					return Testo.ASPIAG;
				} else	if (line.toUpperCase().trim().contains(Testo.PAOLINI)){
					return Testo.PAOLINI;
				} else	if (line.toUpperCase().trim().contains(Testo.SICLA)){
					return Testo.SICLA;
				}else	if (line.toUpperCase().trim().contains(Testo.NOSAWA)){
					return Testo.NOSAWA;
				} else	if (line.toUpperCase().trim().contains(Testo.BAUER)){
					return Testo.BAUER;
				} else	if (line.toUpperCase().trim().contains(Testo.ELIOR)){
					return Testo.ELIOR;
				} else	if (line.toUpperCase().trim().contains(Testo.IPERCONAD)){
					return Testo.IPERCONAD;
				} else	if (line.toUpperCase().trim().contains(Testo.MAXIDI) || 
								line.toUpperCase().trim().contains(Testo.BELFIORE) ||
								line.toUpperCase().contains(Testo.MAXIDI_CODE) || line.toUpperCase().contains(Testo.MAXIDI_FRE_CODE) ||
								line.toUpperCase().contains(Testo.MAXIDI_TON_CODE) && !azienda.equals(Testo.AZIENDA_SOLIGO) ||
								line.toUpperCase().contains(Testo.MAXIDI_TON_CODE_2) && !azienda.equals(Testo.AZIENDA_SOLIGO)){
					return Testo.MAXIDI;
				} else	if (line.toUpperCase().trim().contains(Testo.SABELLI)){
					return Testo.SABELLI;
				} else	if (line.toUpperCase().trim().contains(Testo.BIOLOGICA)){
					return Testo.BIOLOGICA;
				} else	if (line.toUpperCase().trim().contains(Testo.CIRFOOD)){
					return Testo.CIRFOOD;
				} else	if (line.toUpperCase().trim().contains(Testo.CRAI_COD_2)){
					return Testo.CRAI_FRE;
				} else	if (line.toUpperCase().trim().contains(Testo.SIAF_CODE)){
					return Testo.SIAF;
				} else	if (line.toUpperCase().trim().contains(Testo.RESINELLI)){
					return Testo.RESINELLI;
				} else	if (line.toUpperCase().trim().contains(Testo.GS_CODE)){
					return Testo.GS;
				} else	if (line.toUpperCase().trim().contains(Testo.VIS_FOOD_CODE)){
					return Testo.VIS_FOOD;
				} else	if (line.toUpperCase().trim().contains(Testo.GMF_CODE_NEW)){
					return Testo.GMF;
				} else	if (line.toUpperCase().trim().contains(Testo.NUME) && azienda.equals(Testo.AZIENDA_FRESCOLAT) && !line.toUpperCase().equals(Testo.NUMERO)){
					return Testo.NUME;
				} else	if (line.toUpperCase().trim().contains(Testo.PREGIS)){
					return Testo.PREGIS;
				} else	if (line.toUpperCase().trim().contains(Testo.ROSSI_CODE)){
					return Testo.ROSSI;
				} else	if (line.toUpperCase().trim().contains(Testo.RIALTO)){
					return Testo.RIAFRE;
				} else	if (line.toUpperCase().trim().contains(Testo.IGES_CODE)){
					return Testo.IGES;
				} else	if (line.toUpperCase().trim().contains(Testo.MADIA_CODE)){
					return Testo.MADIA;
				} else	if (line.toUpperCase().trim().contains(Testo.ECOR_CODE)){
					return Testo.ECOR;
				} else	if (line.toUpperCase().trim().contains(Testo.RDAVENETA)){
					return Testo.RDAVENETA;
				} else	if (line.toUpperCase().trim().contains(Testo.LEGNARO)){
					return Testo.LEGNARO;
				} else	if (line.toUpperCase().trim().contains(Testo.SGR_IPER_CODE)){
					return Testo.SGR_IPER;
				} else	if (line.toUpperCase().trim().contains(Testo.WEIHE)){
					return Testo.WEIHE;
				} else	if (line.toUpperCase().trim().contains(Testo.SERENISSIMA)){
					return Testo.SERENISSIMA;
				} else	if (line.toUpperCase().trim().contains(Testo.RETAILPRO)){
					return Testo.RETAILPRO;
				} else	if (line.toUpperCase().trim().contains(Testo.VIVO_CODE)){
					return Testo.VIVO;
				} else	if (line.toUpperCase().trim().contains(Testo.TAVELLA)){
					return Testo.TAVELLA;
				} else	if (line.toUpperCase().trim().contains(Testo.DAC_CODE)){
					return Testo.DAC;
				} else	if (line.toUpperCase().trim().contains(Testo.CATTEL)){
					return Testo.CATTEL;
				} else	if (line.toUpperCase().trim().contains(Testo.WOERNDLE)){
					return Testo.WOERNDLE;
				} else	if (line.toUpperCase().trim().contains(Testo.RAGIS)){
					return Testo.RAGIS;
				} else	if (line.toUpperCase().trim().contains(Testo.MARR)){
					return Testo.MARR;
				} else	if (line.toUpperCase().trim().contains(Testo.AUCHAN)){
					return Testo.AUCHAN;
				} else	if (line.toUpperCase().trim().contains(Testo.LADISA)){
					return Testo.LADISA;
				} else	if (line.toUpperCase().trim().contains(Testo.MULTICEDI)){
					return Testo.MULTICEDI;
				} else	if (line.toUpperCase().trim().contains(Testo.GASTRO)){
					return Testo.GASTRO;
				} else	if (line.toUpperCase().trim().contains(Testo.TOCAL_CODE)){
					return Testo.TOCAL;
				} else	if (isPAC2000){
					if (line.toUpperCase().trim().contains(Testo.FIANO_ROMANO) ||line.toUpperCase().trim().contains(Testo.CASERTA) || line.toUpperCase().trim().contains(Testo.CALABRIA) || line.toUpperCase().trim().contains(Testo.PERUGIA)){
						return Testo.PAC;		 
					} else if (line.toUpperCase().trim().contains(Testo.CARINI)){
						return Testo.PAC2000; 
					}else if (line.toUpperCase().trim().contains(Testo.MONTALTO) ){
						return Testo._2000PAC; 
					}
				} else	if (line.toUpperCase().trim().contains(Testo.CARAMICO)){
					return Testo.CARAMICO;
				} else	if (line.toUpperCase().trim().contains(Testo.CSS_CODE)){
					return Testo.CSS;
				} else	if (line.toUpperCase().trim().contains(Testo.CEDI_SIGMA_CODE)){
					return Testo.CEDI_SIGMA;
				} else	if (line.toUpperCase().trim().contains(Testo.SUPEREMME)){
					return Testo.SUPEREMME;
				} else	if (line.toUpperCase().trim().contains(Testo.REALCO)){
					return Testo.REALCO;
				} else	if (line.toUpperCase().trim().contains(Testo.SISA_CODE)){
					return Testo.SISA;
				} else	if (line.toUpperCase().trim().contains(Testo.SUPERCENTRO)){
					return Testo.SUPERCENTRO;
				} else	if (line.toUpperCase().trim().contains(Testo.MARTINELLI)){
					return Testo.MARTINELLI;
				} else	if (line.toUpperCase().trim().contains(Testo.GRAMM) && (!line.toUpperCase().trim().contains(Testo.PROGRAMMATA))){
					return Testo.GRAMM;
				} else	if (line.toUpperCase().trim().contains(Testo.MODERNA)){
					return Testo.MODERNA;
				} else	if (line.toUpperCase().trim().contains(Testo.MOSCA_CODE)){
					return Testo.MOSCA;
				} else	if (line.toUpperCase().trim().contains(Testo.UNES_CODE)){
					return Testo.UNES;
				} else	if (line.toUpperCase().trim().contains(Testo.DIMAR)){
					return Testo.DIMAR;
				} else	if (line.toUpperCase().trim().contains(Testo.CARREFOUR)){
					return Testo.CARREFOUR;
				} else	if (line.toUpperCase().trim().contains(Testo.PRIX_CODE)){
					return Testo.PRIX;
				} else	if (line.toUpperCase().trim().contains(Testo.ISA_CODE)){
					return Testo.ISA;
				} else	if (line.toUpperCase().trim().contains(Testo.CDC_CODE)){
					return Testo.CDC;
				} else	if (line.toUpperCase().trim().contains(Testo.MAIORA)){
					return Testo.MAIORA;
				} else	if (line.toUpperCase().trim().contains(Testo.TOSANO)){
					return Testo.TOSANO;
				} else	if (line.toUpperCase().trim().contains(Testo.MIGROSS)){
					return Testo.MIGROSS;
				} else	if (line.toUpperCase().trim().contains(Testo.VALFOOD)){
					return Testo.VALFOOD;	
				} else	if (line.toUpperCase().trim().contains(Testo.BRESSANONE)){
					return Testo.BRIMI;
				} else	if (line.toUpperCase().trim().contains(Testo.GANASSA)){
					return Testo.GANASSA;
				} else	if (line.toUpperCase().trim().contains(Testo.GEGEL)){
					return Testo.GEGEL;
				} else	if (line.toUpperCase().trim().contains(Testo.IPERAL)){
					return Testo.IPERAL;
				} else	if (line.toUpperCase().trim().contains(Testo.COAL)){
					return Testo.COAL;
				} else	if (line.toUpperCase().trim().contains(Testo.DCFOOD_COD)){
					return Testo.DCFOOD;
				} else	if (line.toUpperCase().trim().contains(Testo.CRAI_COD)){
					return Testo.CRAI;
				} else	if (line.toUpperCase().trim().contains(Testo.ROSSETTO)){
					return Testo.ROSSETTO;
				} else if (line.toUpperCase().trim().contains(Testo.OPRAMOLLA)){
					return Testo.OPRAMOLLA;
				} else if (line.toUpperCase().trim().contains(Testo.BRENDOLAN)){
					return Testo.BRENDOLAN;
				} else if (line.toUpperCase().trim().contains(Testo.DADO)){
					return Testo.DADO;
				} else if (line.toUpperCase().trim().contains(Testo.LAGOGEL)){
					return Testo.LAGOGEL;
				} else if (line.toUpperCase().trim().contains(Testo.ALFI)){
					return Testo.ALFI;
				} else if (line.toUpperCase().trim().contains(Testo.ALI_PIVA)){
					return Testo.ALI;
				} else	if (line.toUpperCase().trim().contains(Testo.MEGAMARK)){
					return Testo.MEGAMARK; 
				}  else	if (line.toUpperCase().trim().contains(Testo.ERGON)){
					return Testo.ERGON;
				} else	if (line.toUpperCase().trim().contains(Testo.VISOTTO)){
					return Testo.VISOTTO;
				} else	if (line.toUpperCase().trim().contains(Testo.TIGROS)){
					return Testo.TIGROS;
				} else	if (line.toUpperCase().trim().contains(Testo.CEDI_COD) && !line.toUpperCase().contains(Testo.CEDI_GROSS_COD) && !line.toUpperCase().contains(Testo.CEDI_FRESCHI)){
					return Testo.CEDI;
				} else	if (line.toUpperCase().trim().contains(Testo.CEDI_GROSS_COD)){
					return Testo.CEDI_GROSS;
				} else	if (line.toUpperCase().trim().contains(Testo.ARENA) && !line.toUpperCase().trim().contains(Testo.WARENANNAHME)){
					return Testo.ARENA;
				} else	if (line.toUpperCase().trim().contains(Testo.MORGESE)){
					return Testo.MORGESE;
				} else if (line.toUpperCase().trim().contains(Testo.ITALBRIX)){
					return Testo.ITALBRIX;
				} else	if (line.toUpperCase().trim().contains(Testo.VEGA)){
					return Testo.VEGA;
				} else	if (line.toUpperCase().trim().contains(Testo.GARDENA)){
					return Testo.GARDENA;
				} else if (line.toUpperCase().trim().contains(Testo.ROWCLIFFE)) {
					return Testo.ROWCLIFFE;  
				} else if (line.toUpperCase().trim().contains(Testo.ESSELUNGA)) {
					return Testo.ESSELUNGA; 
				} else if (line.toUpperCase().trim().contains(Testo.CARREFOUR)) {
					return Testo.CARREFOUR; 
				} else if (line.toUpperCase().trim().contains(Testo.CARREFOUR)) {
					return Testo.CARREFOUR; 
				} else if (line.toUpperCase().trim().contains(Testo.FIORITAL)) {
					return Testo.FIORITAL; 
				} else if (line.toUpperCase().trim().contains(Testo.EUROSPIN)) {
					return Testo.EUROSPIN;
				} else if (line.toUpperCase().trim().contains(Testo.EUROSPIN_TIRRENICA)) {
					return Testo.EUROSPIN_TIRRENICA; 
				} else if (line.toUpperCase().trim().contains(Testo.SMA)) {
					return Testo.SMA;
				} else if (line.toUpperCase().trim().contains(Testo.DAO_FOR)) {
					return Testo.DAO;  
					//			} else if (line.toUpperCase().trim().contains(Testo.COOP_CONSORZIO_NORD_OVEST) || line.toUpperCase().trim().contains(Testo.CONSORZIO_FOR_1) || line.toUpperCase().trim().contains(Testo.CONSORZIO_FOR_2)) {
				} else if (line.toUpperCase().trim().contains(Testo.COOP_CONSORZIO_NORD_OVEST) || line.toUpperCase().trim().contains(Testo.CONSORZIO_FOR_1) ) {
					return Testo.CONSORZIO_NORD_OVEST;  
				} else if (line.toUpperCase().trim().contains(Testo.ALLEANZA)) {
					return Testo.COOP_ALLEANZA;
				} else if (line.toUpperCase().trim().contains(Testo.UNICOOP_TIRRENO_CODE)) {
					return Testo.UNICOOP_TIRRENO;   
				}	else if (line.toUpperCase().trim().contains(Testo.UNICOOP) && (!line.toUpperCase().contains(Testo.UNICOOP_TIRRENO_CODE))) {
					return Testo.UNICOOP;   
				}   else if (line.toUpperCase().trim().contains(Testo.COOP_VA) && !line.toUpperCase().trim().contains(Testo.COOP_FACCHINI) && !line.toUpperCase().trim().contains(Testo.COOP_COD) && 
						!line.toUpperCase().trim().contains(Testo.COOP_VA_VIPITENO) && !line.toUpperCase().trim().contains(Testo.COOP_VIPITENO) && 
						!line.toUpperCase().trim().contains(Testo.PUNTO_CONSEGNA) && !line.trim().contains(Testo.SOCIETA_COPERATIVA) &&
						!line.toUpperCase().trim().contains(Testo.COOP_VIPITENO_2) && !line.toUpperCase().trim().contains(Testo.COOP_VIPITENO_3) &&
						!line.toUpperCase().trim().contains(Testo.COOP_VIPITENO_4) && !line.toUpperCase().trim().contains(Testo.GEGEL) &&
						!line.toUpperCase().trim().contains(Testo.COOP_VIPITENO_5) && !line.toUpperCase().trim().contains(Testo.ERGON) &&
						!line.toUpperCase().trim().contains(Testo.COOP_VIPITENO_6) && !line.toUpperCase().trim().contains(Testo.ALI) &&
						!line.toUpperCase().trim().contains(Testo.COOP_VIPITENO_7) && !line.toUpperCase().trim().contains(Testo.VALFOOD) &&
						!line.toUpperCase().trim().contains(Testo.COOP_VIPITENO_8) && !line.toUpperCase().trim().contains(Testo.TOSANO) &&
						!line.toUpperCase().trim().contains(Testo.SOC_COOP) && !line.toUpperCase().trim().contains(Testo.UNICOOP) &&
						!line.toUpperCase().trim().contains(Testo.COOP_VIPITENO_9) && !line.toUpperCase().trim().contains(Testo.UNES) &&
						!line.toUpperCase().trim().contains(Testo.COOP_VIPITENO_10) && !line.toUpperCase().trim().contains(Testo.MARTINELLI) &&
						!line.toUpperCase().trim().contains(Testo.COOP_VIPITENO_11) && !line.toUpperCase().trim().contains(Testo.MARTINELLI) &&
						!line.toUpperCase().trim().contains(Testo.COOP_VIPITENO_12) && !line.toUpperCase().trim().contains(Testo.SUPERCENTRO) &&
						!line.toUpperCase().trim().contains(Testo.COOP_VIPITENO_13) && !line.toUpperCase().trim().contains(Testo.SISA) &&
						!line.toUpperCase().trim().contains(Testo.COOP_VIPITENO_14) && !line.toUpperCase().trim().contains(Testo.CEDI) &&
						!line.toUpperCase().trim().contains(Testo.COOP_VIPITENO_14) && !line.toUpperCase().trim().contains(Testo.PAC_2000) &&
						!line.toUpperCase().trim().contains(Testo.COOP_VIPITENO_15) && !line.toUpperCase().trim().contains(Testo.CDC) &&
						!line.toUpperCase().trim().contains(Testo.COOPERATIVA) && azienda.equals(Testo.AZIENDA_SOLIGO) && 
						!line.toUpperCase().trim().contains(Testo.COOP_VA) && azienda.equals(Testo.AZIENDA_SOLIGO) &&
						!line.toUpperCase().trim().contains(Testo.SOC_COOP_1) && azienda.equals(Testo.AZIENDA_SOLIGO) && 
						!line.toUpperCase().trim().contains(Testo.COOPERA) && azienda.equals(Testo.AZIENDA_SOLIGO)) {
					return Testo.COOP_VA; 
				}   else if (line.toUpperCase().trim().contains(Testo.COOP)) {
					return Testo.COOP; 
				} else if (line.toUpperCase().trim().contains(Testo.CENTRALE_PIVA) && !azienda.equals(Testo.AZIENDA_VIPITENO)) {
					return Testo.CENTRALE; 
				} else if ((line.toUpperCase().trim().contains(Testo.CONAD_PIVA) && !azienda.equals(Testo.AZIENDA_CESENA)) ||
						line.toUpperCase().trim().contains(Testo.CONAD_GLN_FORLI) ||
						line.toUpperCase().trim().contains(Testo.CONAD_GLN_POZZUOLO) ||
						line.toUpperCase().trim().contains(Testo.CONAD_GLN_SCORZE) ||
						line.toUpperCase().trim().contains(Testo.CONAD_GLN_FANO)) {
					return Testo.CONAD;
				} else if (line.toUpperCase().trim().contains(Testo.CONAD_PIVA_3) && azienda.equals(Testo.AZIENDA_ANTICA)) {
					return Testo.CONAD_ACS;
				} else if (line.toUpperCase().trim().contains(Testo.CONAD_PIVA_2)) {
					return Testo.CONAD_FRE;
				} else if (line.toUpperCase().trim().contains(Testo.EMMECI)) {
					return Testo.EMMECI;
				} else if (line.toUpperCase().trim().contains(Testo.EFFELLE)) {
					return Testo.EMMECI;
				} else if (line.toUpperCase().trim().contains(Testo.SPAZIO_CONAD)) {
					return Testo.EMMECI;
				} else if (line.toUpperCase().trim().contains(Testo.FORLIIMPOPOLI)) {
					return Testo.EMMECI; 
				} else if (line.toUpperCase().trim().contains(Testo.CADORO)) {
					return Testo.CADORO; 
				} else if (line.toUpperCase().trim().contains(Testo.GUARNIER)) {
					return Testo.GUARNIER; 
				}  else if (line.toUpperCase().trim().contains(Testo.OTTAVIAN)) {
					return Testo.OTTAVIAN; 
				} else if (line.toUpperCase().trim().contains(Testo.GARDERE)) {
					return Testo.GARDERE; 
				} else if (line.toUpperCase().trim().contains(Testo.APULIA)) {
					return Testo.APULIA; 
				} else if (line.toUpperCase().trim().contains(Testo.UNICOMM)) {
					return Testo.UNICOMM; 
				}  else if (line.toUpperCase().trim().contains(Testo.TONYS_FINE_FOOD)) {
					return Testo.TONYS_FINE_FOOD; 
				} else if (line.toUpperCase().trim().contains(Testo.HEIDERBECK)) {
					return Testo.HEIDERBECK; 
				} else if (line.toUpperCase().trim().contains(Testo.QUESOS)) {
					return Testo.QUESOS; 
				} else if (line.toUpperCase().trim().contains(Testo.KERAVA)) {
					return Testo.EMMI; 
				} else if (line.toUpperCase().trim().contains(Testo.STOCKHOLM)) {
					return Testo.EMMI; 
				}  else if (line.toUpperCase().trim().contains(Testo.HELSINKI)) {
					return Testo.EMMI; 
				} else if (line.toUpperCase().trim().contains(Testo.GEIA)) {
					return Testo.GEIA; 
				} else if (line.toUpperCase().trim().contains(Testo.CHASAL)) {
					return Testo.CHASAL; 
				} else if (line.toUpperCase().trim().contains(Testo.RIMI_LATVIA)) {
					return Testo.RIMI; 
				} else if (line.toUpperCase().trim().contains(Testo.EURO_FRESH_FOOD)) {
					return Testo.EURO; 
				}  else if (line.toUpperCase().trim().contains(Testo.NADIA) && !line.toUpperCase().contains(Testo.REFERENTE_NADIA)) {
					return Testo.NADIA; 
				}  else if (line.toUpperCase().trim().contains(Testo.DA_VINCI_FOOD)) {
					return Testo.DA_VINCI;   
				} else if (line.toUpperCase().trim().contains(Testo.CARNIATO)) {
					return Testo.CARNIATO;   
				} else if (line.toUpperCase().trim().contains(Testo.SEVEN)) {
					return Testo.SEVEN;   
				} else if (line.toUpperCase().trim().contains(Testo.BORG_ACQUILINA)) {
					return Testo.BORG;   
				} else if (line.toUpperCase().trim().contains(Testo.CIRCLEVIEW)) {
					return Testo.CIRCLEVIEW;   
				} else if (line.toUpperCase().trim().contains(Testo.METRO)) {
					return Testo.METRO;   
				} else if (line.toUpperCase().trim().contains(Testo.LANDO) && !line.toUpperCase().trim().contains(Testo.ORLANDO)) {
					if (azienda.equals(Testo.AZIENDA_FRESCOLAT) || azienda.equals(Testo.AZIENDA_TONIOLO)){
						return Testo.LANDO_FRE;
					} else {
						return Testo.LANDO;	
					}
				} else if (line.toUpperCase().trim().contains(Testo.SALVO_APPROVAZIONE)) {
					return Testo.ROMA;  
				} else if (line.toUpperCase().trim().contains(Testo.SOGEGROSS)) {
					return Testo.SOGEGROSS; 
				} else if (line.toUpperCase().trim().contains(Testo.TRENTO_SVILUPPO)) {
					return Testo.TRENTO;  
				}  else if (line.toUpperCase().trim().contains(Testo.BENNET)) {
					return Testo.BENNET;  
				} else if (line.toUpperCase().trim().contains(Testo.GEGEL)) {
					return Testo.GEGEL;  
				} else if (line.toUpperCase().trim().contains(Testo.NOSTRANA)) {
					return Testo.CUCINA_NOSTRANA;  
				}
			}

			return type;
		}

		catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}

}
