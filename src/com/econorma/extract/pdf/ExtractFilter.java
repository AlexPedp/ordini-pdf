package com.econorma.extract.pdf;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.econorma.testo.Testo;

public class ExtractFilter {

	private String cliente;
	private String azienda;

	public ExtractFilter(String azienda, String cliente) {
		this.azienda=azienda;
		this.cliente=cliente;
	}

	public String scanLine(String line) {
 	
	 	if ((line.toUpperCase().trim().contains(Testo.TOTALE) && cliente != Testo.GARDENA  
				&& cliente != Testo.ALI  && cliente != Testo.GEGEL && cliente != Testo.PAC2000
				&& cliente != Testo._2000PAC 
				&& cliente != Testo.SUPERCENTRO && cliente != Testo.SISA)
				&& cliente != Testo.DAC
				|| (azienda.equals(Testo.AZIENDA_SOLIGO) && cliente == Testo.ALI && line.toUpperCase().trim().contains(Testo.TOTALI))
				|| (azienda.equals(Testo.AZIENDA_SOLIGO) && cliente != Testo.VEGA && cliente != Testo.ALI && line.toUpperCase().trim().contains(Testo.TOTALE)|| 
				line.trim().contains(Testo.VALORIZZAZIONE) || 
				line.trim().contains(Testo.FINE_STAMPA) ||
				(line.trim().contains(Testo.T_PAG) && cliente != Testo.CONAD))) {
			return "break";

		}

		if (cliente == Testo.BENNET && line.trim().contains(Testo.TOTALE_BENNET)){ 
			return "break";
		}

		if (cliente == Testo.TIGROS) {

			if (line.toUpperCase().contains(Testo.MENO_PERCENTO)){
				return "continue";
			}

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}
		
		if (cliente == Testo.BRUNELLO) {

		 	if (line.trim().length() < 30){					
				return "continue";
			}

		 	if (line.toUpperCase().contains(Testo.CONDIZIONI_GENERALI)){
				return "break";
			}

		}
		
		
		if (cliente == Testo.DECARNE) {

		 	if (line.trim().length() < 30){					
				return "continue";
			}

		 	if (line.toUpperCase().contains(Testo.NOTE)){
				return "break";
			}

		}
		
		if (cliente == Testo.RIAFRE) {

		 	if (line.trim().length() < 30){					
				return "continue";
			}

		 	if (line.toUpperCase().contains(Testo.FINE_ORDINE)){
				return "break";
			}

		}
		
		if (cliente == Testo.GEMOS) {

		 	if (line.trim().length() < 30){					
				return "continue";
			}

		 	if (line.toUpperCase().contains(Testo.GEMOS)){
				return "break";
			}

		}
		
		if (cliente == Testo.SAIT) {

		 	if (line.trim().length() < 30){					
				return "continue";
			}

		 	if (line.toUpperCase().contains(Testo.FINE_ORDINE)){
				return "break";
			}

		}
		
		if (cliente == Testo.BERETTA) {

		 	if (line.trim().length() < 30){					
				return "continue";
			}

		 	if (line.toUpperCase().contains(Testo.ORARIO)){
				return "break";
			}

		}
		
		if (cliente == Testo.BOCON) {
 
			if (line.toUpperCase().contains(Testo.CARTON_BOX)|| line.toUpperCase().contains(Testo.PANI_SFUSI)){
				return "continue";
			}
			
		 	if (line.toUpperCase().contains(Testo.ORARIO)){
				return "break";
			}

		}
		
		if (cliente == Testo.UNICANT) {

		 	if (line.toUpperCase().contains(Testo.ART_FOR_1)){					
				return "continue";
			}

		 	if (line.toUpperCase().contains(Testo.FINE_ORDINE)){
				return "break";
			}

		}
		
		if (cliente == Testo.CAFORM) {
			
			if (line.trim().length() < 30){					
				return "continue";
			}

		 	if (line.toUpperCase().contains(Testo.CERTIFICATO_SANITARIO)){					
				return "continue";
			}

		 	if (line.toUpperCase().contains(Testo.VIA_BRIGATA_MAZZINI)){
				return "break";
			}

		}
		
		if (cliente == Testo.DIMAR) {

		 	if (line.trim().length() < 30){					
				return "continue";
			}
		 	
		 	if (line.toUpperCase().contains(Testo.CODICI_ARTICOLO)){
				return "continue";
			}

		 	if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}
		
		if (cliente == Testo.VALSANA) {

		 	if (line.trim().length() < 30){					
				return "continue";
			}

		 	if (line.toUpperCase().contains(Testo.FINE_ORDINE)){
				return "break";
			}

		}
		
		if (cliente == Testo.SALAROMEO) {

		 	if (line.trim().length() < 30){					
				return "continue";
			}

		 	if (line.toUpperCase().contains(Testo.PREGO_RISPETTARE)){
				return "break";
			}

		}
		
		if (cliente == Testo.SICONAD) {

//		 	if (line.trim().length() < 30){					
//				return "continue";
//			}

		 	if (line.toUpperCase().contains(Testo.VETTORE)){
				return "break";
			}

		}
		
		if (cliente == Testo.SIDAL) {
 
		 	if (line.trim().length() < 30 || line.toUpperCase().contains(Testo.SCONTO_PER)|| line.toUpperCase().contains(Testo.SCONTO_PROM)){					
				return "continue";
			}
 
		}
		
		if (cliente == Testo.SELECTA) {
			 
		 	if (line.trim().length() < 30 || line.toUpperCase().contains(Testo.ARTICOLO)|| line.toUpperCase().contains(Testo.ITEM)){					
				return "continue";
			}
 
		}
		
		if (cliente == Testo.EURORISTORAZIONE) {
			 
		 	if (line.trim().length() < 30 || line.toUpperCase().contains(Testo.SCONTO_PER)){					
				return "continue";
			}
		 	
		 	if (line.toUpperCase().contains(Testo.NOTE)){
				return "break";
			}
 
 
		}
		
		if (cliente == Testo.CENTRALE_LATTE) {
			 
			if (line.trim().contains(Testo.SOTTOLINEATURA_SHORT)){					
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.VALORE)){
				return "break";
			}
 
		}
		
		if (cliente == Testo.SAN_FELICI) {
			 
			if (line.toUpperCase().contains(Testo.AVVISO)){
				return "break";
			}
			
		 	if (line.trim().length() < 30 || line.toUpperCase().contains(Testo.NO_SCONTI)
		 			|| line.toUpperCase().contains(Testo.PROMOZIONE)|| line.toUpperCase().contains(Testo.GLI_ALTRI)){						
				return "continue";
			}
 
		}
		
		if (cliente == Testo.GARBELOTTO) {
			
		 	if (line.trim().length() < 30 || line.toUpperCase().contains(Testo.SCONTO_PER)){					
				return "continue";
			}
 
		}
		
		if (cliente == Testo.CUCINA_NOSTRANA) {

//		 	if (line.trim().length() < 30 && !line.contains(Testo.ASTERISCO)){					
//				return "continue";
//			}
			if (line.toUpperCase().contains(Testo.PAGINA)){
				return "break";
			}
	 
		}
		
		if (cliente == Testo.NOSAWA) {

		 	if (line.toUpperCase().contains(Testo.TOTAL)){
				return "break";
			}
		 	
			if (line.trim().length() < 30){					
				return "continue";
			}

		}
		
		if (cliente == Testo.PAOLINI) {

		 	if (line.toUpperCase().contains(Testo.CONSEGNA_2)){
				return "break";
			}
		 	
			if (line.trim().length() < 30 && !line.contains(Testo.CT) && !line.contains(Testo.KILI)){					
				return "continue";
			}

		}
		
		if (cliente == Testo.ASPIAG) {

		 	if (line.toUpperCase().contains(Testo.TOTAL)){
				return "break";
			}
		 	
			if (line.trim().length() < 30 || line.trim().contains(Testo.PERCENTO) ||  line.trim().contains(Testo.BARRA_KG) ||  line.trim().contains(Testo.BARRA_KG_2)){					
				return "continue";
			}

		}
		
		if (cliente == Testo.RESINELLI) {

		 	if (line.trim().length() < 10){					
				return "continue";
			}
		 
		 	if (!line.toUpperCase().contains(Testo.CT)){
		 		if (line.toUpperCase().contains(Testo.ATTENZIONE) || line.toUpperCase().contains(Testo.SCADENZA)  ||
						line.toUpperCase().contains(Testo.COD_ART_1) ||  line.toUpperCase().contains(Testo.COLLI)){
					return "continue";
				}	
		 	}
	
		 	if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}
		
		if (cliente == Testo.EUROSPIN || cliente == Testo.EUROSPIN_TIRRENICA || cliente == Testo.EUROSPIN_LAZIO) {

		 	if (line.trim().length() < 30){					
				return "continue";
			}
		 	
		 	if (line.toUpperCase().contains(Testo.SOTTOLINEATURA_2) || line.toUpperCase().contains(Testo.DESCRIZIONE)
		 			|| line.toUpperCase().contains(Testo.CONVENZIONE)){
				return "continue";
			}

		 	if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}
		
		if (cliente == Testo.ECOR) {

		 	if (line.trim().length() < 40){					
				return "continue";
			}
		 	
		 	if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}
		
		if (cliente == Testo.CIRFOOD) {
		 	
		 	if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		 	if (line.trim().length() < 40){					
				return "continue";
			}
		 	
		}
		
		if (cliente == Testo.BIOLOGICA) {

		 	if (line.trim().length() < 40){					
				return "continue";
			}
		 	
			
		 	if (line.toUpperCase().contains(Testo.EUROPALL) || line.toUpperCase().contains(Testo.PEDANE) || line.toUpperCase().contains(Testo.BIOLOGICO)){
				return "continue";
			}

		 	if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}
		
		if (cliente == Testo.MADIA) {

			line = line.trim().replaceAll("\\s+","");

		 	if (line.toUpperCase().contains(Testo.NOTE)){
				return "break";
			}
			
		 	if (line.trim().length() < 30){					
				return "continue";
			}

		}
		
		if (cliente == Testo.ELIOR) {
			
			line = line.trim().replaceAll("\\s+","");

		 	if (line.toUpperCase().contains(Testo.EGREGIO) || line.toUpperCase().contains(Testo.NOTE)){
				return "break";
			}
		 	
		 	String regex = "(\\d{2}-\\d{2}-\\d{4})";
		 	Matcher m = Pattern.compile(regex).matcher(line);
		 	if (m.find()) {
		 		return "continue";
		 	}
			
//			
//		 	if (line.trim().length() < 50){					
//				return "continue";
//			}
//		
		}
		
		if ( cliente == Testo.LANDO_FRE || cliente == Testo.SOGEGROSS || cliente == Testo.IGES || cliente == Testo.NUME) {
			

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}
			
			line = line.trim().replaceAll("\\s+","");

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.U1)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA) || line.toUpperCase().contains(Testo.FILLER)){
				return "continue";
			}

		}
		
		if (cliente == Testo.RETAILPRO) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.STOREC)){
				return "continue";
			}
		}
		
		if (cliente == Testo.VIS_FOOD) {

//			if (line.trim().length() < 50){					
//				return "continue";
//			}

			if (line.toUpperCase().contains(Testo.ARTICOLO)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.ANNOTAZIONI)){
				return "break";
			}
		}
		
		if (cliente == Testo.GMF) {

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}
			
			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.COSTO_BASE)){
				return "continue";
			}
		}
		
		if (cliente == Testo.DAC) {
			
			line = line.trim().replaceAll("\\s+","");
			
			if (line.toUpperCase().contains(Testo.CODICE_DAC) || line.toUpperCase().contains(Testo.DESCRIZIONE)){
				return "continue";
			}

			if (line.trim().length() < 30 && !line.toUpperCase().contains(Testo.VS_COD)){					
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA) || line.toUpperCase().contains(Testo.GIORNI) || line.toUpperCase().contains(Testo.SCONTI)){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.PAGINE) || line.toUpperCase().contains(Testo.TOT_COLLI)){
				return "break";
			}
		}
		
		if (cliente == Testo.PRIX) {
			
			line = line.trim().replaceAll("\\s+","");

			if (line.trim().length() < 30 && !line.toUpperCase().contains(Testo.VS_COD)){					
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA) || line.toUpperCase().contains(Testo.GIORNI) || line.toUpperCase().contains(Testo.SCONTI)){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALI)){
				return "break";
			}
		}
		
		if (cliente == Testo.VIVO) {
			
			line = line.trim().replaceAll("\\s+","");

			if (line.trim().length() < 30){					
				return "continue";
			}
		
			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}
		}
		
		if (cliente == Testo.GS) {

			if (line.trim().length() < 30){					
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA_2) || line.toUpperCase().contains(Testo.SOTTOLINEATURA_4)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.CONSEGNA_XLS)){
				return "break";
			}
		}
		
		if (cliente == Testo.PREGIS) {
			
			if (line.toUpperCase().contains(Testo.VALORE_NETTO)){
				return "break";
			}

			if (line.trim().length() < 30){					
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA_2) || line.toUpperCase().contains(Testo.SOTTOLINEATURA_4) || line.toUpperCase().contains(Testo.LORDO)){
				return "continue";
			}
			
		}
		
		if (cliente == Testo.ROSSI) {

			if (line.trim().length() < 30){					
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.FORNITORE)){
				return "continue";
			}

			if (line.replace(" ", "").toUpperCase().contains(Testo.INDIRIZZO)){
				return "break";
			}
		}
		
		if (cliente == Testo.LADISA) {

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.IMPOSTA)){
				return "break";
			}
		}
		
		if (cliente == Testo.CONAD_FRE) {

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.STOREC)){
				return "continue";
			}
		}
		
		if (cliente == Testo.CONAD_ACS) {

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.PERCENTO)){
				return "continue";
			}
		}

		if (cliente == Testo.WEIHE) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.BITTE_DE)){
				return "continue";
			}
		}
		
		if (cliente == Testo.MAXIDI) {

//			if (line.trim().length() < 50){					
//				return "continue";
//			}
			
//			if (line.toUpperCase().contains(Testo.PALLET)){
//				return "continue";
//			}
//			if (line.toUpperCase().contains(Testo.LEGENDA)){
//				return "continue";
//			}

			if (line.toUpperCase().contains(Testo.TOTALE_MAXIDI)){
				return "break";
			}
		}
		
		if (cliente == Testo.RAGIS) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.BITTE_DE)){
				return "continue";
			}
		}
		
		if (cliente == Testo.SERENISSIMA) {

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.STAMPATO)){
				return "break";
			}
		}
		
		if (cliente == Testo.ISA) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.FRISCHDIENST)){
				return "break";
			}
		}
		
		if (cliente == Testo.SABELLI) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.CORTESEMENTE)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.SOLLECITO)){
				return "continue";
			}
		}
		
		if (cliente == Testo.IPERCONAD || cliente == Testo.RDAVENETA || cliente == Testo.SGR_IPER || cliente == Testo.LEGNARO) {

			if (line.trim().length() < 50){					
				return "continue";
			}
			
			if (line.trim().toUpperCase().contains(Testo.RIFERIMENTO)){
				return "continue";
			}

		}
		
		if (cliente == Testo.CATTEL) {

			if (line.trim().length() < 50){					
				return "continue";
			}
			
			if (line.trim().toUpperCase().contains(Testo.IMPORTO)){
				return "break";
			}

		}
		
		if (cliente == Testo.BAUER) {

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.FRISCHDIENST)){
				return "break";
			}
		}
		
		if (cliente == Testo.TAVELLA) {

			if (line.trim().length() < 50){					
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.CODICE)){
				return "continue";
			}
 
		}
		
		if (cliente == Testo.WOERNDLE) {
			
			if (line.toUpperCase().contains(Testo.GESAMTWERT)){
				return "break";
			}

			if (line.trim().length() < 50){					
				return "continue";
			}

		}
		
		if (cliente == Testo.TOCAL) {

			if (line.trim().length() < 50){					
				return "continue";
			}

		
			
			if (line.toUpperCase().contains(Testo.PAGAMENTO)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.PROBLEMI)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.SOTTOSCRIZIONE)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.QUANTITATIVI)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.IMPORTO)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.FORNITORE)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.ASSOLUTAMENTE)){
				return "continue";
			}

		}

		if (cliente == Testo.CSS) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}

		if (cliente == Testo.CAMST) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA_2)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.VALORE_NETTO_TOTALE)){
				return "break";
			}

		}

		if (cliente == Testo.CDC) {

			if (line.toUpperCase().contains(Testo.LOGISTICA)){
				return "break";
			}

			if (line.trim().length() < 50){					
				return "continue";
			}

		}

		if (cliente == Testo.CEDI_SIGMA) {

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

			//			if (line.trim().length() < 20){					
			//				return "continue";
			//			}

			if (line.toUpperCase().contains(Testo.PROMO)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.LINEA)){
				return "continue";
			}

		}

		if (cliente == Testo.GRAMM) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA_2)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}

		if (cliente == Testo.REALCO) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.ATTENZIONE)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}

		if (cliente == Testo.CARAMICO) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.ATTENZIONE)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}

		if (cliente == Testo.SISA) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.PRZ_ORDINE) || line.toUpperCase().contains(Testo.ZONA)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.ANNOTAZIONI) || line.toUpperCase().contains(Testo.PEC)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.DISTRIBUZIONE) || line.toUpperCase().contains(Testo.CAP_SOC)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE) || line.toUpperCase().contains(Testo.TASSATIVAMENTE) || (line.toUpperCase().contains(Testo.RIEPILOGO) || (line.toUpperCase().contains(Testo.RIEPILOGATIVO)))){
				return "break";
			}

		}

		if (cliente == Testo.SUPERCENTRO) {

			if (line.trim().length() < 20){					
				return "continue";
			}


			if (line.toUpperCase().contains(Testo.PALLET)){
				return "continue";
			}


			if (line.toUpperCase().contains(Testo.TOTALI)){
				return "break";
			}

		}

		if (cliente == Testo.SUPEREMME) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}

		if (cliente == Testo.UNES) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}

		if (cliente == Testo.MODERNA) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE) || line.toUpperCase().contains(Testo.GENERALE)){
				return "break";
			}

		}

		if (cliente == Testo.MOSCA) {

			if (line.trim().length() < 20){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.BOLLICINE) || (line.toUpperCase().contains(Testo.SECCHIELLO))){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.MITTENTE)){
				return "break";
			}



		}

		if (cliente == Testo.MAIORA) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SCONTO_CANALE)||line.toUpperCase().contains(Testo.SCONTO_CANVAS)){			
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}

		if (cliente == Testo.TOSANO) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}

		if (cliente == Testo.VALFOOD) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SPARK)){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}

		if (cliente == Testo.MIGROSS) {

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.PERCENTO) && line.toUpperCase().contains(Testo.PZ)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}

		if (cliente == Testo.BRIMI) {

			//			if (line.trim().length() < 50){					
			//				return "continue";
			//			}

			if (line.toUpperCase().contains(Testo.DIE_PRODUKTE)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.BRIXEN)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.VAHRN)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.CODICE_FISCALE)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.BRIMI_SITE)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.GENOSSE)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.BITTE)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.POS_UNS)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.L_TERMIN)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.ZAH)){
				return "brak";
			}

		}

		if (cliente == Testo.IPERAL) {

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA)){
				return "continue";
			}

		}	

		if (cliente == Testo.MULTICEDI) {

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA)){
				return "continue";
			}

		}	

		if (cliente == Testo.PAC2000) {

			if (line.toUpperCase().contains(Testo.TOTALE_GENERALE) || line.toUpperCase().contains(Testo.BONIFICO)){
				return "break";
			}

			if (line.trim().length() < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA) || line.toUpperCase().contains(Testo.SOTTOLINEATURA_4) || line.toUpperCase().contains(Testo.PZ)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.COD_ART) || line.toUpperCase().contains(Testo.SCONTI) || line.toUpperCase().contains(Testo.FOR)){
				return "continue";
			}
	
		}
		
		if (cliente == Testo._2000PAC) {
			
			if (line.toUpperCase().contains(Testo.BONIFICO_2)){
				return "break";
			}
			
			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA) || line.toUpperCase().contains(Testo.SOTTOLINEATURA_4) || line.toUpperCase().contains(Testo.PZ)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.PERCENTUALE_2)){
				return "continue";
			}

		}

		if (cliente == Testo.GEGEL) {

			if (line.toUpperCase().contains(Testo.NOTE_FIRMA)){
				return "continue";
			}

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.CONFERMA)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SCARICO)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.PRODOTTI_SURGELATI)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TIPO_SPEDIZIONE)){
				return "continue";
			}

		}

		if (cliente == Testo.GANASSA) {



		}	

		if (cliente == Testo.COAL) {

			if (line.toUpperCase().contains(Testo.MENO_PERCENTO)){
				return "continue";
			}

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA_3)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.PALLETS) || line.toUpperCase().contains(Testo.PIATTAFORMA)){
				return "break";
			}

		}

		if (cliente == Testo.VEGA) {

			line = line.trim().replaceAll("\\s+","");
 
			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.MENO_PERCENTO)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA_2)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.SCONTO_PUNTI) || line.toUpperCase().contains(Testo.SIGMA) || line.toUpperCase().contains(Testo.COVID)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.SCONTI) || line.toUpperCase().contains(Testo.MANCANZA) || line.toUpperCase().contains(Testo.QUADRO)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.RICHIAMANDONE) || line.toUpperCase().contains(Testo.CONVERTITO) || line.toUpperCase().contains(Testo.PROROGHE) || line.toUpperCase().contains(Testo.DIVIETO)){
				return "continue";
			}
			
			if (azienda.equals(Testo.AZIENDA_SOLIGO) && line.toUpperCase().contains(Testo.TOTALE)){
				return "continue";
			}

			if (azienda.equals(Testo.AZIENDA_VIPITENO) && line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}

		if (cliente == Testo.ROSSETTO) {

			line = line.trim().replaceAll("\\s+","");

			if (line.trim().length() < 30 && !line.contains(Testo.COSTO_NETTO)){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA_2)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.LEGENDA)){
				return "break";
			}
		}

		if (cliente == Testo.CEDI_GROSS) {

			line = line.trim().replaceAll("\\s+","");

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOT_CARTONI)){
				return "break";
			}

			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA_2)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.LEGENDA)){
				return "break";
			}
		}

		if (cliente == Testo.CRAI) {

			line = line.trim().replaceAll("\\s+","");

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.FORN_ORD) && line.toUpperCase().contains(Testo.NOTE)){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}


		}

		if (cliente == Testo.CRAI_FRE) {

			if (line.toUpperCase().contains(Testo.ASSOLVE)){
				return "break";
			}
			
		}
		
		if (cliente == Testo.SIAF) {


			if (line.toUpperCase().contains(Testo.IMPORTANT)){
				return "break";
			}
			
		}
		
		if (cliente == Testo.DCFOOD) {

			line = line.trim().replaceAll("\\s+","");

			if (line.trim().length() < 30){					
				return "continue";
			}
		}

		if (cliente == Testo.OPRAMOLLA) {

			line = line.trim().replaceAll("\\s+","");

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.CODICE)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.LEGGE)){
				return "continue";
			}
		}

		if (cliente == Testo.GARDENA) {

			line = line.trim().replaceAll("\\s+","");

			if (line.trim().length() < 30){					
				return "continue";
			}


			if (line.toUpperCase().contains(Testo.PERCENTO) && line.toUpperCase().contains(Testo.MERCE)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.EUR) || line.toUpperCase().contains(Testo.CODICE_FORNITORE)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.ANNOTAZIONI)){
				return "continue";
			}


		}

		if (cliente == Testo.MEGAMARK) {

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.COLLI)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}

		if (cliente == Testo.LAGOGEL) {

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.trim().contains(Testo.PERCENTO) && line.trim().contains(Testo.CNP)){				
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.PAG_2)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.LEGENDA)){
				return "break";
			}

		}

		if (cliente == Testo.ALFI) {
			
			line = line.trim().replaceAll("\\s+","");

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.trim().contains(Testo.PERCENTO)){				
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.PAG_2)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}

		if (cliente == Testo.ALI) {

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.trim().contains(Testo.PEZZO_ORD)){				
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.PAG_2)){
				return "continue";
			}

//			if (line.toUpperCase().contains(Testo.TOTALE)){
//				return "continue";
//			}

			if (line.toUpperCase().contains(Testo.ARTICOLI)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.OMAG)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE_SMA)){
				return "break";
			}

		}

		if (cliente == Testo.DADO) {

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.PEZZO_ORD)){				
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SCALA_SCONTI)){				
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.PERCENTO) && line.toUpperCase().contains(Testo.LA2)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SEPARATORE_3)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.ARTICOLI)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

			if (line.toUpperCase().contains(Testo.TOTALE_SMA)){
				return "break";
			}

		}

		if (cliente == Testo.CEDI) {

			if (line.toUpperCase().contains(Testo.PERCENTO) && line.trim().contains(Testo.MENO)){
				return "continue";
			}

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

		}

		if (cliente == Testo.VISOTTO || cliente == Testo.VISOTTO2) {

			line = line.trim().replaceAll("\\s+","");
			
			if (!line.toUpperCase().contains(Testo.EAN) && line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TRATTINO) || (line.toUpperCase().contains(Testo.ARTICOLO))){
				return "continue";
			}

			if (!line.toUpperCase().contains(Testo.EAN) && line.toUpperCase().contains(Testo.PERCENTO) && line.toUpperCase().contains(Testo.MENO)){
				return "continue";
			}
			
//			if (line.toUpperCase().contains(Testo.EAN) || line.toUpperCase().contains(Testo.VATBARBARA) || line.toUpperCase().contains(Testo.ORDINE)){
//				return "continue";
//			}
			
			if (line.toUpperCase().contains(Testo.VATBARBARA) || line.toUpperCase().contains(Testo.ORDINE) || line.toUpperCase().contains(Testo.NOTE)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.SOTTOLINEATURA_4) || line.toUpperCase().contains(Testo.INFORMAZIONI) || line.toUpperCase().contains(Testo.NOME_PGM)){
				return "continue";
			}

		}

		if (cliente == Testo.ERGON) {

			if (line.trim().length() < 30){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.CANALE)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TERMINE)){
				return "continue";
			}
			if (line.toUpperCase().contains(Testo.ACCETTAZIONE)){
				return "continue";
			}
			if (line.toUpperCase().contains(Testo.ACCETTANO)){
				return "continue";
			}
			if (line.toUpperCase().contains(Testo.PREGA)){
				return "continue";
			}
			if (line.toUpperCase().contains(Testo.BOLLA)){
				return "continue";
			}
			if (line.toUpperCase().contains(Testo.OMAGGIO)){
				return "continue";
			}
		}

		if (cliente == Testo.ITALBRIX) {

			if (line.toUpperCase().contains(Testo.PEZZI) || line.toUpperCase().contains(Testo.TRATTINO) || line.toUpperCase().contains(Testo.BARRA_KILI)){
				return "continue";
			}
		}

		if (cliente == Testo.ARENA) {

			if (line.toUpperCase().contains(Testo.MITTENTE) || line.toUpperCase().contains(Testo.CATANIA) || line.toUpperCase().contains(Testo.CAP)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.CODICE) || line.toUpperCase().contains(Testo.CF) || line.toUpperCase().contains(Testo.ORDINI)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SOGGETTA) || line.toUpperCase().contains(Testo.NUMERO) || line.toUpperCase().contains(Testo.FRATELLI)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.RIPORTARE) || line.toUpperCase().contains(Testo.LUOGO) || line.toUpperCase().contains(Testo.TERMINI) || line.toUpperCase().contains(Testo.NOTE)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TRATTINO) || line.toUpperCase().contains(Testo.CODICE_PRODOTTO) || line.toUpperCase().contains(Testo.IMBALLO)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.PREZZO_LISTINO) || line.toUpperCase().contains(Testo.COSTO_COMMERCIALE) || line.toUpperCase().contains(Testo.ANNOTAZIONI)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.COSTO_LISTINO) || line.toUpperCase().contains(Testo.ASSOLVE)  || line.toUpperCase().contains(Testo.COSTO_FATTURA)){
				return "continue";
			}

			if (line.trim().length() < 20){					
				return "continue";
			}

		}

		if (cliente == Testo.MORGESE) {

			if (line.toUpperCase().contains(Testo.TRATTINO)){
				return "break";
			}

			if (line.toUpperCase().contains(Testo.PREZZO_LISTINO) || line.toUpperCase().contains(Testo.COSTO_COMMERCIALE) || line.toUpperCase().contains(Testo.ANNOTAZIONI)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.COSTO_LISTINO) || line.toUpperCase().contains(Testo.ASSOLVE)  || line.toUpperCase().contains(Testo.COSTO_FATTURA)){
				return "continue";
			}

			if (line.trim().length() < 20){					
				return "continue";
			}

		}

		if (cliente == Testo.BRENDOLAN) {

			if (line.toUpperCase().contains(Testo.TOTALI)){
				return "break";
			}

			if (line.toUpperCase().contains(Testo.EAN) || line.toUpperCase().contains(Testo.PERCENTO)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.VS_ARTICOLO) || line.toUpperCase().contains(Testo.DESCRIZIONE)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.PRZ_LORDO)){
				return "continue";
			}

			if (line.trim().length() < 20){					
				return "continue";
			}

		}

		if (cliente == Testo.CADORO) {

			if (line.toUpperCase().contains(Testo.SCONTO)){
				return "continue";
			}
		}

		if (cliente == Testo.CONAD) {
			
			if (azienda.equals(Testo.AZIENDA_SOLIGO)) {
				if (!line.contains(Testo.PERCENTO) && line.trim().length() < 20){					
					return "continue";
				}	
			} else {
				if (line.trim().length() < 20){					
					return "continue";
				}
			}
			
			if (line.toUpperCase().contains(Testo.DATA_RIC_FATTURA) || line.contains(Testo.T_PAG)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.COMMISSIONE) || (!azienda.equals(Testo.AZIENDA_SOLIGO) && line.toUpperCase().contains(Testo.PERCENTO))){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.DATA_FATTURA) || (!azienda.equals(Testo.AZIENDA_SOLIGO) && line.toUpperCase().contains(Testo.PERCENTO))){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.COD_ART_2) || line.toUpperCase().contains(Testo.DEPERIBILI)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.CENTRO) && line.toUpperCase().contains(Testo.TOT_KG)){
				return "continue";
			}


		}

		if (cliente == Testo.OTTAVIAN) {

			if (line.toUpperCase().contains(Testo.DESCRIZIONE)){
				return "continue";
			}

		}

		if (cliente == Testo.GARDERE) {

			if (line.toUpperCase().contains(Testo.DESCRIZIONE)){
				return "continue";
			}

		}

		if (cliente == Testo.APULIA) {

			if (line.toUpperCase().contains(Testo.ARTICOLO)){
				return "continue";
			}
		}


		if (cliente == Testo.CARREFOUR) {

			if (line.toUpperCase().contains(Testo.CARTON) || 
					line.toUpperCase().contains(Testo.NOTE) ||
					line.toUpperCase().contains(Testo.RESPONSABILE) ||
					line.toUpperCase().contains(Testo.CARREFOUR) ||
					line.toUpperCase().contains(Testo.PAG)){
				return "continue";
			}

		}

		if (cliente == Testo.COOP) {
			line = line.replaceAll(Testo.LINE, "");
			if (line.toUpperCase().contains(Testo.FATTURA) ||
					line.toUpperCase().contains(Testo.EXTRA)) {
				return "continue";
			}

		}

		if (cliente == Testo.TONYS_FINE_FOOD) {
			int length = line.length();
			if (line.toUpperCase().contains(Testo.LINE_STR) || length < 20){					
				return "continue";
			}

		}

		if (cliente == Testo.CHASAL || cliente == Testo.CIRCLEVIEW) {
			int length = line.length();
			if (length < 50){					
				return "continue";
			}

		}

		if (cliente == Testo.HEIDERBECK) {
		
			if (line.toUpperCase().contains(Testo.COD_ART) || line.toUpperCase().contains(Testo.FORNITORE) || line.toUpperCase().contains(Testo.PESO_NETTO)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.BANCALI) || line.toUpperCase().contains(Testo.CASO)){
				return "break";
			}
			
			if (line.toUpperCase().contains(Testo.COD_ART_DE) || line.toUpperCase().contains(Testo.LIEFERUNG) || line.toUpperCase().contains(Testo.FREITAG) || line.toUpperCase().contains(Testo.BESTELLUNG)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.LUNEDI) || line.toUpperCase().contains(Testo.VENERDI)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.BANCALI) || line.toUpperCase().contains(Testo.CASO) || line.toUpperCase().contains(Testo.SOLLTE) || line.toUpperCase().contains(Testo.TAGE_NETTO) ){
				return "break";
			}
 

		}

		if (cliente == Testo.AZIENDA_AMBROSI) {

			int length = line.length();
			if (length < 50){					
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.NUMERO)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.ARTICOLO)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}
		}

		if (cliente == Testo.COOP_VA) {

			line = line.replaceAll(Testo.SEPARATORE2,"");

			if (line.toUpperCase().contains(Testo.ORARI) || line.toUpperCase().contains(Testo.NR_ORDINE) || line.toUpperCase().contains(Testo.PRESENTAZIONE)){
				return "break";
			}

			int len =  line.trim().length();
			if (line.toUpperCase().contains(Testo.OS_N) || line.toUpperCase().contains(Testo.OS18) 
					|| line.toUpperCase().contains(Testo.SMK) || line.toUpperCase().contains(Testo.PROMO)
					|| line.toUpperCase().contains(Testo.SEPARATORE2) || line.toUpperCase().contains(Testo.LS_NORD) 
					|| line.toUpperCase().contains(Testo.DATA_CONSEGNA) || line.toUpperCase().contains(Testo.RIFERIMENTO)
					|| (line.toUpperCase().contains(Testo.OS) && len<60) || line.toUpperCase().contains(Testo.OS11)
					|| line.toUpperCase().contains(Testo.REFERENZE) || line.toUpperCase().contains(Testo.CONVENIENZA)||
					line.toUpperCase().contains(Testo.INTEGRAZIONE)
					){
				return "continue";
			}

			String newLine = line.replaceAll("\\s+","");

			if (newLine.length() < 30){
				return "continue";
			}


		}

		if (cliente == Testo.QUESOS){

			if (line.toUpperCase().contains(Testo.PERCENTO)){
				return "continue";
			}
			if (line.toUpperCase().contains(Testo.DESCRIPTION)){
				return "continue";
			}
			if (line.toUpperCase().contains(Testo.TOTAL)){
				return "break";
			}
		}

		if (cliente == Testo.GEIA){

			if (line.toUpperCase().contains(Testo.PALLET) || line.toUpperCase().contains(Testo.WEIGHT)|| line.toUpperCase().contains(Testo.IBAN) ){
				return "continue";
			}
			if (line.toUpperCase().contains(Testo.IMPORTANT) || line.toUpperCase().contains(Testo.ONLY_CONTAIN)){
				return "continue";
			}
			if (line.toUpperCase().contains(Testo.TRANSFERED) || line.toUpperCase().contains(Testo.UNANNOUNCED)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.EMAIL_GEIA) || line.toUpperCase().contains(Testo.SITE_GEIA) || line.toUpperCase().contains(Testo.EMAIL_GEIA_2) ){
				return "continue";
			}

			if (line.length() < 50){
				return "continue";
			}

		}

		if (cliente == Testo.RIMI){

			if (line.toUpperCase().contains(Testo.EAN)){
				return "continue";
			}

		}

		if (cliente == Testo.EURO){

			if (line.toUpperCase().contains(Testo.SCADENZA)){
				return "break";
			}

		}

		if (cliente == Testo.NADIA){

			if (line.equals(Testo.ZEROS)){
				return "break";
			}

		}

		if (cliente == Testo.DA_VINCI){

			if (line.length() < 50){
				return "continue";
			}

		}

		if (cliente == Testo.CARNIATO){

			String newLine = line.replaceAll("\\s+","").replaceAll("!", "");

			if (newLine.length() < 30){
				return "continue";
			}
			if (line.contains(Testo.NOMBRE)){
				return "continue";
			}

		}

		if (cliente == Testo.SEVEN){
			
			if (line.toUpperCase().contains(Testo.TOTALE)){
				return "break";
			}

			if (line.toUpperCase().contains(Testo.PERCENTO) && line.toUpperCase().contains(Testo.PZ)){
				return "continue";
			}

			String newLine = line.replaceAll("\\s+","");

			if (newLine.length() < 40){
				return "continue";
			}

		}

		if (cliente == Testo.BORG){

			if (line.toUpperCase().contains(Testo.NOTES) && line.toUpperCase().contains(Testo.EUR)){
				return "continue";
			}

			String newLine = line.replaceAll("\\s+","");

			if (newLine.length() < 40){
				return "continue";
			}

		}

		if (cliente == Testo.UNICOOP){

			if (line.toUpperCase().contains(Testo.UDM)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SCONTO_VALUTA)){
				return "break";
			}
		}

		if (cliente == Testo.UNICOOP_TIRRENO){
		
			if (line.toUpperCase().contains(Testo.SICOM) || line.toUpperCase().contains(Testo.DETTAGLIO_ORDINE)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.CONDIZIONI_CONTRATTUALI)){
				return "break";
			}
			
			if (line.trim().length() < 40){
				return "continue";
			}


		}

		if (cliente == Testo.CONSORZIO_NORD_OVEST) {

			if (line.toUpperCase().contains(Testo.EXTRA) || line.toUpperCase().contains(Testo.PROMO)){
				return "continue";
			}


		}

		if (cliente == Testo.DAO) {

			if (line.toUpperCase().contains(Testo.ATTENZIONE)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.ART_FOR)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.LEGENDE)){
				return "break";
			}


		}	

		if (cliente == Testo.MARTINELLI) {

			if (line.toUpperCase().contains(Testo.ARTICOLI)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.ORDINATO)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.STRATO)){
				return "continue";
			}

			if (line.length() < 50){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.DESCRIPTION)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.STORE)){
				return "continue";
			}

		}	

		if (cliente == Testo.ROWCLIFFE) {

			if (line.length() < 50){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.DESCRIPTION)){
				return "continue";
			}

		}	

		if (cliente == Testo.COOP_ALLEANZA) {

			if (line.toUpperCase().contains(Testo.NOTE)){
				return "continue";
			}


		}
		
		if (cliente == Testo.COOP_ALLEANZA_SIC) {

			if (line.toUpperCase().contains(Testo.NOTE)){
				return "continue";
			}


		}

		if (cliente == Testo.COOP_ALLEANZA_30) {

			if (line.toUpperCase().contains(Testo.ARTICOLO)){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.NOTE) || line.toUpperCase().contains(Testo.PAGINA)){
				return "continue";
			}


		}

		if (cliente == Testo.GASTRO) {

			if (line.toUpperCase().contains(Testo.ARTIKEL)){
				return "continue";
			}

			if (line.length() < 50){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.WARENEINGANG)){
				return "break";
			}


		}
		
		if (cliente == Testo.MARR) {

			
			if (line.length() < 50){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.MARR_SPA)){
				return "break";
			}


		}


		if (cliente == Testo.PAC) {

			if (line.toUpperCase().contains(Testo.PERCENTUALE)){
				return "continue";
			}


		}
		
		if (cliente == Testo.LANDO && azienda.equals(Testo.AZIENDA_SOLIGO)) {
			if (line.toUpperCase().contains(Testo.STAMPATO)){
				return "continue";
			}
			if (line.toUpperCase().trim().length()<30 && !line.toUpperCase().contains("-") && !line.toUpperCase().contains("CTX") && !line.toUpperCase().contains("**")){
				return "continue";
			}
		}

		if (cliente == Testo.LANDO && azienda.equals(Testo.AZIENDA_VIPITENO)) {
			if (line.toUpperCase().trim().length()<30 && !line.toUpperCase().contains("CTX")){
				return "continue";
			}
			if (line.toUpperCase().contains(Testo.RIEPILOGO)) {
				return "break";
			}
			if (line.toUpperCase().contains(Testo.ARTICOLO) || line.toUpperCase().contains(Testo.CONFERMA) || line.toUpperCase().contains(Testo.VIA) || line.toUpperCase().contains(Testo.TEL) && !line.toUpperCase().contains(Testo.STRACCIATELLA)){
				return "continue";
			}
			if (line.toUpperCase().contains(Testo.DATA_ORDINE)) {
				return "continue";
			}
			if (line.toUpperCase().contains(Testo.DATA_CONSEGNA) || line.toUpperCase().contains(Testo.FINE_ORDINE)) {
				return "continue";
			}
			if (line.toUpperCase().contains(Testo.STAMPATO)) {
				return "continue";
			}
			if (line.toUpperCase().contains(Testo.PUNTO_CONSEGNA)){
				return "continue";
			}

		}

		if (cliente == Testo.ROMA) {

			if (line.toUpperCase().replaceAll("\\W", "").trim().length()<15){
				return "continue";
			}

			if (line.toUpperCase().contains(Testo.SALVO_APPROVAZIONE) || line.toUpperCase().contains(Testo.CONSEGNA_2)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.AGGIUNGERE) && line.toUpperCase().contains(Testo.TEST)){
				return "continue";
			}


			if (line.toUpperCase().contains(Testo.DESCRIZIONE) || line.toUpperCase().contains(Testo.IN_AGGIUNTA) || line.toUpperCase().contains(Testo.NETTO_ORDINE)){
				return "continue";
			}
			
			if (line.toUpperCase().contains(Testo.RICHIESTA_CLIENTE)){
				return "continue";
			}

		}


		if (cliente == Testo.TRENTO) {

			if (line.toUpperCase().contains(Testo.NOTE)){
				return "continue";
			}
		}

		if (cliente == Testo.TATO) {

			if (line.toUpperCase().replaceAll("\\W", "").trim().length()<30){
				return "continue";
			}
		}	

		if (cliente == Testo.EMMECI) {

			if (line.toUpperCase().replaceAll("\\W", "").trim().length()<30){
				return "continue";
			}
			if (line.toUpperCase().contains(Testo.RIFERIMENTO_CONTRATTO)) {
				return "continue";
			}

		}

		if (cliente == Testo.BENNET) {

			if (line.toUpperCase().replaceAll("\\W", "").trim().length()<30){
				return "continue";
			}
			if (line.toUpperCase().contains(Testo.SCADENZA_MINIMA)) {
				return "continue";
			}

		}
		
		if (cliente == Testo.GARDINI) {
			
			line = line.replace("gr. 500", "");
			line = line.replace("gr. 250", "");
			line = line.replace("gr. 125", "");
			line = line.replace("g 125", "");
			line = line.replace("g 250", "");
			line = line.replace("g 500", "");
			line = line.replace("g 200x10", "");
			line = line.replace("LT. 4", "");
			line = line.replace("LT. 1", "");
			line = line.replace("200ml", "");
			line = line.replace("1500g", "");
			line = line.replace("1000g", "");
			line = line.replace("2000g", "");
			line = line.replace("250g", "");
			line = line.replace("500g", "");
			line = line.replace("1000g", "");
			
			line = line.replace("1000ml", "");
			line = line.replace("500ml", "");
			line = line.replace("ML 1000", "");
			
			String [] rowArray = line.trim().split("\\s+");
			String last = null;
			String secondLast = null;
			
			try {
				last = rowArray[rowArray.length-1];
				secondLast = rowArray[rowArray.length-2];
			} catch (Exception e) {
				 
			}
			
			
			if (!last.trim().contains("50")) {
				if (!StringUtils.isNumeric(last)) {
					return "continue";
				}
			} else {

				if (!StringUtils.isNumeric(last) || !StringUtils.isNumeric(secondLast)){
					return "continue";
				}
			}
		
		}

		return "";


	}


}
