package com.econorma.extract.docx;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import com.econorma.logic.Parsers;
import com.econorma.testo.Testo;

public class ExtractDocx {

	private String type;
	private String deposito;

	public void execute(File file, String azienda) throws IOException, ParseException{

		FileInputStream fs= new FileInputStream(file.getAbsoluteFile());
		BufferedReader br = new BufferedReader(new InputStreamReader(fs));

		//		HWPFDocument doc = new HWPFDocument(fs);
		//		WordExtractor extractor = new WordExtractor(doc);

		XWPFDocument docx = new XWPFDocument(new FileInputStream(file.getAbsoluteFile()));
		XWPFWordExtractor extractor = new XWPFWordExtractor(docx);

 
		String text = extractor.getText();

		type = getType(text);
		deposito = getDeposito(text);

		File csvFile = new File(type + "_" +UUID.randomUUID() + ".csv");
		PrintWriter pw = new PrintWriter(csvFile);
		StringBuilder sb = new StringBuilder();

		if (text != null) {
			boolean startReading = false;
			String art = null;
			String desc = null;
			String qta = null;
			String ordine=null;
			String consegna=null;
			String rowArray[] =null;
			String[] dati = new String[2];

			for (String line : text.split("\n|\r")) {

				if (line.isEmpty() || line.trim().length() == 0){
					continue;
				}

				if (line.contains("possibile")) {
					String ok = null;
				}
				
				switch (type) {
				case Testo.SICLA:

					if (line.toUpperCase().contains(Testo.DESCRIZIONE)){
						startReading = true;
						continue;
					}

					if (line.toUpperCase().contains(Testo.ORDINE_DEL)){
						int indexOf = line.indexOf(Testo.ORDINE_DEL);
						String newLine = line.substring(indexOf+10, indexOf+30);
						dati = Parsers.extract(azienda, Testo.SICLA, Testo.ORDINE, newLine);
						ordine = dati[1];
					}

					if (line.toUpperCase().contains(Testo.CONSEGNA_TASSATIVA)){
						int indexOf = line.indexOf(Testo.CONSEGNA_TASSATIVA);
						String newLine = line.substring(indexOf+10, indexOf+30);
						dati = Parsers.extract(azienda, Testo.SICLA, Testo.DATA_CONSEGNA, newLine);
						consegna = dati[0];
					}

					if (startReading) {
						if (line.toUpperCase().contains(Testo.SCONTO_PUNTI)) {
							int indexOf = line.toUpperCase().indexOf(Testo.SCONTO_PUNTI);
							if (indexOf!=-1){
								line = line.substring(0, indexOf);	
							}
						}
						rowArray = line.trim().split("\\s+");
						
						art = rowArray[0];
						desc = rowArray[1]+ rowArray[2];
						
						int index = line.toUpperCase().lastIndexOf("SV");
						if (index==-1){
							index = line.toUpperCase().lastIndexOf("PZ");
							if (index==-1){
								index = line.toUpperCase().lastIndexOf("KG");
								if (index==-1){
									index = line.toUpperCase().lastIndexOf("CT"); 
								}
							}
						}
						
						if (index!=-1){
							try {
								line = line.substring(index, index+7);	
							} catch (Exception e) {
								line = line.substring(index, line.length());
							}
							 
						} 
 						
						rowArray = line.trim().split("\\s+");
						
						boolean isNumber=StringUtils.isNumeric(rowArray[rowArray.length-1]);
						boolean isNumber2=StringUtils.isNumeric(rowArray[rowArray.length-2]);
						if(isNumber&&isNumber2){
							qta = rowArray[rowArray.length-1];	
						} else {
							continue;
						}
						
					}

					break;
				}


				if (startReading){
					System.out.println(line);
					sb.append(type);
					sb.append(';');
					sb.append(deposito);
					sb.append(';');
					sb.append("");
					sb.append(';');
					sb.append(ordine);
					sb.append(';');
					sb.append(consegna);
					sb.append(';');
					sb.append(art);
					sb.append(';');
					sb.append(desc);
					sb.append(';');
					sb.append(qta);
					sb.append('\n');
				}


			}

		}


		if (br != null){
			br.close();
		}

		pw.write(sb.toString());
		pw.close();
		extractor.close();
		System.out.println("Done");
	}


	public int getFirstNumber(String data){

		for (int i = 0; i < data.length(); i++) {

			if (Character.isDigit(data.charAt(i))){
				return i;
			}

		}
		return 0;
	}

	private String getType(String text)  throws IOException{

		String type = Testo.SICLA;

		//		for (String line : text.split("\n|\r")) {
		//			if (line.toUpperCase().trim().contains(Testo.NARITA)){
		//				return Testo.NARITA;
		//			} else if (line.toUpperCase().trim().contains(Testo.VRM_CODE)) {
		//				return Testo.VRM;   
		//			} 
		//		}

		return type;

	}

	private String getDeposito(String text)  throws IOException{

		for (String line : text.split("\n|\r")) {
//			if (line.toUpperCase().trim().contains(Testo.PI_CF)){
			if (line.toUpperCase().trim().contains(Testo.COD)){
				String rowArray[] = line.trim().split("\\s+");
				return rowArray[1];
			} 
		}

		return Testo.SICLA;

	}

}


