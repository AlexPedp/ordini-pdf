package com.econorma.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;

import org.apache.commons.io.FilenameUtils;

import com.econorma.util.Logger;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;

public class ConvertFile {
	
	private static final String TAG = ConvertFile.class.getSimpleName();
	private static final Logger logger = Logger.getLogger(ConvertFile.class);
	
	private File file;
	
	public ConvertFile(File file) {
		this.file=file;
	}
	
	
	public void run() {

		BufferedReader input = null;
		Document output = null;
		logger.info(TAG, "Starting converting file txt to pdf");

		try {
 			
			String pdfFile = FilenameUtils.getFullPathNoEndSeparator(file.getAbsolutePath()) + "\\" +   FilenameUtils.getBaseName(file.getAbsolutePath()) + ".pdf";
			
			input = new BufferedReader (new FileReader(file));

			output = new Document(PageSize.LETTER, 40, 40, 40, 40);

			PdfWriter.getInstance(output, new FileOutputStream (pdfFile));

			output.open();
			output.addAuthor("Alessandro Mattiuzzi");
			output.addSubject(file.getName());
			output.addTitle(file.getName());
			
			BaseFont courier = BaseFont.createFont(BaseFont.COURIER, BaseFont.CP1252, BaseFont.EMBEDDED);
		    Font myfont = new Font(courier, 10);


			String line = "";
			while(null != (line = input.readLine())) {
				logger.debug(TAG,  line);
				Paragraph p = new Paragraph(line, myfont);
				p.setAlignment(Element.ALIGN_JUSTIFIED);
				output.add(p);
			}

			logger.debug(TAG, "Ending converting file txt to pdf");
			output.close();
			input.close();
			 
		}
		catch (Exception e) {
			logger.error(TAG, e);
		}
	}

}
