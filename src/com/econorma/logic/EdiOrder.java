package com.econorma.logic;

import java.io.File;
import java.io.FileInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import com.econorma.data.Order;
import com.econorma.testo.Testo;
import com.econorma.util.Logger;

public class EdiOrder {

	private static final String TAG = EdiOrder.class.getSimpleName();
	private static final Logger logger = Logger.getLogger(EdiOrder.class);

	private String azienda;
	private Order ordine;
	private String ediDirectory;
	private String extractBackup;
	private String documentaleBackup;
	private File finder[];

	public EdiOrder(String azienda, Order ordine, String ediDirectory, String extractBackup, String documentaleBackup) {
		this.azienda = azienda;
		this.ordine=ordine;
		this.ediDirectory=ediDirectory;
		this.extractBackup=extractBackup;
		this.documentaleBackup=documentaleBackup;
	}

	public File run() throws Exception{

		String nomeFile = ordine.getNomePdf().trim();

		String ordineFile = ordine.getNomeFile().trim() + ".pdf";
		String riferimentoOrdine = ordine.getRiferimentoOrdine();

		if (riferimentoOrdine!=null) {
			logger.info(TAG, "Edi active riferimenti ordine: " +  riferimentoOrdine);	
		}

		logger.debug(TAG, "Ordine AS400: " +  nomeFile + "|" + ordineFile);
		File file= getFileName(nomeFile, riferimentoOrdine);
		if (file!=null) {
			renameFile(file.getName(), ordineFile);
			logger.info(TAG, "Ordine ridenominato: " + ordine.getCliente() + "|" + ordine.getRiferimentoOrdine() + "|" + ordineFile);
			return file;
		}

		return null;

	}


	public File getFileName(String nomePdf, String riferimentoOrdine){

		File files = new File(ediDirectory);
		finder = files.listFiles(new FileExtensionFilter(".pdf", ".txt", ".csv"));

		for (File file: finder){
			if (file.isDirectory()){
				continue;
			}
			String fileName = file.getName(); 
			String extension = FilenameUtils.getExtension(file.getAbsolutePath());
			logger.debug(TAG, "Start searching: " +  fileName);
			
			if (fileName.trim().contains(riferimentoOrdine)){
				logger.info(TAG, "File trovato: " +  fileName);
				return file;
			} else {
				boolean found = getReference(file, extension, riferimentoOrdine);
				if (found) {
					return file;
				}
			}
		}

		return null;
	}

	public void renameFile(String nomeFile, String ordineFile){

		try {

			File fromFile = new File(ediDirectory +  "\\" + nomeFile);

			String extension = FilenameUtils.getExtension(fromFile.getAbsolutePath());
			
			if (extension.toUpperCase().equals(Testo.TXT)) {
				ConvertFile convertFile = new ConvertFile(fromFile);
				convertFile.run();
				logger.info(TAG, "Trying to delete file: " + fromFile.getAbsolutePath());
				fromFile.delete();
				fromFile = new File(FilenameUtils.removeExtension(fromFile.getAbsolutePath()) + "." + Testo.PDF);
			}
		
//			ordineFile = FilenameUtils.removeExtension(ordineFile) + "." + extension;
			ordineFile = FilenameUtils.removeExtension(ordineFile) + "." + Testo.PDF;

			File toFile = new File(ediDirectory +  "\\" + ordineFile);
			File toAS400File = new File(extractBackup +  "\\" + ordineFile);
			File documentaleFile = new File(documentaleBackup +  "\\" + ordineFile);

			if (!fromFile.getParentFile().exists()){
				fromFile.getParentFile().mkdirs();
			}
			if (toFile.exists()){
				toFile.delete();
			}

			fromFile.renameTo(toFile);
			if (documentaleBackup.trim().length()>0) {
				FileUtils.copyFile(toFile, documentaleFile);
			}
 			toFile.renameTo(toAS400File);
 			logger.info(TAG, "File renamed from:" + nomeFile + " to: " + toFile.getAbsolutePath() +  " to AS400 file " + toAS400File.getAbsolutePath()); 

		} catch (Exception e) {
			logger.error(TAG, e);
		}

	}
	
	
	public boolean getReference(File file, String extension, String riferimentoOrdine) {
		
		boolean found = false;
		String text=null;
		
		try {
			switch (extension.toUpperCase()) {
			case Testo.TXT:
				FileInputStream is = new FileInputStream(file);
				text = IOUtils.toString(is, "UTF-8");
			case Testo.PDF:
				PDDocument doc = PDDocument.load(file);
				text=  new PDFTextStripper().getText(doc);
				doc.close();
			}
			
		} catch (Exception e) {
//			logger.error(TAG, e);
		}
		
		
		if (text!=null){
			for (String line : text.split("\n|\r")) {
				if (line.contains(riferimentoOrdine) || line.contains(StringUtils.stripStart(riferimentoOrdine,"0")) || line.contains("0"+riferimentoOrdine)) {
					found= true;
					return found;
				}
				
			}
		}
		
		return found;
		
	}

}
