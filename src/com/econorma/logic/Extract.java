package com.econorma.logic;

import java.io.File;

import com.econorma.extract.doc.ExtractDoc;
import com.econorma.extract.docx.ExtractDocx;
import com.econorma.extract.pdf.ExtractPdf;
import com.econorma.extract.rtf.ExtractRtf;
import com.econorma.extract.txt.ExtractTxt;
import com.econorma.extract.xls.ExtractXls;
import com.econorma.extract.xlsx.ExtractXlsx;
import com.econorma.extract.xml.ExtractXml;



public class Extract {

	private final String TXT = "txt";
	private final String PDF = "pdf";
	private final String XLSX = "xlsx";
	private final String XLS = "xls";
	private final String DOC = "doc";
	private final String RTF = "rtf";
	private final String XML = "xml";
	private final String DOCX = "docx";
	
	public void run(File file, String azienda) throws Exception{
		
		String extension  = getFileExtension(file);

		switch (extension.toLowerCase()) {
		case TXT:
			ExtractTxt txt = new ExtractTxt();
			txt.execute(file, azienda);
			break;
		case PDF:
			ExtractPdf pdf = new ExtractPdf();
			pdf.init(azienda);
			pdf.execute(file, azienda);
			break;
		case XLSX:
			ExtractXlsx xlsx = new ExtractXlsx();
			xlsx.execute(file, azienda);
			break;
		case XLS:
			ExtractXls xls = new ExtractXls();
			xls.execute(file, azienda);
			break;
		case DOC:
			ExtractDoc doc = new ExtractDoc();
			doc.execute(file, azienda);
			break;
		case DOCX:
			ExtractDocx docx = new ExtractDocx();
			docx.execute(file, azienda);
			break;
		case RTF:
			ExtractRtf rtf = new ExtractRtf();
			rtf.execute(file, azienda);
			break;
		case XML:
			ExtractXml xml = new ExtractXml();
			xml.execute(file, azienda);
			break;
		default:
			break;
		}

	}
	
	
	private String getFileExtension(File file) {
	    String name = file.getName();
	    try {
	        return name.substring(name.lastIndexOf(".") + 1);
	    } catch (Exception e) {
	        return "";
	    }
	}

}
