package com.econorma.logic;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Map;

import com.econorma.dao.DAO;
import com.econorma.data.Order;
import com.econorma.properties.OrdersProperties;
import com.econorma.util.Logger;

public class Reader {
	
	private static final String TAG = Reader.class.getSimpleName();
	private static final Logger logger = Logger.getLogger(Reader.class);
	private static final String SEMAFORO = "import.sem";
	private DAO dao;
	
	public Reader(DAO dao){
		this.dao=dao;
	}
	
	public void run(String azienda) throws Exception{
 		
		Map<String, String> map = OrdersProperties.getInstance().load();
		String dir = (String) map.get("directory");
		String backupDir = (String) map.get("backup_directory");
		boolean isExtractor = Boolean.parseBoolean(map.get("extractor_active"));
		String extractorDirectory = (String) map.get("extractor_directory");
		String extractorBackup = (String) map.get("extractor_backup");
		boolean ediActive = Boolean.parseBoolean(map.get("edi_active"));
		String ediDirectory = (String) map.get("edi_directory");
		String documentaleBackup = (String) map.get("documentale_backup");
		
		if (!backupDir.endsWith("\\")){
			backupDir = backupDir + "\\";
		}

		File sem = new File(dir +  "\\" + SEMAFORO);
		sem.createNewFile();
		
		File files = new File(dir);
		File finder[] = files.listFiles(new FileExtensionFilter(".pdf", ".txt", ".xls", ".xlsx", ".doc", ".rtf", ".xml", ".docx"));
		
		for (File file: finder){
			
			if (file.isDirectory()){
				continue;
			}
			
			Extract extract = new Extract();
			extract.run(file, azienda);
			
			Path src = Paths.get(file.getAbsolutePath());
			Path dst = Paths.get(backupDir + file.getName());

			if (!Files.exists(Paths.get(backupDir))) {
				try {
					Files.createDirectories(dst);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			Files.copy(src, dst, StandardCopyOption.REPLACE_EXISTING);
			file.delete();
			
		}
		
		if (isExtractor || ediActive){
			
			List<Order> ordini = dao.readOrdini();
			for (Order ordine : ordini) {
				
				if (isExtractor) {
					Extractor extractor = new Extractor(azienda, ordine, extractorDirectory, extractorBackup, documentaleBackup);
					File file = extractor.run();
					if (file!=null) {
						dao.updateOrdine(ordine);
 					}
				}
				
				if (ediActive) {
					EdiOrder edi = new EdiOrder(azienda, ordine, ediDirectory, extractorBackup, documentaleBackup);
					File file = edi.run();
					if (file!=null) {
						dao.updateOrdine(ordine);
 					}
				}
				
			}
			
		}
		
				
		sem.delete();
	}

 

}
