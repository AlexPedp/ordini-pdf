package com.econorma.logic;

import java.io.File;

import org.apache.commons.io.FileUtils;

import com.econorma.data.Order;
import com.econorma.testo.Testo;
import com.econorma.util.Logger;

public class Extractor {

	private static final String TAG = Reader.class.getSimpleName();
	private static final Logger logger = Logger.getLogger(Reader.class);

	private String azienda;
	private String extractorDirectory;
	private String extractorBackup;
	private String documentaleBackup;
	private Order ordine;
	private File finder[];

	public Extractor(String azienda, Order ordine, String extractorDirectory, String extractorBackup, String documentaleBackup){
		this.azienda=azienda;
		this.ordine=ordine;
		this.extractorDirectory=extractorDirectory;
		this.extractorBackup=extractorBackup;
		this.documentaleBackup=documentaleBackup;
	}


	public File run() throws Exception{
	 
			String nomePdf = null;
			if (azienda.equals(Testo.AZIENDA_VIPITENO)) {
				nomePdf = ordine.getNomePdf().trim() + ".pdf";
			} else {
				nomePdf = ordine.getNomePdf().trim();
			}

			String ordineFile = ordine.getNomeFile().trim() + ".pdf";
			logger.debug(TAG, "Ordine AS400: " +  nomePdf + "|" + ordineFile);
			if (getFileName(nomePdf)){
				renameFile(nomePdf, ordineFile);
				logger.info(TAG, "Ordine ridenominato: " + ordine.getCliente() + "|" + ordine.getRiferimentoOrdine() + "|" + ordine.getData());
				return new File(nomePdf);
			}
			
			return null;
		
	}


	public boolean getFileName(String nomePdf){
		
		File files = new File(extractorDirectory);
		finder = files.listFiles(new FileExtensionFilter(".pdf"));
		boolean found = false;

		for (File file: finder){
			if (file.isDirectory()){
				continue;
			}
			String fileName = file.getName(); 
			logger.debug(TAG, "File pdf extractor: " +  file.getAbsolutePath());
			if (fileName.trim().equals(nomePdf)){
				logger.info(TAG, "File pdf trovato: " +  fileName);
				found=true;
			}
		}

		return found;
	}
	
	public void renameFile(String nomePdf, String ordineFile){

		try {

			File fromFile = new File(extractorDirectory +  "\\" + nomePdf);
			File toFile = new File(extractorDirectory +  "\\" + ordineFile);
			File toAS400File = new File(extractorBackup +  "\\" + ordineFile);
			File documentaleFile = new File(documentaleBackup +  "\\" + ordineFile);

			if (!fromFile.getParentFile().exists()){
				fromFile.getParentFile().mkdirs();
			}
			if (toFile.exists()){
				toFile.delete();
			}

			fromFile.renameTo(toFile);
			if (documentaleBackup.trim().length()>0) {
				FileUtils.copyFile(toFile, documentaleFile);
			}
			toFile.renameTo(toAS400File);
			logger.info(TAG, "File PDF renamed to: " + toFile.getAbsolutePath());
			
			
		} catch (Exception e) {
			logger.error(TAG, e);
		}


	}



}
