package com.econorma.logic;

import java.io.File;
import java.io.FileFilter;

class FileExtensionFilter implements FileFilter {

    private final String[] validExtensions;

    public FileExtensionFilter(String... validExtensions) {
        this.validExtensions = validExtensions;
    }
 

	@Override
	public boolean accept(File pathname) {
		 if (pathname.isDirectory()) {
	            return true;
	        }

	        String name = pathname.getName().toLowerCase();

	        for (String ext : validExtensions) {
	            if (name.endsWith(ext)) {
	                return true;
	            }
	        }

	        return false;
	}
}
