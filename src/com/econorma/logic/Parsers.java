package com.econorma.logic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.econorma.testo.Testo;
import com.econorma.util.Logger;

public class Parsers {

	private static SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yy");
	private static SimpleDateFormat format3 = new SimpleDateFormat("dd.MM.yyyy");
	private static SimpleDateFormat format4 = new SimpleDateFormat("dd-MM-yyyy");
	private static SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");
	private static SimpleDateFormat format5 = new SimpleDateFormat("dd-MM-yyyy");
	private static SimpleDateFormat format6 = new SimpleDateFormat("dd.MM.yy");
	private static SimpleDateFormat format7 = new SimpleDateFormat("dd-MM-yyyy");
	private static SimpleDateFormat format8 = new SimpleDateFormat("dd.MM.yyyy");
	private static SimpleDateFormat format9 = new SimpleDateFormat("d/MM/yy");
	private static SimpleDateFormat formatUsa = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat formatCustom = new SimpleDateFormat("dd. MMMM yyyy", Locale.GERMAN);
	private static SimpleDateFormat formatCustomIta = new SimpleDateFormat("dd MMMM yyyy", Locale.ITALIAN);
	private static SimpleDateFormat formatCustomIta2 = new SimpleDateFormat("dd. MMMM yyyy", Locale.ITALIAN);
	private static Locale danish = new Locale("da", "DK");
	private static SimpleDateFormat formatCustomDK = new SimpleDateFormat("dd. MMMM yyyy", danish);
	private static SimpleDateFormat formatCustomUSA = new SimpleDateFormat("dd. MMMM yyyy", Locale.ENGLISH);
	private static SimpleDateFormat formatCustomJapan = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
	
	private static SimpleDateFormat formatCustomItalian = new SimpleDateFormat("EEEE, dd MMMM yyyy", Locale.ITALIAN);

	private static final Logger logger = Logger.getLogger(Parsers.class);
	private static final String TAG = "Parsers";



	public static String[] extract(String azienda, String cliente, String key, String text) throws ParseException{

		String[] parser = new String[3];
		String[] dati = new String[2];
		int indexOf=0;
		String regex;

		switch (cliente) {

		case Testo.SICLA:

			switch (key) {
			case Testo.ORDINE:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = "";
				dati[1] = parser[2];
				if (dati[1]==null){
					dati[1] = format2.format(new Date());
				}
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;
			
		case Testo.SAIT:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Ordine Nr.   :", "").trim();
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1].replace("del", "").replaceAll("\\s+","");
				dati[1] = parser[2];
				if (dati[1]==null){
					dati[1] = format2.format(new Date());
				}
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;
			
			
		case Testo.GEMOS:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", " ");
				text = text.toUpperCase().replaceAll("ORDINE DEL:", "").trim();
				text = text.toUpperCase().replaceAll("CONSEGNA IL:", "").trim();
				text = text.toUpperCase().replaceAll("ORDINE N°:", "").trim();
				regex = "(\\d{2}/\\d{2}/\\d{4})(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[4];
				dati[1] = parser[1];
				if (dati[1]==null){
					dati[1] = format2.format(new Date());
				}
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;
			
			
		case Testo.COOP_ALLEANZA_SIC:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", " ");
				text = text.toUpperCase().replaceAll("NUMERO ORDINE:", "").trim();
				text = text.toUpperCase().replaceAll("DATA CONSEGNA:", "").trim();
				text = text.toUpperCase().replaceAll("DATA ORDINE:", "").trim();
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[2];
				if (dati[1]==null){
					dati[1] = format2.format(new Date());
				}
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replaceAll("[\n\r]", " ");
				regex = "(\\d{2}/\\d{2}/\\d{4})(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[3];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;
			
		case Testo.BRUNELLO:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text.replace(".", ""), regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;
			
		case Testo.SALAROMEO:

			switch (key) {
			case Testo.ORDINE:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[2];
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;
			
		case Testo.VALSANA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace("del", " ");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[2];
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;
			
		case Testo.PAOLINI:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				if (dati[1]==null){
					dati[1] = format2.format(new Date());
				}
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;
			
		case Testo.SELECTA:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				if (dati[1]==null){
					dati[1] = format2.format(new Date());
				}
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;
			
		case Testo.SICONAD:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				if (dati[1]==null){
					dati[1] = format2.format(new Date());
				}
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;
			
		case Testo.GARBELOTTO:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace(".", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				if (dati[1]==null){
					dati[1] = format2.format(new Date());
				}
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;
			
		case Testo.SAN_FELICI:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace(".", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				if (dati[1]==null){
					dati[1] = format2.format(new Date());
				}
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;
			
		case Testo.BOCON:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace(".", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				if (dati[1]==null){
					dati[1] = format2.format(new Date());
				}
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;
			
		case Testo.EURORISTORAZIONE:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace(".", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = parser[1].trim();
				dati[1] = reformatData(format1, parser[2]);
				if (dati[1]==null){
					dati[1] = format2.format(new Date());
				}
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;
			
		case Testo.UNICANT:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Ordine a fornitore", "").trim();
				text = text.replaceAll("del", "").trim();
				regex = "([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{2})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				dati[1] = reformatData(format1, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}.\\d{2}.\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;
			
		case Testo.CENTRALE_LATTE:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Numero Ordine", "").trim();
				text = text.replaceAll("Data", "").trim();
//				regex = "([0-9]+)(\\d{2}.\\d{2}/.\\d{4})";
				regex = "([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{2})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format8, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format8, parser[2]);
			default:
				break;
			}

			break;

		case Testo.OPRAMOLLA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", " ");
				text = text.replaceAll("documento:", " ");
				text = text.replace("N.", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[3];
				dati[1] = parser[2];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;
			
		case Testo.CAFORM:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", " ");
				text = text.replaceAll("Ordine Acquisto ", " ");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format1, parser[2]);

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;
			
		case Testo.BERETTA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", " ");
				regex = "(.?)([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				dati[1] =  reformatData(format3, parser[4]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format3, parser[1]);
			default:
				break;
			}

			break;
			
		case Testo.DECARNE:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", " ");
				text = text.replaceAll("Ordine n. ", " ");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1].replace("del", "");
				dati[1] = reformatData(format1, parser[2]);

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.GMF:

			switch (key) {
			case Testo.ORDINE:
				//				String[] splitted = text.split("N.ordine:");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format1, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;
			
		case Testo.GARDINI:

			switch (key) {
			case Testo.ORDINE:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = "";
				dati[1] = parser[2];
				break;
			case Testo.DATA_CONSEGNA:
//				regex = "(.*)((Jan(?:uar)?|Feb(?:ruar)?|Mar(?:z)?|Apr(?:il)?|Mai|Jun(?:i)?|Jul(?:i)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|(Nov|Dec)(?:ember)?) \\d{1,2}(st|nd|rd|th), \\d{4})(.*)";
				dati[0] = reformatData(formatCustomItalian, text);
			default:
				break;
			}

			break;

		case Testo.NOSAWA:

			switch (key) {
			case Testo.ORDINE:
				try {
					regex = "([0-9]+)(.*)";
					parser = execute(text, regex);
					dati[0] = parser[1];
					dati[1] = format2.format(new Date());
				} catch (Exception e) {
					dati[0] = "";
					dati[1] = format2.format(new Date());
				}

				break;
			case Testo.DATA_CONSEGNA:
				try {
					text = text.replaceAll("[\n\r]", " ");
					regex = "(.*)((Jan(?:uar)?|Feb(?:ruar)?|Mar(?:z)?|Apr(?:il)?|Mai|Jun(?:i)?|Jul(?:i)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|(Nov|Dec)(?:ember)?) \\d{1,2}(st|nd|rd|th), \\d{4})(.*)";
					parser = execute(text, regex);
					if (parser[2]==null){
						regex = "(.*)((Jan(?:uar)?|Feb(?:ruar)?|Mar(?:z)?|Apr(?:il)?|Mai|Jun(?:i)?|Jul(?:i)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|(Nov|Dec)(?:ember)?) \\d{2}(st|nd|rd|th), \\d{4})(.*)";
					}
					dati[0] = reformatData(formatCustomJapan, parser[2].replace("st", "").replace("nd", "").replace("rd", "").replace("th", ""));
				} catch (Exception e) {
					dati[0] = format2.format(new Date());
				}

			default:
				break;
			}

			break;

		case Testo.EUROSPIN:
		case Testo.EUROSPIN_TIRRENICA:
		case Testo.EUROSPIN_LAZIO:
			
			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", " ");
				text = text.replaceAll("N.Avv.Carico", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				if (parser[3]==null){
					regex = "([0-9]+)(.*)(\\d{1,2}/\\d{2}/\\d{4})";
					parser = execute(text, regex);
				}
				dati[0] = parser[1];
				dati[1] = reformatData(format2, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				if (parser[2]==null){
					regex = "(.*)(\\d{1,2}/\\d{2}/\\d{4})";	
					parser = execute(text, regex);
				}
				dati[0] = reformatData(format2, parser[2]);
			default:
				break;
			}

			break;

		case Testo.ELIOR:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace("Ordine numero ", "");
				regex = "(.*)(\\d{2} (?:gen(?:naio)?|feb(?:braio)?|mar(?:zo)?|apr(?:ile)?|mag(?:gio)|giu(?:gno)?|lug(?:lio)?|ago(?:sto)?|set(?:tembre)?|ott(?:obre)?|(nov|dic)(?:embre)?) \\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1].replace("del", "");
				dati[1] = reformatData(formatCustomIta, parser[2]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}-\\d{2}-\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format5, parser[2]);
			default:
				break;
			}

			break;

		case Testo.PRIX:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("ORDINE N°", "");
				text = text.replace(" ", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{1,2}/\\d{4})";
				parser = execute(text, regex);
				if (parser[2].length()<10) {
					if (parser[2].substring(4, 5).equals("/")) {
						parser[2]= parser[2].substring(0, 3) + "0" + parser[2].substring(3, parser[2].length());
					}
				}
				dati[0] = parser[2];
				
			default:
				break;
			}

			break;

		case Testo.CIRFOOD:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("N� ORDINE: ", "");
				text = text.replace(" ", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.DAC:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace("Ordine n. ", "");
				text = text.replace(" ", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format1, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.GS:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				try {
					parser = execute(text, regex);
					dati[0] = parser[2];
					if (dati[0]==null){
						dati[0] = format2.format(new Date());
					}
				} catch (Exception e) {
					dati[0] = format2.format(new Date());
				}

			default:
				break;
			}

			break;

		case Testo.RETAILPRO:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace("ORDINE N�", "");
				text = text.replace("DEL", " ");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1].replaceAll(" ", "");
				dati[1] = parser[2];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.SERENISSIMA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("N�Ordine: ", " ");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;

		case Testo.MAXIDI:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Ordine n. ", " ");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;

		case Testo.HAMBERGER:

			switch (key) {
			case Testo.ORDINE:
				regex = "(.?)([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				dati[1] =  reformatData(format3, parser[4]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format3, parser[1]);
			default:
				break;
			}

			break;

		case Testo.VIVO:

			switch (key) {
			case Testo.ORDINE:
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})";
				try {
					parser = execute(text, regex);
					dati[0] = parser[2];
					dati[1] =  reformatData(format1, parser[4]);
				} catch (Exception e) {
					dati[0] = "";
					dati[1] =  ""; 
				}

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(\\d{2}/\\d{2}/\\d{2})";
				try {
					parser = execute(text, regex);
					dati[0] = reformatData(format1, parser[1]);
				} catch (Exception e) {
					dati[0] = "";
				}

			default:
				break;
			}

			break;

		case Testo.PREGIS:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", " ");
				regex = "(.?)([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				dati[1] =  reformatData(format3, parser[4]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format3, parser[1]);
			default:
				break;
			}

			break;

		case Testo.LADISA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", " ");
				//				text = text.replaceAll("[\n\r]", " ");
				regex = "(.?)([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{2})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				dati[1] =  reformatData(format6, parser[4]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{2}.\\d{2}.\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format6, parser[1]);
			default:
				break;
			}

			break;

		case Testo.ROSSI:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll(" ", "");
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				dati[1] = parser[4];
				if (parser[4]==null){
					regex = "(.?)([0-9]+)(.*)(\\d{1,2}/\\d{2}/\\d{4})";
					parser = execute(text, regex);
					dati[0] = parser[2];
					dati[1] = "0" + parser[4];
				}
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replaceAll(" ", "");
				regex = "(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				if (parser[1]==null){
					regex = "(\\d{1,2}/\\d{2}/\\d{4})";
					parser = execute(text, regex);
					dati[0] = "0" + parser[1];
				}

			default:
				break;
			}

			break;


		case Testo.WEIHE:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Bestellung:", " ");
				text = text.replace("N.", "");
				regex = "([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format3, parser[3]);

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format3, parser[2]);
				break;

			default:
				break;
			}

			break;

		case Testo.UNES:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Ordine ", " ");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;
			
		case Testo.DIMAR:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Ns.Ordine ", " ").replace("Del", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2].replace(":", "").replace("/", "");
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.SABELLI:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("ORDINE", "").replaceAll("Numero", "").replaceAll("Data documento", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[2];
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.IPERCONAD:
		case Testo.RDAVENETA:
		case Testo.SGR_IPER:
		case Testo.LEGNARO:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Ordine ", " ");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.BIOLOGICA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", " ");
				text = text.replaceAll("ORDINE NR:", " ");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.BAUER:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Ordine ", " ");
				regex = "([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format3, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format3, parser[2]);
			default:
				break;
			}

			break;

		case Testo.CATTEL:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Ordine ", " ");
				regex = "([0-9]+).([0-9]+).(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
				dati[1] = reformatData(format1, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.RAGIS:

			switch (key) {
			case Testo.ORDINE:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[3];
				dati[1] = reformatData(format1, parser[2]);
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.AMACRAI:

			switch (key) {
			case Testo.ORDINE:
				text = text.toUpperCase().replaceAll(Testo.RIFERIMENTO_AMA, " ");
				text = text.toUpperCase().replaceAll(Testo.RIFERIMENTO_AMA_1, " ");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)([0-9]+)";
				parser = execute(text, regex);
				if (parser[2]==null){
					regex = "(.*)(\\d{1,2}/\\d{2}/\\d{2})(.*)([0-9]+)";
					parser = execute(text, regex);
				}
				dati[0] = parser[3]+parser[4];
				dati[1] = reformatData(format1, parser[2]);
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace("Consegna Del:", "");
				regex = "(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				if (parser[1]==null){
					regex = "(\\d{1,2}/\\d{2}/\\d{2})";	
				}
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[1]);
			default:
				break;
			}

			break;

		case Testo.WOERNDLE:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
			default:
				break;
			}

			break;

		case Testo.MARR:

			switch (key) {
			case Testo.ORDINE:

				//				List<String> lines = searchOrdineByLine(text, Testo.ORDINE);
				//				dati[0] = lines.get(0);
				//				dati[1] = lines.get(1);

				//				text = "   92064398181   del  5/10/21    5/1 /21el";

				try {
					text = text.replaceAll(" ", "");
					regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
					parser = execute(text, regex);
					if (parser[3]==null){
						regex = "([0-9]+)(.*)(\\d{1,2}/\\d{2}/\\d{2})(.*)";
						parser = execute(text, regex);
						parser[3] = "0" + parser[3];
					}
					if (parser[1]==null){
						regex = "([0-9]+)(.*)";
						parser = execute(text, regex);
						parser[3] = format1.format(new Date());
					}
					dati[0] = parser[1];
					dati[1] = reformatData(format1, parser[3]);
				} catch (Exception e) {
					dati[0] = "";
					dati[1] = format2.format(new Date()); 
				}

				break;
			case Testo.DATA_CONSEGNA:
				try {
					regex = "(\\d{2}/\\d{2}/\\d{2})";
					parser = execute(text, regex);
					if (parser[1]==null){
						regex = "(\\d{1,2}/\\d{2}/\\d{2})";
						parser = execute(text, regex);
						parser[1] = "0" + parser[1];
					}
					dati[0] = reformatData(format1, parser[1]);
				} catch (Exception e) {
					dati[0] = format2.format(new Date()); 
				}

			default:
				break;
			}

			break;

		case Testo.TAVELLA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("\r\n", " ");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[1] = parser[2];

				try {
					indexOf = text.indexOf("IORD");
					if (indexOf!=-1){
						dati[0] = text.substring(indexOf, text.length());
					}
				} catch (Exception e) {
					dati[0] = "";
				}

				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] =  parser[1];
			default:
				break;
			}

			break;

		case Testo.CDC:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll(" ", "");
				regex = "([0-9]+)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format1, parser[2]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[1]);
			default:
				break;
			}

			break;

		case Testo.CSS:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("\r\n", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1].replaceAll("[^\\d.]", "");
				dati[1] = reformatData(format1, parser[2]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[1]);
			default:
				break;
			}

			break;

		case Testo.CEDI_SIGMA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Nr. ordine ", "");
				regex = "(.*)(\\d{2}. (?:Gen(?:naio)?|Feb(?:braio)?|Mar(?:zo)?|Apr(?:ile)?|Mag(?:gio)|Giu(?:gno)?|Lug(?:lio)?|Ago(?:sto)?|Set(?:tembre)?|Ott(?:obre)?|(Nov|Dic)(?:embre)?) \\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1].replace("Del", "");
				dati[1] = reformatData(formatCustomIta2, parser[2]);

				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[1]);
			default:
				break;
			}

			break;

		case Testo.CAMST:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("ORDINE: ", " ");
				regex = "([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format8, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format8, parser[1]);
			default:
				break;
			}

			break;

		case Testo.SUPEREMME:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Ordine ", " ");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.REALCO:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Nostro ordine", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1].replace("del", "");
				dati[1] = parser[2];
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.MULTICEDI:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Nostro ordine", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1].trim();
				dati[1] = reformatData(format1, parser[2]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.TOCAL:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Nostro ordine", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.GRAMM:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("_", "");
				text = text.replaceAll("BESTELLUNG N�:", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				if (parser[2]==null){
					regex = "(.*)(\\d{1,2}/\\d{2}/\\d{2})(.*)";
					parser = execute(text, regex);
				}
				dati[0] = parser[1].replace("-", "");
				dati[1] = reformatData(format1, parser[2]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				if (parser[2]==null){
					regex = "(.*)(\\d{1,2}/\\d{2}/\\d{2})(.*)";	
					parser = execute(text, regex);
				}
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.PAC2000:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("\r\n", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1].replace("AULLO", "").replace("del", "").replace("O LAMPASONA", "");
				dati[1] = reformatData(format1, parser[2]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;
			
		case Testo._2000PAC:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("\r\n", "");
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[3].replace("del", "");
				dati[1] = reformatData(format1, parser[4]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.CARAMICO:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("\r\n", "");
				text = text.replaceAll("OrdineNr.", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				//				regex="(.*)([0-9]+)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1].replace("Data", "");
				dati[1] = parser[2];
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.SUPERCENTRO:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("\r\n ", "");
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
				dati[1] = reformatData(format1, parser[4]);
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replaceAll("\r\n ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.SISA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("\r\n ", "");
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
				dati[1] = reformatData(format1, parser[4]);
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replaceAll("\r\n ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.COAL:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format8.format(new Date());
				}
			default:
				break;
			}

			break;

		case Testo.MARTINELLI:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format1, parser[3]);

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.MODERNA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace(" ", "");
				regex = "([0-9]+)(-)([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1]+parser[2]+parser[3];
				dati[1] = parser[5];
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				//				dati[0] = reformatData(format8, parser[2]);
				dati[0] = parser[2];
				break;
			}

			break;

		case Testo.MOSCA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", " ");
				text = text.replaceAll("Nr. Ordine", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)([0-9]+)";
				parser = execute(text, regex);
				dati[0] = parser[3]+parser[4];
				dati[1] = parser[2];
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				break;
			}

			break;

		case Testo.MAIORA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", " ");
				regex = "([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format3, parser[3]);

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format3, parser[2]);
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;

		case Testo.ISA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Ordine Nr. :", " ");
				text = text.replace("del", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[2];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{1,2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format9, parser[2]);
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;

		case Testo.MIGROSS:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Ordine Nr.   :", " ");
				text = text.replace("del", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1].replace("FOR", "");
				dati[1] = parser[2];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(\\d{1,2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format9, parser[1]);
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;

		case Testo.TOSANO:

			switch (key) {
			case Testo.ORDINE:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1].replace("del", "");
				dati[1] = parser[2];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace("Data Consegna", "");
				regex = "(.*)(\\d{2} (?:gen(?:naio)?|feb(?:braio)?|mar(?:zo)?|apr(?:ile)?|mag(?:gio)|giu(?:gno)?|lug(?:lio)?|ago(?:sto)?|set(?:tembre)?|ott(?:obre)?|(nov|dic)(?:embre)?) \\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(formatCustomIta, parser[2]);
			default:
				break;
			}

			break;

		case Testo.VALFOOD:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Ordine Nr.   :", " ");
				text = text.replace("del", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[2];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{1,2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format9, parser[2]);
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;

		case Testo.BRIMI:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace("\r\n", "");
				regex = "(.?)([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
				dati[1] = reformatData(format6, parser[4]);

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace("\r\n", "");
				regex = "(.*)(\\d{2}.\\d{2}.\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format6, parser[2]);
			default:
				break;
			}

			break;

		case Testo.GANASSA:

			switch (key) {
			case Testo.ORDINE:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = "";
				dati[1] = parser[2];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace("\r\n", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;

		case Testo.CRAI:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+/[0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;

		case Testo.VIS_FOOD:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+/[0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;

		case Testo.CRAI_FRE:

			switch (key) {
			case Testo.ORDINE:
				regex = "(.?)([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
				dati[1] = reformatData(format8, parser[4]);

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format8, parser[2]);
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;

		case Testo.SIAF:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace("Ordine numero", "");
				indexOf = text.indexOf("Consegna");
				if (indexOf!=-1){
					text = text.substring(0, indexOf);
				}

				regex = "([0-9]+)(.*)(\\d{2} (?:gen(?:naio)?|feb(?:braio)?|mar(?:zo)?|apr(?:ile)?|mag(?:gio)|giu(?:gno)?|lug(?:lio)?|ago(?:sto)?|set(?:tembre)?|ott(?:obre)?|(nov|dic)(?:embre)?) \\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(formatCustomIta, parser[3]);

				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2} (?:gen(?:naio)?|feb(?:braio)?|mar(?:zo)?|apr(?:ile)?|mag(?:gio)|giu(?:gno)?|lug(?:lio)?|ago(?:sto)?|set(?:tembre)?|ott(?:obre)?|(nov|dic)(?:embre)?) \\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(formatCustomIta, parser[2]);
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;


		case Testo.DCFOOD:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;

		case Testo.ROSSETTO:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;

		case Testo.BRENDOLAN:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format1, parser[3]);

				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.CEDI_GROSS:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace(" ", "");
				text = text.replace("Numero", " ");
				regex = "(.*)(\\d{2}/\\d{1,2}/\\d{2})(.*)([0-9]+)";
				parser = execute(text, regex);
				if (parser[2]==null){
					regex = "(.*)(\\d{1,2}/\\d{1,2}/\\d{2})(.*)([0-9]+)";
					parser = execute(text, regex);
				}
				dati[0] = parser[3]+parser[4];
				dati[1] = reformatData(format1, parser[2]);

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{1,2}/\\d{2})";
				parser = execute(text, regex);
				if (parser[2]==null){
					regex = "(.*)(\\d{1,2}/\\d{1,2}/\\d{2})";
					parser = execute(text, regex);
				}
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.MEGAMARK:

			switch (key) {
			case Testo.ORDINE:
				indexOf = text.indexOf("/");
				if (indexOf!=-1){
					text = text.substring(indexOf+1, text.length());
				}
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				if (dati[0]==null){
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;

		case Testo.GARDENA:

			switch (key) {
			case Testo.ORDINE:
				indexOf = text.indexOf("/");
				if (indexOf!=-1){
					text = text.substring(indexOf+1, text.length());
				}
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;
			
		case Testo.ASPIAG:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format8, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format8, parser[2]);
			default:
				break;
			}

			break;

		case Testo.GEGEL:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace(".", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];

				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{1,2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				//				dati[0] = parser[2];
				dati[0] = reformatData(format9, parser[2]);
			default:
				break;
			}

			break;

		case Testo.IPERAL:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace("Ordine Nr.", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				if (parser[3]==null){
					regex = "([0-9]+)(.*)(\\d{1,2}/\\d{2}/\\d{2})";
					parser = execute(text, regex);
				}
				dati[0] = parser[1];
				dati[1] = reformatData(format1, parser[3]);

				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{1,2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[1]);
			default:
				break;
			}

			break;

		case Testo.ERGON:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.ITALBRIX:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format1, parser[3]);

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[3]);
			default:
				break;
			}

			break;

		case Testo.MORGESE:
		case Testo.ARENA:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.TATO:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace(" ", "");
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[3].replace("del", "").replace("-", "");
				dati[1] = parser[4];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[6];
			default:
				break;
			}

			break;

		case Testo.BENNET: 

			text = text.replaceAll("[\n\r]", " ");

			switch (key) {
			case Testo.ORDINE:
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				dati[1] = parser[4];

				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[6];
			default:
				break;
			}

			break;

		case Testo.LAGOGEL:

			switch (key) {
			case Testo.ORDINE:
				text = text.substring(indexOf+4, text.length());
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text.trim(), regex);
				dati[0] = parser[2];
				dati[1] = reformatData(format1, parser[4]);

				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.ALFI:

			switch (key) {
			case Testo.ORDINE:
				text = text.substring(indexOf+4, text.length());
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text.trim(), regex);
				dati[0] = parser[2];
				dati[1] = parser[4];

				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.ALI:

			switch (key) {
			case Testo.ORDINE:
				text = text.substring(indexOf+4, text.length());
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text.trim(), regex);
				dati[0] = parser[2];
				dati[1] = parser[4];

				break;
			case Testo.DATA_CONSEGNA:
				indexOf = text.indexOf("Tassativa:");
				if (indexOf!=-1){
					text = text.substring(indexOf+10, text.length());
				}
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.DADO:

			switch (key) {
			case Testo.ORDINE:
				text = text.substring(indexOf+4, text.length());
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text.trim(), regex);
				dati[0] = parser[3].replace("del", "").replace("000", "").trim();
				dati[1] = parser[4];

				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.CONAD_FRE:

			switch (key) {
			case Testo.ORDINE:
				//				text = text.substring(indexOf+4, text.length());
				text = text.replace("Tipo ORDINE Numero", " ");
				regex = "(\\d{2}/\\d{2}/\\d{4})(.*)([0-9]+)";
				parser = execute(text.trim(), regex);
				dati[0] = parser[2]+ parser[3];
				dati[1] = parser[1];

				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.CONAD:

			switch (key) {
			case Testo.ORDINE:
				Calendar calendar = new GregorianCalendar();
				calendar.setTime(new Date());
				int year = calendar.get(Calendar.YEAR);
				indexOf = text.indexOf(String.valueOf(year));
				text = text.substring(indexOf+4, text.length());
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text.trim(), regex);
				dati[0] = parser[1].replace("Del", "");
				dati[1] = reformatData(format1, parser[2]);

				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;
			
		case Testo.CONAD_ACS:

			switch (key) {
			case Testo.ORDINE:
				indexOf = text.indexOf("PI");
				text = text.substring(indexOf+2, text.length());
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text.trim(), regex);
				dati[0] = parser[1];
				dati[1] = parser[3];

				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.EMMECI:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace(" ", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[5];
			default:
				break;
			}

			break;

		case Testo.AZIENDA_AMBROSI:

			switch (key) {
			case Testo.ORDINE:
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text.trim(), regex);
				dati[0] = parser[2];
				dati[1] = parser[4];

				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.ROMA:
			indexOf = text.indexOf("Ordine n.");
			text = text.substring(indexOf, text.length());

			switch (key) {
			case Testo.ORDINE:

				text = text.replace("?", "").replace("|", "").trim();
				regex = "(.?)([0-9]+)(.*)(\\d{2}-\\d{2}-\\d{4})";
				parser = execute(text.trim(), regex);
				dati[0] = parser[2];
				dati[1] = reformatData(format5, parser[4]);

				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace("?", "").replace("|", "").trim();
				regex = "(.?)([0-9]+)(.*)(\\d{2}-\\d{2}-\\d{4})";
				parser = execute(text.trim(), regex);
				dati[0] = reformatData(format5, parser[4]);
			default:
				break;
			}

			break;

		case Testo.ROWCLIFFE:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", " ");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text.trim(), regex);
				dati[0] = parser[1];
				dati[1] = parser[2];

				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				if (dati[0] != null){
					dati[0] =  parser[2];
				} else {
					dati[0] = format2.format(new Date());
				}
			default:
				break;
			}

			break;

		case Testo.CADORO:

			switch (key) {
			case Testo.ORDINE:
				indexOf = text.indexOf("N.");
				text = text .substring(indexOf+2, text.length());
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format1, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				indexOf = text.indexOf("N.");
				text = text .substring(indexOf+2, text.length());
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[5]);
			default:
				break;
			}

			break;

		case Testo.GUARNIER:
		case Testo.UNICOMM:

			switch (key) {
			case Testo.ORDINE:
				indexOf = text.indexOf("GU");
				text = text .substring(indexOf+2, text.length()); 
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format1, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.METRO:


			switch (key) {
			case Testo.ORDINE:
				indexOf = text.indexOf("n.");
				text = text .substring(indexOf+2, text.length()); 
				text = text.replaceAll("[\n\r]", "");
				regex = "(\\d+)(.*[^\\d+])(\\d+.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format3, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format3, parser[2]);
			default:
				break;
			}

			break;

		case Testo.LANDO:


			switch (key) {
			case Testo.ORDINE:
				indexOf = text.indexOf("Num. Ordine:");
				text = text .substring(indexOf+12, text.length()); 
				text = text.replaceAll("[\n\r]", "");
				regex = "(\\d+)(.*[^\\d+])(\\d+/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.TRENTO:


			switch (key) {
			case Testo.ORDINE:
				indexOf = text.indexOf("Numero Ordine:");
				text = text .substring(indexOf+12, text.length()); 
				text = text.replaceAll("[\n\r]", "");
				regex = "(\\d+)(.*[^\\d+])(\\d+/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.OTTAVIAN:

			switch (key) {
			case Testo.ORDINE:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				String[] split = text.split("\n");

				String numero1 = split[(split.length-2)];
				String numero2 = split[(split.length-3)];
				String numero3 = split[(split.length-4)];

				if (numero1.trim().length()<=10){
					dati[0] = numero1;
				} else if (numero2.trim().length()<=10){
					dati[0] = numero2;
				} else if (numero3.trim().length()<=10){
					dati[0] = numero3;
				}


				parser = execute(text, regex);
				dati[1] = reformatData(format1, parser[2]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.GARDERE:

			switch (key) {
			case Testo.ORDINE:
				indexOf = text.indexOf("n.");
				text = text .substring(indexOf+2, text.length()); 
				text = text.replaceAll("[\n\r]", "");
				regex = "(\\d+)(.*[^\\d+])(\\d+.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format3, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format3, parser[2]);
			default:
				break;
			}

			break;
			
		case Testo.SIDAL:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}-\\d{2}-\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format4, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}-\\d{2}-\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format4, parser[1]);
			default:
				break;
			}

			break;

		case Testo.APULIA:

			switch (key) {
			case Testo.ORDINE:
				
				if (azienda.equals(Testo.AZIENDA_SOLIGO)) {
					text = text.replaceAll("B2", " ");
					regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
					parser = execute(text, regex);
					String[] split =  parser[3].split(" ");
					dati[0] = split[2].trim();
					dati[1] = reformatData(format1, parser[2]);
				}
				
				if (azienda.equals(Testo.AZIENDA_TONIOLO)) {
					regex = "([0-9]+)(.*)(\\d{1,2}/\\d{2}/\\d{4})(.*)";
					parser = execute(text, regex);
					dati[0] = parser[1];
					dati[1] = reformatData(format2, parser[3]);
				}
				
				break;
			case Testo.DATA_CONSEGNA:
				if (azienda.equals(Testo.AZIENDA_SOLIGO)) {
					regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
					parser = execute(text, regex);
					dati[0] = reformatData(format1, parser[2]);
				}
				if (azienda.equals(Testo.AZIENDA_TONIOLO)) {
					regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
					parser = execute(text, regex);
					dati[0] = parser[2];
				}
				
			default:
				break;
			}

			break;

		case Testo.CARREFOUR:

			switch (key) {
			case Testo.ORDINE:
				indexOf = text.indexOf("n°:");
				text = text .substring(indexOf+3, text.length()); 
				text = text.replaceAll("[\n\r]", " ");

				regex = "(\\d+)(.*)(\\d{2}-\\d{2}-\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format4, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}-\\d{2}-\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format4, parser[2]);
			default:
				break;
			}

			break;

		case Testo.COOP:

			switch (key) {
			case Testo.ORDINE:
				indexOf = text.indexOf("n.");
				text = text .substring(indexOf+2, text.length()); 
				text = text.replaceAll("[\n\r]", "");
				regex = "(\\d+)(.*[^\\d+])(\\d+.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format3, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format3, parser[2]);
			default:
				break;
			}

			break;

		case Testo.TONYS_FINE_FOOD:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				regex = "([0-9]+)(.*)(\\d{1,2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(formatUsa, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{1,2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(formatUsa, parser[2]);
			default:
				break;
			}

			break;

		case Testo.HEIDERBECK:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("\r\n", "");
				text = text.replaceAll("Pagina 1 - 1", "");
				text = text.replaceAll("Pagina 1 - 2", "");
				text = text.replaceAll("Seite 1 von 1", "");
				regex = "(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format6, parser[2]);
				break;
			case Testo.DATA_CONSEGNA:
				try {
					text = text.trim().toUpperCase().split(Testo.CONSEGNA)[1];	
				} catch (Exception e) {
					text = text.trim().toUpperCase().split(Testo.LIEFERDATUM)[1];
				}

				regex = "(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format6, parser[2]);
			default:
				break;
			}

			break;

		case Testo.GASTRO:

			switch (key) {
			case Testo.ORDINE:
				regex = "(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format3, parser[2]);
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replaceAll("\r\n", "");
				regex = "(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format3, parser[2]);
			default:
				break;
			}

			break;

		case Testo.QUESOS:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				//				regex = "(\\d{1,2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				//				int pos = text.toUpperCase().lastIndexOf("ORDER");
				//				if (pos > 0) {
				//					dati[0] =  text.substring(pos+5, text.length()).trim();
				//				}
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replaceAll("[\n\r]", "");
				regex = "(\\d{1,2}-\\d{2}-\\d{4})";
				parser = execute(text, regex);
				if (parser[1] != null){
					dati[0] = reformatData(format5, parser[1]);
				} else {
					regex = "(\\d{2}-\\d{2}-\\d{4})";
					parser = execute(text, regex);
					if (parser[1] != null){
						dati[0] = reformatData(format5, parser[1]);
					} else {
						dati[0] = format2.format(new Date());	
					}

				}
			default:
				break;
			}

			break;

		case Testo.EMMI:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				regex = "([0-9]+)(.*)(\\d{1,2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format2, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{1,2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format2, parser[1]);
			default:
				break;
			}

			break;

		case Testo.COOP_VA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				String [] rowArray = text.trim().split("\\s+");
				dati[0] = rowArray[rowArray.length - 3];
				dati[1] = reformatData(format3, rowArray[rowArray.length - 1]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{1,2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format3, parser[1]);
			default:
				break;
			}

			break;

		case Testo.COOP_ALLEANZA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format2, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{1,2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format2, parser[1]);
			default:
				break;
			}

			break;

		case Testo.COOP_ALLEANZA_30:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{1,2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
			default:
				break;
			}

			break;

		case Testo.GEIA:

			switch (key) {
			case Testo.ORDINE:

				String search1 = "ORDER CONFIRMATION NO";
				String search2 = "MARK WITH ORDER NO.";
				List<String> lines = searchMutipleOrder(text, search1, search2);
				if (lines.size() == 0) {
					dati[0] = "";
				} else {
					if (lines.size() > 2) {
						dati[0] = lines.get(0) + "|" + lines.get(2);	
					} else {
						dati[0] = lines.get(0);
					}
				}
				
				try {
					dati[1] = reformatData(formatCustomDK, lines.get(1));	
				} catch (Exception e) {
					int myIndexOf = text.indexOf("Document Date");
					text = text.substring(myIndexOf+14, text.length()).trim();
					regex = "(.*)(\\d{2}-\\d{2}-\\d{4})(.*)";
					parser = execute(text, regex);
					dati[1] = reformatData(format7, parser[2]);
				}

				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}-\\d{2}-\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format7, parser[2]);
			default:
				break;
			}

			break;

		case Testo.CHASAL:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				regex = "([0-9]+)(.*)(\\d{1,2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format2, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{1,2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format2, parser[1]);
			default:
				break;
			}

			break;

		case Testo.CIRCLEVIEW:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format2, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{1,2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format2, parser[1]);
			default:
				break;
			}

			break;

		case Testo.RIMI:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				regex = "(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = "";
				dati[1] = reformatData(format3, parser[1]);
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replaceAll("[\n\r]", "");
				indexOf = text.indexOf("Delivery Date");
				if (indexOf != -1){
					text = text.substring(indexOf, text.length());	
				} 
				regex = "(.?)(\\d{1,2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format3, parser[2]);
			default:
				break;
			}

			break;

		case Testo.VLEESWAREN:

			switch (key) {
			case Testo.ORDINE:
				regex = "(.?)([0-9]+)(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
				break;
			case Testo.DATA_ORDINE:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format2, parser[2]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format2, parser[2]);
			default:
				break;
			}

			break;

		case Testo.KALLAS:

			switch (key) {
			case Testo.ORDINE:
				regex = "(.?)([0-9]+)(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
				break;
			case Testo.DATA_ORDINE:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format2, parser[2]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format2, parser[2]);
			default:
				break;
			}

			break;

		case Testo.EURO:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[2];
				dati[1] = parser[4];
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format2, parser[2]);
			default:
				break;
			}

			break;

		case Testo.NADIA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				regex = "([0-9]+)(.?)(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format8, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format8, parser[2]);
			default:
				break;
			}

			break;

		case Testo.DA_VINCI:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				regex = "([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format8, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format8, parser[2]);
			default:
				break;
			}

			break;

		case Testo.CARNIATO:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				indexOf = text.indexOf("NUMERO");
				text = text.substring(indexOf+6, text.length());
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format1, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.VISOTTO:

			switch (key) {
			case Testo.ORDINE:
				int year = Calendar.getInstance().get(Calendar.YEAR);
				if (text.contains(String.valueOf(year)+"/")){
					indexOf = text.indexOf("/");
					if (indexOf!=-1){
						text = text.substring(indexOf+1, text.length());
					}
				}

				//				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = parser[1];
				//				dati[1] = parser[3];
				dati[1] = reformatData(format1, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				//				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				//				dati[0] = parser[2];
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.VISOTTO2:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace(" ", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.RESINELLI:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				String[] split = text.split("Ordine");
				regex = "(\\d{4}/)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(split[2], regex);
				dati[0] = parser[1]+parser[2];
				dati[1] = parser[4];
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;
			
		case Testo.CUCINA_NOSTRANA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				text = text.replace("Ordine a fornitore", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				text = text.replace(" ", "");
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.TIGROS:
		case Testo.LANDO_FRE:
		case Testo.SOGEGROSS:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace(" ", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format1, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.IGES:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("\r\n", " ");
				text = text.replaceAll("del", " ");
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[3].replace("-", "");
				dati[1] = reformatData(format1, parser[4]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.NUME:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("\r\n", " ");
				text = text.replaceAll("del", " ");
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
				dati[1] = reformatData(format1, parser[4]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.MADIA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("Numero", " ");
				regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				if (parser[1]==null){
					regex = "(.?)([0-9]+)(.*)(\\d{1,2}/\\d{2}/\\d{2})(.*)";	
					parser = execute(text, regex);
					parser[4] = "0" + parser[4];
				}
				dati[0] = parser[3].replace("del", "");
				dati[1] = reformatData(format1, parser[4]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				if (parser[1]==null){
					regex = "(\\d{1,2}/\\d{2}/\\d{2})(.*)";
					parser = execute(text, regex);
					parser[1] = "0" + parser[1];
				}
				dati[0] = reformatData(format1, parser[1]);
			default:
				break;
			}

			break;

		case Testo.CEDI:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace(" ", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.VEGA:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace(" ", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				if (parser[3]==null){
					regex = "([0-9]+)(.*)(\\d{1,2}/\\d{2}/\\d{2})(.*)";
					parser = execute(text, regex);
				}
				dati[0] = parser[1];
				dati[1] = reformatData(format1, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				if (parser[2]==null){
					regex = "(.*)(\\d{1,2}/\\d{2}/\\d{2})(.*)";
					parser = execute(text, regex);
				}
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.SEVEN:

			switch (key) {
			case Testo.ORDINE:
				indexOf = text.indexOf("SE");
				text = text.substring(indexOf+2, text.length());
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format1, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[2]);
			default:
				break;
			}

			break;

		case Testo.BORG:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("\r\n", " ");
				text = text.replaceAll("Date", "");
				text = text.replaceAll(":", "");
				indexOf = text.indexOf("No ");
				text = text.substring(indexOf+2, text.length()).trim();
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format2, parser[2]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format2, parser[2]);
			default:
				break;
			}

			break;

		case Testo.IPER:

			switch (key) {
			case Testo.ORDINE:
				regex = "(.?)([0-9]+)(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
				break;
			case Testo.DATA_ORDINE:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format2, parser[2]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format2, parser[2]);
			default:
				break;
			}

			break;

		case Testo.RIALTO:

			switch (key) {
			case Testo.ORDINE:
				regex = "(.?)([0-9]+)(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[1]);
			default:
				break;
			}

			break;

		case Testo.RIAFRE:

			switch (key) {
			case Testo.ORDINE:
				text = text.replace("Ordine n.", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format2, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[1]);
			default:
				break;
			}

			break;

		case Testo.ECOR:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				text = text.replace("Numero ", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format2, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{2}/\\d{2}/\\d{2})";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[1]);
			default:
				break;
			}

			break;

		case Testo.PAC:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("[\n\r]", "");
				indexOf = text.indexOf(":");
				regex = "(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = text.substring(indexOf+1, indexOf+23).replace("FOR", "");
				dati[1] = parser[1];
				if (azienda.equals(Testo.AZIENDA_ANTICA)) {
					regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
					parser = execute(text, regex);
					dati[0] = parser[2].replace("Pdel", "").replace("LZ", "").replace("-", "");
				}
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{2}/\\d{2}/\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format2, parser[1]);
			default:
				break;
			}

			break;

		case Testo.UNICOOP_TIRRENO:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})(.*)(\\d{2}/\\d{2}/\\d{2})(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format1, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{2})(.*)(\\d{2}/\\d{2}/\\d{2})(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format1, parser[4]);
			default:
				break;
			}

			break;

		case Testo.UNICOOP:

			switch (key) {
			case Testo.ORDINE:
				text = text.replaceAll("ORDINE A FORNITORE", "");
				text = text.replaceAll("\r\n", "");
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = parser[3];
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[2];
			default:
				break;
			}

			break;

		case Testo.DAO:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format2, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = reformatData(format2, parser[2]);
			default:
				break;
			}

			break;

		case Testo.CONSORZIO_NORD_OVEST:

			switch (key) {
			case Testo.ORDINE:
				regex = "([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
				parser = execute(text, regex);
				dati[0] = parser[1];
				dati[1] = reformatData(format8, parser[3]);
				break;
			case Testo.DATA_CONSEGNA:
				regex = "(\\d{2}.\\d{2}.\\d{4})";
				parser = execute(text, regex);
				dati[0] = reformatData(format8, parser[1]);
			default:
				break;
			}

		default:
			break;
		}

		return dati;

	}

	public static String [] execute(String line, String regex){
		Matcher m = Pattern.compile(regex).matcher(line);
		String [] dati =  new String[10];
		if (m.find()) {

			int groupCount = m.groupCount();

			for (int i=0; i <= groupCount; i++){
				if (i==0)
					continue;
				dati[i] = m.group(i);
				logger.info(TAG, m.group(i));
			}


			//			logger.info(TAG, m.group(0));
			//			logger.info(TAG, m.group(1));
			//			logger.info(TAG, m.group(2));
			//			logger.info(TAG, m.group(3));
			//			dati[0] = m.group(1);
			//			dati[1] = m.group(3);
			//			dati[2] = m.group(2);
		}

		return dati;

	}


	public static String extractString(String azienda, String cliente, String key, String text) throws ParseException{
		String consegna = null;
		String[] split = new String[10];
		String[] split2 = new String[10];
		String[] words = new String[10];
		String real = null;

		switch (cliente) {
		case Testo.GUARNIER:
			split = text.split(" ");
			consegna = split[2];
			break;
		case Testo.TATO:
			consegna = Testo.BARLETTA;
			break;
		case Testo.EMMECI:
			words = new String[]{"EMMECI", "EFFELLE", "SPAZIO CONAD", "FORLIIMPOPOLI"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			if (consegna==null){
				consegna=Testo.EMMECI;
			}
			break;
		case Testo.UNICANT:
			words = new String[]{"SAN PIETRO IN GU"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			if (consegna==null){
				consegna=Testo.EMMECI;
			}
			break;
		case Testo.CONAD_ACS:
			words = new String[]{"S.SALVO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			if (consegna==null){
				consegna=Testo.EMMECI;
			}
			break;
		case Testo.AZIENDA_AMBROSI:
			consegna = text.trim();
			break;
		case Testo.ROWCLIFFE:
			consegna = Testo.ROWCLIFFE;
			break;
		case Testo.TRENTO:
			consegna = Testo.TRENTO;
			break;
		case Testo.UNICOMM:
			split = text.split(" ");
			consegna = split[3];
			break;
		case Testo.OTTAVIAN:
			split = text.split("\n");
			consegna = split[1];
			split2 = split[1].split(" ");
			if (split2.length>1){
				consegna = split2[1];
			}else {
				consegna = split2[0];
			}
			break;
		case Testo.APULIA:
			
			if (azienda.equals(Testo.AZIENDA_SOLIGO)) {
				split = text.split("\n");
				consegna = split[1];
				split2 = split[1].split(" ");
				if (split2.length>1){
					consegna = split2[4];
				}else {
					consegna = split2[0];
				}
			}
			if (azienda.equals(Testo.AZIENDA_TONIOLO)) {
				words = new String[]{"RUTIGLIANO"};
				real = null;
				for (String s: words){
					if (text.toUpperCase().replaceAll("[\\n\\r]", " ").contains(s)){
						real = s;
						break;
					}
				}
				consegna =  real;
			}
			
			break;
		case Testo.GARDERE:
			split = text.split("\n");
			split2 = split[1].split(" ");
			consegna = split2[2];
			break;
		case Testo.SIDAL:
			words = new String[]{"DEPOSITO GASTRONOMIA", "DEPOSITO GENERI VARI"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().replaceAll("[\\n\\r]", " ").contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.CONAD:
			//			split = text.split(" ");
			//			consegna = split[2];

			words = new String[]{"FORLI", "SCORZE", "FANO", "POZZUOLO", "MARTELLAGO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.CARREFOUR:
			words = new String[]{"S.PALOMBA (AW)", "MASSALENGO (AU)", "MASSALENGO (AT)", "AIROLA (BN)", "RIVALTA (AM)", "RIVALTA STOCK (AX)"};
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			if(real == null){
				consegna =  "";
				break;
			}

			if (real.trim().contains(Testo.SANTA_PALOMBA)){
				real = Testo.SANTA_PALOMBA_RIF;
			} else if (real.trim().contains(Testo.MASSALENGO_AT)) {
				return Testo.MASSALENGO_AT_RIF; 
			} else if (real.trim().contains(Testo.MASSALENGO_AU)) {
				return Testo.MASSALENGO_AU_RIF; 
			} else if (real.trim().contains(Testo.RIVALTA_AM)) {
				return Testo.RIVALTA_AM_RIF; 
			} else if (real.trim().contains(Testo.RIVALTA_AX)) {
				return Testo.RIVALTA_AX_RIF; 
			} else if (real.trim().contains(Testo.AIROLA_BN)) {
				return Testo.AIROLA_BN_RIF; 
			} 


			consegna =  real;
			break;
		case Testo.COOP:
			words = new String[]{"RIVALTA SCRIVIA", "MILANO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.CEDI_GROSS:
			words = new String[]{"ROMA", "CAMERATA"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.GARDENA:
			words = new String[]{"MOGLIANO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.ASPIAG:
			words = new String[]{"MONSELICE", "NOVENTA", "FERRARA", "PARMA", "OSPEDALETTO", "PARMA", "BOLOGNA", "TRIBANO", "CARTURA",
					"CERVIA", "CARPI", "CRESPELLANO", "IMOLA", "PADOVA", "PORDENONE",
					"VICENZA", "ALBIGNASEGO", "MESTRE", "MESTRINO", "CENTO", "CODIGORO", "LUGO", "ARGELATO", "MISANO", "MODENA", 
					"PORTO GARIBALDI", "REGGIO EMILIA", "RAVENNA"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.GEGEL:
			words = new String[]{"ARIELLI"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  Testo.ARIELLI;
			break;
		case Testo.SUPEREMME:
			words = new String[]{"SESTU"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.REALCO:
			words = new String[]{"REGGIO EMILIA"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.MULTICEDI:
			words = new String[]{"PASTORANO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.GRAMM:
			words = new String[]{"BOLZANO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.CATTEL:
			consegna =  Testo.CATTEL;
			break;

		case Testo.RAGIS:
			words = new String[]{"ACQUI TERME", "MILANO", "CHIERI", "ORBASSANO", "CAPRIATE"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.PAC2000:
			words = new String[]{"PONTE FELCINO", "CARINI" , "PERUGIA", "FIANO ROMANO", "TRENTOLA", 
					"CARINARO", "CORIGLIANO", "TERNI", "VITERBO", "ANZIO", "CORETTO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
			
		case Testo._2000PAC:
			words = new String[]{"MONTALTO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.CARAMICO:
			words = new String[]{"SALERNO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  Testo.SALERNO;
			break;

		case Testo.SUPERCENTRO:
			words = new String[]{"TARANTO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.SISA:
			words = new String[]{"GRICIGNANO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.CDC:
			consegna =  Testo.SANZA;
			break;

		case Testo.TAVELLA:
			consegna =  Testo.TAVELLA;
			break;
			
		case Testo.EURORISTORAZIONE:
			words = new String[]{"PIEVE DI SOLIGO", "SILEA", "PADOVA", "SAN DONA DI PIAVE", "VERONA", 
					"SAN VENDEMIANO", "PESCANTINA", "LIDO DI VENEZIA", "VENEZIA", "TREVISO", "PONTE DI PIAVE",
					"GARBAGNATE", "TORRI DI QUARTESOLO", "MASERADA", "TEZZE SUL BRENTA", "PORTOGRUARO", "LEGNARO", "BASSANO DEL GRAPPA"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.MARR:
			words = new String[]{"47921", "20090", "39040", "32040", "39049", "30028", "09010", "38062", "27010", "00060", 
					"51032", "57037", "00156", "47921", "47838", "56023", "00040", "10156", "61012", "31015", 
					"70043", "40011", "87019", "80026", "16042", "36078", "47922", "47924", "65010", "47924", "47922", "95121", "24040"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.WOERNDLE:
			//			consegna =  Testo.WOERNDLE;
			//			break;
			words = new String[]{"BOZEN", "CASTENEDOLO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.CEDI_SIGMA:
			consegna =  Testo.AVERSA;
			break;

		case Testo.ISA:
			words = new String[]{"VILLACIDRO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.MIGROSS:
			words = new String[]{"BUSSOLENGO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.CSS:
			words = new String[]{"VILLACIDRO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.TOSANO:
			words = new String[]{"CEREA"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.VALFOOD:
			words = new String[]{"CORATO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.BRIMI:
			consegna =  Testo.BRIMI;
			break;
		case Testo.IPERAL:
			words = new String[]{"PIANTEDO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.ERGON:
			words = new String[]{"BELPASSO", "RAGUSA"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.DADO:
			words = new String[]{"LUPARI"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.CONAD_FRE:
			words = new String[]{"MONTOPOLI"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.CEDI:
			words = new String[]{"CAMERATA", "ROMA"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;	

		case Testo.HAMBERGER:
			consegna = Testo.HAMBERGER;
			break;

		case Testo.ROSSETTO:
			words = new String[]{"BIGARELLO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.LADISA:
			words = new String[]{"16157", "10135", "70100", "16141", "20864", "33070", "00155", "16165"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.ROSSI:
			consegna =  Testo.ROSSI;
			break;
		case Testo.GARBELOTTO:
			consegna =  Testo.GARBELOTTO;
			break;
		case Testo.SAN_FELICI:
			consegna =  Testo.SAN_FELICI;
			break;
		case Testo.OPRAMOLLA:
			words = new String[]{"SERRE"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;	
		case Testo.CENTRALE_LATTE:
			words = new String[]{"TORINO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;	
		case Testo.CAFORM:
			words = new String[]{"THIENE"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;	
		case Testo.BERETTA:
			words = new String[]{"CARESANABLOT"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;	
		case Testo.DECARNE:
			words = new String[]{"BITONTO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;	
		case Testo.RETAILPRO:
			consegna =  Testo.RETAILPRO;
			break;	
		case Testo.BOCON:
			consegna =  Testo.BOCON;
			break;	
		case Testo.COAL:
			words = new String[]{"CAMERANO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.GS:
			words = new String[]{"VIGLIANO" , "TORINO" , "NOVARA", "ALESSANDRIA" , "BUROLO", "POLLEIN", "ACQUI TERME", "VADO LIGURE", "TORINO" , "VIGEVANO" , "NICHELINO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.PREGIS:
			words = new String[]{"SAN VITO", "SAN BONIFACIO", "CESENA" , "SANT'OMERO" , "CHIESINA", "GALLIATE", "CAPRIATE", "RIVA DEL GARDA"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.DAC:
			words = new String[]{"FLERO", "SAVIO", "VILLAFRANCA", "IDEM", "IMMEDIATA"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			if (real.trim().equals(Testo.IDEM) || real.trim().contains(Testo.IMMEDIATA)){
				consegna =  Testo.FLERO;
			} else {
				consegna =  real;	
			}

			break;
		case Testo.VIVO:
			words = new String[]{"GORIZIA", "MONTEBELLUNA", "PORDENONE" , "MARTIGNACCO", "CUSSIGNACCO", "LIGNANO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.MAIORA:
			words = new String[]{"CORATO", "ARGENTANO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.MODERNA:
			words = new String[]{"AVERSA"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.MOSCA:
			words = new String[]{"MELZO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.GANASSA:
			consegna =  Testo.GANASSA;
			break;
		case Testo.DCFOOD:
			words = new String[]{"CASAMASSIMA"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;	
		case Testo.CRAI:
			words = new String[]{"TIRRENO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;

			if(consegna==null) {
				words = new String[]{"ORISTANO"};
				real = null;
				for (String s: words){
					if (text.toUpperCase().contains(s)){
						real = s;
						break;
					}
				}
				consegna =  real;
			}
			break;

		case Testo.CRAI_FRE:
			words = new String[]{"LEINI"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.SIAF:
			words = new String[]{"NICCHERI"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;	

		case Testo.VEGA:
			words = new String[]{"MOGLIANO", "SAN STINO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;	
		case Testo.TIGROS:
			words = new String[]{"SOLBIATE"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;	
		case Testo.LANDO_FRE:
			words = new String[]{"CAZZAGO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;	
		case Testo.IGES:
			words = new String[]{"RIANO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;	
		case Testo.NUME:
			words = new String[]{"GENOVA"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.SOGEGROSS:
			//			words = new String[]{"BOLZANETO", "CHIUSI", "ARMA", "CASALE", "CERNUSCO", "NOVARA", "LUSIGNANO", "CAMPOROSSO", "CARASCO", "BOLOGNA", "BADESSE", "RIVOLI", "ALESSANDRIA", "REGGIO EMILIA", "ALBISOLA", "GENOVA", "ASTI", "BAGNO", "BUSALLA", "COSTA", "VALBISAGNO", "DALMINE", "SAMPIERDARENA", "MONTANO", "IMOLA", "ALBENGA"};
			//			real = null;
			//			for (String s: words){
			//				if (text.toUpperCase().contains(s)){
			//					real = s;
			//					break;
			//				}
			//			}
			consegna = getNumberFromString(text);
			if (consegna.isEmpty()){
				consegna = Testo.CEDEP;
			}
			break;	
		case Testo.HEIDERBECK:
			if (consegna==null)
				consegna = Testo.HEIDERBECK;
			break;
		case Testo.PRIX:
			words = new String[]{"GRISIGNANO"};

			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.CIRFOOD:
			if (consegna==null)
				consegna = Testo.CIRFOOD;
			break;
		case Testo.ELIOR:
			words = new String[]{"MONFUMO", "SCORZE'", "OLMI", "CORNUDA", "PIACENZA", "NOVENTA", "ORMELLE", "MOTTA"};

			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.MADIA:
			if (consegna==null)
				consegna = Testo.MADIA;
			break;
		case Testo.GASTRO:
			if (consegna==null)
				consegna = Testo.GASTRO;
			break;
		case Testo.QUESOS:
			words = new String[]{"ALCAMPO", "ALDANONDO", "MAQUESA", "SKI", "LACTOMAJERID", "LACTOMAJ"};
			real = Testo.ALCAMPO;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					if(s.equals("LACTOMAJ")){
						real = "LACTOMAJERID";
					}
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.LAGOGEL:
			words = new String[]{"VAREDO"};

			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.ALFI:
			words = new String[]{"CASALNOCETO"};

			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.ALI:
			words = new String[]{"PADOVA"};

			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.CAMST:
			words = new String[]{"BENTIVOGLIO"};

			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.PAOLINI:
			consegna =  Testo.PAOLINI;
			break;
		case Testo.SALAROMEO:
			consegna =  Testo.SALAROMEO;
			break;
		case Testo.VALSANA:
			consegna =  Testo.VALSANA;
			break;
		case Testo.SELECTA:
			consegna =  Testo.SELECTA;
			break;
		case Testo.BRUNELLO:
			consegna =  Testo.BRUNELLO;
			break;
		case Testo.SICONAD:
//			words = new String[]{"GABICCE MARE", "PUNTA MARINA", "RAVENNA", "CESENA", "CERVIA", 
//					"FAENZA", "RUSSI", "CASTEL BOLOGNESE", "FORLI", "MODIGLIANA",
//					"PINARELLA", "LUGO", "BAGNACAVALLO", "BRISIGHELLA", "RIOLO TERME", "PREDAPPIO", "SAN PIETRO IN VINCOLI", "RIMINI"};
//
//			for (String s: words){
//				if (text.toUpperCase().contains(s)){
//					real = s;
//					break;
//				}
//			}
			consegna =  text.trim();
			break;
		case Testo.BENNET:
			words = new String[]{"SAN BELLINO", "TURATE", "CALCINATE"};

			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.DIMAR:
			words = new String[]{"PALMERO"};

			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.UNES:
			words = new String[]{"VIMODRONE"};

			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			if (consegna==null){
				consegna = Testo.VIMODROME;
			}
			break;
		case Testo.SERENISSIMA:
			words = new String[]{"UNIVERSITA","GRAZIE","RODOLFI","BRENDOLA","CONCORDIA","1� STRADA","NOVEMBRE","SAVONAROLA","CHIMICA","CARTIEA","LUZZATI","LAVERDA","GAMBERO","ARTIGIANATO","ALESSIO","VINCENZO","BENZI","VITO","CASTELLO","PONTEDERA","NATISONE","MORO","MADONNA","BISSOLATI","SURCEZE","MAGELLANO","GARZIERE","PILA","COLONIA","BORASIO","RODOLFI","SIDERURGIA","SARTORI","LOTTI","ALFIERI","GAGLIARDI","FILIPPI","SALVAGNINI","VERONESE","LAMARMORA","BALLA","CHIUSAFORTE","SPALTI","CARDO","FIUME","PERTINI","FORLANINI","MONTANARA","PAVERANO","FONTANELLE","LAMBRANZI","GIANELLA","SOPRA","ALPO","CHIMICA","GOTTARDO","BARBE","TRENTO","SIDERURGIA","CROCIANO","GALILEO","VERROTTI", "VALBELLUNA"};

			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.SABELLI:
			words = new String[]{"ROMA"};

			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.MAXIDI:
			words = new String[]{"SANSEPOLCRO", "BELFIORE", "SPINETTA", "VERCELLI", "SAN GIOVANNI"};

			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.GMF:
			words = new String[]{"PERUGIA"};

			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.GARDINI:
			consegna =  Testo.GARDINI;
			break;
		case Testo.EUROSPIN:
		case Testo.EUROSPIN_TIRRENICA:
		case Testo.EUROSPIN_LAZIO:
			text = text.replace(" ", "");
			words = new String[]{"CHIONS", "S.MARTINO", "ROMENTINO", "MAGIONE", "CALCINATE", "POMEZIA", "APRILIA", "FARAINSABINA"};

			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
		case Testo.VIS_FOOD:
			consegna =  text.trim();
			break;
		case Testo.NOSAWA:
			consegna =  Testo.NOSAWA;
			break;
		case Testo.SAIT:
			consegna =  Testo.SAIT;
			break;
		case Testo.GEMOS:
			consegna =  Testo.GEMOS;
			break;
		case Testo.BAUER:
			consegna =  Testo.BAUER;
			break;
		case Testo.WEIHE:
			consegna =  Testo.WEIHE;
			break;
		case Testo.EMMI:
			split = text.split("\n");
			if (text.contains(Testo.KERAVA)){
				consegna = Testo.KERAVA;	
			}
			if (text.contains(Testo.STOCKHOLM)){
				consegna = Testo.STOCKHOLM;	
			}
			if (text.contains(Testo.HELSINKI)){
				consegna = Testo.HELSINKI;	
			}

			break;
		case Testo.COOP_VA:
			split = text.split("\n");
			String newText = StringUtils.stripAccents(text);
			if (newText.toUpperCase().contains(Testo.FORLI)){
				consegna = Testo.FORLI;	
			}
			if (newText.toUpperCase().contains(Testo.REGGIO_EMILIA)){
				consegna = Testo.REGGIO_EMILIA;	
			}
			if (newText.toUpperCase().contains(Testo.ANAGNI)){
				consegna = Testo.ANAGNI;	
			}
			if (newText.toUpperCase().contains(Testo.RUTIGLIANO)){
				consegna = Testo.RUTIGLIANO;	
			}
			if (newText.toUpperCase().contains(Testo.UDINE)){
				consegna = Testo.UDINE;	
			}
			if (newText.toUpperCase().contains(Testo.CATANZARO)){
				consegna = Testo.CATANZARO;	
			}
			if (newText.toUpperCase().contains(Testo.MOLFETTA)){
				consegna = Testo.MOLFETTA;	
			}
			if (newText.toUpperCase().contains(Testo.CATANIA)){
				consegna = Testo.CATANIA;	
			}
			if (newText.toUpperCase().contains(Testo.SAN_DONA)){
				consegna = Testo.SAN_DONA;	
			}
			if (newText.toUpperCase().contains(Testo.SAN_VITO)){
				consegna = Testo.SAN_VITO;	
			}
			break;
		case Testo.GEIA:
			split = text.split("\n");
			text = text.replaceAll("\\W", ""); 
			if (text.toUpperCase().contains(Testo.REITAN)){
				consegna = Testo.REITAN;	
			} else if (text.toUpperCase().contains(Testo.NAGEL)){
				consegna = Testo.NAGEL;	
			} else if (text.toUpperCase().contains(Testo.BRONDY_NO_ACCENT)){
				consegna = Testo.BRONDY;	
			} else if (text.toUpperCase().contains(Testo.SALLING)){
				consegna = Testo.SALLING;	
			}
			if(consegna == null){
				consegna = Testo.REITAN;
			}

			break;
		case Testo.CHASAL:
			if (text.toUpperCase().contains(Testo.MARCINELLE)){
				consegna = Testo.MARCINELLE;	
			}  
			break;
		case Testo.ITALBRIX:
			consegna = Testo.ITALBRIX;	
			break;
		case Testo.SICLA:
			for (String line : text.split("\n|\r")) {
				if (line.toUpperCase().trim().contains(Testo.CODICE_CLIENTE)) {
					consegna = StringUtils.getDigits(line);	
					break;
				}
			}
			break;
		case Testo.MORGESE:
			if (text.toUpperCase().contains(Testo.PORTICI)){
				consegna = Testo.PORTICI;	
			}  
			break;
		case Testo.ARENA:
			if (text.toUpperCase().contains(Testo.VALGUARNERA)){
				consegna = Testo.VALGUARNERA;	
			}  
			break;
		case Testo.CIRCLEVIEW:
			consegna = Testo.CIRCLEVIEW;	
			break;
		case Testo.RIMI:
			if (text.toUpperCase().contains(Testo.RIMI_LATVIA)){
				consegna = Testo.RIMI_LATVIA;	
			}  
			break;

		case Testo.VLEESWAREN:
			split = text.split("\n");
			if (text.toUpperCase().contains(Testo.DEPUYDT)){
				consegna = Testo.DEPUYDT;	
			}
			break;

		case Testo.EURO:
			split = text.split("\n");
			if (text.toUpperCase().contains(Testo.EURO_FRESH_FOOD)){
				consegna = Testo.EURO;	
			}
			break;

		case Testo.NADIA:
			split = text.split("\n");
			if (text.toUpperCase().contains(Testo.NADIA)){
				consegna = Testo.NADIA;	
			}
			break;

		case Testo.DA_VINCI:
			split = text.split("\n");
			if (text.toUpperCase().contains(Testo.DA_VINCI_FOOD)){
				consegna = Testo.DA_VINCI;	
			}
			break;

		case Testo.CARNIATO:
			split = text.split("\n");
			if (text.toUpperCase().contains(Testo.CARNIATO)){
				consegna = Testo.CARNIATO;	
			}
			break;

		case Testo.SEVEN:
			split = text.split("\n");
			if (text.contains(Testo.BLUE)){
				consegna = Testo.BLUE;	
			}
			break;

		case Testo.BORG:
			split = text.split("\n");
			if (text.toUpperCase().contains(Testo.VALLETTA)){
				consegna = Testo.VALLETTA;	
			}
			break;

		case Testo.TOCAL:
			consegna = Testo.TOCAL;	
			break;

		case Testo.IPER:
			words = new String[]{"IPER LAME", "IPER BORGO", "IPER NOVA", "IPER S.BENEDETTO", "IPER RIMINI", "IPER PIAVE", "IPER LUGO", "IPER IMOLA", "IPER PESARO", "IPER CENTRO BORGO", "IPER RAVENNA", "IPER SENIGALLIA", "IPER CESENA", "IPER CAMPOLUNGO", "IPER SCHIO", "IPER FAENZA", "IPER VIGONZA", "IPER CONEGLIANO", "IPER CHIOGGIA", "IPER ASCOLI", "IPER ABRUZZO", "IPERCOOP BORGONUOVO"};

			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}

			if (real.contains("IPER BORGO")){
				real = "BOLOGNA";
			} else if (real.contains("IPER NOVA")){
				real = "VILLANOVA";
			} else if (real.contains("IPER LAME")){
				real = "CASTENASO";
			} else if (real.contains("IPER S.BENEDETTO")){
				real = "SAN BENEDETTO";
			} else if (real.contains("IPER RIMINI")){
				real = "RIMINI";
			} else if (real.contains("IPER PIAVE")){
				real = "SAN DONA";
			} else if (real.contains("IPER LUGO")){
				real = "LUGO";
			}else if (real.contains("IPER IMOLA")){
				real = "IMOLA";
			} else if (real.contains("IPER PESARO")){
				real = "PESARO";
			} else if (real.contains("IPER CENTRO BORGO")){
				real = "BORGO";
			} else if (real.contains("IPER RAVENNA")){
				real = "RAVENNA";
			} else if (real.contains("IPER SENIGALLIA")){
				real = "SENIGALLIA";
			} else if (real.contains("IPER CESENA")){
				real = "CESENA";
			} else if (real.contains("IPER CAMPOLUNGO")){
				real = "CAMPOLUNGO";
			} else if (real.contains("IPER SCHIO")){
				real = "SCHIO";
			} else if (real.contains("IPER FAENZA")){
				real = "FAENZA";
			} else if (real.contains("IPER VIGONZA")){
				real = "VIGONZA";
			} else if (real.contains("IPER CONEGLIANO")){
				real = "CONEGLIANO";
			} else if (real.contains("IPER CHIOGGIA")){
				real = "CHIOGGIA";
			}  else if (real.contains("IPER ASCOLI")){
				real = "ASCOLI";
			}  else if (real.contains("IPER ABBRUZZO")){
				real = "SAN GIOVANNI";
			} 

			consegna =  real;
			break;

		case Testo.COOP_ALLEANZA:
			//			words = new String[]{"TRIESTE", "REGGIO EMILIA", "PARMA", "VIRGILIO", "MANTOVA", "SUZZARA", "PORDENONE", "PIACENZA", "MUGGIA", "PARMA", "VILLESSE", "RAGUSA", "MILAZZO", "GRAVINA", "PALERMO", "SAN GIOVANNI", "TREMESTIERI"};
			//			real = null;
			//			for (String s: words){
			//				if (text.toUpperCase().contains(s)){
			//					real = s;
			//					break;
			//				}
			//			}
			//			consegna =  real;
			// 			break;

			words = new String[]{"IPERCOOP ARIOSTO", "IPERCOOP CENTROTORRI", "IPERCOOP VIRGILIO", "IPERCOOP FAVORITA", "IPERCOOP SUZZARA", "IPERCOOP MEDUNA", "IPER GOTICO", "IPERCOOP MONTEDORO", "IPERCOOP EUROSIA", "IPERCOOP TIARE", 
					"IPER TORRI D'EUROPA", "IPERCOOP BARAGALLA", "IPERCOOP RAGUSA", "IPERCOOP MILAZZO", "IPERCOOP KATANE", "IPERCOOP ROCCELLA", "IPERCOOP BORGONUOVO", "IPERCOOP LE ZAGARE", "IPERCOOP LE GINESTRE", "IPERCOOP GRAVINA"};

			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}

			if (real.contains("IPERCOOP ARIOSTO")){
				real = "REGGIO EMILIA1";
			} else if (real.contains("IPERCOOP CENTROTORRI")){
				real = "PARMA1";
			} else if (real.contains("IPERCOOP VIRGILIO")){
				real = "VIRGILIO";
			} else if (real.contains("IPERCOOP FAVORITA")){
				real = "MANTOVA";
			} else if (real.contains("IPERCOOP SUZZARA")){
				real = "SUZZARA";
			} else if (real.contains("IPERCOOP MEDUNA")){
				real = "PORDENONE";
			} else if (real.contains("IPER GOTICO")){
				real = "PIACENZA";
			}else if (real.contains("IPERCOOP MONTEDORO")){
				real = "MUGGIA";
			} else if (real.contains("IPERCOOP EUROSIA")){
				real = "PARMA2";
			} else if (real.contains("IPERCOOP TIARE")){
				real = "VILLESSE";
			} else if (real.contains("IPER TORRI D'EUROPA")){
				real = "TRIESTE";
			} else if (real.contains("IPERCOOP BARAGALLA")){
				real = "REGGIO EMILIA2";
			} else if (real.contains("IPERCOOP RAGUSA")){
				real = "RAGUSA";
			} else if (real.contains("IPERCOOP MILAZZO")){
				real = "MILAZZO";
			} else if (real.contains("IPERCOOP KATANE")){
				real = "GRAVINA DI CATANIA";
			} else if (real.contains("IPERCOOP ROCCELLA")){
				real = "PALERMO1";
			} else if (real.contains("IPERCOOP BORGONUOVO")){
				real = "PALERMO2";
			} else if (real.contains("IPERCOOP LE ZAGARE")){
				real = "SAN GIOVANNI";
			} else if (real.contains("IPERCOOP LE GINESTRE")){
				real = "TREMESTIERI";
			} else if (real.contains("IPERCOOP GRAVINA")){
				real = "GRAVINA";
			} 

			consegna =  real;
			break;
			
		case Testo.COOP_ALLEANZA_SIC:
 
			text = text.replaceAll("FAENZA\\(", "");
			
			for (String s: words){
				if (text.toUpperCase().contains("CAPUCCINI")){
					real = "CAPUCCINI";
					break;
				}
				if (text.toUpperCase().contains("IL BORGO")){
					real = "IL BORGO";
					break;
				}
			}
			
			if (real==null) {
				words = new String[]{"ALFONSINE", "FAENZA", "FAENZA-IL BORGO", "RUSSI I PORTICI",
						"FAENZA CAPUCCINI", "LAVEZZOLA", "LUGO", "FORLIREPUBBLICA NEW", "RAVENNA TEODORA", "FORLI I PORTICI"};

				real = null;
				for (String s: words){
					if (text.toUpperCase().contains(s)){
						real = s;
						break;
					}
				}
			}
		
			consegna =  real;
			break;

		case Testo.IPERCONAD:
		case Testo.RDAVENETA:
		case Testo.SGR_IPER:
		case Testo.LEGNARO:
			words = new String[]{"SAN BIAGIO", "MESTRE", "PADOVA", "PORTOGRUARO", "LEGNARO", "BUSSOLENGO", "TORREANO", "PIOVE DI SACCO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.BIOLOGICA:
			//			consegna =  text.split("\\(")[0];
			words = new String[]{"QUARTO", "BOLOGNA", "VEICOLATE", "CUCINA GE  L.CENTRO EST", "CUCINA GE LOTTO CENTRO OVEST"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			if (real.contains("CUCINA GE  L.CENTRO EST")){
				real = "CUCINA GE EST";
			}
			if (real.contains("CUCINA GE LOTTO CENTRO OVEST")){
				real = "CUCINA GE OVEST";
			}
			consegna =  real;
			break;

		case Testo.UNICOOP:
			words = new String[]{"FIRENZE"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.COOP_ALLEANZA_30:
			words = new String[]{"FORMIGINE"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.MARTINELLI:
			words = new String[]{"VILLAFRANCA"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.UNICOOP_TIRRENO:
			words = new String[]{"LINEA IPER", "LINEA SUPER", "PTF LATTICINI"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.MEGAMARK:
			words = new String[]{"BARI", "GRICIGNANO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.BRENDOLAN:
			words = new String[]{"ROVEREDO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.METRO:
			words = new String[]{"BOLZANO", "LANA", "TRENTO", "BRUNICO", "BRATISLAVA"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.LANDO:
			words = new String[]{"PIANIGA", "NOALE", "ROMEA", "PREGANZIOL", "ROSOLINA", "GAMBARE", "CONSELVE", "CAMPOSAMPIERO", "PADOVA", "SUSEGANA",  "TREVISO", "VIGONZA", "MIRANO", "VEGGIANO", "CITTADELLA", "MESTRE"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.DAO:
			consegna = Testo.DAO;	
			break;


		case Testo.ROMA:
			text = text.toUpperCase().replaceAll("\\W", "X");
			text = text.replace("X", " ");
			text = text.replace("_", " ");
			text = text.replace("COD", " ");
 			
			try {
				String splitX[] = text.trim().split("\\s+");
				consegna = splitX[splitX.length-1];
				if (!isNumeric(consegna) || consegna.trim().length()<6){
					consegna = splitX[splitX.length-2];
				}
				String consegnaX = splitX[0];
				if (!isNumeric(consegna)){
					consegna = splitX[0];
				}
				if (!isNumeric(consegna) && isNumeric(consegnaX)){
					consegna = splitX[0];
				}

			} catch (Exception e) {
				consegna = text.toUpperCase().replaceAll("\\W", "");	
			}
			
			if (!isNumeric(consegna)){
				consegna = "000000"; 
			}

			break;

		case Testo.CONSORZIO_NORD_OVEST:
			words = new String[]{"RIVALTA", "COGOLETO", "VERCELLI", "SERRAVALLE", "PIEVE"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.RIALTO:
			words = new String[]{"BRESSO"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.RIAFRE:
			consegna =  Testo.RIALTO;
			break;

		case Testo.ECOR:
			consegna =  Testo.ECOR;
			break;

		case Testo.HAMMUNCHEN:
			words = new String[]{"SIEGMUND", "FRIEDENSTR", "ERNA-SAMUEL-STR"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.PAC:
			words = new String[]{"CALABRIA", "CASERTA", "FIANO", "PERUGIA"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		case Testo.RESINELLI:
			words = new String[]{"CASTIONE"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;
			
		case Testo.CUCINA_NOSTRANA:
			consegna =  Testo.CUCINA_NOSTRANA;
			break;

		case Testo.VISOTTO:
		case Testo.VISOTTO2:
			words = new String[]{"MOTTA", "UDINE", "SANTA LUCIA", "PORTOGRUARO", "CECCHINI", "CORDENONS", "SAN STINO", "CEGGIA", "BUDOIA", "VILLOTTA", "BUJA", "BIBIONE", "MEDUNA", "SAN POLO", "JESOLO", "TREVISO", "CAPPELLA" ,"PONTE DI PIAVE", "ODERZO", "PORDENONE", "MASERADA"};
			real = null;
			for (String s: words){
				if (text.toUpperCase().contains(s)){
					real = s;
					break;
				}
			}
			consegna =  real;
			break;

		default:
			consegna = text;
			break;
		}
		return consegna;
	}



	public static String reformatData(SimpleDateFormat fromFormat, String originDate){
		String reformatteDate = null;
		try {
			reformatteDate = format2.format(fromFormat.parse(originDate.trim()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reformatteDate;
	}

	private static String leftPadding(String data){
		String newData = StringUtils.leftPad(data, 10, "0");
		newData = newData.replace("-", "/");
		newData = newData.replace(".", "/");
		return newData;
	}

	private static  List<String> searchMutipleOrder(String s, String search1, String search2){
		List<String> lines = new ArrayList<String>();

		if (search1 != null && search2 != null) {
			for (String line : s.split("\\n+")) {
				if (line.toUpperCase().trim().contains(search1) || (line.toUpperCase().trim().contains(search2))){
					lines.add(line.replaceAll("\\D+",""));
				}

				String regex = "(\\d{1,2}. (?:jan(?:uar)?|feb(?:ruar)?|mar(?:z)?|apr(?:il)?|mai|jun(?:i)?|jul(?:i)?|aug(?:ust)?|sep(?:tember)?|oct(?:ober)?|(nov|dec)(?:ember)?) \\d{4})(.*)";
				String [] parser = execute(line, regex);
				if (parser[1] == null){
					regex = "(\\d{1,2}. (?:jan(?:uar)?|feb(?:ruar)?|mart(?:s)?|apr(?:il)?|maj|jun(?:i)?|jul(?:i)?|aug(?:ust)?|sep(?:tember)?|okt(?:ober)?|(nov|dec)(?:ember)?) \\d{4})(.*)";
					parser = execute(line, regex);
					if (parser[1] != null){
						lines.add(line);	
					}
				} else {
					//					String data = reformatData(formatCustomDK, parser[1]);
					lines.add(line);
				}

			}

		}
		return lines;

	}

	private static  List<String> searchOrdineByLine(String s, String search1){
		List<String> lines = new ArrayList<String>();

		if (search1!=null){
			for (String line : s.split("\\n+")) {
				if (line.toUpperCase().trim().contains(search1)){

					String regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
					String [] parser = execute(line, regex);
					if (parser[1] == null){
						regex = "([0-9]+)(.*)(\\d{1,2}/\\d{2}/\\d{2})(.*)";
						parser = execute(line, regex);
					}
					lines.add(parser[1]);
					lines.add(reformatData(format1, parser[3]));
					break;
				}
			}

		}
		return lines;
	}

	public static boolean isNumeric(String str) {  
		try {   
			double d = Double.parseDouble(str);  
		}  
		catch(NumberFormatException nfe) {  
			return false;  
		}  
		return true;  
	}

	public static String getNumberFromString(String strValue){
		String str = strValue.trim();
		String digits="";
		for (int i = 0; i < str.length(); i++) {
			char chrs = str.charAt(i);              
			if (Character.isDigit(chrs))
				digits = digits+chrs;
		}
		return digits;
	}

}
