package com.econorma.main;

import java.util.Map;

import org.jdesktop.application.SingleFrameApplication;

import com.econorma.dao.DAO;
import com.econorma.dao.DatabaseManager;
import com.econorma.extract.pdf.ExtractPdf;
import com.econorma.logic.Reader;
import com.econorma.properties.OrdersProperties;
import com.econorma.testo.Testo;
import com.econorma.util.Logger;
 

public class Application extends SingleFrameApplication{

	private static final Logger logger = Logger.getLogger(ExtractPdf.class);
	private static final String TAG = "ExtractPDF";
	
	private DatabaseManager sqliteManager;
	private DatabaseManager as400Manager;
	private DAO dao;
	private String azienda;
	private String host_as400;
	private String user_as400;
	private String password_as400;
	private String library_as400;
	private int sub_days;
	
	public void init()  {
		Map<String, String> map = OrdersProperties.getInstance().load();
		host_as400 = (String) map.get("host_as400");
		user_as400 = (String) map.get("user_as400");
		password_as400 = (String) map.get("password_as400");
		library_as400 = (String) map.get("library_as400");
		sub_days = 	Integer.parseInt(map.get("sub_days"));
		
		sqliteManager = new DatabaseManager(Testo.SQLITE, azienda);
		as400Manager = new DatabaseManager(Testo.AS400, azienda);
		dao = DAO.create(sqliteManager, as400Manager);
		sqliteManager.init();
		as400Manager.init();
		dao.init(); // after db
		
		try {
			Reader reader = new Reader(dao);
			reader.run(azienda);
		} catch (Exception e) {
			logger.error(TAG, "Exception: " + e);
		} 
		
		exit();
 
	}
	
	
	
	@Override
	protected void initialize(String[] args) {
		azienda = args[0];
		super.initialize(args);
	}



	public static synchronized Application getInstance() {
		return (Application) org.jdesktop.application.Application.getInstance();
	}

	public DatabaseManager getSqliteManager() {
		return sqliteManager;
	}

	public void setSqliteManager(DatabaseManager sqliteManager) {
		this.sqliteManager = sqliteManager;
	}

	public DAO getDao() {
		return dao;
	}

	public void setDao(DAO dao) {
		this.dao = dao;
	}
	
	@Override
	protected void shutdown() {
 	 		System.exit(1);
	}

	@Override
	protected void startup() {
		init();
	}

	public String getHost_as400() {
		return host_as400;
	}

	public String getUser_as400() {
		return user_as400;
	}

	public String getPassword_as400() {
		return password_as400;
	}

	public String getLibrary_as400() {
		return library_as400;
	}

	public int getSub_days() {
		return sub_days;
	}

	public void setSub_days(int sub_days) {
		this.sub_days = sub_days;
	}
	
}
