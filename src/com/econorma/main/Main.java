package com.econorma.main;

import java.io.File;

import com.econorma.properties.OrdersProperties;
import com.econorma.util.Logger;
 

public class Main {

	private static final Logger logger = Logger.getLogger(Main.class);
	private static final String TAG = "Main";
	
	public static void main(String[] args) {
		  
		if (args.length == 0) {
			logger.error(TAG, "Azienda non esistente");
			System.exit(3);
		}
		logger.info(TAG, "Azienda: " + args[0]);
		File f = new File("application.properties");
		if(!f.exists()) { 
			OrdersProperties.getInstance().create();
		}
		  
		Application.launch(Application.class, args);
		
	}
	
}
