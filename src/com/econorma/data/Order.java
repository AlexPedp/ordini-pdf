package com.econorma.data;

import java.util.Date;

public class Order {
	
	String numero;
	Date data;
	String cliente;
	String giro;
	String stato;
	String riferimentoOrdine;
	String nomeFile;
	String nomePdf;
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getGiro() {
		return giro;
	}
	public void setGiro(String giro) {
		this.giro = giro;
	}
	public String getStato() {
		return stato;
	}
	public void setStato(String stato) {
		this.stato = stato;
	}
	public String getRiferimentoOrdine() {
		return riferimentoOrdine;
	}
	public void setRiferimentoOrdine(String riferimentoOrdine) {
		this.riferimentoOrdine = riferimentoOrdine;
	}
	public String getNomeFile() {
		return nomeFile;
	}
	public void setNomeFile(String nomeFile) {
		this.nomeFile = nomeFile;
	}
	public String getNomePdf() {
		return nomePdf;
	}
	public void setNomePdf(String nomePdf) {
		this.nomePdf = nomePdf;
	}
	
}
