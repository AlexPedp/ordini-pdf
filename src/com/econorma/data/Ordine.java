package com.econorma.data;

public class Ordine {
	
	private String azienda;
	private String cliente;
	private String consegna;
	private String spedizione;
	private String ordine;
	private String data;
	private String content;
	
	public String getAzienda() {
		return azienda;
	}
	public void setAzienda(String azienda) {
		this.azienda = azienda;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getConsegna() {
		return consegna;
	}
	public void setConsegna(String consegna) {
		this.consegna = consegna;
	}
	public String getOrdine() {
		return ordine;
	}
	public void setOrdine(String ordine) {
		this.ordine = ordine;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getSpedizione() {
		return spedizione;
	}
	public void setSpedizione(String spedizione) {
		this.spedizione = spedizione;
	}
	
}
