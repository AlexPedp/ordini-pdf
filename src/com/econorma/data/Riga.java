package com.econorma.data;

import com.econorma.testo.Testo;

public class Riga {

	public String cliente;
	public String tipo;
	public String numero;
	public String data;
	public String spedizione;
	public String consegna;
	public String articolo;
	public String descrizione;
	public String quantita;
	public String prezzo;
	public String um;
	public String note;
	public String um2;
	public String lotto;
	
	public String getDeposito() {
		return spedizione;
	}
	public void setDeposito(String spedizione) {
		this.spedizione = spedizione;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	 
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getConsegna() {
		return consegna;
	}
	public void setConsegna(String consegna) {
		this.consegna = consegna;
	}
	public String getArticolo() {
		return articolo;
	}
	public void setArticolo(String articolo) {
		this.articolo = articolo;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getQuantita() {
		return quantita;
	}
	public void setQuantita(String quantita) {
		this.quantita = quantita;
	}
	public String getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(String prezzo) {
		this.prezzo = prezzo;
	}
	public String getSpedizione() {
		return spedizione;
	}
	public void setSpedizione(String spedizione) {
		this.spedizione = spedizione;
	}
	public String getUm() {
		if (um!=null && !um.isEmpty()){
			return um;
		 }else{
			 return "";
		}
					
	}
	public void setUm(String um) {
		this.um = um;
	}
	
	public String getNote() {
		if (note!=null && !note.isEmpty()){
			return note;
		 }else{
			 return "";
		}
					
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	public String getUm2() {
		if (um2!=null && !um2.isEmpty()){
			return um2;
		 }else{
			 return "";
		}
					
	}
	public void setUm2(String um2) {
		this.um2 = um2;
	}
	
	public String getTipo() {
		if (tipo==null)
			return Testo.ATTIVO;
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getLotto() {
		if (lotto!=null && !lotto.isEmpty()){
			return lotto;
		 }else{
			 return "";
		}
					
	}
	public void setLotto(String lotto) {
		this.lotto = lotto;
	}
	
}
