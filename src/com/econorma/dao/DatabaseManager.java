package com.econorma.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

import com.econorma.main.Application;
import com.econorma.properties.OrdersProperties;
import com.econorma.testo.Testo;
import com.econorma.util.Logger;
 
 
 

public class DatabaseManager {

	private static DatabaseManager manager;
	private String type;
	private String azienda;
	
	private static final Logger logger = Logger.getLogger(DatabaseManager.class);
	private static final String TAG = DatabaseManager.class.getCanonicalName();
	
//	public static synchronized DatabaseManager getInstance() {
//		if (manager == null) {
//			manager = new DatabaseManager();
//		}
//		return manager;
//	}
	
	public DatabaseManager(String type, String azienda){
		this.type = type;
		this.azienda=azienda;
	}

	public boolean init() {
		// check del driver

		try {
			
			switch (type) {
			case Testo.SQLITE:
				Class.forName("org.sqlite.JDBC");
				break;
			case Testo.FIREBIRD:
				Class.forName("org.firebirdsql.jdbc.FBDriver");
				break;
			case Testo.MSSQL:
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				break;
			case Testo.AS400:	
				Class.forName("com.ibm.as400.access.AS400JDBCDriver");
				break;
			default:
				break;
			}
			 
		} catch (ClassNotFoundException e) {
			logger.error(TAG, e);
			return false;
		}

		 
		return true;
	}

	public Connection getConnection() {
		Connection conn = null;
		
//		String libraries= ";libraries=GA_V01_O, QGPL, QTEMP, IT_V02_W, IT_EVO_F, IT_V02_F, IT_V02_O, KMS63L_DAT, BMS63L_DAT, ACK63L_OBJ, PLEX510, PCNFE_BAS, DA_V02_O, MMAIL;";
		String libraries= ";libraries=GA_V01_O, QGPL, QTEMP, IT_SOL_PF, IT_SOL_PO, IT_SOL_F, IT_V02_O, KMS60L_DAT, BMS60L_DAT, ACK60L_OBP, ACK60L_OBJ, PCNTJRN, DA_V02_O, PCNFE_SOL, PLEX510, KEYTOOL, MMAIL";
		
		try {
			
			switch (type) {
			case Testo.SQLITE:
				conn = DriverManager.getConnection("jdbc:sqlite:database.db");
				break;
			case Testo.AS400:
				Map<String, String> map = OrdersProperties.getInstance().load();
				String host_as400 = (String) map.get("host_as400");
				String user_as400 = (String) map.get("user_as400");
				String password_as400 = (String) map.get("password_as400");
				
				if (azienda.equals(Testo.AZIENDA_SOLIGO)) {
					logger.debug(TAG, "SOLIGO libraries: " + libraries);
					conn = DriverManager.getConnection("jdbc:as400://" + host_as400 + libraries, user_as400, password_as400);	
				} else {
					conn = DriverManager.getConnection("jdbc:as400://" + host_as400, user_as400, password_as400);	
				}
				
				break;
			default:
				break;
			}
		} catch (SQLException e) {
			logger.error(TAG, e);
		}
		return conn;
 
	}

}
