package com.econorma.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.econorma.data.Order;
import com.econorma.main.Application;
import com.econorma.util.Logger;
 
public class DAOAS400 {
	
	private static final String TAG = DAOAS400.class.getSimpleName();
	private static final Logger logger = Logger.getLogger(DAOAS400.class);

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");	 
//	private static final String LIBRARY = "IT_V02_W";
//	private static final String LIBRARY = "IT_VIP_F";
	private static final String LIBRARY = Application.getInstance().getLibrary_as400();
	private static final String ORDINI = LIBRARY + ".OXCLI01L";
	

	private static final String F_CLIENTE =  "OXCLSP";
	private static final String F_NUMERO =  "OXNORC";
	private static final String F_ANNO =  "OXAORC";
	private static final String F_MESE =  "OXMORC";
	private static final String F_GIORNO =  "OXGORC";
	private static final String F_STATO =  "OXFELA" ;
	private static final String F_RIFERIMENTO_ORDINE =  "OXRIFC";
	private static final String F_NOME_FILE =  "OXNFIL";
	private static final String F_NOME_PDF =  "OXNPDF";
	

	public static List<Order> readOrdini(Connection connection){

		try{

			int days = Application.getInstance().getSub_days();
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			if (days > 0){
				cal.add(Calendar.DATE, -days);
			}
			
			String dataOrdine = sdf.format(cal.getTime());
			
			PreparedStatement st =null; 

			String select = "SELECT " +
					F_CLIENTE +", " +
					F_NUMERO+", " +
					F_ANNO+", " +
					F_MESE+", " +
					F_GIORNO+", " +
					F_STATO+", " +
					F_RIFERIMENTO_ORDINE+", " +
					F_NOME_FILE+", " +
					F_NOME_PDF+"  " +
					" FROM "+ ORDINI +
					" WHERE " + F_STATO + "=" + "'N'" +
					" AND " + F_ANNO+ "*10000+ " +F_MESE + "*100+ "+F_GIORNO  + ">=" + dataOrdine; 
					  
	
			logger.debug(TAG, select);
			
			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();
			List<Order> ordini = new ArrayList<Order>();
			
			while(rs.next()){

				try {
					String cliente = rs.getString(1);
					String numero = rs.getString(2);
					String anno = rs.getString(3);
					String mese = StringUtils.leftPad(rs.getString(4), 2, "0");
					String giorno = StringUtils.leftPad(rs.getString(5), 2, "0");
					String stato = rs.getString(6);
					String riferimentoOrdine = rs.getString(7);
					String nomeFile = rs.getString(8);
					String nomePdf = rs.getString(9);
					
					logger.info(TAG, "Ordine trovato: " +  numero + "|" + nomeFile.trim() + "|" + nomePdf.trim() + "|" + riferimentoOrdine.trim() + "|" +  giorno+mese+anno + "|" + cliente + "|" + riferimentoOrdine + "|" +stato + "|");
					
					Order ordine = new Order();
					ordine.setCliente(cliente);
					ordine.setNumero(numero);
					ordine.setData(sdf.parse(anno+mese+giorno));
					ordine.setStato(stato);
					ordine.setRiferimentoOrdine(riferimentoOrdine.trim());
					ordine.setNomeFile(nomeFile);
					ordine.setNomePdf(nomePdf);
					ordini.add(ordine);
					
				} catch (Exception e) {
					logger.error(TAG, e);
				}
				
			}

			rs.close();

			return ordini;

		}

		catch(SQLException e){
			System.out.println(e);
			return null;
		}
	}
	
	public static boolean updateOrdine(Connection connection, Order ordine) throws SQLException{

		boolean result = false;
		 
		String dataOrdine = sdf.format(ordine.getData());
		String anno = dataOrdine.substring(0, 4);
		String mese = StringUtils.leftPad(dataOrdine.substring(4, 6), 2, "0");
		String giorno = StringUtils.leftPad(dataOrdine.substring(6, 8), 2, "0");
		String riferimentoOrdine = ordine.getRiferimentoOrdine();
		
		try{
			PreparedStatement st =null;

			String select = "UPDATE " +
					ORDINI +  
					" SET " + F_STATO + " = " + "'" + "S" + "'" +
					" WHERE " + F_NUMERO + " = " + "'" + ordine.getNumero() + "'" +
					" AND " + F_GIORNO + " = " +  giorno +   
					" AND " + F_MESE + " = " + mese +   
					" AND " + F_ANNO + " = " + anno;  
			logger.debug(TAG, select);
			 
			st = connection.prepareStatement(select);
			st.executeUpdate();
			result = true;
			
			st.close();
			return result;

		}catch (SQLException e) {
			System.out.println(e);
			result = false;
		}
		return result;
	}
 	 
	public static String getFromDate(int subbedDays) {
		Date dateFrom = DateUtils.addDays(new Date(), -subbedDays);
		return sdf.format(dateFrom);
	}

}
