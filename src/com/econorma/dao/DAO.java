package com.econorma.dao;

import java.awt.geom.Rectangle2D;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.List;

import com.econorma.data.Chiave;
import com.econorma.data.Coordinate;
import com.econorma.data.Order;
import com.econorma.testo.Testo;
import com.econorma.util.Logger;


public class DAO {

	private static final Logger logger = Logger.getLogger(DAO.class);

	private static final String TAG = "DAO";


	public static final boolean USE_SQLITE = true;

	static final String TABLE_COORDINATES = "COORDINATES";
	 
	private DatabaseManager sqliteManager;
	private DatabaseManager as400Manager;

	public DAO(){

	}

	public void setSqliteManager(DatabaseManager dm){
		this.sqliteManager = dm;
	}

	public void setAs400Manager(DatabaseManager dm){
		this.as400Manager = dm;
	}
 
	public void init() {
		if (USE_SQLITE) {
			Connection conn = sqliteManager.getConnection();
			// creiamo le tabelle
			try {
				try {
					Statement stat;
					stat = conn.createStatement();
					// stat.executeUpdate("drop table if exists people;");
					stat.executeUpdate("CREATE TABLE " + TABLE_COORDINATES
							+ " (" + "ID INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL, "
							+ "AZIENDA TEXT," + "CLIENTE TEXT, " + "PAGE INTEGER, " + "KEY TEXT, "
							+ "X INTEGER, " + "Y INTEGER, "
							+ "WIDTH INTEGER, " + "HEIGHT INTEGER, "
							+ "DATA TIMESTAMP, " + "VERSION TEXT)");
							 

				} catch (SQLException e) {
					// mLogger.e(TAG, e.getMessage());
					// ignore,
					// table already exists...
				}
 

			} finally {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}


	}

	public boolean insertCoordinates(Coordinate coordinate) {
		Connection connection = null;
		boolean insert = false;
		try {
			connection = sqliteManager.getConnection();
			insert = DAOCoordinates.insert(connection, coordinate);
		} finally {
			silentClose(connection);
		}
		return insert;
	}
	
	
	public void insertAllCoordinates(String azienda, LinkedHashMap<Chiave, Rectangle2D> rectangles) {
		Connection connection = null;
		try {
			connection = sqliteManager.getConnection();
			DAOCoordinates.insertAll(connection, azienda, rectangles);
		} finally {
			silentClose(connection);
		}
	
	}
	
	public List<Coordinate> findAllByAzienda(String azienda, String cliente, int page) {
		Connection connection = null;
		 List<Coordinate> findAllByAzienda;
		try {
			connection = sqliteManager.getConnection();
		    findAllByAzienda = DAOCoordinates.findAllByAzienda(connection, azienda, cliente, page);
		} finally {
			silentClose(connection);
		}
		return findAllByAzienda;
	}
	
 
	
	public boolean deleteAllCoordinates(String azienda) {
		Connection connection = null;
		boolean delete = false;
		try {
			connection = sqliteManager.getConnection();
			delete = DAOCoordinates.deleteAllCoordinates(connection, azienda);
		} finally {
			silentClose(connection);
		}
		return delete;
	}
	
	public List<Order> readOrdini() throws SQLException {
		Connection connection = null;
		List<Order> ordini = null;
		try {
			connection = as400Manager.getConnection();
			ordini = DAOAS400.readOrdini(connection);
		}catch (Exception e) {
			System.out.println(e);
		} 

		finally {
			silentClose(connection);
		}
		return ordini;
	}
	
	public boolean updateOrdine(Order ordine) throws SQLException {
		Connection connection = null;
		boolean result = false;
		try {
			connection = as400Manager.getConnection();
			result = DAOAS400.updateOrdine(connection, ordine);
		}catch (Exception e) {
			result = false;
			System.out.println(e);
		} 

		finally {
			silentClose(connection);
		}
		return result;
	}

	
	public static DAO create(DatabaseManager sqliteManager, DatabaseManager as400Manager){
		DAO dao = new DAO();
		dao.setSqliteManager(sqliteManager);
		dao.setAs400Manager(as400Manager);
		return dao;
	}
	
	public static void silentClose(Connection conn) {
		try {
			if (conn != null)
				conn.close();
		} catch (Exception e) {
			// ignore
		}
	}
	
	public Connection getConnection(String type){
		Connection conn = null;
		switch (type) {
		case Testo.SQLITE:
			conn = sqliteManager.getConnection();
			break;
		case Testo.AS400:
			conn = as400Manager.getConnection();
			break;

		default:
			break;
		}
		return conn;
	}

	


}
