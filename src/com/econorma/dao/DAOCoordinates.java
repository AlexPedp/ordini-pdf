package com.econorma.dao;

import java.awt.geom.Rectangle2D;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.econorma.data.Chiave;
import com.econorma.data.Coordinate;
import com.econorma.util.Logger;


public class DAOCoordinates {

	private static final Logger logger = Logger.getLogger(DAOCoordinates.class);
	private static final String TAG = DAOCoordinates.class.getCanonicalName();

	private static final String TABLE = "COORDINATES";

	private static final String F_ID = "ID";
	private static final String F_AZIENDA="AZIENDA";
	private static final String F_CLIENTE="CLIENTE";
	private static final String F_PAGE="PAGE";
	private static final String F_KEY="KEY";
	private static final String F_X="X";
	private static final String F_Y="Y";
	private static final String F_WIDTH ="WIDTH";
	private static final String F_HEIGHT ="HEIGHT";
	private static final String F_VERSION ="VERSION";

	public static List<Coordinate>findAllByAzienda(Connection connection, String azienda, String cliente, int page){

		try{
			PreparedStatement st =null;

			String select = "SELECT " +
					F_ID+", "+
					F_AZIENDA+", "+
					F_CLIENTE+", " +
					F_PAGE+", " +
					F_KEY  + ", " + 
					F_X  + ", " +
					F_Y  + ", " +
					F_WIDTH  + ", " +
					F_HEIGHT  + ", " +
					F_VERSION + " " +
					" FROM "+TABLE + " WHERE " + F_AZIENDA + "='" + azienda + "'" +
					" AND " + F_CLIENTE + " = " + "'" + cliente + "'" + 
					" AND " + F_PAGE + " = " + page +  " ORDER BY " + F_ID; 

			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();

			List<Coordinate> coordinates = new ArrayList<Coordinate>();

			while(rs.next()){
				Long id = rs.getLong(1);
				String azi = rs.getString(2);
				String cli = rs.getString(3);
				int pagina = rs.getInt(4);
				String key = rs.getString(5);
				int x = rs.getInt(6);
				int y = rs.getInt(7);
				int width = rs.getInt(8);
				int height = rs.getInt(9);
				String version = rs.getString(10);

				Coordinate coordinate = new Coordinate();
				coordinate.setAzienda(azi);
				coordinate.setCliente(cli);
				coordinate.setPage(pagina);
				coordinate.setKey(key);
				coordinate.setX(x);
				coordinate.setY(y);
				coordinate.setWidth(width);
				coordinate.setHeight(height);
				coordinate.setVersion(version);
				coordinates.add(coordinate);

			}
			rs.close();
			return coordinates;
		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}

	public static boolean insert(Connection connection, Coordinate coordinate){

		try{
			
			boolean execute=true;

			PreparedStatement st = connection.prepareStatement("INSERT INTO COORDINATES (AZIENDA, CLIENTE, PAGE, KEY, X, Y, WIDTH, HEIGHT, VERSION) VALUES (?,?,?,?,?,?,?,?,?)");
			st.setString(1,coordinate.getAzienda());
			st.setString(2, coordinate.getCliente());
			st.setInt(3, coordinate.getPage());
			st.setString(4,coordinate.getKey());
			st.setInt(5, coordinate.getX());
			st.setInt(6, coordinate.getY());
			st.setInt(7, coordinate.getWidth());
			st.setInt(8, coordinate.getHeight());
			st.setString(9,  coordinate.getVersion());
			execute = st.execute();
		
			return execute;
		}


		catch(SQLException e){
			logger.error(TAG, e);
			return false;

		}

	}	
	
	public static void insertAll(Connection connection, String azienda, LinkedHashMap<Chiave, Rectangle2D> rectangles){

		try{
			PreparedStatement st = connection.prepareStatement("INSERT INTO COORDINATES (AZIENDA, CLIENTE, PAGE, KEY, X, Y, WIDTH, HEIGHT, VERSION) VALUES (?,?,?,?,?,?,?,?,?)");
			
			connection.setAutoCommit(false);
			
			
			for (Map.Entry<Chiave, Rectangle2D> entry : rectangles.entrySet()) {
				
		 
				Chiave key = entry.getKey();
				Rectangle2D value = entry.getValue();
	
				Coordinate coordinate = new Coordinate();
				coordinate.setAzienda(azienda);
				coordinate.setKey(key.getKey());
				coordinate.setCliente(key.getCliente());
				coordinate.setPage(key.getPage());
				coordinate.setVersion(key.getVersion());
	
				Double x = value.getX();
				Double y = value.getY();
				Double width = value.getWidth();
				Double height = value.getHeight();
	
				coordinate.setX(x.intValue());
				coordinate.setY(y.intValue());
				coordinate.setWidth(width.intValue());
				coordinate.setHeight(height.intValue());
				
				st.setString(1,coordinate.getAzienda());
				st.setString(2, coordinate.getCliente());
				st.setInt(3, coordinate.getPage());
				st.setString(4,coordinate.getKey());
				st.setInt(5, coordinate.getX());
				st.setInt(6, coordinate.getY());
				st.setInt(7, coordinate.getWidth());
				st.setInt(8, coordinate.getHeight());
				st.setString(9,  coordinate.getVersion());
				st.addBatch();

			} 
			
			int[] count = st.executeBatch();
			connection.commit();
			 
		}


		catch(SQLException e){
			logger.error(TAG, e);
			}

	}	
	 
	public static boolean deleteAllCoordinates(Connection connection, String azienda){

		try{
			
			boolean execute=true;

			PreparedStatement st = connection.prepareStatement("DELETE FROM COORDINATES WHERE AZIENDA = ?");
			st.setString(1,azienda);
			execute = st.execute();
			return execute;
		}


		catch(SQLException e){
			logger.error(TAG, e);
			return false;

		}

	}	
	
}
