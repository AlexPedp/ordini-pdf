package com.econorma.testo;

public class Testo {
	
	public static final String YOGURT = "YOGURT";
	public static final String MORGESE = "MORGESE";
	public static final String ARENA = "ARENA";
	public static final String WARENANNAHME = "WARENANNAHME";
	public static final String AUCHAN = "AUCHAN";
	public static final String GANASSA = "GANASSA";
	public static final String COAL = "COAL";
	public static final String MEGAMARK = "MEGAMARK";
	public static final String ERGON = "ERGON";
	public static final String VISOTTO = "VISOTTO";
	public static final String VISOTTO2 = "VISOTTO2";
	public static final String CEDI = "CEDI";
	public static final String TIGROS = "TIGROS";
	public static final String MAIORA = "MAIORA";
	public static final String CEDI_COD = "CE.DI.";
	public static final String CEDI_GROSS_COD = "CE.DI.GROS";
	public static final String CEDI_GROSS = "GROS";
	public static final String ESSELUNGA = "ESSELUNGA";
	public static final String CARREFOUR = "CARREFOUR";
	public static final String FIORITAL = "FIORITAL";
	public static final String EUROSPIN = "INTELLIGENTE";
	public static final String EUROSPIN_TIRRENICA = "EUROSPIN";
	public static final String EUROSPIN_LAZIO_CODE = "EUROSPIN LAZIO";
	public static final String EUROSPIN_LAZIO = "EUROLAZIO";
	public static final String UNICOMM = "UNICOMM";
	public static final String UNICANT = "UNICANT";
	public static final String SMA = "SMA";
	public static final String CONAD = "CONAD";
	public static final String COMMERCIANTI_INDIPENDENTI = "COMMERCIANTI INDIPENDENTI";
	public static final String COMMERCIANTI_INDIPENDENTI_1 = "COMMERCIANTI INDIP.";
	public static final String COOP_NORDOVEST = "COOP CONSORZIO NORD OVEST";
	public static final String CONAD_FRE = "CONAD_FRE";
	public static final String CONAD_ACS = "ADRIATICO";
	public static final String OPRAMOLLA = "OPRAMOLLA";
	public static final String DCFOOD_COD = "DC FOOD"; 
	public static final String DCFOOD = "DCFOOD";
	public static final String GUARNIER = "GUARNIER";
	public static final String ITALBRIX = "ITALBRIX";
	public static final String CRAI = "CRAI";
	public static final String CRAI_COD = "F.LLI IBBA";
	public static final String CRAI_FRE = "CRAI_FRE	";
	public static final String CRAI_COD_2 = "C R A I  O V E S T";
	public static final String CENTRALE_PIVA = "IT02795150362";
	public static final String CONAD_PIVA = "00138950407";
	public static final String CONAD_PIVA_2 = "01977130473";
	public static final String CONAD_PIVA_3 = "00105820443";
	public static final String CONAD_GLN_FORLI = "8003160035648";
	public static final String CONAD_GLN_POZZUOLO = "8003160067328";
	public static final String CONAD_GLN_FANO = "8003160000851";
	public static final String CONAD_GLN_SCORZE = "8003160037314";
	public static final String CENTRALE = "CENTRALE";
	public static final String ISA = "ISASPA";
	public static final String ISA_CODE = "I.S.A. S.P.A.";
	public static final String CDC = "CDCSRL";
	public static final String CDC_CODE = "359/33";
	public static final String BENNET = "BENNET";
	public static final String IPER = "IPER";
	public static final String IPER_PORTOGRUARO = "IPER PORTOGRUARO";
	public static final String IPER_SAN_BIAGIO = "IPER SAN BIAGIO";
	public static final String IPERMERCATO = "IPERMERCATO";
	public static final String LS_NORD = "LS NORD";
	public static final String IPERCOOP = "IPERCOOP";
	public static final String TOTALE = "TOTAL";
	public static final String EUROPALL = "EUROPALL";
	public static final String PEDANE = "PEDANE";
	public static final String BIOLOGICO = "BIOLOGICO";
	public static final String U1 = "U1";
	public static final String FRISCHDIENST = "FRISCHDIENST";
	public static final String CORTESEMENTE = "CORTESEMENTE";
	public static final String SOLLECITO = "SOLLECITO";
	public static final String GENERALE = "GENERALE";
	public static final String BOLLICINE = "BOLLICINE";
	public static final String SECCHIELLO = "SECCHIELLO";
	public static final String DIE_PRODUKTE = "DIE PRODUKTE";
	public static final String SPARK = "ELABORAZIONE DATI";
	public static final String BRIXEN = "BRIXEN";
	public static final String TOSANO = "TOSANO";
	public static final String VAHRN = "VAHRN";
	public static final String CODICE_FISCALE = "COD. FISC.";
	public static final String GENOSSE = "GENOSSENSCHAFTSREG";
	public static final String BRIMI_SITE = "WWW.BRIMI.IT";
	public static final String BITTE = "BITTE WARE";
	public static final String BITTE_DE = "BITTE";
	public static final String TOTALE_MAXIDI = "T O T A L E";
	public static final String ZAH = "ZAHLUNGSBEDINGUNGEN";
	public static final String POS_UNS = "POS.  UNS.";
	public static final String L_TERMIN = "L.TERMIN";
	public static final String TOTALI = "TOTALI";
	public static final String VRM_CODE = "VRM";
	public static final String FORN_ORD = "FORN.ORD.";
	public static final String VALORIZZAZIONE = "VALORIZZAZIONE";
	public static final String TOTALE_BENNET = "T O T A L E";
	public static final String SEPARATORE1 = "----------------------";
	public static final String SEPARATORE2 = "________________________";
	public static final String TOTALE_SMA = "TOTALI";
	public static final String ARTICOLI = "ARTICOLI";
	public static final String ORDINATO = "ORDINATI";
	public static final String STRATO = "STRATO";
	public static final String OMAG = "OMAG";
	public static final String SEPARATORE = "--------------------";
	public static final String SEPARATORE_3 = "_____________________________________________________";
	public static final String ZONA = "ZONA";
	public static final String SMA_FILE = "S_M_A";
	public static final String SMA_DEPOSITO = "CHIARI";
	public static final String VRM = "V_R_M";
	public static final String VRM_DEPOSITO = "";
	public static final String PRODOTTI = "PRODOTTI";
	public static final String KG = "kg";
	public static final String KILI = "KG";
	public static final String KGM = "KG";
	public static final String PCE = "PC";
	public static final String BARRA_KILI = "/ KG";
	public static final String ASTERISCO = "*";
	public static final String DOP = "D.O.P";
	public static final String BIORDO = "BIORDO";
	public static final String P = "P";
	public static final String SALUTI = "SALUTI";
	public static final String CADORO = "CADORO";
	public static final String APULIA = "APULIA";
	public static final String GARDERE = "GARDERE";
	public static final String COSTO = "COSTO IN FATT.";
	public static final String SCALA = "% SCAL.";
	public static final String SCONTO = "% SCAL.";
	public static final String SCONTO_PER = "SC. %";
	public static final String SCONTO_PROM = "PROM. %";
	public static final String VALORE = "VALORE";
	public static final String NO_SCONTI = "NO SCONTI";
	public static final String AVVISO = "AVVISO IMPORTANTE";
	public static final String SCONTO_CANALE = "SCONTO CANALE";
	public static final String SCONTO_CANVAS = "SCONTO CANVAS";
	public static final String PEZZI = "/ PZ";
	public static final String COSTO_LISTINO = "COSTO LISTINO";
	public static final String COSTO_FATTURA = "COSTO FATTURA";
	public static final String ASSOLVE = "ASSOLVE";
	public static final String TRATTINO = "---------------";
	public static final String TERMINE = "TERMINE DI PAGAMENTO";
	public static final String PROBLEMI = "PROBLEMI";
	public static final String PAGAMENTO = "PAGAMENTO";
	public static final String SOTTOSCRIZIONE = "SOTTOSCRIZIONE";
	public static final String QUANTITATIVI = "QUANTITATIVI";
	public static final String IMPORTO = "IMPORTO";
	public static final String ASSOLUTAMENTE = "ASSOLUTAMENTE";
	public static final String ACCETTAZIONE = "ACCETTAZIONE";
	public static final String ACCETTANO = "ACCETTANO";
	public static final String NOME_PGM ="OF005JC22";
	public static final String CEDI_FRESCHI ="CE.DI.-FRESCHI";
	public static final String PREGA = "PREGA";
	public static final String BOLLA = "BOLLA";
	public static final String OMAGGIO = "OMAGGIO";
	public static final String CANALE = "SC.% CANALE";
	public static final String CODICE_PRODOTTO = "CODICE PROD.";
	public static final String CODICI_ARTICOLO = "CODICI ARTICOLO";
	public static final String IMBALLO = "IMBALLO";
	public static final String RIPORTARE = "RIPORTARE SULLA BOLLA";
	public static final String TERMINI = "TERMINI";
	public static final String SOGGETTA = "SOGGETTA";
	public static final String LUOGO = "LUOGO";
	public static final String MITTENTE = "MITTENTE";
	public static final String CATANIA = "CATANIA";
	public static final String SAN_DONA = "SAN DONA";
	public static final String SAN_VITO = "S.VITO AL TAGLIAMENTO";
	public static final String CAP = "94019";
	public static final String ORDINI = "ORDINI";
	public static final String PREZZO_LISTINO = "PREZZO DI LISTINO";
	public static final String FRATELLI = "FRATELLI";
	public static final String COSTO_COMMERCIALE = "COSTO COMMERCIALE";
	public static final String CADORO_0 = "CADORO_0";
	public static final String CADORO_1 = "CADORO_1";
	public static final String GUARNIER_0 = "GUARNIER_0";
	public static final String GUARNIER_1 = "GUARNIER_1";
	public static final String OTTAVIAN = "OTTAVIAN";
	public static final String COOP = "COOP CONSORZIO";
	public static final String COOP_VA = "COOP";
	public static final String COOP30_VA = "COOP30";
	public static final String SEVEN = "SEVEN";
	public static final String UNICOOP = "UNICOOP";
	public static final String UNICOOP_TIRRENO_CODE = "UNICOOPTIRRENO";
	public static final String UNICOOP_TIRRENO = "TIRRENO";
	public static final String COOP_FACCHINI = "COOP. FACCHINI";
	public static final String COOP_COD = "COD. COOP";
	public static final String COOP_VA_VIPITENO = "COOP.VA LATTERIA VIPITENO";
	public static final String COOP_VIPITENO = "COOP.LATTERIE VIPITENO";
	public static final String HEIDERBECK = "HEIDERBECK";
	public static final String QUESOS = "QUESOS";
	public static final String FINE_STAMPA = "FINE STAMPA";
	public static final String T_PAG = "T.Pag.";
	public static final String PAG = "Pag.";
	public static final String PAG_2 = "PAG.";
	public static final String SC_PAG = "SC.PAG";
	public static final String PEZZO_ORD = "PEZZO ORD";
	public static final String SCALA_SCONTI = "SCALA SCONTI";
	public static final String COSTO_KG = "/ KG";
	public static final String COSTO_PZ = "/ PZ";
	public static final String COMMISSIONE = "COMMISSIONE";
	public static final String DATA_RIC_FATTURA = "DATA RIC. FATTURA";
	public static final String MENO_PERCENTO = "-%";
	public static final String PERCENTO = "%";
	public static final String BARRA_KG = " / KG";
	public static final String BARRA_KG_2 = " / K G";
	public static final String PRZ_LORDO = "PRZ. LORDO";
	public static final String VS_ARTICOLO = "VS. ARTICOLO";
	public static final String LA2 = "LA2";
	public static final String CNP = "CNP";
	public static final String MERCE = "MERCE";
	public static final String TOT_CARTONI = "TOT.CARTONI";
	public static final String PALLETS = "PALLETS";
	public static final String CENTRALINO = "CENTRALINO";
	public static final String PIATTAFORMA = "PIATTAFORMA";
	public static final String NOTE_FIRMA = "NOTE FIRMA";
	public static final String CONFERMA_ORDINE = "CONFERMA";
	public static final String VALORE_RIGA = "VALORE DI RIGA IMPONIBILE";
	public static final String COSTO_NETTO = "COSTONETTO";
	public static final String FILLER = "537SL";
	public static final String SOTTOLINEATURA_SHORT = "______________";
	public static final String SOTTOLINEATURA = "____________________________________";
	public static final String SOTTOLINEATURA_2 = "---------------------------------";
	public static final String SOTTOLINEATURA_4 = "==========================";
	public static final String SOTTOLINEATURA_5 = "*****************************";
	public static final String SOTTOLINEATURA_3 = "--------";
	public static final String SOTTOLINEATURA_6 = "****";
	public static final String SEPARATORE_4 = "|";
	public static final String INFORMAZIONI = "INFORMAZIONI";
	public static final String LORDO = "LORDO";
	public static final String MONTEBELLUNA = "MONTEBELLUNA";
	public static final String TOTALE_IN_FATTURA = "TOTALE IN FATTURA";
	public static final String LEGENDA = "LEGENDA";
	public static final String MENO = "-";
	public static final String VATBARBARA = "VATBARBARA";
	public static final String DATA_FATTURA = "DATA FATTURA";
	public static final String BRIMI = "BRIMI";
	public static final String BRESSANONE = "CENTRO LATTE BRESSANONE";
	public static final String LOGISTICA = "IMPORTANTE NOTA LOGISTICA";
	public static final String DEPERIBILI = "DEPERIBILI";
	public static final String CENTRO = "CENTRO";
	public static final String TOT_KG = "TOT.KG";
	public static final String DESCRIZIONE = "DESCRIZIONE";
	public static final String CONVENZIONE = "CONVENZIONE";
	public static final String IN_AGGIUNTA = "IN AGGIUNTA";
	public static final String ARTICOLO = "ARTICOLO";
	public static final String ITEM = "ITEM";
	public static final String TONYS_FINE_FOOD = "FINE FOODS - REED";
	public static final String ALCAMPO = "ALCAMPO";
	public static final String KERAVA = "KERAVA";
	public static final String STOCKHOLM = "STOCKHOLM";
	public static final String HELSINKI = "HELSINKI";
	public static final String EMMI = "EMMI";
	public static final String FORLI = "FORLI";
	public static final String FORL = "FORL";
	public static final String REGGIO_EMILIA = "REGGIO EMILIA";
	public static final String ANAGNI = "ANAGNI";
	public static final String RUTIGLIANO = "RUTIGLIANO";
	public static final String UDINE = "UDINE";
	public static final String CATANZARO = "CATANZARO";
	public static final String MOLFETTA = "MOLFETTA";
	public static final String MIGUEL = "MIGUEL";
	public static final String BONTALIA = "BONTALIA";
	public static final String BONIFICO = "BONIFICO";
	public static final String BONIFICO_2 = "BONIF.BANCARI A62";
	public static final String TOTALE_GENERALE = "TOTALE_GENERALE";
	public static final String REITAN = "REITAN";
	public static final String MARCINELLE = "MARCINELLE";
	public static final String PORTICI = "PORTICI";
	public static final String VALGUARNERA = "VALGUARNERA";
	public static final String NAGEL = "NAGEL";
	public static final String BRONDY_NO_ACCENT = "BRNDBY";
	public static final String BRONDY = "BRONDBY";
	public static final String SALLING = "SALLING";
	public static final String GEIA = "GEIA";
	public static final String ORARI = "ORARI";
	public static final String NR_ORDINE = "NRO ORDINE";
	public static final String PRESENTAZIONE = "PRESENTAZIONE";
	public static final String OS11 = "OS11";
	public static final String REFERENZE = "REFERENZE";
	public static final String REFERENTE_NADIA = "REFERENTE:NADIA";
	public static final String OS_N = "OS N";
	public static final String OS18 = "OS18";
	public static final String OS = "OS";
	public static final String SMK = "SMK";
	public static final String PROMO = "PROMO";
	public static final String PROMOZIONE = "PROMOZIONE";
	public static final String LINEA = "LINEA";
	public static final String DESCRIPTION = "DESCRIPTION";
	public static final String STORE = "STORE";
	public static final String PALLET = "PALLET";
	public static final String EAN = "EAN";
	public static final String LISTINO = "LISTINO";
	public static final String DOT = ".";
	public static final String IMPORTANT = "IMPORTANT";
	public static final String TRANSFERED = "TRANSFE.";
	public static final String EMAIL_GEIA = "INFO@GEIAFOOD.DK";
	public static final String EMAIL_GEIA_2 = "INVOICE@GEIAFOOD.DK";
	public static final String SITE_GEIA = "WWW.GEIAFOOD.COM";
	public static final String UNANNOUNCED = "UNANNOUNCED";
	public static final String ONLY_CONTAIN = "ONLY CONTAIN";
	public static final String WEIGHT = "WEIGHT";
	public static final String IBAN = "IBAN";
	
	public static final String SANTA_PALOMBA = "S.PALOMBA (AW)";
	public static final String SANTA_PALOMBA_RIF = "10000AW";
	public static final String MASSALENGO_AT = "MASSALENGO (AT)";
	public static final String MASSALENGO_AT_RIF = "10000AT";
	public static final String MASSALENGO_AU = "MASSALENGO (AU)";
	public static final String MASSALENGO_AU_RIF = "10000AU";
	public static final String AIROLA_BN = "AIROLA (BN)";
	public static final String AIROLA_BN_RIF = "10000AO";
	public static final String RIVALTA_AM = "RIVALTA (AM)";
	public static final String RIVALTA_AM_RIF = "10000AM ";
	public static final String RIVALTA_AX = "RIVALTA STOCK (AX)";
	public static final String RIVALTA_AX_RIF = "10000AX ";
	
	public static final String AZIENDA_SOLIGO = "SOLIGO";
	public static final String AZIENDA_COPEGO = "COPEGO";
	public static final String AZIENDA_AMBROSI = "AMBROSI";
	public static final String AZIENDA_AMBROSI_USA = "AFU";
	public static final String AZIENDA_VIPITENO = "VIPITENO";
	public static final String AZIENDA_ANTICA = "ANTICA";
	public static final String AZIENDA_CESENA = "CESENA";
	public static final String AZIENDA_FRESCOLAT = "FRESCOLAT";
	public static final String AZIENDA_TONIOLO = "TONIOLO";
	
	public static final String LAGOGEL = "LAGOGEL";
	public static final String VALFOOD = "VALFOOD";
	public static final String ALFI = "ALFI";
	public static final String ALI = "ALI_";
	public static final String DADO = "DADO";
	public static final String ALI_PIVA = "00348980285";
	public static final String BRENDOLAN = "BRENDOLAN";
	public static final String MIGROSS = "MIGROSS";
	
	public static final String ROSSETTO = "ROSSETTO";
	public static final String SPEDIZIONE = "SPEDIZIONE";
	public static final String NUMERO_ORDINE = "NUMERO ORDINE";
	public static final String DATA_ORDINE = "DATA ORDINE";
	public static final String DATA_INVIO = "DATA INVIO";
	public static final String ORDINE = "ORDINE";
	public static final String ORDINE_DEL = "ORDINE DEL";
	public static final String DATA_CONSEGNA = "DATA CONSEGNA";
	public static final String CONSEGNA_DEL = "CONSEGNA DEL";
	public static final String CONSEGNARE = "CONSEGNARE";
	public static final String RIGHE = "RIGHE";
	public static final String CARTON = "CARTON";
	public static final String NOTE = "NOTE";
	public static final String EGREGIO = "EGREGIO";
	public static final String FUSIONE = "FUSIONE";
	public static final String ARTIKEL = "ARTIKEL";
	public static final String WARENEINGANG = "WARENEINGANG";
	public static final String SOGEGROSS = "SOGEGROSS";
	public static final String VALSANA = "VALSANA";
	public static final String RAGIS = "RAGIS";
	public static final String SERENISSIMA = "SERENISSIMA";
	
	public static final String RESPONSABILE = "RESPONSABILE";
	public static final String EXTRA = "EXTRA";
	public static final String INTEGRAZIONE = "INTEGRAZIONE";
	public static final String CONVENIENZA = "CONVENIENZA";
	public static final String LINE = "____________________________";
	public static final String LINE_STR = "LINE";
	public static final String FATTURA = "FATTURA";
	public static final String BANCALI = "BANCALI";
	public static final String CASO = "NEL CASO";
	public static final String PEDIDO = "PEDIDO";
	public static final String CAJAS = "CAJAS";
	public static final String CHASAL = "CHASAL";
	public static final String CIRCLEVIEW = "CIRCLEVIEW";
	public static final String EURO_SIMBOLO = "�";
	public static final String EMMECI = "EMMECI";
	public static final String EFFELLE = "EFFELLE";
	public static final String SPAZIO_CONAD = "SPAZIO CONAD";
	public static final String FORLIIMPOPOLI = "FORLIIMPOPOLI";
	
	public static final String V1 = "V1";
	public static final String V2 = "V2";
	
	public static final String VLEESWAREN = "VLEESWAREN";
	public static final String DATUM = "DATUM";
	public static final String ORDER = "ONZE";
	public static final String ORDINI_FORNITORI = "ORDINIFORNITORI";
	public static final String BESTELLING = "BESTELLING";
	public static final String BESTELLUNG = "BESTELLUNG";
	public static final String LEVERADRES = "LEVERADRES";
	public static final String DEPUYDT = "DEPUYDT";
	public static final String CODE = "CODE";
	
	public static final String KALLAS = "KALLAS";
	public static final String PURCHASE_ORDER = "PURCHASE ORDER";
	public static final String ORDER_DATE = "DATE";
	
	public static final String RIMI_LATVIA = "RIMI LATVIA";
	public static final String RIMI = "RIMI";
	public static final String ARKUSZ1 = "ARKUSZ1";
	public static final String JANSEN = "JANSEN";
	public static final String MINI_ELLADA = "MINI ELLADA";
	public static final String MINI = "MINI";
	public static final String ROWCLIFFE = "ROWCLIFFE";
	public static final String GRANDAMA = "GRANDAMA";
	public static final String GIUBELLI = "GIUBELLI";
	public static final String GARDINI = "GARDINI";
	public static final String SALA_ROMEO_CODE = "SALA ROMEO";
	public static final String SALAROMEO = "SALAROMEO";
	
	public static final String PREGO_RISPETTARE = "PREGO RISPETTARE";
	
	public static final String EURO_FRESH_FOOD = "EURO FRESH FOODS";  
	public static final String EURO = "EURO";
	
	public static final String ATTENZIONE = "ATTENZIONE";
	public static final String PREPARAZIONE = "PREPARAZIONE";
	public static final String CARICO = "CARICO";
	public static final String RITIRO = "RITIRO";
	public static final String ALTRIMENTI = "ALTRIMENTI";
	public static final String SCADENZA = "SCADENZA";
	public static final String SANZA = "SANZA";
	public static final String AVERSA = "AVERSA";
	public static final String NADIA = "NADIA";
	public static final String ZEROS = "0.00";
	
	public static final String DA_VINCI_FOOD = "DA VINCI FOOD";
	public static final String DA_VINCI = "DA_VINCI";
	
	public static final String CARNIATO = "CARNIATO";
	public static final String NOMBRE = "NOMBRE";
	public static final String BLUE ="BLUE CITY";
	public static final String METRO ="METRO";
	public static final String METRO_DEPOSITO = "BOLZANO";
	public static final String QUANTITA = "QUANTITA'";
	public static final String ORDINE_MERCE = "ORDINE MERCE";
	public static final String DATA = "DATA";
	
	public static final String ORDIN_N = "ORDINE N.";
	public static final String RIFERIMENTO_AMA = "RIFERIMENTO AMA NR: F";
	public static final String RIFERIMENTO_AMA_1 = "RIF. AMA NR: F";
	public static final String RIFERIMENTO = "RIFERIMENTO";
	
	public static final String RIFERIMENTI_CONTRATTO = "RIFERIMENTI AL CONTRATTO";
	
	public static final String CODICE = "CODICE";
	public static final String CODICE_FORNITORE = "CODICEFORNITORE";
	public static final String LEGGE = "LEGGE";
	public static final String CF = "C.F.:";
	public static final String CART = "CART.";
	public static final String FINE_ORDINE = "FINE ORDINE";
	
	public static final String ORARIO = "ORARIO";
	public static final String CARTON_BOX = "CARTONBOX";
	public static final String PANI_SFUSI = "PANI SFUSI";
	
	public static final String CERTIFICATO_SANITARIO = "CERTIFICATO SANITARIO";
	public static final String VIA_BRIGATA_MAZZINI = "VIA BRIGATA MAZZINI";
	
	public static final String TOTAL = "TOTAL";
	
	public static final String CORROCHER = "CORROCHER";
	
	public static final String CONDIZIONI_CONTRATTUALI = "CONDIZIONI CONTRATTUALI";
	public static final String RIFERIMENTO_CONTRATTO = "RIFERIMENTO AL CONTRATTO";
	public static final String DETTAGLIO_ORDINE = "DETTAGLIO ORDINE";
	
	public static final String SCONTO_VALUTA = "SCONTO VALUTA";
	
	public static final String SCADENZA_MINIMA = "SCAD. MIN";
	
	public static final String UDM = "UDM:";

	public static final String SICOM = "SICOM";
	public static final String VIGNALE = "VIGNALE";
	
	public static final String PZ = "PZ";
	public static final String SCONTI = "SCONTI";
	public static final String SCONTO_PUNTI = "SCONTO";
	public static final String SIGMA = "SIGMA";
	public static final String GIORNI = "GIORNI";
	public static final String COVID = "COVID";
	public static final String FOR = "FOR.";
	
	public static final String QUADRO = "QUADRO";
	public static final String MANCANZA = "MANCANZA";
	public static final String RICHIAMANDONE = "RICHIAMANDONE";
	public static final String CONVERTITO = "CONVERTITO";
	public static final String PROROGHE = "PROROGHE";
	public static final String DIVIETO = "DIVIETO";
	
	public static final String COD_ART = "COD.ART.";
	public static final String COD_ART_1 = "COD. ART. ";
	
	public static final String COD_ART_DE = "ARTIKELNR";
	public static final String LIEFERUNG = "LIEFERUNG";
	public static final String FREITAG = "FREITAG";
	public static final String TAGE_NETTO = "TAGE NETTO";
	public static final String SOLLTE = "SOLLTE";
	
	public static final String COD_ART_2 = "COD.ART";
	public static final String FORNITORE = "FORNITORE";
	public static final String CONSEGNA = "DATA DI CONSEGNA";
	public static final String CONSEGNA_RICHIESTA = "CONSEGNA RICHIESTA";
	public static final String CONSEGNA_TASSATIVA = "CONSEGNA TASSATIVA";
	public static final String CONSEGNA_XLS = "CONSEGNA";
	public static final String LIEFERDATUM = "LIEFERDATUM";
	
	public static final String LUNEDI = "LUNED";
	public static final String VENERDI = "VENERD";
	
	public static final String CONSEGNA_2 = "CONSEGNA";
	public static final String PESO_NETTO = "PESO NETTO";
	
	public static final String NETTO_ORDINE = "NETTO ORDINE";
	
	public static final String ALLEANZA = "COOP ALLEANZA";
	public static final String COOP_ALLEANZA = "ALLEANZA";
	public static final String COOP_ALLEANZA_SIC = "ALLEANZA_SIC";
	public static final String ALLEANZA_CODE = "0000066927";
	
	public static final String FERRARA = "FERRARA";
	
	public static final String DAO_FOR = "132480";
	public static final String DAO = "D_A_O";
	public static final String PAGINA = "PAGINA";
	
	public static final String CONSORZIO_FOR_1 = "329203";
	public static final String CONSORZIO_FOR_2 = "329201";
	public static final String CONSORZIO_FOR_3 = "329200";
	public static final String CONSORZIO_FOR_4 = "2791300";
	public static final String CONSORZIO_FOR_5 = "2024000";
	public static final String CONSORZIO_FOR_6 = "2027800";
	public static final String CONSORZIO_FOR_7 = "2791500";
	public static final String CONSORZIO_FOR_8 = "535100";
	public static final String CONSORZIO_FOR_9 = "4634400";
	public static final String CONSORZIO_FOR_10 = "2791501";
	public static final String CONSORZIO_FOR_11 = "535101";
	
	public static final String CONSORZIO_NORD_OVEST = "CONSORZIO";
	public static final String COOP_CONSORZIO_NORD_OVEST = "COOP CONSORZIO NORD OVEST";
	public static final String COOP_CONSORZIO_NORD_OVEST_1 = "COOP CONS.NORD OVEST";
	
	public static final String ETA_SRL = "ETA SRL";
	public static final String MAGA_FRESCHI = "MAGA FRESCHI";
	public static final String TATO_PARIDE_NAME = "TATO' PARIDE SPA";
	public static final String LE_DUE_SICILIE ="LE DUE SICILIE LDS";
	public static final String SICILIE ="SICILIE";
	
	public static final String APULIA_DISTRIBUZIONE ="APULIA DISTRIBUZIONE";
	
	public static final String MAIORANA ="MAIORANA";
	public static final String MAIORANA_MAGGIORINO ="MAIORANA MAGGIORINO";
	
	public static final String MAXI_DI ="MAXIDI";
	public static final String MAXI_DI_SRL ="MAXI DI SRL";
	
	public static final String PREGIS_SPA ="PREGIS S.P.A.";
	
	public static final String RIALTO = "RIALTO";
	public static final String RIAFRE = "RIAFRE";
	public static final String AMACRAI = "AMACRAI";
	public static final String AMACRAI_CODE = "AMA-CRAI EST";
	public static final String VS_CODE = "VS.CODICE";
	public static final String COLLI = "COLLI";
	public static final String TOTALE_COLLI = "TOTALE COLLI";
	public static final String PARA = "(";
	public static final String IDONEI = "IDONEI";
	public static final String YOG = "YOG";
	
	public static final String LEGENDE = "LEGENDE";
	public static final String ART_FOR = "ART.FOR.";
	public static final String ART_FOR_1 = "ART.FOR";
	
	public static final String PAC = "P_A_C";
	public static final String PAC_2000 = "PAC 2000";
	public static final String PERCENTUALE = "%-";
	public static final String PERCENTUALE_2 = "-%";
	
	public static final String BORG_ACQUILINA = "BORG AND AQUILINA LTD";
	public static final String BORG = "B_O_R_G";
	public static final String EUR = "EUR";
	public static final String NOTES = "NOTES";
	public static final String VALLETTA = "VALLETTA";
	
	public static final String AMBROSI_PRODUZIONE_CODE = "AMBROSI PRODUZIONE";
	public static final String AMBROSI_PRODUZIONE = "AMBROSI";
	
	public static final String AMBROSI_COMMERCIALIZZATI_CODE = "AMBROSI COMMERCIALIZZATI";
	public static final String AMBROSI_COMMERCIALIZZATI = "AMBROSI_COMMERCIALIZZATI";
	
	public static final String AMBROSI_USA = "AMBUSA";
	public static final String AMBROSI_USA_CODE = "AMBROSI USA";
	
	public static final String MAKRO_CODE = "MAKRO CECHIA";
	public static final String MAKRO = "MAKRO";
	public static final String MAKRO_GRE_CODE = "MAKRO GREECE";
	public static final String MAKRO_GRE = "MAKGRE";
	
	public static final String NARITA = "NARITA";
	public static final String ITEM_NAME = "ITEM NAME:";
	public static final String QUANTITY = "QUANTITY:";
	public static final String CTN = "CTN";
	
	public static final String CT = "CT";
	
	public static final String SCONTO_PROMO = "SCONTO PROMO";
	public static final String COMMA = ",";
	
	public static final String AMBROSI_REG_IMP = "BS 00842710170";
	
	public static final String CLICK_HERE = "CLICK HERE";
	public static final String NUMERO = "NUMERO";
	
	public static final String LANDO = "LANDO";
	public static final String LANDO_FRE = "LANDO_FRE";
	public static final String ORLANDO = "ORLANDO";
	public static final String RIEPILOGO = "RIEPILOGO";
	public static final String RIEPILOGATIVO = "RIEPILOGATIVO";
	public static final String CONFERMA = "CONFERMA";
	public static final String SCARICO = "SCARICO";
	public static final String VIA = "VIA";
	public static final String TEL = "TEL";
	public static final String PRODOTTI_SURGELATI = "PRODOTTI SURGELATI";
	public static final String TIPO_SPEDIZIONE = "TIPO SPEDIZIONE";
	public static final String PUNTO_CONSEGNA = "PUNTO DI CONSEGNA";
	public static final String SOCIETA_COPERATIVA = "Societ� Cooperativa";
	public static final String STAMPATO = "STAMPATO";
	public static final String STRACCIATELLA = "STRACCIATELLA";
	
	public static final String COOP_VIPITENO_2 = "COOP.LATTERIA VIPITENO SOC.AG.";
	public static final String COOP_VIPITENO_3 = "COOPERATIVA LATTERIA VIPITENO";
	public static final String COOP_VIPITENO_4 = "COOP.LATTERIA VIPITENO SOC.AGRICOLA";
	public static final String COOP_VIPITENO_5 = "COOP. LATTERIA VIPITENO SOC. AGRICOLA";
	public static final String COOP_VIPITENO_6 = "COOP LATTERIA VIPITENO SOC.AGRFORN.";
	public static final String COOP_VIPITENO_7 = "COOP. LATTERIA VIPITENO SOC.";
	public static final String COOP_VIPITENO_8 = "COOP.LATTERIA VIPITENO SOC.AGR.";
	public static final String COOP_VIPITENO_9 = "COOP.LATTERIA VIPITENO SOC.";
	public static final String COOP_VIPITENO_10 = "COOP. LATTERIA VIPITENO SOCIETA` AGRICOLA";
	public static final String COOP_VIPITENO_11 = "COOP. LATTERIA VIPITENO SOCIETA`";
	public static final String COOP_VIPITENO_12 = "LATTERIA SOC.VIPITENO COOP.A R";
	public static final String COOP_VIPITENO_13 = "COOP.LAT.VIPITENO SOC.AGR."; 
	public static final String COOP_VIPITENO_14 = "COOP. LATTERIA VIPITENO SOC AGR.";
	public static final String COOP_VIPITENO_15 = "LATTERIA SOCIALE VIPITENO SOC. COOP. A";
	public static final String SOC_COOP = "SOCIET� COOPERATIV";
	public static final String COOPERATIVA = "COOPERATIVA";
	public static final String SOC_COOP_1 = "SOC.COOP";
	public static final String COOPERA = "COOPERA";
	
	public static final String SIGOR = "SIGOR";
	public static final String INGROSS = "INGROSS";
	public static final String ROMA = "ROMA";
	public static final String SALVO_APPROVAZIONE = "SALVO APPROVAZIONE DELLA CASA";
	public static final String AGGIUNGERE = "AGGIUNGERE";
	public static final String TEST = "TEST";
	public static final String RICHIESTA_CLIENTE = "RICHIESTA CLIENTE PER";
	
	public static final String TRENTO_SVILUPPO = "TRENTOSVILUPPO";
	public static final String TRENTO = "TRENTO";
	
	public static final String CAFORM = "CAFORM";
	public static final String CAFORM_CODE = "CA.FORM";
	
	public static final String BERETTA = "BERETTA";
	public static final String BERETTA_CODE = "PIATTI FRESCHI ITALIA";
	
	public static final String DECARNE = "DECARNE";
	public static final String DECARNE_CODE = "FORMAT DISTRIBUZIONE";
	
	public static final String GEMOS = "GEMOS";
	public static final String TATO_PARIDE = "TATO' PARIDE SPA";
	public static final String TATO = "TATO";
	public static final String SIDAL_SRL= "S.I.D.AL. S.R.L.";
	public static final String ANTICA_CASCINA_SRL= "L'ANTICA CASCINA S.R.L.";
	public static final String SIDAL = "SIDAL";
	public static final String SICONAD = "SICONAD";
	public static final String VETTORE = "VETTORE";
	public static final String GARBELOTTO_CODE = "FIL.OPERATIVA TRIVENETO ORIENTALE";
	public static final String GARBELOTTO = "GARBELOTTO";
	public static final String BARLETTA = "BARLETTA";
	public static final String SAN_FELICI = "SANFELICI";
	public static final String SAN_FELICI_CODE = "SAN FELICI FRANCO";
	public static final String EURORISTORAZIONE = "EURORISTORAZIONE";
	public static final String CENTRALE_LATTE_ITALIA = "CENTRALE DEL LATTE D'ITALIA";
	public static final String CENTRALE_LATTE = "CENTRALE_LATTE";
	public static final String TASSATIVAMENTE = "TASSATIVAMENTE";
	
	public static final String GEGEL = "GEGEL";
	public static final String IPERAL = "IPERAL";
	public static final String VEGA = "VEGA";
	public static final String GARDENA = "GARDENA";
	public static final String GARDENA_SOL = "VALGARDENA";
	public static final String ARIELLI = "ARIELLI";
	public static final String ANNOTAZIONI = "ANNOTAZIONI";
	public static final String DISTRIBUZIONE = "DISTRIBUZIONE";
	public static final String PEC = "PEC:";
	public static final String CAP_SOC = "CAP.SOC.";
	
	public static final String MODERNA = "MODERNA";
	public static final String MOSCA = "MOSCA";
	public static final String MOSCA_CODE = "MOSCA GIUSEPPE";
	
	public static final String UNES = "UNES";
	public static final String UNES_CODE = "07515280159";
	
	public static final String DIMAR = "DIMAR";
	
	public static final String MARTINELLI = "MARTINELLI";
	public static final String SUPEREMME = "SUPEREMME";
	public static final String SUPERCENTRO = "SUPERCENTRO";
	
	public static final String PRZ_ORDINE = "PRZ.ORDINE";
	public static final String SISA_CODE = "DISTRIBUZIONE SISA";
	public static final String SISA = "SISA";
	
	public static final String REALCO = "REALCO";
	public static final String CARAMICO = "CARAMICO";
	public static final String SALERNO = "SALERNO";
	
	public static final String MULTICEDI = "MULTICEDI";
	public static final String PASTORANO = "PASTORANO";
	
	public static final String PAC2000 = "PAC2000";
	public static final String PAC2000_CODE = "PAC 2000";
	public static final String _2000PAC = "2000PAC";
	public static final String FIANO_ROMANO = "FIANO ROMANO";
	public static final String CARINI = "CARINI";
	public static final String PERUGIA = "PERUGIA";
	public static final String CASERTA = "CASERTA";
	public static final String CALABRIA = "CALABRIA";
	public static final String MONTALTO = "MONTALTO";
	
	public static final String VALORE_NETTO_TOTALE = "VALORE NETTO TOTALE";
	public static final String GRAMM = "GRAMM";
	public static final String PROGRAMMATA = "PROGRAMMATA";
	public static final String CAMST = "CAMST";
	public static final String NOSAWA = "NOSAWA";
	
	public static final String ASPIAG = "ASPIAG";
	public static final String PAOLINI = "PAOLINI";
	public static final String SPESE = "SPESE TRASP.";
	
	public static final String CEDI_SIGMA_CODE = "CE. DI. SIGMA";
	public static final String CEDI_SIGMA = "SIGMA";
	
	public static final String COOP_ALLEANZA_30_CODE = "COOP ALLEANZA 3.0 S.C";
	public static final String COOP_ALLEANZA_30 = "30ALLEANZA";
	public static final String FORMIGINE = "FORMIGINE";
	public static final String PARMA = "PARMA";
	
	public static final String CENTRALE_ADRIATICA = "CENTRALE ADRIATICA";
	public static final String ADRIATICA = "ADRIATICA";
	
	public static final String CSS_CODE = "SARDEGNA CEDI SALUMI";
	public static final String CSS = "SARDI";
	
	public static final String GASTRO = "GASTROFRESH";
	
	public static final String TOCAL_CODE = "TO.CAL.";
	public static final String TOCAL = "TOCAL";
	
	public static final String WOERNDLE = "WOERNDLE"; 
	public static final String GESAMTWERT = "GESAMTWERT";
	
	public static final String MARR = "MARR";
	public static final String MARR_SPA = "\"MARR S.P.A.";
	
	public static final String TAVELLA = "TAVELLA";
	
	public static final String METRO_SLOVACCHIA = "METSLO";
	public static final String METRO_SERBIA = "METSRB";
	public static final String METRO_SLOVACCHIA_CODE = "METRO SLOVACCHIA";
	public static final String METRO_SERBIA_CODE = "METRO SERBIA";
	
	public static final String BAUER = "BAUER";
	public static final String IPERCONAD = "IPERCONAD";
	public static final String RDAVENETA = "RDAVENETA";
	public static final String SGR_IPER_CODE = "SGR IPERMERCATI";
	public static final String SGR_IPER = "SGR_IPER";
	public static final String LEGNARO = "LEGNARO";
	
	public static final String SABELLI = "SABELLI";
	public static final String WEIHE = "WEIHE";
	
	public static final String CATTEL = "CATTEL";
	public static final String ELIOR = "ELIOR";
	public static final String MAXIDI = "MAXIDI";
	public static final String BELFIORE = "BELFIORE";
	public static final String MAXIDI_CODE = "251872";
	public static final String MAXIDI_FRE_CODE = "010481";
	public static final String MAXIDI_TON_CODE = "009531";
	public static final String MAXIDI_TON_CODE_2 = "501123";
	
	public static final String ART_NO = "ART.-NO.";
	public static final String ART_LU = "A r t i c o l o";
	public static final String ORDER_NUMMBER = "ORDERNUMBER";
	public static final String DELIVERY_DATE = "DEL. DATE";
	public static final String PLEASE_NOTE = "PLEASE NOTE";
	public static final String HAMBERGER = "HAMBERGER";
	public static final String MUNCHEN = "MUNCHEN";
	public static final String BERLIN = "GROSSMARKT BERLIN";
	public static final String BERLIN_ERNA = "ERNA-SAMUEL-STR";
	public static final String HAMBERLIN = "HAMBERLIN";
	public static final String HAMMUNCHEN = "HAMMUNCHEN";
	public static final String DESIGNATION = "DESIGNATION";
	
	public static final String SIGNATURE = "SIGNATURE";
	
	public static final String RETAILPRO = "RETAILPRO";
	public static final String STOREC = "STOREC11";
	public static final String CEDEP = "CEDEP";
	
	public static final String LADISA = "LADISA";
	public static final String IMPOSTA = "IMPOSTA";
	
	public static final String GMF_CODE ="GMF SPA";
	public static final String GMF_CODE_NEW ="GMF S.R.L.";
	public static final String GMF ="GMFSPA";
	public static final String GRANDI_MAGAZZINI ="GRANDI MAGAZZINI FIORONI";
	
	
	public static final String COSTO_BASE ="COSTO BASE";
	public static final String VIS_FOOD_CODE ="FOOD CONSULTING";
	public static final String VIS_FOOD ="VISFOOD";
	
	public static final String ROSSI_CODE = "ROSSI GIANTS";
	public static final String ROSSI = "ROSSI";
	public static final String INDIRIZZO = "INDIRIZZO";
	
	public static final String GS_CODE = "GS SPA";
	public static final String GS = "GSSPA";
	
	public static final String PREGIS = "PREGIS";
	public static final String VALORE_NETTO = "VALORE NETTO";
	
	public static final String VIVO_CODE = "VIVO FRIULI";
	public static final String VIVO = "VIVO";
	public static final String DESTINAZIONE = "DESTINAZIONE";
	
	public static final String DAC_CODE = "DAC SPA";
	public static final String DAC = "DACSPA";
	public static final String PAGINE = "PAGINE";
	public static final String TOT_COLLI = "TOT.COLLI";
	
	public static final String VS_COD = "VS.COD.";
	public static final String CODICE_DAC = "CODICEDAC";
	
	public static final String PRIX = "PRIX";
	public static final String PRIX_CODE = "PRIX QUALITY";
	
	public static final String ECOR = "ECOR";
	public static final String ECOR_CODE = "ECORNATURASI";
	
	public static final String IDEM = "IDEM";
	public static final String FLERO = "FLERO";
	public static final String IMMEDIATA = "IMMEDIATA";
	
	public static final String IGES_CODE = "IGES SRL";
	public static final String IGES = "IGES";
	
	public static final String MADIA_CODE = "M A D I A S P A";
	public static final String MADIA = "MADIA";
	
	public static final String BIOLOGICA = "BIOLOGICA";
	public static final String CIRFOOD = "CIRFOOD";
	
	public static final String VIMODROME = "VIMODROME";
	
	public static final String SIAF_CODE = "O.S.M.A.";
	public static final String SIAF = "SIAF";
	
	public static final String NUME = "NUME";
	
	public static final String PIVA_COOP = "IT03503411203";
	
	
	public static final String RESINELLI = "RESINELLI";
	
	public static final String DOCUMENT = "DOCUMENT";
	public static final String ExportName = "ExportName";
	
	public static final String FIELD_GROUP = "FIELDGROUP";
	public static final String ROW = "ROW";
	public static final String ATTRIBUTES = "ATTRIBUTES";
	
	public static final String NAME = "Name";
	public static final String VALUE = "Value";
	public static final String KEY = "Key";
	
	public static final String ORD_DATE = "ORD_DATE";
	public static final String ORD_DELIVERY_DATE = "ORD_DELIVERY_DATE";
	public static final String ORD_NUMBER = "ORD_NUMBER";
	public static final String OR_SUPPLIER_NUMBER = "OR_SUPPLIER_NUMBER";
	public static final String OR_INVOICE_NUMBER = "OR_INVOICE_NUMBER";
	public static final String OR_NAME1 = "OR_NAME1";
	public static final String OR_CITY = "OR_CITY";
	public static final String OR_VAT_ID_NO = "OR_VAT_ID_NO";
	
	public static final String ORI_ARTICLE_NO = "ORI_ARTICLE_NO";
	public static final String ORI_QUANTITY = "ORI_QUANTITY";
	
	public static final String SQLITE = "SQLITE";
	public static final String MSSQL = "MSSQL";
	public static final String FIREBIRD = "FIREBIRD";
	public static final String MSSQL_EQ = "MSSQL_EQ";
	public static final String AS400 = "AS400";
	
	public static final String SICLA = "SICLA";
	public static final String PI_CF = "PI/CF";
	public static final String NOME_CLIENTE = "NOME CLIENTE";
	public static final String COD = "COD.";
	
	public static final String NOSTRANA = "CUCINA NOSTRANA S.R.L.";
	public static final String CUCINA_NOSTRANA = "CUCINA_NOSTRANA";
	
	public static final String SELECTA = "SELECTA";
	public static final String SELECTA_CODE = "SELECTA S.P.A.";
	
	public static final String BOCON = "BOCON";
	
	public static final String PDF = "PDF";
	public static final String TXT = "TXT";
	
	public static final String GLI_ALTRI = "GLI ALTRI";
	
	public static final String BRAGA = "BRAGA";
	public static final String SAIT = "SAIT";
	
	public static final String BRUNELLO = "BRUNELLO";
	public static final String CONDIZIONI_GENERALI = "CONDIZIONI GENERALI DI VENDITA";
	
	public static final String ATTIVO = "ATTIVO";
	public static final String PASSIVO = "PASSIVO";
	
	public static final String CODICE_CLIENTE = "COD. CLIENTE";
	
}




