package com.econorma.properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class OrdersProperties {
	
	private static OrdersProperties instance = null;
	
	 public static OrdersProperties getInstance() {
	      if(instance == null) {
	         instance = new OrdersProperties();
	      }
	      return instance;
	   }

	public void create(){
	 
		Properties prop = new Properties();
		OutputStream output = null;

		try {

			output = new FileOutputStream("application.properties");
			
			Path currentRelativePath = Paths.get("");
			String dir = currentRelativePath.toAbsolutePath().toString();
			
			prop.setProperty("directory", dir);
			prop.setProperty("backup_directory", dir + "\\backup");
			prop.setProperty("file_extension", "csv|xls|xlsx|pdf|doc|txt|xml");
			prop.setProperty("extractor_active", dir + "false");
			prop.setProperty("extractor_directory", dir + "\\extractor");
			prop.setProperty("extractor_backup", dir + "\\\\192.168.1.2\\alex");
			prop.setProperty("host_as400", dir + "192.168.1.2");
			prop.setProperty("user_as400", dir + "QSECOFR");
			prop.setProperty("password_as400", dir + "SECOFRPW");
			prop.setProperty("library_as400", dir + "IT_V02_W");
			prop.setProperty("edi_active", dir + "false");
			prop.setProperty("edi_directory", dir + "\\edi");
			prop.setProperty("sub_days", dir + "15");
			prop.setProperty("documentale_backup", dir + "");
			prop.store(output, null);

		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}
	
	public Map<String, String> load(){
		
		Map<String, String> map = new HashMap<String, String>();
		
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("application.properties");

			// load a properties file
			prop.load(input);
			
			for (final String name: prop.stringPropertyNames()) {
				map.put(name, prop.getProperty(name));
			}
			 

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return map;
	}
	
}
